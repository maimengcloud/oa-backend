package com.oa.crm.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiModelProperty;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CrmUtils {

    /**
     * 获取IP地址
     * @param request
     * @return
     */
    public static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }


    /**
     * 获取两个对象同名属性内容不相同的列表
     * @param oldCustomer
     * @param newCustomer
     */
    public static List<Map<String, Object>> getNotEqualsParams(Object oldCustomer, Object newCustomer) throws IllegalAccessException {
        List<Map<String, Object>> historys = new ArrayList<Map<String, Object>>();
        Class<?> claszz1 = oldCustomer.getClass();
        Class<?> claszz2 = newCustomer.getClass();
        Field[] field1 = claszz1.getDeclaredFields();
        Field[] field2 = claszz2.getDeclaredFields();
        //遍历属性列表field1
        for (int i = 0; i < field1.length; i++) {
            //遍历属性列表field2
            for (int j = 0; j < field2.length; j++) {
                field1[i].setAccessible(true);
                field2[j].setAccessible(true);
                if (field1[i].getName().equals(field2[j].getName())) {
                    if (!compareTwo(field1[i].get(oldCustomer), field2[j].get(newCustomer))) {
                        Map<String, Object> map2 = new HashMap<String, Object>();
                        if(!"updateTime".equals(field1[i].getName())) {
                            //获取实体类注解上的notes注解
                            map2.put("name", field1[i].getName());
                            map2.put("doc", field1[i].getAnnotation(ApiModelProperty.class).notes());
                            map2.put("old", field1[i].get(oldCustomer));
                            map2.put("new", field2[j].get(newCustomer));
                            historys.add(map2);
                        }
                    }
                    break;
                }
            }
        }
        return historys;
    }


    //对比两个数据是否内容相同
    public static boolean compareTwo(Object object1, Object object2) {
        if (object1 == null && object2 == null) {
            return true;
        }
        if (object1 == "" && object2 == null) {
            return true;
        }
        if (object1 == null && object2 == "") {
            return true;
        }
        if (object1 == null && object2 != null) {
            return false;
        }
        if (object1.equals(object2)) {
            return true;
        }

        if(object1 instanceof BigDecimal && object2 instanceof BigDecimal) {
            return ((BigDecimal) object1).compareTo((BigDecimal) object2) == 0 ? true : false;
        }
        return false;
    }

    public static String obj2Json(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }


    public static List<Map<String, Object>> paramsTransferOper(String name, String doc, String oldValue, String newValue) {
        List<Map<String,Object>> list = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("doc",doc);
        map.put("old", oldValue);
        map.put("new", newValue);
        list.add(map);
        return list;
    }

}
