package com.oa.crm.enums;

public enum CustomerShareEnum {

    SHARE_LEVEL_SEE("see", "查看"),
    SHARE_LEVEL_EDIT("edit", "编辑"),
    SHARE_TYPE_DEPT("dept", "部门"),
    SHARE_TYPE_USER("user", "用户"),
    SHARE_TYPE_ROLE("role", "角色");

    CustomerShareEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;

    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }



}
