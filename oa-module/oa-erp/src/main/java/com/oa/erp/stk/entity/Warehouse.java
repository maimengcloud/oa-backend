package com.oa.erp.stk.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("stk_warehouse")
@ApiModel(description="仓库")
public class Warehouse  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="仓库名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="仓库所属区域",allowEmptyValue=true,example="",allowableValues="")
	String districtId;

	
	@ApiModelProperty(notes="区域名称",allowEmptyValue=true,example="",allowableValues="")
	String districtName;

	
	@ApiModelProperty(notes="仓库所属的省",allowEmptyValue=true,example="",allowableValues="")
	String province;

	
	@ApiModelProperty(notes="仓库所属的市",allowEmptyValue=true,example="",allowableValues="")
	String city;

	
	@ApiModelProperty(notes="仓库所属的街道",allowEmptyValue=true,example="",allowableValues="")
	String street;

	
	@ApiModelProperty(notes="仓库所属的省份代码",allowEmptyValue=true,example="",allowableValues="")
	String provinceCode;

	
	@ApiModelProperty(notes="仓库所属的城市代码",allowEmptyValue=true,example="",allowableValues="")
	String cityCode;

	
	@ApiModelProperty(notes="仓库所属的街道代码",allowEmptyValue=true,example="",allowableValues="")
	String streetCode;

	
	@ApiModelProperty(notes="仓库所属地址",allowEmptyValue=true,example="",allowableValues="")
	String address;

	
	@ApiModelProperty(notes="地址定位",allowEmptyValue=true,example="",allowableValues="")
	String addressGps;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="机构名称",allowEmptyValue=true,example="",allowableValues="")
	String branchName;

	
	@ApiModelProperty(notes="仓库员名称",allowEmptyValue=true,example="",allowableValues="")
	String houseUser;

	
	@ApiModelProperty(notes="仓库员联系电话",allowEmptyValue=true,example="",allowableValues="")
	String phone;

	
	@ApiModelProperty(notes="库房数量",allowEmptyValue=true,example="",allowableValues="")
	String houseNum;

	
	@ApiModelProperty(notes="物品存放类型",allowEmptyValue=true,example="",allowableValues="")
	String houseClass;

	
	@ApiModelProperty(notes="物品存放类别编号",allowEmptyValue=true,example="",allowableValues="")
	String classId;

	/**
	 *主键
	 **/
	public Warehouse(String id) {
		this.id = id;
	}
    
    /**
     * 仓库
     **/
	public Warehouse() {
	}

}