package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_business_prod")
@ApiModel(description="crm_customer_business_prod")
public class CustomerBusinessProd  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="客户商机关联产品id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="商机id",allowEmptyValue=true,example="",allowableValues="")
	String businessId;

	
	@ApiModelProperty(notes="产品id",allowEmptyValue=true,example="",allowableValues="")
	String productId;

	
	@ApiModelProperty(notes="单价",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal unitPrice;

	
	@ApiModelProperty(notes="数量",allowEmptyValue=true,example="",allowableValues="")
	Integer count;

	/**
	 *客户商机关联产品id
	 **/
	public CustomerBusinessProd(String id) {
		this.id = id;
	}
    
    /**
     * crm_customer_business_prod
     **/
	public CustomerBusinessProd() {
	}

}