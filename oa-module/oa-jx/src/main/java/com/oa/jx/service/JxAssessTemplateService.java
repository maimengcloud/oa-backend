package com.oa.jx.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.core.utils.BaseUtils;
import com.oa.jx.entity.JxAssessContent;
import com.oa.jx.entity.JxAssessTemplate;
import com.oa.jx.entity.JxAssessTemplateLink;
import com.oa.jx.mapper.JxAssessTemplateMapper;
import com.oa.jx.vo.AddAssessTemplateVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class JxAssessTemplateService extends BaseService<JxAssessTemplateMapper, JxAssessTemplate> {
    static Logger logger = LoggerFactory.getLogger(JxAssessTemplateService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }


    @Autowired
    JxAssessContentService jxAssessContentService;
    @Autowired
    JxAssessTemplateLinkService jxAssessTemplateLinkService;
    @Autowired
    JxAssessGradeService jxAssessGradeService;
    @Autowired
    JxAssessGradeDetailService jxAssessGradeDetailService;


    @Transactional
    public void insertAccessTemplate(AddAssessTemplateVo addAssessTemplateVo) {
        JxAssessTemplate jxAssessTemplate = new JxAssessTemplate();
        //向考核模板表添加数据
        BeanUtils.copyProperties(addAssessTemplateVo, jxAssessTemplate);
        this.insert(jxAssessTemplate);
        //向考核内容表添加数据
        if (addAssessTemplateVo.getAccessContents() != null && addAssessTemplateVo.getAccessContents().size() > 0) {
            addAssessTemplateVo.getAccessContents().forEach(item -> {
                item.setBranchId(jxAssessTemplate.getBranchId());
                item.setTemplateId(jxAssessTemplate.getId());
                item.setId(jxAssessContentService.createKey("id"));
            });
            jxAssessContentService.batchInsert(addAssessTemplateVo.getAccessContents());
        }


        if (addAssessTemplateVo.getLinks() != null && addAssessTemplateVo.getLinks().size() > 0) {
            addAssessTemplateVo.getLinks().forEach(item -> {
                item.setTemplateId(jxAssessTemplate.getId());
                item.setBranchId(jxAssessTemplate.getBranchId());
            });
            jxAssessTemplateLinkService.batchInsert(addAssessTemplateVo.getLinks());
        }
    }


    public Map<String, Object> getJxAssessTemplateByTemplateId(IPage page, QueryWrapper qw, Map<String, Object> jxAssessTemplate) {
        List<Map<String, Object>> maps = this.selectListMapByWhere(page, qw, jxAssessTemplate);
        Map<String, Object> map = new HashMap<>();
        map.put("templateId", jxAssessTemplate.get("id"));
        map.put("branchId", jxAssessTemplate.get("branchId"));
        List<Map<String, Object>> contents = jxAssessContentService.selectListMapByWhere(page, qw, map);
        List<Map<String, Object>> users = jxAssessTemplateLinkService.selectListMapByWhere(page, qw, map);
        maps.get(0).put("accessContents", contents);
        maps.get(0).put("links", users);
        return maps.get(0);
    }

    @Transactional
    public void editAccessTemplate(AddAssessTemplateVo editAssessTemplateVo) {
        JxAssessTemplate jxAssessTemplate = new JxAssessTemplate();
        BeanUtils.copyProperties(editAssessTemplateVo, jxAssessTemplate);
        //修改模板信息
        this.updateByPk(jxAssessTemplate);
        //删除模板内容重新添加
        Map<String, Object> map = new HashMap<>();
        map.put("templateId", editAssessTemplateVo.getId());
        map.put("branchId", editAssessTemplateVo.getBranchId());
        //先删除
        jxAssessContentService.deleteByWhere(BaseUtils.fromMap(map, JxAssessContent.class));
        if (editAssessTemplateVo.getAccessContents() != null && editAssessTemplateVo.getAccessContents().size() > 0) {
            editAssessTemplateVo.getAccessContents().forEach(item -> {
                item.setBranchId(jxAssessTemplate.getBranchId());
                item.setTemplateId(jxAssessTemplate.getId());
                if (!StringUtils.hasText(item.getId())) {
                    item.setId(jxAssessContentService.createKey("id"));
                }
            });
            jxAssessContentService.batchInsert(editAssessTemplateVo.getAccessContents());
        }

        //删除与模板绑定的用户重新添加
        jxAssessTemplateLinkService.deleteByWhere(BaseUtils.fromMap(map, JxAssessTemplateLink.class));
        if (editAssessTemplateVo.getLinks() != null && editAssessTemplateVo.getLinks().size() > 0) {
            editAssessTemplateVo.getLinks().forEach(item -> {
                item.setTemplateId(jxAssessTemplate.getId());
                item.setBranchId(jxAssessTemplate.getBranchId());
            });
            jxAssessTemplateLinkService.batchInsert(editAssessTemplateVo.getLinks());
        }
    }

    @Transactional
    public void deleteByTemplateId(JxAssessTemplate jxAssessTemplate) {
        Map<String, Object> map = new HashMap<>();
        map.put("templateId", jxAssessTemplate.getId());
        this.deleteByPk(jxAssessTemplate);
        jxAssessContentService.deleteByWhere(BaseUtils.fromMap(map, JxAssessContent.class));
        jxAssessTemplateLinkService.deleteByWhere(BaseUtils.fromMap(map, JxAssessTemplateLink.class));
    }

    public void copyAccessTemplate(Map<String, Object> jxAssessTemplate) {
        String oldId = (String) jxAssessTemplate.get("id");
        JxAssessTemplate jxAssessTemplateDb = this.selectOneObject(new JxAssessTemplate(oldId));

        JxAssessTemplate jxAssessTemplateNew = new JxAssessTemplate();
        BeanUtils.copyProperties(jxAssessTemplateDb, jxAssessTemplateNew);

        jxAssessTemplateNew.setId(this.createKey("id"));
        jxAssessTemplateNew.setName(jxAssessTemplateNew.getName() + "(复制)");
        this.insert(jxAssessTemplateNew);

        JxAssessContent contentQuery = new JxAssessContent();
        contentQuery.setTemplateId(oldId);

        List<JxAssessContent> contents = jxAssessContentService.selectListByWhere(contentQuery);
        if (contents != null && contents.size() > 0) {
            for (JxAssessContent content : contents) {
                content.setTemplateId(jxAssessTemplateNew.getId());
                content.setId(this.jxAssessContentService.createKey("id"));
            }
            this.jxAssessContentService.batchInsert(contents);

        }

        JxAssessTemplateLink linkQuery = new JxAssessTemplateLink();
        linkQuery.setTemplateId(oldId);
        List<JxAssessTemplateLink> links = jxAssessTemplateLinkService.selectListByWhere(linkQuery);
        if (links != null && links.size() > 0) {
            for (JxAssessTemplateLink link : links) {
                link.setTemplateId(jxAssessTemplateNew.getId());
            }
            this.jxAssessTemplateLinkService.batchInsert(links);
        }
    }
}

