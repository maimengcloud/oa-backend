package  com.oa.om.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import com.mdp.core.dao.annotation.TableIds;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 唛盟开源 code-gen
 * @since 2024-8-22
 */
@Data
@TableName("om_appliance_purchase")
@Schema(description="办公用品采购表")
public class OmAppliancePurchase  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@Schema(description="主键,主键")
	String id;

	
	@Schema(description="用品id")
	String applianceId;

	
	@Schema(description="用品名称")
	String applianceName;

	
	@Schema(description="规格")
	String standard;

	
	@Schema(description="单价")
	BigDecimal price;

	
	@Schema(description="金额")
	BigDecimal totalPrice;

	
	@Schema(description="申请采购数量")
	BigDecimal purchaseNum;

	
	@Schema(description="操作人id")
	String userid;

	
	@Schema(description="操作人名字")
	String username;

	
	@Schema(description="申请日期")
	Date createTime;

	
	@Schema(description="备注")
	String remark;

	
	@Schema(description="供应商id")
	String supplierId;

	
	@Schema(description="供应商")
	String supplierName;

	
	@Schema(description="入库状态0未审核1审核中2待入库3已入库4流程取消5审核不通过")
	String storageStatus;

	
	@Schema(description="机构号")
	String branchId;

	
	@Schema(description="当前流程状态0初始1审批中2审批通过3审批不通过4流程取消或者删除")
	String bizFlowState;

	
	@Schema(description="审批流实例编号")
	String bizProcInstId;

	
	@Schema(description="ltime")
	Date ltime;

	
	@Schema(description="已入库数量，汇总出入库流水表得出")
	BigDecimal totalArrNum;

	
	@Schema(description="部门编号")
	String deptid;

	
	@Schema(description="单位")
	String unit;

	
	@Schema(description="部门名称")
	String deptName;

	
	@Schema(description="单据类型固定值purchase")
	String billType;

	/**
	 *主键
	 **/
	public OmAppliancePurchase(String id) {
		this.id = id;
	}
    
    /**
     * 办公用品采购表
     **/
	public OmAppliancePurchase() {
	}

}