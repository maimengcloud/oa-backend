package com.oa.ht.entity;

import java.util.List;

public class ContractReceiveVo extends ContractReceive{

    /**
     * 关联发票编号
     */
    List<String> invoiceIds;

    public List<String> getInvoiceIds() {
        return invoiceIds;
    }

    public void setInvoiceIds(List<String> invoiceIds) {
        this.invoiceIds = invoiceIds;
    }
}
