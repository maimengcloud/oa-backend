package com.oa.meet.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.meet.entity.MeetMeetingExecAppendix;
import com.oa.meet.service.MeetMeetingExecAppendixService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value="/*/oa/meet/meetMeetingExecAppendix")
@Api(tags = {"相关附件表-操作接口"})
public class MeetMeetingExecAppendixController {

    static Logger logger = LoggerFactory.getLogger(MeetMeetingExecAppendixController.class);

    @Autowired
    private MeetMeetingExecAppendixService meetMeetingExecAppendixService;

    @ApiOperation(value = "相关附件表-查询列表", notes = " ")
    @ApiEntityParams(MeetMeetingExecAppendix.class)
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAppendix.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMeetMeetingExecAppendix(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");

        QueryWrapper<MeetMeetingExecAppendix> qw = QueryTools.initQueryWrapper(MeetMeetingExecAppendix.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = meetMeetingExecAppendixService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "相关附件表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAppendix.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMeetMeetingExecAppendix(@RequestBody MeetMeetingExecAppendix meetMeetingExecAppendix) {

        
            boolean createPk = false;
            if (StringUtils.isEmpty(meetMeetingExecAppendix.getId())) {
                createPk = true;
                meetMeetingExecAppendix.setId(meetMeetingExecAppendixService.createKey("id"));
            }
            if (createPk == false) {
                if (meetMeetingExecAppendixService.selectOneObject(meetMeetingExecAppendix) != null) {
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            meetMeetingExecAppendixService.insert(meetMeetingExecAppendix);
        
        return Result.ok("add-ok", "添加成功！").setData(meetMeetingExecAppendix);
    }

    @ApiOperation(value = "相关附件表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMeetMeetingExecAppendix(@RequestBody MeetMeetingExecAppendix meetMeetingExecAppendix) {

        
            meetMeetingExecAppendixService.deleteByPk(meetMeetingExecAppendix);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "相关附件表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAppendix.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMeetMeetingExecAppendix(@RequestBody MeetMeetingExecAppendix meetMeetingExecAppendix) {

        
            meetMeetingExecAppendixService.updateByPk(meetMeetingExecAppendix);
        
        return Result.ok("edit-ok", "修改成功！").setData(meetMeetingExecAppendix);
    }

    @ApiOperation(value = "相关附件表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MeetMeetingExecAppendix.class, props = {}, remark = "相关附件表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAppendix.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        meetMeetingExecAppendixService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "相关附件表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMeetMeetingExecAppendix(@RequestBody List<MeetMeetingExecAppendix> meetMeetingExecAppendixs) {
        User user = LoginUtils.getCurrentUserInfo();
        if (meetMeetingExecAppendixs.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MeetMeetingExecAppendix> datasDb = meetMeetingExecAppendixService.listByIds(meetMeetingExecAppendixs.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MeetMeetingExecAppendix> can = new ArrayList<>();
        List<MeetMeetingExecAppendix> no = new ArrayList<>();
        for (MeetMeetingExecAppendix data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            meetMeetingExecAppendixService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "相关附件表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAppendix.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MeetMeetingExecAppendix meetMeetingExecAppendix) {
        MeetMeetingExecAppendix data = (MeetMeetingExecAppendix) meetMeetingExecAppendixService.getById(meetMeetingExecAppendix);
        return Result.ok().setData(data);
    }

}
