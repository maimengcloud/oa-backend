package com.oa.meet.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.meet.entity.MeetBoardroomType;
import com.oa.meet.service.MeetBoardroomTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value="/*/oa/meet/meetBoardroomType")
@Api(tags = {"会议室类型-操作接口"})
public class MeetBoardroomTypeController {

    static Logger logger = LoggerFactory.getLogger(MeetBoardroomTypeController.class);

    @Autowired
    private MeetBoardroomTypeService meetBoardroomTypeService;

    @ApiOperation(value = "会议室类型-查询列表", notes = " ")
    @ApiEntityParams(MeetBoardroomType.class)
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroomType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMeetBoardroomType(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<MeetBoardroomType> qw = QueryTools.initQueryWrapper(MeetBoardroomType.class, params);

        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = meetBoardroomTypeService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "会议室类型-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroomType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMeetBoardroomType(@RequestBody MeetBoardroomType meetBoardroomType) {


        if (StringUtils.isEmpty(meetBoardroomType.getBranchId())) {
            meetBoardroomType.setBranchId(LoginUtils.getCurrentUserInfo().getBranchId());
        }

        
            if (StringUtils.isEmpty(meetBoardroomType.getId())) {
                meetBoardroomType.setId(meetBoardroomTypeService.createKey("id"));
            } else {
                MeetBoardroomType meetBoardroomTypeQuery = new MeetBoardroomType(meetBoardroomType.getId());
                if (meetBoardroomTypeService.countByWhere(meetBoardroomTypeQuery) > 0) {
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            meetBoardroomTypeService.insert(meetBoardroomType);
        
        return Result.ok("add-ok", "添加成功！").setData(meetBoardroomType);
    }

    @ApiOperation(value = "会议室类型-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMeetBoardroomType(@RequestBody MeetBoardroomType meetBoardroomType) {

        
            meetBoardroomTypeService.deleteByPk(meetBoardroomType);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "会议室类型-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroomType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMeetBoardroomType(@RequestBody MeetBoardroomType meetBoardroomType) {

        
            meetBoardroomTypeService.updateByPk(meetBoardroomType);
        
        return Result.ok("edit-ok", "修改成功！").setData(meetBoardroomType);
    }

    @ApiOperation(value = "会议室类型-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MeetBoardroomType.class, props = {}, remark = "会议室类型", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroomType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        meetBoardroomTypeService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "会议室类型-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMeetBoardroomType(@RequestBody List<MeetBoardroomType> meetBoardroomTypes) {
        User user = LoginUtils.getCurrentUserInfo();
        if (meetBoardroomTypes.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MeetBoardroomType> datasDb = meetBoardroomTypeService.listByIds(meetBoardroomTypes.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MeetBoardroomType> can = new ArrayList<>();
        List<MeetBoardroomType> no = new ArrayList<>();
        for (MeetBoardroomType data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            meetBoardroomTypeService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "会议室类型-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroomType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MeetBoardroomType meetBoardroomType) {
        MeetBoardroomType data = (MeetBoardroomType) meetBoardroomTypeService.getById(meetBoardroomType);
        return Result.ok().setData(data);
    }

}
