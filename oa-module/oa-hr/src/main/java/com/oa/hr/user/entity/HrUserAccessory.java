package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_user_accessory")
@ApiModel(description="hr_user_accessory")
public class HrUserAccessory  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="附件名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="类型0-合同，1-简历，2-资格证书，3-其他",allowEmptyValue=true,example="",allowableValues="")
	String type;

	
	@ApiModelProperty(notes="附件地址",allowEmptyValue=true,example="",allowableValues="")
	String accessory;

	
	@ApiModelProperty(notes="用户id",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="上传时间",allowEmptyValue=true,example="",allowableValues="")
	Date uploadTime;

	
	@ApiModelProperty(notes="关联id",allowEmptyValue=true,example="",allowableValues="")
	String relevanceId;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public HrUserAccessory(String id) {
		this.id = id;
	}
    
    /**
     * hr_user_accessory
     **/
	public HrUserAccessory() {
	}

}