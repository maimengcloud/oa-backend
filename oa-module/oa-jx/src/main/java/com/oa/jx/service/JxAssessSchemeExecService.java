package com.oa.jx.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.jx.entity.JxAssessSchemeExec;
import com.oa.jx.mapper.JxAssessSchemeExecMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class JxAssessSchemeExecService extends BaseService<JxAssessSchemeExecMapper, JxAssessSchemeExec> {
    static Logger logger = LoggerFactory.getLogger(JxAssessSchemeExecService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    @Autowired
    JxAssessTemplateLinkService jxAssessTemplateLinkService;

    @Autowired
    JxAssessGradeService jxAssessGradeService;

    public List<Map<String, Object>> selectListByAssUserid(IPage page, QueryWrapper qw, Map<String, Object> jxAssessSchemeExecQuery) {
        return this.selectListMapByWhere(page, qw, jxAssessSchemeExecQuery);
    }

    /**
     * 批量设置执行方案下的员工信息
     *
     * @param jxAssessSchemeExecDb
     */
    @Transactional
    public void batchSetJxAssessGrade(JxAssessSchemeExec jxAssessSchemeExecDb) {
        baseMapper.batchSetJxAssessGrade(jxAssessSchemeExecDb);
        JxAssessSchemeExec update = new JxAssessSchemeExec(jxAssessSchemeExecDb.getId());
        update.setInitStatus("1");
        update.setInitDate(new Date());
        this.updateSomeFieldByPk(update);
    }


    @Transactional
    public void startExec(JxAssessSchemeExec jxAssessSchemeExecDb) {
        boolean update = false;
        JxAssessSchemeExec jxAssessSchemeExecUpdate = new JxAssessSchemeExec(jxAssessSchemeExecDb.getId());
        if ("0".equals(jxAssessSchemeExecDb.getExecStatus()) || !StringUtils.hasText(jxAssessSchemeExecDb.getExecStatus())) {
            jxAssessSchemeExecUpdate.setExecStatus("1");
            update = true;
        }
        if ("0".equals(jxAssessSchemeExecDb.getInitStatus()) || !StringUtils.hasText(jxAssessSchemeExecDb.getInitStatus())) {
            jxAssessSchemeExecUpdate.setInitStatus("1");
            jxAssessSchemeExecUpdate.setInitDate(new Date());
            update = true;
        }
        if ("0".equals(jxAssessSchemeExecDb.getStatus()) || !StringUtils.hasText(jxAssessSchemeExecDb.getStatus())) {
            jxAssessSchemeExecUpdate.setStatus("1");
            update = true;
        }
        if (update) {
            this.updateSomeFieldByPk(jxAssessSchemeExecUpdate);
        }
        jxAssessGradeService.batchOpenToConfirm(jxAssessSchemeExecDb.getId());

    }

    /**
     * 是否启动方案（1-已启动；0-初始; 2-已结束; 3-暂停；4-已关闭）
     *
     * @param jxAssessSchemeExecDb
     */
    @Transactional
    public void stopExec(JxAssessSchemeExec jxAssessSchemeExecDb) {
        JxAssessSchemeExec jxAssessSchemeExecUpdate = new JxAssessSchemeExec(jxAssessSchemeExecDb.getId());
        jxAssessSchemeExecUpdate.setStatus("2");
        this.updateSomeFieldByPk(jxAssessSchemeExecUpdate);

    }

    /**
     * 是否启动方案（1-已启动；0-初始; 2-已结束; 3-暂停；4-已关闭）
     *
     * @param jxAssessSchemeExecDb
     */
    @Transactional
    public void closeExec(JxAssessSchemeExec jxAssessSchemeExecDb) {
        JxAssessSchemeExec jxAssessSchemeExecUpdate = new JxAssessSchemeExec(jxAssessSchemeExecDb.getId());
        jxAssessSchemeExecUpdate.setStatus("4");
        this.updateSomeFieldByPk(jxAssessSchemeExecUpdate);

    }

    /**
     * 是否启动方案（1-已启动；0-初始; 2-已结束; 3-暂停；4-已关闭）
     *
     * @param jxAssessSchemeExecDb
     */
    @Transactional
    public void suspendExec(JxAssessSchemeExec jxAssessSchemeExecDb) {
        JxAssessSchemeExec jxAssessSchemeExecUpdate = new JxAssessSchemeExec(jxAssessSchemeExecDb.getId());
        jxAssessSchemeExecUpdate.setStatus("3");
        this.updateSomeFieldByPk(jxAssessSchemeExecUpdate);

    }

    /**
     * 是否启动方案（1-已启动；0-初始; 2-已结束; 3-暂停；4-已关闭）
     *
     * @param jxAssessSchemeExecDb
     */
    @Transactional
    public void restartExec(JxAssessSchemeExec jxAssessSchemeExecDb) {
        JxAssessSchemeExec jxAssessSchemeExecUpdate = new JxAssessSchemeExec(jxAssessSchemeExecDb.getId());
        jxAssessSchemeExecUpdate.setStatus("1");
        this.updateSomeFieldByPk(jxAssessSchemeExecUpdate);

    }

    /**
     * 计算执行方案的人数信息
     *
     * @param schemeExecIds
     */
    public void calcExecListDataFromJxGrade(List<String> schemeExecIds) {
       baseMapper.calcExecListDataFromJxGrade(schemeExecIds);
    }

    /**
     * 计算执行方案的人数信息
     *
     * @param schemeExecId
     */
    public void calcExecDataFromJxGrade(String schemeExecId) {
        baseMapper.calcExecDataFromJxGrade(schemeExecId);
    }
}

