package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_user_incentive_info")
@ApiModel(description="员工奖惩信息表")
public class HrUserIncentiveInfo  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="用户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="奖惩名称",allowEmptyValue=true,example="",allowableValues="")
	String certificateName;

	
	@ApiModelProperty(notes="奖惩日期",allowEmptyValue=true,example="",allowableValues="")
	Date time;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="奖惩类型---0-现金，1-豪车，2-豪宅",allowEmptyValue=true,example="",allowableValues="")
	String type;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public HrUserIncentiveInfo(String id) {
		this.id = id;
	}
    
    /**
     * 员工奖惩信息表
     **/
	public HrUserIncentiveInfo() {
	}

}