package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserRecord;
import com.oa.hr.user.service.HrUserRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value="/*/oa/hr/user/hrUserRecord")
@Api(tags = {"用工任职记录表-操作接口"})
public class HrUserRecordController {

    static Logger logger = LoggerFactory.getLogger(HrUserRecordController.class);

    @Autowired
    private HrUserRecordService hrUserRecordService;

    @ApiOperation(value = "用工任职记录表-查询列表", notes = " ")
    @ApiEntityParams(HrUserRecord.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrUserRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrUserRecord(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("branchId", user.getBranchId());

        QueryWrapper<HrUserRecord> qw = QueryTools.initQueryWrapper(HrUserRecord.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = hrUserRecordService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "用工任职记录表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrUserRecord(@RequestBody HrUserRecord hrUserRecord) {
        
        
            User user = LoginUtils.getCurrentUserInfo();
            hrUserRecord.setBranchId(user.getBranchId());
            if (StringUtils.isEmpty(hrUserRecord.getId())) {
                hrUserRecord.setId(hrUserRecordService.createKey("id"));
            } else {
                HrUserRecord hrUserRecordQuery = new HrUserRecord(hrUserRecord.getId());
                if (hrUserRecordService.countByWhere(hrUserRecordQuery) > 0) {
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            hrUserRecordService.insert(hrUserRecord);
        
        return Result.ok("add-ok", "添加成功！").setData(hrUserRecord);
    }

    @ApiOperation(value = "用工任职记录表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrUserRecord(@RequestBody HrUserRecord hrUserRecord) {
        
        
            hrUserRecordService.deleteByPk(hrUserRecord);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "用工任职记录表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrUserRecord(@RequestBody HrUserRecord hrUserRecord) {
        
        
            hrUserRecordService.updateByPk(hrUserRecord);
        
        return Result.ok("edit-ok", "修改成功！").setData(hrUserRecord);
    }

    @ApiOperation(value = "用工任职记录表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrUserRecord.class, props = {}, remark = "用工任职记录表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        hrUserRecordService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "用工任职记录表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrUserRecord(@RequestBody List<HrUserRecord> hrUserRecords) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrUserRecords.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrUserRecord> datasDb = hrUserRecordService.listByIds(hrUserRecords.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrUserRecord> can = new ArrayList<>();
        List<HrUserRecord> no = new ArrayList<>();
        for (HrUserRecord data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            hrUserRecordService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "用工任职记录表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrUserRecord hrUserRecord) {
        HrUserRecord data = (HrUserRecord) hrUserRecordService.getById(hrUserRecord);
        return Result.ok().setData(data);
    }

}
