package com.oa.ht.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.ht.entity.ContractInvoiceReceive;
import com.oa.ht.mapper.ContractInvoiceReceiveMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class ContractInvoiceReceiveService extends BaseService<ContractInvoiceReceiveMapper, ContractInvoiceReceive> {
    static Logger logger = LoggerFactory.getLogger(ContractInvoiceReceiveService.class);

    @Autowired
    private ContractCardService contractCardService;

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }


    @Transactional
    public void add(ContractInvoiceReceive contractInvoiceReceive) {
        super.insert(contractInvoiceReceive);
        if (StringUtils.hasText(contractInvoiceReceive.getConditionId())) {
            this.updateConditionReceiveInvoicedAmount(contractInvoiceReceive.getConditionId());
        }
        this.updateContractCardShouPiaoAmount(contractInvoiceReceive.getHtId());
    }

    public void updateContractCardShouPiaoAmount(String htId) {
        contractCardService.updateContractCardShouPiaoAmount(htId);
    }


    public void updateConditionReceiveInvoicedAmount(String conditionId) {
        baseMapper.updateConditionReceiveInvoicedAmount(conditionId);
    }

    public void updateStatus(Map<String, Object> params) {
        baseMapper.updateStatus(params);
    }
}

