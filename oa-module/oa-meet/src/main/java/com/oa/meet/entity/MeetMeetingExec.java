package com.oa.meet.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("meet_meeting_exec")
@ApiModel(description="会议执行明细表")
public class MeetMeetingExec  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="会议定义",allowEmptyValue=true,example="",allowableValues="")
	String meetingId;

	
	@ApiModelProperty(notes="执行归属业务日期yyyy-MM-dd类型字符串",allowEmptyValue=true,example="",allowableValues="")
	String execDate;

	
	@ApiModelProperty(notes="完成时间",allowEmptyValue=true,example="",allowableValues="")
	Date execFinishDate;

	
	@ApiModelProperty(notes="执行状态0未完成1已完成",allowEmptyValue=true,example="",allowableValues="")
	String execStatus;

	
	@ApiModelProperty(notes="创建日期",allowEmptyValue=true,example="",allowableValues="")
	Date ctime;

	
	@ApiModelProperty(notes="星期几0、1、2、3、4、5、6代表周日到周六",allowEmptyValue=true,example="",allowableValues="")
	Integer dayOfWeek;

	
	@ApiModelProperty(notes="执行人编号",allowEmptyValue=true,example="",allowableValues="")
	String execUserid;

	
	@ApiModelProperty(notes="执行人姓名",allowEmptyValue=true,example="",allowableValues="")
	String execUsername;

	
	@ApiModelProperty(notes="1-31号",allowEmptyValue=true,example="",allowableValues="")
	Integer dayOfMonth;

	
	@ApiModelProperty(notes="会议执行标题-默认=会议定义标题",allowEmptyValue=true,example="",allowableValues="")
	String execTitle;

	
	@ApiModelProperty(notes="附件数",allowEmptyValue=true,example="",allowableValues="")
	Integer fileCnt;

	/**
	 *主键
	 **/
	public MeetMeetingExec(String id) {
		this.id = id;
	}
    
    /**
     * 会议执行明细表
     **/
	public MeetMeetingExec() {
	}

}