package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserFamilyInfo;
import com.oa.hr.user.service.HrUserFamilyInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value="/*/oa/hr/user/hrUserFamilyInfo")
@Api(tags = {"员工家庭信息表-操作接口"})
public class HrUserFamilyInfoController {

    static Logger logger = LoggerFactory.getLogger(HrUserFamilyInfoController.class);

    @Autowired
    private HrUserFamilyInfoService hrUserFamilyInfoService;

    @ApiOperation(value = "员工家庭信息表-查询列表", notes = " ")
    @ApiEntityParams(HrUserFamilyInfo.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrUserFamilyInfo.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrUserFamilyInfo(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("branchId", user.getBranchId());

        QueryWrapper<HrUserFamilyInfo> qw = QueryTools.initQueryWrapper(HrUserFamilyInfo.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = hrUserFamilyInfoService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "员工家庭信息表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserFamilyInfo.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrUserFamilyInfo(@RequestBody HrUserFamilyInfo hrUserFamilyInfo) {
        
        
            User user = LoginUtils.getCurrentUserInfo();
            hrUserFamilyInfo.setBranchId(user.getBranchId());
            if (StringUtils.isEmpty(hrUserFamilyInfo.getId())) {
                hrUserFamilyInfo.setId(hrUserFamilyInfoService.createKey("id"));
            } else {
                HrUserFamilyInfo hrUserFamilyInfoQuery = new HrUserFamilyInfo(hrUserFamilyInfo.getId());
                if (hrUserFamilyInfoService.countByWhere(hrUserFamilyInfoQuery) > 0) {
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            hrUserFamilyInfo.setBranchId(user.getBranchId());
            hrUserFamilyInfoService.insert(hrUserFamilyInfo);
        
        return Result.ok("add-ok", "添加成功！").setData(hrUserFamilyInfo);
    }

    @ApiOperation(value = "员工家庭信息表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrUserFamilyInfo(@RequestBody HrUserFamilyInfo hrUserFamilyInfo) {
        
        
            hrUserFamilyInfoService.deleteByPk(hrUserFamilyInfo);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "员工家庭信息表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserFamilyInfo.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrUserFamilyInfo(@RequestBody HrUserFamilyInfo hrUserFamilyInfo) {
        
        
            hrUserFamilyInfoService.updateByPk(hrUserFamilyInfo);
        
        return Result.ok("edit-ok", "修改成功！").setData(hrUserFamilyInfo);
    }

    @ApiOperation(value = "员工家庭信息表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrUserFamilyInfo.class, props = {}, remark = "员工家庭信息表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserFamilyInfo.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        hrUserFamilyInfoService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "员工家庭信息表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrUserFamilyInfo(@RequestBody List<HrUserFamilyInfo> hrUserFamilyInfos) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrUserFamilyInfos.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrUserFamilyInfo> datasDb = hrUserFamilyInfoService.listByIds(hrUserFamilyInfos.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrUserFamilyInfo> can = new ArrayList<>();
        List<HrUserFamilyInfo> no = new ArrayList<>();
        for (HrUserFamilyInfo data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            hrUserFamilyInfoService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "员工家庭信息表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserFamilyInfo.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrUserFamilyInfo hrUserFamilyInfo) {
        HrUserFamilyInfo data = (HrUserFamilyInfo) hrUserFamilyInfoService.getById(hrUserFamilyInfo);
        return Result.ok().setData(data);
    }

}
