package com.oa.crm.enums;

public enum  CustomerOpertionEnum {

    OPER_CREATE("create", "创建"),
    OPER_UPDATE("update", "修改"),
    OPER_DELETE("delete", "删除");

    CustomerOpertionEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;

    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }



}
