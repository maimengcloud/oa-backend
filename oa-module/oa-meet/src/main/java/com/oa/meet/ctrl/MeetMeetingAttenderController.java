package com.oa.meet.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.meet.entity.MeetMeetingAttender;
import com.oa.meet.service.MeetMeetingAttenderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value="/*/oa/meet/meetMeetingAttender")
@Api(tags = {"会议参会人员表-操作接口"})
public class MeetMeetingAttenderController {

    static Logger logger = LoggerFactory.getLogger(MeetMeetingAttenderController.class);

    @Autowired
    private MeetMeetingAttenderService meetMeetingAttenderService;

    @ApiOperation(value = "会议参会人员表-查询列表", notes = " ")
    @ApiEntityParams(MeetMeetingAttender.class)
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMeetMeetingAttender(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<MeetMeetingAttender> qw = QueryTools.initQueryWrapper(MeetMeetingAttender.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = meetMeetingAttenderService.selectListMapByWhere(page, qw, params);


        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "会议参会人员表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMeetMeetingAttender(@RequestBody MeetMeetingAttender meetMeetingAttender) {
        meetMeetingAttenderService.save(meetMeetingAttender);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "会议参会人员表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMeetMeetingAttender(@RequestBody MeetMeetingAttender meetMeetingAttender) {
        meetMeetingAttenderService.removeById(meetMeetingAttender);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "会议参会人员表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMeetMeetingAttender(@RequestBody MeetMeetingAttender meetMeetingAttender) {
        meetMeetingAttenderService.updateById(meetMeetingAttender);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "会议参会人员表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MeetMeetingAttender.class, props = {}, remark = "会议参会人员表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        meetMeetingAttenderService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "会议参会人员表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMeetMeetingAttender(@RequestBody List<MeetMeetingAttender> meetMeetingAttenders) {
        User user = LoginUtils.getCurrentUserInfo();
        if (meetMeetingAttenders.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MeetMeetingAttender> datasDb = meetMeetingAttenderService.listByIds(meetMeetingAttenders.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MeetMeetingAttender> can = new ArrayList<>();
        List<MeetMeetingAttender> no = new ArrayList<>();
        for (MeetMeetingAttender data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            meetMeetingAttenderService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "会议参会人员表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MeetMeetingAttender meetMeetingAttender) {
        MeetMeetingAttender data = (MeetMeetingAttender) meetMeetingAttenderService.getById(meetMeetingAttender);
        return Result.ok().setData(data);
    }

}
