package com.oa.sig.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("sig_seal")
@ApiModel(description="用印申请表")
public class SigSeal  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="申请单编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="申请人编号",allowEmptyValue=true,example="",allowableValues="")
	String sealUserid;

	
	@ApiModelProperty(notes="申请人姓名",allowEmptyValue=true,example="",allowableValues="")
	String sealUsername;

	
	@ApiModelProperty(notes="申请部门编号",allowEmptyValue=true,example="",allowableValues="")
	String sealDeptid;

	
	@ApiModelProperty(notes="申请部门",allowEmptyValue=true,example="",allowableValues="")
	String sealDeptName;

	
	@ApiModelProperty(notes="联系电话",allowEmptyValue=true,example="",allowableValues="")
	String sealPhoneno;

	
	@ApiModelProperty(notes="申请时间",allowEmptyValue=true,example="",allowableValues="")
	Date reqTime;

	
	@ApiModelProperty(notes="用印开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	
	@ApiModelProperty(notes="用印结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="用印主体机构号",allowEmptyValue=true,example="",allowableValues="")
	String sealBranchId;

	
	@ApiModelProperty(notes="用印主题机构名称",allowEmptyValue=true,example="",allowableValues="")
	String sealBranchName;

	
	@ApiModelProperty(notes="流程状态",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	
	@ApiModelProperty(notes="流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="申请状态",allowEmptyValue=true,example="",allowableValues="")
	String sealStatus;

	
	@ApiModelProperty(notes="用印原因",allowEmptyValue=true,example="",allowableValues="")
	String sealReason;

	
	@ApiModelProperty(notes="其它说明",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	/**
	 *申请单编号
	 **/
	public SigSeal(String id) {
		this.id = id;
	}
    
    /**
     * 用印申请表
     **/
	public SigSeal() {
	}

}