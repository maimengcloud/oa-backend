package com.oa.erp.ass.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ass_asset_category")
@ApiModel(description="ass_asset_category")
public class AssetCategory  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
    @TableIds
	
    @ApiModelProperty(notes="资产分类编码,主键",allowEmptyValue=true,example="",allowableValues="")
    String categoryId;
    @TableIds
	
    @ApiModelProperty(notes="组织机构,主键",allowEmptyValue=true,example="",allowableValues="")
    String branchId;

	
	@ApiModelProperty(notes="资产分类名称",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	
	@ApiModelProperty(notes="上级Id,顶级为0",allowEmptyValue=true,example="",allowableValues="")
	String parentId;

	
	@ApiModelProperty(notes="层次排序",allowEmptyValue=true,example="",allowableValues="")
	String level;

	
	@ApiModelProperty(notes="排序",allowEmptyValue=true,example="",allowableValues="")
	Integer seq;

	/**
	 *资产分类编码,组织机构
	 **/
	public AssetCategory(String categoryId,String branchId) {
		this.categoryId = categoryId;
		this.branchId = branchId;
	}
    
    /**
     * ass_asset_category
     **/
	public AssetCategory() {
	}

}