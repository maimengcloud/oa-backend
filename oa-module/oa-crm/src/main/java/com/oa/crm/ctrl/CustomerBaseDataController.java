package com.oa.crm.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.crm.entity.CustomerBaseData;
import com.oa.crm.service.CustomerBaseDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/crm/customerBaseData")
@Api(tags = {"crm_customer_base_data-操作接口"})
public class CustomerBaseDataController {

    static Logger logger = LoggerFactory.getLogger(CustomerBaseDataController.class);

    @Autowired
    private CustomerBaseDataService customerBaseDataService;

    @ApiOperation(value = "crm_customer_base_data-查询列表", notes = " ")
    @ApiEntityParams(CustomerBaseData.class)
    @ApiResponses({@ApiResponse(code = 200, response = CustomerBaseData.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCustomerBaseData(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CustomerBaseData> qw = QueryTools.initQueryWrapper(CustomerBaseData.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = customerBaseDataService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "crm_customer_base_data-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerBaseData.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCustomerBaseData(@RequestBody CustomerBaseData customerBaseData) {
        customerBaseDataService.save(customerBaseData);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "crm_customer_base_data-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCustomerBaseData(@RequestBody CustomerBaseData customerBaseData) {
        customerBaseDataService.removeById(customerBaseData);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "crm_customer_base_data-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerBaseData.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCustomerBaseData(@RequestBody CustomerBaseData customerBaseData) {
        customerBaseDataService.updateById(customerBaseData);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "crm_customer_base_data-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CustomerBaseData.class, props = {}, remark = "crm_customer_base_data", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerBaseData.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        customerBaseDataService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "crm_customer_base_data-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCustomerBaseData(@RequestBody List<CustomerBaseData> customerBaseDatas) {
        User user = LoginUtils.getCurrentUserInfo();
        if (customerBaseDatas.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CustomerBaseData> datasDb = customerBaseDataService.listByIds(customerBaseDatas.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CustomerBaseData> can = new ArrayList<>();
        List<CustomerBaseData> no = new ArrayList<>();
        for (CustomerBaseData data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            customerBaseDataService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "crm_customer_base_data-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerBaseData.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CustomerBaseData customerBaseData) {
        CustomerBaseData data = (CustomerBaseData) customerBaseDataService.getById(customerBaseData);
        return Result.ok().setData(data);
    }

}
