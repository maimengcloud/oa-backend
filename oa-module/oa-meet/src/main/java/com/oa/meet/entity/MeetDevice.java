package com.oa.meet.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("meet_device")
@ApiModel(description="会议设备管理")
public class MeetDevice  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="设备名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="设备型号",allowEmptyValue=true,example="",allowableValues="")
	String version;

	
	@ApiModelProperty(notes="用途",allowEmptyValue=true,example="",allowableValues="")
	String apply;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="状态1启用0禁用",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="归属机构",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *主键
	 **/
	public MeetDevice(String id) {
		this.id = id;
	}
    
    /**
     * 会议设备管理
     **/
	public MeetDevice() {
	}

}