package com.oa.jx.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.query.QueryTools;
import com.mdp.core.service.BaseService;
import com.mdp.core.utils.BaseUtils;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.jx.entity.JxAssessGrade;
import com.oa.jx.entity.JxAssessSchemeExec;
import com.oa.jx.service.JxAssessGradeDetailService;
import com.oa.jx.service.JxAssessGradeService;
import com.oa.jx.service.JxAssessSchemeExecService;
import com.oa.jx.vo.BatchGradeVo;
import com.oa.jx.vo.JxAssessInfoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/jx/jxAssessGrade")
@Api(tags = {"考核总分表-操作接口"})
public class JxAssessGradeController {

    static Logger logger = LoggerFactory.getLogger(JxAssessGradeController.class);

    @Autowired
    private JxAssessGradeService jxAssessGradeService;
    @Autowired
    private JxAssessGradeDetailService jxAssessGradeDetailService;

    @Autowired
    private JxAssessSchemeExecService jxAssessSchemeExecService;

    @ApiOperation(value = "考核总分表-查询列表", notes = " ")
    @ApiEntityParams(JxAssessGrade.class)
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGrade.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listJxAssessGrade(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        RequestUtils.transformArray(params, "deptids");
        String queryScene = (String) params.get("queryScene");
        if ("my".equals(queryScene)) {
            params.put("myUserid", user.getUserid());
        }
        if ("his".equals(queryScene)) {
            String hisUserid = (String) params.get("hisUserid");
            if (!StringUtils.hasText(hisUserid)) {
                return Result.error("hisUserid-must", "请上送参数hisUserid");
            }
            params.put("hisUserid", hisUserid);
        }
        QueryWrapper<JxAssessGrade> qw = QueryTools.initQueryWrapper(JxAssessGrade.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = jxAssessGradeService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "考核总分表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGrade.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addJxAssessGrade(@RequestBody JxAssessGrade jxAssessGrade) {
        jxAssessGradeService.save(jxAssessGrade);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "考核总分表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delJxAssessGrade(@RequestBody JxAssessGrade jxAssessGrade) {
        if (StringUtils.isEmpty(jxAssessGrade.getId())) {
            return Result.error("id-is-null", "id不能为空");
        }
        jxAssessGrade = this.jxAssessGradeService.selectOneObject(jxAssessGrade);
        if (jxAssessGrade == null) {
            return Result.error("data-is-null", "数据不存在");
        }
        if ("1".equals(jxAssessGrade.getAssCstatus())) {
            return Result.error("assCstatus-1", "员工已签订确认，不能删除");
        }
        jxAssessGradeService.deleteByPk(jxAssessGrade);
        this.jxAssessSchemeExecService.calcExecDataFromJxGrade(jxAssessGrade.getSchemeExecId());
        return Result.ok("del-ok", "删除成功！");
    }

    /**
     *
     */
    @ApiOperation(value = "完成绩效考核", notes = "editJxAssessGrade")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGrade.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/closeGrade", method = RequestMethod.POST)
    public Result closeGrade(@RequestBody JxAssessGrade jxAssessGrade) {
        if (!StringUtils.hasText(jxAssessGrade.getId())) {
            return Result.error("id-is-null", "id不能为空");
        }
        JxAssessGrade jxAssessGradeDb = this.jxAssessGradeService.selectOneObject(new JxAssessGrade(jxAssessGrade.getId()));
        if (jxAssessGradeDb == null) {
            return Result.error("data-is-null", "数据不存在");
        }
        if (!"6".equals(jxAssessGradeDb.getStatus()) && StringUtils.hasText(jxAssessGradeDb.getStatus())) {
            return Result.error("status-not-6", "当前数据不是发放奖励状态，不能变更为完成操作");
        }
        JxAssessGrade update = new JxAssessGrade(jxAssessGrade.getId());
        update.setStatus("7");
        jxAssessGradeService.updateSomeFieldByPk(update);
        return Result.ok("update-ok", "成功更新一条数据").setData(jxAssessGrade);
    }

    /**
     *
     */
    @ApiOperation(value = "开放员工签约", notes = "editJxAssessGrade")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGrade.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/openToConfirm", method = RequestMethod.POST)
    public Result openToConfirm(@RequestBody JxAssessGrade jxAssessGrade) {
        if (!StringUtils.hasText(jxAssessGrade.getId())) {
            return Result.error("id-is-null", "id不能为空");
        }
        JxAssessGrade jxAssessGradeDb = this.jxAssessGradeService.selectOneObject(new JxAssessGrade(jxAssessGrade.getId()));
        if (jxAssessGradeDb == null) {
            return Result.error("data-is-null", "数据不存在");
        }
        if (!"0".equals(jxAssessGradeDb.getStatus()) && StringUtils.hasText(jxAssessGradeDb.getStatus())) {
            return Result.error("status-not-0", "当前数据非初始状态不能开启签约");
        }
        JxAssessGrade update = new JxAssessGrade(jxAssessGrade.getId());
        update.setStatus("1");
        update.setAssCstatus("0");
        jxAssessGradeService.updateSomeFieldByPk(update);

        return Result.ok().setData(jxAssessGrade);
    }

    //批量设置核定人
    @RequestMapping(value = "/batchSetGrade", method = RequestMethod.POST)
    public Result batchSetGrade(@RequestBody BatchGradeVo batchGradeVo) {
        Tips tips = new Tips("设置成功");
        if (batchGradeVo.getIds() == null || batchGradeVo.getIds().size() == 0) {
            return Result.error("ids-null", "请上送ids");
        }
        if (batchGradeVo.getCommand() == null || batchGradeVo.getCommand().equals("")) {
            return Result.error("command-null", "请上送command");
        }

        if (batchGradeVo.getCommand().equals("batchSetReUser")) {
            if (!StringUtils.hasText(batchGradeVo.getReUserid())) {
                return Result.error("reUserid-null", "请上送reUserid");
            }
            if (!StringUtils.hasText(batchGradeVo.getReUsername())) {
                return Result.error("reUsername-null", "请上送reUsername");
            }
        } else if (batchGradeVo.getCommand().equals("batchSetVouchUser")) {
            if (!StringUtils.hasText(batchGradeVo.getVouchId())) {
                return Result.error("vouchId-null", "请上送vouchId");
            }
            if (!StringUtils.hasText(batchGradeVo.getVouchName())) {
                return Result.error("vouchName-null", "请上送vouchName");
            }
        }
        if (batchGradeVo.getIds().size() > 100) {
            return Result.error("ids-size-too-big", "一次最多处理100条数据");
        }
        List<Map<String, Object>> datas = this.jxAssessGradeService.selectListMapByWhere(null, null, BaseService.map("ids", batchGradeVo.getIds()));
        List<String> ids = new ArrayList<>();
        Map<String, String> schemeExecIdsMap = new HashMap<>();
        batchGradeVo.setStatus(null);
        for (Map<String, Object> data : datas) {
            String status = (String) data.get("status");
            String id = (String) data.get("id");
            String schemeExecId = (String) data.get("schemeExecId");
            if (batchGradeVo.getCommand().equals("batchSetReUser")) {
                if (!("5".equals(status) || "6".equals(status) || "7".equals(status))) {
                    ids.add(id);
                    schemeExecIdsMap.put(schemeExecId, schemeExecId);
                }
            } else if (batchGradeVo.getCommand().equals("batchSetVouchUser")) {
                if (!("6".equals(status) || "7".equals(status))) {
                    ids.add(id);
                    schemeExecIdsMap.put(schemeExecId, schemeExecId);
                }
            } else if (batchGradeVo.getCommand().equals("batchFinishGrade")) {
                if ("6".equals(status)) {
                    ids.add(id);
                    batchGradeVo.setStatus("7");
                    schemeExecIdsMap.put(schemeExecId, schemeExecId);
                }
            } else if (batchGradeVo.getCommand().equals("batchStartGrade")) {
                if ("0".equals(status)) {
                    ids.add(id);
                    batchGradeVo.setStatus("1");
                    schemeExecIdsMap.put(schemeExecId, schemeExecId);
                }
            } else if (batchGradeVo.getCommand().equals("batchDel")) {
                if ("0".equals(status)) {
                    ids.add(id);
                    schemeExecIdsMap.put(schemeExecId, schemeExecId);
                }
            }
        }
        if (ids.size() > 0) {
            batchGradeVo.setIds(ids);
            if (schemeExecIdsMap.size() > 1) {
                return Result.error("schemeExecId-too-more", "批量操作只允许在同一个考核方案下进行，请选定考核方案再操作");
            }
            if (batchGradeVo.getCommand().equals("batchDel")) {
                this.jxAssessGradeService.batchDelGrade(batchGradeVo);
                this.jxAssessSchemeExecService.calcExecListDataFromJxGrade(schemeExecIdsMap.values().stream().collect(Collectors.toList()));
                tips.setOkMsg("删除成功" + ids.size() + "条数据");
            } else {
                this.jxAssessGradeService.batchUpdateGrade(batchGradeVo);
                tips.setOkMsg("更新成功" + ids.size() + "条数据");
            }
        }

        return Result.ok("set-ok", "").setTips(LangTips.fromTips(tips));
    }

    //保存考核信息
    @RequestMapping(value = "/saveAssessInfo", method = RequestMethod.POST)
    public Result saveAssessInfo(@RequestBody JxAssessInfoVo jxAssessInfoVo) {
        Tips tips = new Tips("保存成功");
        JxAssessGrade jxAssessGradeDb = null;
        User user = LoginUtils.getCurrentUserInfo();
        if (StringUtils.hasText(jxAssessInfoVo.getId())) {
            jxAssessInfoVo.setBranchId(user.getBranchId());
            jxAssessGradeDb = this.jxAssessGradeService.selectOneObject(new JxAssessGrade(jxAssessInfoVo.getId()));
            tips = this.jxAssessGradeService.checkJxAssessGradeStatusByAction(jxAssessGradeDb, jxAssessInfoVo.getAction(), user);
            if (!tips.isOk()) {
                return Result.error().setData(tips);
            }
        }
        JxAssessSchemeExec jxAssessSchemeExecDb = jxAssessSchemeExecService.selectOneObject(new JxAssessSchemeExec(jxAssessGradeDb.getSchemeExecId()));
        if (jxAssessSchemeExecDb == null) {
            return Result.error("schemeExec-null", "执行方案已不存在，不允许再修改数据");
        }
        if (!"1".equals(jxAssessSchemeExecDb.getStatus()) && !"0".equals(jxAssessSchemeExecDb.getStatus())) {
            return Result.error("schemeExec-status-not-01", "执行方案已经不是执行或者初始状态，不允许修改绩效数据");
        }
        jxAssessGradeService.saveAssessInfo(jxAssessInfoVo, jxAssessGradeDb);
        this.jxAssessSchemeExecService.calcExecDataFromJxGrade(jxAssessGradeDb.getSchemeExecId());

        return Result.ok().setData(jxAssessInfoVo).setTips(LangTips.fromTips(tips));
    }

    //获取考核统计
    @RequestMapping(value = "/listUserAssessStatistics", method = RequestMethod.GET)
    public Result getUserAssessStatistics(@RequestParam Map<String, Object> jxAssessGrade) {
//        PageUtils.startPage(jxAssessGrade);
        String schemeId = (String) jxAssessGrade.get("schemeId");
        String schemeType = (String) jxAssessGrade.get("schemeType");
        String assDeptid = (String) jxAssessGrade.get("assDeptid");
        User user = LoginUtils.getCurrentUserInfo();
        jxAssessGrade.put("branchId", user.getBranchId());
        if (!StringUtils.hasText(schemeId)) {
            return Result.error("schemeId-is-null", "schemeId不能为空");
        }
        if (!StringUtils.hasText(schemeId)) {
            return Result.error("schemeType-is-null", "schemeType不能为空");
        }
        List<Map<String, Object>> jxAssessGradeList = jxAssessGradeService.selectUserAssessStatistics(jxAssessGrade);    //列出JxAssessGrade列表
//        PageUtils.responePage(m, jxAssessGradeList);
        return Result.ok().setData(jxAssessGradeList);
    }

    //获取考核统计
    @RequestMapping(value = "/listDeptAssessStatistics", method = RequestMethod.GET)
    public Result listDeptAssessStatistics(@RequestParam Map<String, Object> jxAssessGrade) {
        String schemeId = (String) jxAssessGrade.get("schemeId");
        String schemeType = (String) jxAssessGrade.get("schemeType");
        String assDeptid = (String) jxAssessGrade.get("assDeptid");
        User user = LoginUtils.getCurrentUserInfo();
        jxAssessGrade.put("branchId", user.getBranchId());

        if (!StringUtils.hasText(schemeId)) {
            return Result.error("schemeId-is-null", "schemeId不能为空");
        }
        if (!StringUtils.hasText(schemeType)) {
            return Result.error("schemeType-is-null", "schemeType不能为空");
        }
        if (!StringUtils.hasText(assDeptid)) {
            return Result.error("assDeptid-is-null", "assDeptid不能为空");
        }
        List<Map<String, Object>> jxAssessGradeList = jxAssessGradeService.selectDeptAssessStatistics(jxAssessGrade);    //列出JxAssessGrade列表
//        PageUtils.responePage(m, jxAssessGradeList);

        return Result.ok().setData(jxAssessGradeList);
    }


    @ApiOperation(value = "考核总分表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGrade.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editJxAssessGrade(@RequestBody JxAssessGrade jxAssessGrade) {
        jxAssessGradeService.updateById(jxAssessGrade);
        return Result.ok("edit-ok", "修改成功！");
    }

    /**
     * 获取全年执行方案的统计数据
     * id
     *
     * @param map
     * @return
     */
    @RequestMapping(value = "/listFullYear", method = RequestMethod.GET)
    public Result listFullYear(@RequestParam Map<String, Object> map) {
        User user = LoginUtils.getCurrentUserInfo();
        Map<String, Object> jxAssessSchemeExecQuery = new HashMap<>();
        String year = (String) map.get("year");
        String assUserid = (String) map.get("assUserid");
        if (!StringUtils.hasText(assUserid)) {
            return Result.error("assUserid-is-null", "员工编号assUserid缺失");
        }
        if (!StringUtils.hasText(year)) {
            return Result.error("year-is-null", "请选择年份");
        }
        jxAssessSchemeExecQuery.put("assUserid", assUserid);
        jxAssessSchemeExecQuery.put("branchId", user.getBranchId());
        jxAssessSchemeExecQuery.put("year", year);

        QueryWrapper<JxAssessSchemeExec> jxAssessSchemeExecQueryWrapper = QueryTools.initQueryWrapper(JxAssessSchemeExec.class, jxAssessSchemeExecQuery);
        IPage jxAssessSchemeExecQueryIPage = QueryTools.initPage(jxAssessSchemeExecQuery);
        List<Map<String, Object>> jxAssessSchemeExecList = jxAssessSchemeExecService.selectListMapByWhere(jxAssessSchemeExecQueryIPage, jxAssessSchemeExecQueryWrapper, jxAssessSchemeExecQuery);

        JxAssessGrade jxAssessGradeQuery = new JxAssessGrade();
        jxAssessGradeQuery.setYear(year);
        jxAssessGradeQuery.setAssUserid(assUserid);
        List<JxAssessGrade> jxAssessGradeList = jxAssessGradeService.selectListByWhere(jxAssessGradeQuery);

        return Result.ok().setData(jxAssessGradeList).put("schemeExecList",jxAssessSchemeExecList);
    }

    /**
     * 通过id获取详细的考核信息
     * id
     *
     * @param map
     * @return
     */
    @RequestMapping(value = "/getDetail", method = RequestMethod.GET)
    public Result getDetail(@RequestParam Map<String, Object> map) {
        String jxAssessGradeId = (String) map.get("id");
        //查询被考核人的考核情况
        JxAssessGrade jxAssessGrade = this.jxAssessGradeService.selectOneObject(new JxAssessGrade(jxAssessGradeId));
        List<Map<String, Object>> contents = this.jxAssessGradeDetailService.selectListMapByWhereWithTemplateContents(jxAssessGrade.getId(), jxAssessGrade.getTemplateId());
        return Result.ok().setData(jxAssessGrade);
    }

    /**
     * 通过 年+方案类型+日期(根据方案类型取年月季) 获取详细的考核信息
     * id
     *
     * @param map
     * @return
     */
    @RequestMapping(value = "/getAssessInfo", method = RequestMethod.GET)
    public Result getAssessInfo(@RequestParam Map<String, Object> map) {
        Map<String, Object> m = new HashMap<>();
        JxAssessGrade jxAssessGrade = BaseUtils.fromMap(map, JxAssessGrade.class);
        JxAssessGrade query = new JxAssessGrade();
        if ("0".equals(jxAssessGrade.getSchemeType())) {
            query.setMonth(jxAssessGrade.getMonth());
        } else if ("1".equals(jxAssessGrade.getSchemeType())) {
            query.setQuarter(jxAssessGrade.getQuarter());
        } else if ("2".equals(jxAssessGrade.getSchemeType())) {
            query.setSemiAnnual(jxAssessGrade.getSemiAnnual());
        } else if ("3".equals(jxAssessGrade.getSchemeType())) {
            query.setAnnual(jxAssessGrade.getAnnual());
        }
        if (!StringUtils.hasText(jxAssessGrade.getAssUserid())) {
            return Result.error("ByAssUserid-is-null", "员工编号不能为空");
        }
        query.setAssUserid(jxAssessGrade.getAssUserid());
        query.setBranchId(jxAssessGrade.getBranchId());
        query.setYear(jxAssessGrade.getYear());
        List<JxAssessGrade> jxAssessGradeList = this.jxAssessGradeService.selectListByWhere(query);
        if (jxAssessGradeList != null && jxAssessGradeList.size() > 0) {
            JxAssessGrade jxAssessGradeDb = jxAssessGradeList.get(0);
            List<Map<String, Object>> contents = this.jxAssessGradeDetailService.selectListMapByWhereWithTemplateContents(jxAssessGradeDb.getId(), jxAssessGradeDb.getTemplateId());
            m.put("data", jxAssessGrade);
            m.put("contents", contents);
        } else {
            return Result.error("data-is-null", "找不到数据");
        }

        return Result.ok().setData(m.get("data")).put("contents", m.get("contents"));
    }

    //获取用户所在的部门和当前机构号的的评优等级比率
    @RequestMapping(value = "/deptAssessmentOf", method = RequestMethod.GET)
    public Result deptAssessmentOf(@RequestParam Map<String, Object> map) {
        Map<String, Object> assessmentInformation = jxAssessGradeService.getDeptAssessmentOf(map);    //列出JxAssessGrade列表
        return Result.ok().setData(assessmentInformation);
    }

    @ApiOperation(value = "考核总分表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = JxAssessGrade.class, props = {}, remark = "考核总分表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGrade.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        jxAssessGradeService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "考核总分表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelJxAssessGrade(@RequestBody List<JxAssessGrade> jxAssessGrades) {
        User user = LoginUtils.getCurrentUserInfo();
        if (jxAssessGrades.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<JxAssessGrade> datasDb = jxAssessGradeService.listByIds(jxAssessGrades.stream().map(i -> i.getId()).collect(Collectors.toList()));
        List<JxAssessGrade> can = new ArrayList<>();
        List<JxAssessGrade> no = new ArrayList<>();
        for (JxAssessGrade data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            jxAssessGradeService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "考核总分表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGrade.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(JxAssessGrade jxAssessGrade) {
        JxAssessGrade data = (JxAssessGrade) jxAssessGradeService.getById(jxAssessGrade);
        return Result.ok().setData(data);
    }

}
