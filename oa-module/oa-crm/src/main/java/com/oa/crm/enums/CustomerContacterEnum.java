package com.oa.crm.enums;

public enum CustomerContacterEnum {

    MAIN_CONTACTER("1", "主联系人"),
    NOT_MAIN_CONTACTER("0", "非主联系人");

    CustomerContacterEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;

    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
