package com.oa.matter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("sxk_matter_inspect_content")
@ApiModel(description="事项检查内容")
public class MatterInspectContent  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="编号",allowEmptyValue=true,example="",allowableValues="")
	String serialNumber;

	
	@ApiModelProperty(notes="检查内容名称",allowEmptyValue=true,example="",allowableValues="")
	String contentName;

	
	@ApiModelProperty(notes="检查方法",allowEmptyValue=true,example="",allowableValues="")
	String inspectMethod;

	
	@ApiModelProperty(notes="法定频次（检查频次）",allowEmptyValue=true,example="",allowableValues="")
	String legalFrequency;

	
	@ApiModelProperty(notes="建议频次（检查频次）",allowEmptyValue=true,example="",allowableValues="")
	String adviseFrequency;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remarks;

	
	@ApiModelProperty(notes="检查项目id",allowEmptyValue=true,example="",allowableValues="")
	String itemId;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public MatterInspectContent(String id) {
		this.id = id;
	}
    
    /**
     * 事项检查内容
     **/
	public MatterInspectContent() {
	}

}