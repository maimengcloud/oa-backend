package com.oa.jx.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.jx.entity.JxAssessScheme;
import com.oa.jx.entity.JxAssessTemplate;
import com.oa.jx.service.*;
import com.oa.jx.vo.AddAssessTemplateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/jx/jxAssessTemplate")
@Api(tags = {"考核模板表-操作接口"})
public class JxAssessTemplateController {

    static Logger logger = LoggerFactory.getLogger(JxAssessTemplateController.class);


    @Autowired
    private JxAssessTemplateService jxAssessTemplateService;

    @Autowired
    private JxAssessSchemeService jxAssessSchemeService;

    @Autowired
    JxAssessContentService jxAssessContentService;
    @Autowired
    JxAssessTemplateLinkService jxAssessTemplateLinkService;

    @Autowired
    JxAdmDsService jxAdmDsService;

    @ApiOperation(value = "考核模板表-查询列表", notes = " ")
    @ApiEntityParams(JxAssessTemplate.class)
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessTemplate.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listJxAssessTemplate(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");

        QueryWrapper<JxAssessTemplate> qw = QueryTools.initQueryWrapper(JxAssessTemplate.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = jxAssessTemplateService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "考核模板表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessTemplate.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addJxAssessTemplate(@RequestBody JxAssessTemplate jxAssessTemplate) {


        User user = LoginUtils.getCurrentUserInfo();
        if (StringUtils.isEmpty(jxAssessTemplate.getId())) {
            jxAssessTemplate.setId(jxAssessTemplateService.createKey("id"));
        } else {
            JxAssessTemplate jxAssessTemplateQuery = new JxAssessTemplate(jxAssessTemplate.getId());
            if (jxAssessTemplateService.countByWhere(jxAssessTemplateQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        jxAssessTemplate.setBranchId(user.getBranchId());
        jxAssessTemplateService.insert(jxAssessTemplate);

        return Result.ok("add-ok", "添加成功！").setData(jxAssessTemplate);
    }

    @ApiOperation(value = "考核模板表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delJxAssessTemplate(@RequestBody JxAssessTemplate jxAssessTemplate) {


        if (!StringUtils.hasText(jxAssessTemplate.getId())) {
            return Result.error("id-null", "请上送模板编号id");
        }
        JxAssessScheme jxAssessScheme = new JxAssessScheme();
        jxAssessScheme.setTemplateId(jxAssessTemplate.getId());
        long count = jxAssessSchemeService.countByWhere(jxAssessScheme);
        if (count > 0) {
            return Result.error("scheme-exists", "存在考核方案使用该模板，请先删除方案");
        }
        jxAssessTemplateService.deleteByTemplateId(jxAssessTemplate);


        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "考核模板表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessTemplate.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editJxAssessTemplate(@RequestBody JxAssessTemplate jxAssessTemplate) {


        jxAssessTemplateService.updateByPk(jxAssessTemplate);

        return Result.ok("edit-ok", "修改成功！").setData(jxAssessTemplate);
    }

    @ApiOperation(value = "考核模板表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = JxAssessTemplate.class, props = {}, remark = "考核模板表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessTemplate.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        jxAssessTemplateService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "考核模板表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelJxAssessTemplate(@RequestBody List<JxAssessTemplate> jxAssessTemplates) {
        User user = LoginUtils.getCurrentUserInfo();
        if (jxAssessTemplates.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<JxAssessTemplate> datasDb = jxAssessTemplateService.listByIds(jxAssessTemplates.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<JxAssessTemplate> can = new ArrayList<>();
        List<JxAssessTemplate> no = new ArrayList<>();
        for (JxAssessTemplate data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            jxAssessTemplateService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "考核模板表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessTemplate.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(JxAssessTemplate jxAssessTemplate) {
        JxAssessTemplate data = (JxAssessTemplate) jxAssessTemplateService.getById(jxAssessTemplate);
        return Result.ok().setData(data);
    }


    //模糊查询用户
    @RequestMapping(value = "/listUsers", method = RequestMethod.GET)
    public Result listUsers(@RequestParam Map<String, Object> params) {
        Map<String, Object> m = new HashMap<>();
        // todo 没有弄好 jxAdmDsService,
//        List<Map<String, Object>> users = jxAdmDsService.selectUserByUsername(params);
        List<Map<String, Object>> users = null;


        return Result.ok();
    }

    //新增模板
    @RequestMapping(value = "/addAccessTemplate", method = RequestMethod.POST)
    public Result addAccessTemplate(@RequestBody AddAssessTemplateVo addAssessTemplateVo) {


        User user = LoginUtils.getCurrentUserInfo();
        addAssessTemplateVo.setId(jxAssessTemplateService.createKey("id"));
        addAssessTemplateVo.setBranchId(user.getBranchId());
        jxAssessTemplateService.insertAccessTemplate(addAssessTemplateVo);

        return Result.ok().setData(addAssessTemplateVo);
    }

    //回显模板
    @RequestMapping(value = "/getJxAssessTemplate", method = RequestMethod.GET)
    public Result getJxAssessTemplate(@RequestParam Map<String, Object> jxAssessTemplate) {
        Map<String, Object> m = new HashMap<>();
        String id = (String) jxAssessTemplate.get("id");
        if (!StringUtils.hasText(id)) {
            return Result.error("id-is-null", "id不能为空");
        }
        QueryWrapper<JxAssessTemplate> qw = QueryTools.initQueryWrapper(JxAssessTemplate.class, jxAssessTemplate);
        IPage page = QueryTools.initPage(jxAssessTemplate);
        Map<String, Object> map = jxAssessTemplateService.getJxAssessTemplateByTemplateId(page, qw, jxAssessTemplate);

        return Result.ok().setData(map);
    }

    //修改模板
    @RequestMapping(value = "/editAccessTemplate", method = RequestMethod.POST)
    public Result editAccessTemplate(@RequestBody AddAssessTemplateVo editAssessTemplateVo) {


        if (StringUtils.isEmpty(editAssessTemplateVo.getId())) {
            return Result.error("id-null", "id不能为空");
        }
        jxAssessTemplateService.editAccessTemplate(editAssessTemplateVo);

        return Result.ok().setData(editAssessTemplateVo);
    }

    @RequestMapping(value = "/copyAccessTemplate", method = RequestMethod.GET)
    public Result copyAccessTemplate(@RequestParam Map<String, Object> jxAssessTemplate) {
        if (StringUtils.isEmpty(jxAssessTemplate.get("id"))) {
            return Result.error("id-null", "id不能为空");
        }
        jxAssessTemplateService.copyAccessTemplate(jxAssessTemplate);

        return Result.ok();
    }


}
