package com.oa.sig.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.sig.entity.SigSignet;
import com.oa.sig.service.SigSignetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/sig/sigSignet")
@Api(tags = {"印章库-操作接口"})
public class SigSignetController {

    static Logger logger = LoggerFactory.getLogger(SigSignetController.class);

    @Autowired
    private SigSignetService sigSignetService;

    @ApiOperation(value = "印章库-查询列表", notes = " ")
    @ApiEntityParams(SigSignet.class)
    @ApiResponses({
            @ApiResponse(code = 200, response = SigSignet.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
    })
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listSigSignet(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        params.put("branchId", branchId);
        QueryWrapper<SigSignet> qw = QueryTools.initQueryWrapper(SigSignet.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = sigSignetService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "印章库-新增", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = SigSignet.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addSigSignet(@RequestBody SigSignet sigSignet) {
        if (StringUtils.isEmpty(sigSignet.getId())) {
            sigSignet.setId(sigSignetService.createKey("id"));
        } else {
            SigSignet sigSignetQuery = new SigSignet(sigSignet.getId());
            if (sigSignetService.countByWhere(sigSignetQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        sigSignetService.insert(sigSignet);
        return Result.ok("add-ok", "添加成功！").setData(sigSignet);
    }

    @ApiOperation(value = "印章库-删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
    })
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delSigSignet(@RequestBody SigSignet sigSignet) {
        sigSignetService.deleteByPk(sigSignet);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "印章库-修改", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = SigSignet.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editSigSignet(@RequestBody SigSignet sigSignet) {
        sigSignetService.updateByPk(sigSignet);
        return Result.ok("edit-ok", "修改成功！").setData(sigSignet);
    }

    /**
     * 根据印章类型查询印章
     *
     * @param sigSignet
     * @return
     */
    @RequestMapping(value = "/listSigSignetBySignetType", method = RequestMethod.GET)
    public Result listSigSignetBySignetType(@RequestParam Map<String, Object> sigSignet) {
        RequestUtils.transformArray(sigSignet, "ids");
        QueryWrapper<SigSignet> qw = QueryTools.initQueryWrapper(SigSignet.class, sigSignet);
        IPage page = QueryTools.initPage(sigSignet);
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        sigSignet.put("branchId", branchId);
//        List<Map<String, Object>> sigSignetList = sigSignetService.listSigSignetBySignetType(sigSignet);    //列出SigSignet列表
        List<Map<String, Object>> sigSignetList = sigSignetService.selectListMapByWhere(page, qw, sigSignet);
        return Result.ok().setData(sigSignetList);
    }


    @ApiOperation(value = "印章库-批量修改某些字段", notes = "")
    @ApiEntityParams(value = SigSignet.class, props = {}, remark = "印章库", paramType = "body")
    @ApiResponses({
            @ApiResponse(code = 200, response = SigSignet.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        sigSignetService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "印章库-批量删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
    })
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelSigSignet(@RequestBody List<SigSignet> sigSignets) {
        User user = LoginUtils.getCurrentUserInfo();
        if (sigSignets.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<SigSignet> datasDb = sigSignetService.listByIds(sigSignets.stream().map(i -> i.getId()).collect(Collectors.toList()));
        List<SigSignet> can = new ArrayList<>();
        List<SigSignet> no = new ArrayList<>();
        for (SigSignet data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            sigSignetService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }
        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "印章库-根据主键查询一条数据", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = SigSignet.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(SigSignet sigSignet) {
        SigSignet data = (SigSignet) sigSignetService.getById(sigSignet);
        return Result.ok().setData(data);
    }

}
