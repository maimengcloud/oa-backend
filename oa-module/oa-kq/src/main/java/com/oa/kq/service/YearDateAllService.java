package com.oa.kq.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.kq.entity.YearDateAll;
import com.oa.kq.mapper.YearDateAllMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class YearDateAllService extends BaseService<YearDateAllMapper, YearDateAll> {
    static Logger logger = LoggerFactory.getLogger(YearDateAllService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    /**
     * 请在此类添加自定义函数
     */
    public YearDateAll updateKqYearDateAll(YearDateAll kqYearDateAll) {
        String id = kqYearDateAll.getId();
        //如果为空就新增，否则就按主键修改
        if (id == null) {
            kqYearDateAll.setId(this.createKey("id"));
            this.insert(kqYearDateAll);
        } else {
            this.updateByPk(kqYearDateAll);
        }
        return kqYearDateAll;
    }

}

