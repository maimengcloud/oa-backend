package com.oa.car.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.car.entity.CarRequireExpenseDetail;
import com.oa.car.service.CarRequireExpenseDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@RestController
@RequestMapping(value = "/*/oa/car/carRequireExpenseDetail")
@Api(tags = {"car_require_expense_detail-操作接口"})
public class CarRequireExpenseDetailController {

    static Logger logger = LoggerFactory.getLogger(CarRequireExpenseDetailController.class);

    @Autowired
    private CarRequireExpenseDetailService carRequireExpenseDetailService;

    @ApiOperation(value = "car_require_expense_detail-查询列表", notes = " ")
    @ApiEntityParams(CarRequireExpenseDetail.class)
    @ApiResponses({
            @ApiResponse(code = 200, response = CarRequireExpenseDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
    })
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCarRequireExpenseDetail(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CarRequireExpenseDetail> qw = QueryTools.initQueryWrapper(CarRequireExpenseDetail.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = carRequireExpenseDetailService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "car_require_expense_detail-新增", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = CarRequireExpenseDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCarRequireExpenseDetail(@RequestBody CarRequireExpenseDetail carRequireExpenseDetail) {
        carRequireExpenseDetailService.save(carRequireExpenseDetail);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "car_require_expense_detail-删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
    })
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCarRequireExpenseDetail(@RequestBody CarRequireExpenseDetail carRequireExpenseDetail) {
        carRequireExpenseDetailService.removeById(carRequireExpenseDetail);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "car_require_expense_detail-修改", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = CarRequireExpenseDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCarRequireExpenseDetail(@RequestBody CarRequireExpenseDetail carRequireExpenseDetail) {
        carRequireExpenseDetailService.updateById(carRequireExpenseDetail);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "car_require_expense_detail-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CarRequireExpenseDetail.class, props = {}, remark = "car_require_expense_detail", paramType = "body")
    @ApiResponses({
            @ApiResponse(code = 200, response = CarRequireExpenseDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        carRequireExpenseDetailService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "car_require_expense_detail-批量删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
    })
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCarRequireExpenseDetail(@RequestBody List<CarRequireExpenseDetail> carRequireExpenseDetails) {
        User user = LoginUtils.getCurrentUserInfo();
        if (carRequireExpenseDetails.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CarRequireExpenseDetail> datasDb = carRequireExpenseDetailService.listByIds(carRequireExpenseDetails.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CarRequireExpenseDetail> can = new ArrayList<>();
        List<CarRequireExpenseDetail> no = new ArrayList<>();
        for (CarRequireExpenseDetail data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            carRequireExpenseDetailService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "car_require_expense_detail-根据主键查询一条数据", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = CarRequireExpenseDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CarRequireExpenseDetail carRequireExpenseDetail) {
        CarRequireExpenseDetail data = (CarRequireExpenseDetail) carRequireExpenseDetailService.getById(carRequireExpenseDetail);
        return Result.ok().setData(data);
    }

}