package com.oa.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月7日
 */
@Data
@TableName("car_driver")
@ApiModel(description="司机表")
public class CarDriver  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="司机编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String driverId;

	
	@ApiModelProperty(notes="汽车司机",allowEmptyValue=true,example="",allowableValues="")
	String driverUserid;

	
	@ApiModelProperty(notes="司机姓名",allowEmptyValue=true,example="",allowableValues="")
	String driverUsername;

	
	@ApiModelProperty(notes="司机电话",allowEmptyValue=true,example="",allowableValues="")
	String driverPhoneno;

	
	@ApiModelProperty(notes="微信号",allowEmptyValue=true,example="",allowableValues="")
	String driverWeixin;

	
	@ApiModelProperty(notes="邮件",allowEmptyValue=true,example="",allowableValues="")
	String driverEmel;

	
	@ApiModelProperty(notes="驾驶证号码",allowEmptyValue=true,example="",allowableValues="")
	String driverCardNum;

	
	@ApiModelProperty(notes="驾驶证照片",allowEmptyValue=true,example="",allowableValues="")
	String driverCardImageUrl;

	
	@ApiModelProperty(notes="驾驶证状态A",allowEmptyValue=true,example="",allowableValues="")
	String driverStatus;

	
	@ApiModelProperty(notes="驾驶证开始领证日期",allowEmptyValue=true,example="",allowableValues="")
	Date driverCardStartTime;

	
	@ApiModelProperty(notes="司机年龄",allowEmptyValue=true,example="",allowableValues="")
	Integer driverAge;

	
	@ApiModelProperty(notes="司机性别",allowEmptyValue=true,example="",allowableValues="")
	String driverSex;

	
	@ApiModelProperty(notes="司机实际驾龄",allowEmptyValue=true,example="",allowableValues="")
	Integer driverActYear;

	
	@ApiModelProperty(notes="增加时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="驾驶证类型",allowEmptyValue=true,example="",allowableValues="")
	String driverCardType;

	
	@ApiModelProperty(notes="司机归属机构",allowEmptyValue=true,example="",allowableValues="")
	String driverBranchId;

	/**
	 *司机编号
	 **/
	public CarDriver(String driverId) {
		this.driverId = driverId;
	}
    
    /**
     * 司机表
     **/
	public CarDriver() {
	}

}