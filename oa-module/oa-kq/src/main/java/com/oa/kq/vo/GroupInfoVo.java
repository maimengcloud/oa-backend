package com.oa.kq.vo;

import com.oa.kq.entity.*;

import java.util.List;
public class GroupInfoVo {
	private Group group;
	
	private List<GroupDept> gDeptList;
	
	private List<GroupNoAttendance> gNoAttList;
	
	private List<GroupOtherAttendance> gOtherAttList;
	
	private List<GroupResponsibility> gResponList;
	
	private GroupSchedule gSchedule;
	
	private GroupScheduleCycle gScheduleCycle;
	
	private List<GroupPosition> gPositionList;
	
	private List<GroupDept> outOfGroupDeptList;//原本是其他考勤组 的。现在放在本考勤组
	
	private List<GroupOtherAttendance> outOfGroupAttList;//原本为其他考勤组的考勤人员。
	
	private List<GroupWifi> gWifiList;//原本为其他考勤组的考勤人员。
	

	public GroupInfoVo() {
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public List<GroupDept> getgDeptList() {
		return gDeptList;
	}

	public void setgDeptList(List<GroupDept> gDeptList) {
		this.gDeptList = gDeptList;
	}

	public List<GroupNoAttendance> getgNoAttList() {
		return gNoAttList;
	}

	public void setgNoAttList(List<GroupNoAttendance> gNoAttList) {
		this.gNoAttList = gNoAttList;
	}

	public List<GroupOtherAttendance> getgOtherAttList() {
		return gOtherAttList;
	}

	public void setgOtherAttList(List<GroupOtherAttendance> gOtherAttList) {
		this.gOtherAttList = gOtherAttList;
	}

	public List<GroupResponsibility> getgResponList() {
		return gResponList;
	}

	public void setgResponList(List<GroupResponsibility> gResponList) {
		this.gResponList = gResponList;
	}

	public GroupSchedule getgSchedule() {
		return gSchedule;
	}

	public void setgSchedule(GroupSchedule gSchedule) {
		this.gSchedule = gSchedule;
	}
	public GroupScheduleCycle getgScheduleCycle() {
		return gScheduleCycle;
	}

	public void setgScheduleCycle(GroupScheduleCycle gScheduleCycle) {
		this.gScheduleCycle = gScheduleCycle;
	}

	public List<GroupPosition> getgPositionList() {
		return gPositionList;
	}

	public void setgPositionList(List<GroupPosition> gPositionList) {
		this.gPositionList = gPositionList;
	}

	public List<GroupDept> getOutOfGroupDeptList() {
		return outOfGroupDeptList;
	}

	public void setOutOfGroupDeptList(List<GroupDept> outOfGroupDeptList) {
		this.outOfGroupDeptList = outOfGroupDeptList;
	}

	public List<GroupOtherAttendance> getOutOfGroupAttList() {
		return outOfGroupAttList;
	}

	public void setOutOfGroupAttList(List<GroupOtherAttendance> outOfGroupAttList) {
		this.outOfGroupAttList = outOfGroupAttList;
	}

	public List<GroupWifi> getgWifiList() {
		return gWifiList;
	}

	public void setgWifiList(List<GroupWifi> gWifiList) {
		this.gWifiList = gWifiList;
	}

	
	
}
