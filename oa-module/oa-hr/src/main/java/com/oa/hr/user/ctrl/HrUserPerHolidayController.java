package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserPerHoliday;
import com.oa.hr.user.service.HrUserPerHolidayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value="/*/oa/hr/user/hrUserPerHoliday")
@Api(tags = {"员工个人假期表-操作接口"})
public class HrUserPerHolidayController {

    static Logger logger = LoggerFactory.getLogger(HrUserPerHolidayController.class);

    @Autowired
    private HrUserPerHolidayService hrUserPerHolidayService;

    @ApiOperation(value = "员工个人假期表-查询列表", notes = " ")
    @ApiEntityParams(HrUserPerHoliday.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrUserPerHoliday.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrUserPerHoliday(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("branchId", user.getBranchId());

        QueryWrapper<HrUserPerHoliday> qw = QueryTools.initQueryWrapper(HrUserPerHoliday.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = hrUserPerHolidayService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "员工个人假期表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserPerHoliday.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrUserPerHoliday(@RequestBody HrUserPerHoliday hrUserPerHoliday) {
        
        
            User user= LoginUtils.getCurrentUserInfo();
            hrUserPerHoliday.setBranchId(user.getBranchId());
            if(StringUtils.isEmpty(hrUserPerHoliday.getId())) {
                hrUserPerHoliday.setId(hrUserPerHolidayService.createKey("id"));
            }else{
                HrUserPerHoliday hrUserPerHolidayQuery = new  HrUserPerHoliday(hrUserPerHoliday.getId());
                if(hrUserPerHolidayService.countByWhere(hrUserPerHolidayQuery)>0){
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            hrUserPerHolidayService.insert(hrUserPerHoliday);
        
        return Result.ok("add-ok", "添加成功！").setData(hrUserPerHoliday);
    }

    @ApiOperation(value = "员工个人假期表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrUserPerHoliday(@RequestBody HrUserPerHoliday hrUserPerHoliday) {
        
        
            hrUserPerHolidayService.deleteByPk(hrUserPerHoliday);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "员工个人假期表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserPerHoliday.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrUserPerHoliday(@RequestBody HrUserPerHoliday hrUserPerHoliday) {
        
        
            hrUserPerHolidayService.updateByPk(hrUserPerHoliday);
        
        return Result.ok("edit-ok", "修改成功！").setData(hrUserPerHoliday);
    }

    @ApiOperation(value = "员工个人假期表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrUserPerHoliday.class, props = {}, remark = "员工个人假期表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserPerHoliday.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        hrUserPerHolidayService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "员工个人假期表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrUserPerHoliday(@RequestBody List<HrUserPerHoliday> hrUserPerHolidays) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrUserPerHolidays.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrUserPerHoliday> datasDb = hrUserPerHolidayService.listByIds(hrUserPerHolidays.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrUserPerHoliday> can = new ArrayList<>();
        List<HrUserPerHoliday> no = new ArrayList<>();
        for (HrUserPerHoliday data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            hrUserPerHolidayService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "员工个人假期表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserPerHoliday.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrUserPerHoliday hrUserPerHoliday) {
        HrUserPerHoliday data = (HrUserPerHoliday) hrUserPerHolidayService.getById(hrUserPerHoliday);
        return Result.ok().setData(data);
    }

}
