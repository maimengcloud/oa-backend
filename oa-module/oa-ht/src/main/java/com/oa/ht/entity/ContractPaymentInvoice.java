package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_payment_invoice")
@ApiModel(description="合同收款与开票关系")
public class ContractPaymentInvoice  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
    @TableIds
	
    @ApiModelProperty(notes="付款表主键,主键",allowEmptyValue=true,example="",allowableValues="")
    String paymentId;
    @TableIds
	
    @ApiModelProperty(notes="收票表主键,主键",allowEmptyValue=true,example="",allowableValues="")
    String invoiceId;

	
	@ApiModelProperty(notes="合同编号",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	/**
	 *付款表主键,收票表主键
	 **/
	public ContractPaymentInvoice(String paymentId,String invoiceId) {
		this.paymentId = paymentId;
		this.invoiceId = invoiceId;
	}
    
    /**
     * 合同收款与开票关系
     **/
	public ContractPaymentInvoice() {
	}

}