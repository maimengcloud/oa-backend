package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_business")
@ApiModel(description="crm_customer_business")
public class CustomerBusiness  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="商机id,主键",allowEmptyValue=true,example="",allowableValues="")
	String businessId;

	
	@ApiModelProperty(notes="商机名称",allowEmptyValue=true,example="",allowableValues="")
	String businessName;

	
	@ApiModelProperty(notes="商机类型",allowEmptyValue=true,example="",allowableValues="")
	String businessType;

	
	@ApiModelProperty(notes="销售预期",allowEmptyValue=true,example="",allowableValues="")
	Date expectDate;

	
	@ApiModelProperty(notes="所属客户id",allowEmptyValue=true,example="",allowableValues="")
	String customerId;

	
	@ApiModelProperty(notes="所属客户名称",allowEmptyValue=true,example="",allowableValues="")
	String customerName;

	
	@ApiModelProperty(notes="预期收益",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal expectEarnings;

	
	@ApiModelProperty(notes="客户经理id",allowEmptyValue=true,example="",allowableValues="")
	String managerUserId;

	
	@ApiModelProperty(notes="客户经理名称",allowEmptyValue=true,example="",allowableValues="")
	String managerUserName;

	
	@ApiModelProperty(notes="可能性",allowEmptyValue=true,example="",allowableValues="")
	Double possibility;

	
	@ApiModelProperty(notes="商机来源",allowEmptyValue=true,example="",allowableValues="")
	String scheduleId;

	
	@ApiModelProperty(notes="成功关键因素",allowEmptyValue=true,example="",allowableValues="")
	String successKeyCause;

	
	@ApiModelProperty(notes="失败关键因素",allowEmptyValue=true,example="",allowableValues="")
	String faulitKeyCause;

	
	@ApiModelProperty(notes="中介机构",allowEmptyValue=true,example="",allowableValues="")
	String intermediaryOrganId;

	
	@ApiModelProperty(notes="中介机构名称",allowEmptyValue=true,example="",allowableValues="")
	String intermediaryOrganName;

	
	@ApiModelProperty(notes="归档状态",allowEmptyValue=true,example="",allowableValues="")
	String archiveStatus;

	
	@ApiModelProperty(notes="商机状态",allowEmptyValue=true,example="",allowableValues="")
	String businessStatus;

	
	@ApiModelProperty(notes="是否关注0",allowEmptyValue=true,example="",allowableValues="")
	String businessIsAttention;

	
	@ApiModelProperty(notes="创建人Id",allowEmptyValue=true,example="",allowableValues="")
	String businessCreateId;

	
	@ApiModelProperty(notes="创建人姓名",allowEmptyValue=true,example="",allowableValues="")
	String businessCreateName;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date businessCreateTime;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *商机id
	 **/
	public CustomerBusiness(String businessId) {
		this.businessId = businessId;
	}
    
    /**
     * crm_customer_business
     **/
	public CustomerBusiness() {
	}

}