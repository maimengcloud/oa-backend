package com.oa.car.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.car.entity.CarRequire;
import com.oa.car.entity.CarRequireVo;
import com.oa.car.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@RestController
@RequestMapping(value = "/*/oa/car/carRequire")
@Api(tags = {"car_require-操作接口"})
public class CarRequireController {

    static Logger logger = LoggerFactory.getLogger(CarRequireController.class);

    @Autowired
    CarCarDriverTplService carCarDriverTplService;

    @Autowired
    private CarRequireService carRequireService;

    @Autowired
    CarRequireCarDriverService carRequireCarDriverService;

    @Autowired
    CarExpenseDetailTplService carExpenseDetailTplService;

    @Autowired
    CarRequireExpenseDetailService carRequireExpenseDetailService;

    @Autowired
    CarRequireRouteService carRequireRouteService;

    @Autowired
    CarRequireRouteDetailService carRequireRouteDetailService;

    @Autowired
    CarRouteDetailTplService carRouteDetailTplService;

    @ApiOperation(value = "car_require-查询列表", notes = " ")
    @ApiEntityParams(CarRequire.class)
    @ApiResponses({@ApiResponse(code = 200, response = CarRequire.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCarRequire(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CarRequire> qw = QueryTools.initQueryWrapper(CarRequire.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = carRequireService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ApiOperation(value = "获取申请用车的其他表的信息", notes = "getOtherMessageById,条件之间是主键id")
    @ApiResponses({
            @ApiResponse(code = 200, response = CarRequire.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},data:[数据对象1,数据对象2,...]}")
    })
    @RequestMapping(value = "/getMessageById", method = RequestMethod.GET)
    public Result getMessageById(@RequestParam Map<String, Object> carRequire) {
        return Result.ok().setData(carRequireService.getMessageById(carRequire));
    }

    @ApiOperation(value = "car_require-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRequire.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCarRequire(@RequestBody CarRequireVo carRequireVo) {
        
        if (StringUtils.isEmpty(carRequireVo.getReqTime())) {
            carRequireVo.setReqTime(new Date());
        }


        carRequireService.save(carRequireVo);

        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "car_require-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCarRequire(@RequestBody CarRequire carRequire) {
        

        carRequireService.deleteByPkAndInfos(carRequire);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "car_require-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRequire.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCarRequire(@RequestBody CarRequireVo carRequireVo) {
        carRequireService.editCarRequireMessage(carRequireVo);
        return Result.ok("edit-ok", "修改成功！").setData(carRequireVo);
    }

    @ApiOperation(value = "car_require-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CarRequire.class, props = {}, remark = "car_require", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CarRequire.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        carRequireService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "car_require-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCarRequire(@RequestBody List<CarRequire> carRequires) {
        User user = LoginUtils.getCurrentUserInfo();
        if (carRequires.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CarRequire> datasDb = carRequireService.listByIds(carRequires.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CarRequire> can = new ArrayList<>();
        List<CarRequire> no = new ArrayList<>();
        for (CarRequire data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            carRequireService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "car_require-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRequire.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CarRequire carRequire) {
        CarRequire data = (CarRequire) carRequireService.getById(carRequire);
        return Result.ok().setData(data);
    }

}
