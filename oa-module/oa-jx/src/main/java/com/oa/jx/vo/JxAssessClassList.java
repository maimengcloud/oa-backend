package com.oa.jx.vo;

import com.oa.jx.entity.JxAssessClass;

import java.util.List;

public class JxAssessClassList {
    private List<JxAssessClass> jxAssessClassList ;

    public List<JxAssessClass> getJxAssessClassList() {
        return jxAssessClassList;
    }

    public void setJxAssessClassList(List<JxAssessClass> jxAssessClassList) {
        this.jxAssessClassList = jxAssessClassList;
    }
}
