package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_payment")
@ApiModel(description="ht_contract_payment")
public class ContractPayment  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="相关合同",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	
	@ApiModelProperty(notes="付款条件id",allowEmptyValue=true,example="",allowableValues="")
	String conditionId;

	
	@ApiModelProperty(notes="合同相对方id",allowEmptyValue=true,example="",allowableValues="")
	String cpId;

	
	@ApiModelProperty(notes="合同名称",allowEmptyValue=true,example="",allowableValues="")
	String htName;

	
	@ApiModelProperty(notes="合同编号",allowEmptyValue=true,example="",allowableValues="")
	String htSn;

	
	@ApiModelProperty(notes="应付金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal payAmount;

	
	@ApiModelProperty(notes="合同相对方名称",allowEmptyValue=true,example="",allowableValues="")
	String cpName;

	
	@ApiModelProperty(notes="相对方银行账号",allowEmptyValue=true,example="",allowableValues="")
	String cpBankAccount;

	
	@ApiModelProperty(notes="相对方开户行",allowEmptyValue=true,example="",allowableValues="")
	String cpOpenBank;

	
	@ApiModelProperty(notes="相对方负责人",allowEmptyValue=true,example="",allowableValues="")
	String cpFzr;

	
	@ApiModelProperty(notes="相对方负责人电话",allowEmptyValue=true,example="",allowableValues="")
	String cpFzrPhone;

	
	@ApiModelProperty(notes="付款流程",allowEmptyValue=true,example="",allowableValues="")
	String payFlow;

	
	@ApiModelProperty(notes="0.待付款",allowEmptyValue=true,example="",allowableValues="")
	String payStatus;

	
	@ApiModelProperty(notes="实际付款日期",allowEmptyValue=true,example="",allowableValues="")
	Date actualDate;

	
	@ApiModelProperty(notes="创建用户id",allowEmptyValue=true,example="",allowableValues="")
	String createUserId;

	
	@ApiModelProperty(notes="创建用户名称",allowEmptyValue=true,example="",allowableValues="")
	String createUserName;

	
	@ApiModelProperty(notes="创建日期",allowEmptyValue=true,example="",allowableValues="")
	Date createDate;

	
	@ApiModelProperty(notes="机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="当前流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="当前流程状态0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	/**
	 *id
	 **/
	public ContractPayment(String id) {
		this.id = id;
	}
    
    /**
     * ht_contract_payment
     **/
	public ContractPayment() {
	}

}