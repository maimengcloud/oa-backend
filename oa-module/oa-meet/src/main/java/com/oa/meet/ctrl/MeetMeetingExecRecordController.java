package com.oa.meet.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.meet.entity.MeetMeetingExecRecord;
import com.oa.meet.service.MeetMeetingExecRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value="/*/oa/meet/meetMeetingExecRecord")
@Api(tags = {"会议记录表-操作接口"})
public class MeetMeetingExecRecordController {

    static Logger logger = LoggerFactory.getLogger(MeetMeetingExecRecordController.class);

    @Autowired
    private MeetMeetingExecRecordService meetMeetingExecRecordService;

    @ApiOperation(value = "会议记录表-查询列表", notes = " ")
    @ApiEntityParams(MeetMeetingExecRecord.class)
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMeetMeetingExecRecord(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<MeetMeetingExecRecord> qw = QueryTools.initQueryWrapper(MeetMeetingExecRecord.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = meetMeetingExecRecordService.selectListMapByWhere(page, qw, params);


        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "会议记录表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMeetMeetingExecRecord(@RequestBody MeetMeetingExecRecord meetMeetingExecRecord) {

        if (StringUtils.isEmpty(meetMeetingExecRecord.getMeetingId())) {
            return Result.error("会议id必传");
        }
        if (StringUtils.isEmpty(meetMeetingExecRecord.getMeetingId())) {
            return Result.error("会议执行编号必传");
        }
        
            boolean createPk = false;
            if (StringUtils.isEmpty(meetMeetingExecRecord.getId())) {
                createPk = true;
                meetMeetingExecRecord.setId(meetMeetingExecRecordService.createKey("id"));
            }
            if (createPk == false) {
                if (meetMeetingExecRecordService.selectOneObject(meetMeetingExecRecord) != null) {
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            meetMeetingExecRecord.setCreateTime(new Date());
            meetMeetingExecRecordService.insert(meetMeetingExecRecord);
        
        return Result.ok("add-ok", "添加成功！").setData(meetMeetingExecRecord);
    }

    @ApiOperation(value = "会议记录表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMeetMeetingExecRecord(@RequestBody MeetMeetingExecRecord meetMeetingExecRecord) {

        

            meetMeetingExecRecordService.deleteByPk(meetMeetingExecRecord);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "会议记录表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMeetMeetingExecRecord(@RequestBody MeetMeetingExecRecord meetMeetingExecRecord) {

        
            if(StringUtils.isEmpty(meetMeetingExecRecord.getId())){
                return Result.error("id必传");
            }
            meetMeetingExecRecordService.updateByPk(meetMeetingExecRecord);
        
        return Result.ok("edit-ok", "修改成功！").setData(meetMeetingExecRecord);
    }

    @ApiOperation(value = "会议记录表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MeetMeetingExecRecord.class, props = {}, remark = "会议记录表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        meetMeetingExecRecordService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "会议记录表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMeetMeetingExecRecord(@RequestBody List<MeetMeetingExecRecord> meetMeetingExecRecords) {
        User user = LoginUtils.getCurrentUserInfo();
        if (meetMeetingExecRecords.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MeetMeetingExecRecord> datasDb = meetMeetingExecRecordService.listByIds(meetMeetingExecRecords.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MeetMeetingExecRecord> can = new ArrayList<>();
        List<MeetMeetingExecRecord> no = new ArrayList<>();
        for (MeetMeetingExecRecord data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            meetMeetingExecRecordService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "会议记录表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MeetMeetingExecRecord meetMeetingExecRecord) {
        MeetMeetingExecRecord data = (MeetMeetingExecRecord) meetMeetingExecRecordService.getById(meetMeetingExecRecord);
        return Result.ok().setData(data);
    }

}
