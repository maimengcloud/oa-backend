package com.oa.ht.enums;


public enum ContractCardEnum {

    CARD_RELIEVE("1", "卡片状态-解除"),
    CARD_TERMINATE("2", "卡片状态-终止"),
    CARD_PAUSE("3", "卡片状态-暂停"),
    CARD_INACTIVE("4", "卡片状态-未激活"),
    CARD_FULFIL("5", "卡片状态-履行中"),
    CARD_SPLIT("6", "卡片状态-已拆分"),
    CARD_OVER("7", "卡片状态-已完成");

    ContractCardEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;

    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
