package com.oa.erp.ass.enums;

public enum AssetStatusEnum {

    LEAVEUNUSED("0", "闲置"),
    ISUSE("1", "在用"),
    BORRON("2", "借用"),
    MAINTAIN("3", "维修"),
    SCRAP("4", "报废");


    AssetStatusEnum(String status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    private String status;

    private String desc;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
