package com.oa.jx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("jx_assess_content")
@ApiModel(description="考核模板内容表")
public class JxAssessContent  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="考核指标名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="权重%",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal weight;

	
	@ApiModelProperty(notes="指标说明",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="考核模板id",allowEmptyValue=true,example="",allowableValues="")
	String templateId;

	
	@ApiModelProperty(notes="组织id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="序号",allowEmptyValue=true,example="",allowableValues="")
	Integer seqNum;

	
	@ApiModelProperty(notes="分类指标名称",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	/**
	 *id
	 **/
	public JxAssessContent(String id) {
		this.id = id;
	}
    
    /**
     * 考核模板内容表
     **/
	public JxAssessContent() {
	}

}