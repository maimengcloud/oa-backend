package com.oa.jx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("jx_assess_scheme_exec")
@ApiModel(description="考核方案")
public class JxAssessSchemeExec  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="所属考核方案",allowEmptyValue=true,example="",allowableValues="")
	String schemeType;

	
	@ApiModelProperty(notes="计划开始天数",allowEmptyValue=true,example="",allowableValues="")
	String planStartDay;

	
	@ApiModelProperty(notes="计划开始类型（1-之前；2-之后）",allowEmptyValue=true,example="",allowableValues="")
	String planStartType;

	
	@ApiModelProperty(notes="说明",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="计划结束天数",allowEmptyValue=true,example="",allowableValues="")
	String planEndDay;

	
	@ApiModelProperty(notes="计划结束类型（1-之前；2-之后）",allowEmptyValue=true,example="",allowableValues="")
	String planEndType;

	
	@ApiModelProperty(notes="组织机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="是否启动方案（1-已启动；0-初始;",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="是否提醒考核（1-提醒；0-不提醒）",allowEmptyValue=true,example="",allowableValues="")
	String remindStatus;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createdAt;

	
	@ApiModelProperty(notes="更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date updatedAt;

	
	@ApiModelProperty(notes="方案主键",allowEmptyValue=true,example="",allowableValues="")
	String schemeId;

	
	@ApiModelProperty(notes="方案归属开始日期",allowEmptyValue=true,example="",allowableValues="")
	Date startDate;

	
	@ApiModelProperty(notes="方案归属结束日期",allowEmptyValue=true,example="",allowableValues="")
	Date endDate;

	
	@ApiModelProperty(notes="0-未初始化，1已初始化，生成每个模板对应的员工的绩效考核表",allowEmptyValue=true,example="",allowableValues="")
	String initStatus;

	
	@ApiModelProperty(notes="0-初始，1-执行中，2-已结束",allowEmptyValue=true,example="",allowableValues="")
	String execStatus;

	
	@ApiModelProperty(notes="初始化日期",allowEmptyValue=true,example="",allowableValues="")
	Date initDate;

	
	@ApiModelProperty(notes="年份",allowEmptyValue=true,example="",allowableValues="")
	String year;

	
	@ApiModelProperty(notes="月份",allowEmptyValue=true,example="",allowableValues="")
	String month;

	
	@ApiModelProperty(notes="季度：1-第一季度，2-第二季度，3-第三季度，4-第四季度",allowEmptyValue=true,example="",allowableValues="")
	String quarter;

	
	@ApiModelProperty(notes="半年度：1-上半年度，2-下半年度",allowEmptyValue=true,example="",allowableValues="")
	String semiAnnual;

	
	@ApiModelProperty(notes="年度",allowEmptyValue=true,example="",allowableValues="")
	String annual;

	
	@ApiModelProperty(notes="方案名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="执行总人数",allowEmptyValue=true,example="",allowableValues="")
	Integer execUserNum;

	
	@ApiModelProperty(notes="核定后总人数",allowEmptyValue=true,example="",allowableValues="")
	Integer vouchUserNum;

	
	@ApiModelProperty(notes="自评后总人数",allowEmptyValue=true,example="",allowableValues="")
	Integer selfUserNum;

	
	@ApiModelProperty(notes="复评后总人数",allowEmptyValue=true,example="",allowableValues="")
	Integer reUserNum;

	
	@ApiModelProperty(notes="模板编号",allowEmptyValue=true,example="",allowableValues="")
	String templateId;

	
	@ApiModelProperty(notes="模板名称",allowEmptyValue=true,example="",allowableValues="")
	String templateName;

	
	@ApiModelProperty(notes="是否自动初始化0否1是",allowEmptyValue=true,example="",allowableValues="")
	String autoInit;

	
	@ApiModelProperty(notes="员工已确认总人数",allowEmptyValue=true,example="",allowableValues="")
	Integer selfCnum;

	
	@ApiModelProperty(notes="复评人确认总人数",allowEmptyValue=true,example="",allowableValues="")
	Integer reCnum;

	
	@ApiModelProperty(notes="核定人确认总人数",allowEmptyValue=true,example="",allowableValues="")
	Integer vouchCnum;

	
	@ApiModelProperty(notes="模板总分",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalPoints;

	/**
	 *id
	 **/
	public JxAssessSchemeExec(String id) {
		this.id = id;
	}
    
    /**
     * 考核方案
     **/
	public JxAssessSchemeExec() {
	}

}