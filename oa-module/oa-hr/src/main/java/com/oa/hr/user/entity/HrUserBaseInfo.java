package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_user_base_info")
@ApiModel(description="员工其他信息表")
public class HrUserBaseInfo  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="用户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="部门编号",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="部门名称",allowEmptyValue=true,example="",allowableValues="")
	String deptName;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="机构名称",allowEmptyValue=true,example="",allowableValues="")
	String branchName;

	
	@ApiModelProperty(notes="汇报上级用户编号",allowEmptyValue=true,example="",allowableValues="")
	String reportId;

	
	@ApiModelProperty(notes="汇报上级用户名称",allowEmptyValue=true,example="",allowableValues="")
	String reportName;

	
	@ApiModelProperty(notes="职务",allowEmptyValue=true,example="",allowableValues="")
	String duty;

	
	@ApiModelProperty(notes="工作地点",allowEmptyValue=true,example="",allowableValues="")
	String workplace;

	
	@ApiModelProperty(notes="银行账号",allowEmptyValue=true,example="",allowableValues="")
	String account;

	
	@ApiModelProperty(notes="所属岗位",allowEmptyValue=true,example="",allowableValues="")
	String post;

	
	@ApiModelProperty(notes="员工状态---试用，实习，正式，临时，试用延期，解聘，离职，退休",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="人员级别---总裁，公司副总，部门经理，中心总监，普通员工",allowEmptyValue=true,example="",allowableValues="")
	String level;

	
	@ApiModelProperty(notes="工作性质",allowEmptyValue=true,example="",allowableValues="")
	String jobNature;

	
	@ApiModelProperty(notes="入职日期",allowEmptyValue=true,example="",allowableValues="")
	Date entryTime;

	
	@ApiModelProperty(notes="转正日期",allowEmptyValue=true,example="",allowableValues="")
	Date positiveTime;

	
	@ApiModelProperty(notes="用工期限，单位是年",allowEmptyValue=true,example="",allowableValues="")
	String jobDeadline;

	
	@ApiModelProperty(notes="试用期限，单位是月",allowEmptyValue=true,example="",allowableValues="")
	String trialDeadline;

	
	@ApiModelProperty(notes="手机号码",allowEmptyValue=true,example="",allowableValues="")
	String phoneno;

	
	@ApiModelProperty(notes="邮箱地址",allowEmptyValue=true,example="",allowableValues="")
	String email;

	
	@ApiModelProperty(notes="办公地址",allowEmptyValue=true,example="",allowableValues="")
	String officeAddress;

	
	@ApiModelProperty(notes="办公电话",allowEmptyValue=true,example="",allowableValues="")
	String officePhoneno;

	
	@ApiModelProperty(notes="紧急联系人",allowEmptyValue=true,example="",allowableValues="")
	String emergencyContact;

	
	@ApiModelProperty(notes="紧急联系人电话",allowEmptyValue=true,example="",allowableValues="")
	String emeContactPho;

	
	@ApiModelProperty(notes="其他联系方式",allowEmptyValue=true,example="",allowableValues="")
	String ortherPhoneno;

	
	@ApiModelProperty(notes="姓名",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="曾用名",allowEmptyValue=true,example="",allowableValues="")
	String formerName;

	
	@ApiModelProperty(notes="身份证号码",allowEmptyValue=true,example="",allowableValues="")
	String idCardNo;

	
	@ApiModelProperty(notes="性别",allowEmptyValue=true,example="",allowableValues="")
	String sex;

	
	@ApiModelProperty(notes="出生日期",allowEmptyValue=true,example="",allowableValues="")
	Date birthdate;

	
	@ApiModelProperty(notes="年龄",allowEmptyValue=true,example="",allowableValues="")
	String age;

	
	@ApiModelProperty(notes="民族",allowEmptyValue=true,example="",allowableValues="")
	String nation;

	
	@ApiModelProperty(notes="政治面貌",allowEmptyValue=true,example="",allowableValues="")
	String politicsStatus;

	
	@ApiModelProperty(notes="入团日期",allowEmptyValue=true,example="",allowableValues="")
	Date smokedDate;

	
	@ApiModelProperty(notes="入党日期",allowEmptyValue=true,example="",allowableValues="")
	Date partyDate;

	
	@ApiModelProperty(notes="最高学历",allowEmptyValue=true,example="",allowableValues="")
	String eduBack;

	
	@ApiModelProperty(notes="最高学位",allowEmptyValue=true,example="",allowableValues="")
	String degree;

	
	@ApiModelProperty(notes="婚姻情况",allowEmptyValue=true,example="",allowableValues="")
	String maritalStatus;

	
	@ApiModelProperty(notes="健康情况",allowEmptyValue=true,example="",allowableValues="")
	String healthyCondition;

	
	@ApiModelProperty(notes="身高(厘米)",allowEmptyValue=true,example="",allowableValues="")
	String stature;

	
	@ApiModelProperty(notes="体重(千克)",allowEmptyValue=true,example="",allowableValues="")
	String weight;

	
	@ApiModelProperty(notes="现居地",allowEmptyValue=true,example="",allowableValues="")
	String presentAddress;

	
	@ApiModelProperty(notes="籍贯",allowEmptyValue=true,example="",allowableValues="")
	String nativePlace;

	
	@ApiModelProperty(notes="出生地",allowEmptyValue=true,example="",allowableValues="")
	String birthplace;

	
	@ApiModelProperty(notes="户口所在地",allowEmptyValue=true,example="",allowableValues="")
	String regPerResidence;

	
	@ApiModelProperty(notes="户口所在派出所",allowEmptyValue=true,example="",allowableValues="")
	String regPerResidenceLocal;

	
	@ApiModelProperty(notes="到本单位日期",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	
	@ApiModelProperty(notes="连续工龄",allowEmptyValue=true,example="",allowableValues="")
	String workingYears;

	
	@ApiModelProperty(notes="本企业工龄",allowEmptyValue=true,example="",allowableValues="")
	String enterpriseYears;

	
	@ApiModelProperty(notes="附件",allowEmptyValue=true,example="",allowableValues="")
	String accessory;

	
	@ApiModelProperty(notes="参加工作日期",allowEmptyValue=true,example="",allowableValues="")
	Date workTime;

	
	@ApiModelProperty(notes="系统登录账号/工号等",allowEmptyValue=true,example="",allowableValues="")
	String displayUserid;

	
	@ApiModelProperty(notes="岗位名称",allowEmptyValue=true,example="",allowableValues="")
	String postName;

	
	@ApiModelProperty(notes="银行名称",allowEmptyValue=true,example="",allowableValues="")
	String bankOrgName;

	
	@ApiModelProperty(notes="银行机构编号",allowEmptyValue=true,example="",allowableValues="")
	Integer bankOrgId;

	/**
	 *id
	 **/
	public HrUserBaseInfo(String id) {
		this.id = id;
	}
    
    /**
     * 员工其他信息表
     **/
	public HrUserBaseInfo() {
	}

}