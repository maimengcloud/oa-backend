package com.oa.mail.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("mail_email_appendix")
@ApiModel(description="邮件附件表")
public class MailEmailAppendix  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="邮件id",allowEmptyValue=true,example="",allowableValues="")
	String emailId;

	
	@ApiModelProperty(notes="附件名字",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="附件地址",allowEmptyValue=true,example="",allowableValues="")
	String addr;

	
	@ApiModelProperty(notes="附件类型",allowEmptyValue=true,example="",allowableValues="")
	String type;

	
	@ApiModelProperty(notes="备注说明",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	/**
	 *主键
	 **/
	public MailEmailAppendix(String id) {
		this.id = id;
	}
    
    /**
     * 邮件附件表
     **/
	public MailEmailAppendix() {
	}

}