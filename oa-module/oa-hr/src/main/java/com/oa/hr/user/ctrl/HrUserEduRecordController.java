package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserEduRecord;
import com.oa.hr.user.service.HrUserBaseInfoService;
import com.oa.hr.user.service.HrUserEduRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/hr/user/hrUserEduRecord")
@Api(tags = {"员工教育记录表-操作接口"})
public class HrUserEduRecordController {

    static Logger logger = LoggerFactory.getLogger(HrUserEduRecordController.class);

    @Autowired
    private HrUserEduRecordService hrUserEduRecordService;
    @Autowired
    public HrUserBaseInfoService hrUserBaseInfoService;

    @ApiOperation(value = "员工教育记录表-查询列表", notes = " ")
    @ApiEntityParams(HrUserEduRecord.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrUserEduRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrUserEduRecord(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("res.branchId", user.getBranchId());
        QueryWrapper<HrUserEduRecord> qw = QueryTools.initQueryWrapper(HrUserEduRecord.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = hrUserEduRecordService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "员工教育记录表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserEduRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrUserEduRecord(@RequestBody HrUserEduRecord hrUserEduRecord) {
        

        User user = LoginUtils.getCurrentUserInfo();
        hrUserEduRecord.setBranchId(user.getBranchId());
        if (StringUtils.isEmpty(hrUserEduRecord.getId())) {
            hrUserEduRecord.setId(hrUserEduRecordService.createKey("id"));
        } else {
            HrUserEduRecord hrUserEduRecordQuery = new HrUserEduRecord(hrUserEduRecord.getId());
            if (hrUserEduRecordService.countByWhere(hrUserEduRecordQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        hrUserEduRecordService.insert(hrUserEduRecord);

        return Result.ok("add-ok", "添加成功！").setData(hrUserEduRecord);
    }

    @ApiOperation(value = "员工教育记录表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrUserEduRecord(@RequestBody HrUserEduRecord hrUserEduRecord) {
        

        hrUserEduRecordService.deleteByPk(hrUserEduRecord);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "员工教育记录表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserEduRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrUserEduRecord(@RequestBody HrUserEduRecord hrUserEduRecord) {
        

        hrUserEduRecordService.updateByPk(hrUserEduRecord);

        return Result.ok("edit-ok", "修改成功！").setData(hrUserEduRecord);
    }

    //上传的excel文件
    @PostMapping("addEduRecordByExcel")
    public Result addWorkedByExcel(MultipartFile file) {
        


        try {
            hrUserEduRecordService.saveSubject(file, hrUserBaseInfoService, hrUserEduRecordService);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return Result.ok("", "成功导入数据");
    }

    @ApiOperation(value = "员工教育记录表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrUserEduRecord.class, props = {}, remark = "员工教育记录表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserEduRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        hrUserEduRecordService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "员工教育记录表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrUserEduRecord(@RequestBody List<HrUserEduRecord> hrUserEduRecords) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrUserEduRecords.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrUserEduRecord> datasDb = hrUserEduRecordService.listByIds(hrUserEduRecords.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrUserEduRecord> can = new ArrayList<>();
        List<HrUserEduRecord> no = new ArrayList<>();
        for (HrUserEduRecord data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            hrUserEduRecordService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "员工教育记录表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserEduRecord.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrUserEduRecord hrUserEduRecord) {
        HrUserEduRecord data = (HrUserEduRecord) hrUserEduRecordService.getById(hrUserEduRecord);
        return Result.ok().setData(data);
    }

}
