package com.oa.matter.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.matter.entity.MatterCompany;
import com.oa.matter.mapper.MatterCompanyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class MatterCompanyService extends BaseService<MatterCompanyMapper,MatterCompany> {
	static Logger logger =LoggerFactory.getLogger(MatterCompanyService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}
	/** 请在此类添加自定义函数 */
	@Transactional
	public void addOrDeleteMatter(Map<String,Object> map) {
		String companyId = (String) map.get("companyId");
		List<MatterCompany> matterCompanies = (List<MatterCompany>) map.get("matterCompanyList");
		MatterCompany matterCompany=new MatterCompany();
		matterCompany.setCompanyId(companyId);
		this.deleteByWhere(matterCompany);
//        this.batchInsert("com.oa.matter.entity.MatterCompany.insert",matterCompanies);
        this.batchInsert(matterCompanies);
	}
}

