package com.oa.rc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("rc_schedule")
@ApiModel(description="rc_schedule")
public class RcSchedule  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="日程id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="日程创建人id",allowEmptyValue=true,example="",allowableValues="")
	String createUserid;

	
	@ApiModelProperty(notes="日程创建人",allowEmptyValue=true,example="",allowableValues="")
	String createUsername;

	
	@ApiModelProperty(notes="日程标题",allowEmptyValue=true,example="",allowableValues="")
	String title;

	
	@ApiModelProperty(notes="日程内容",allowEmptyValue=true,example="",allowableValues="")
	String description;

	
	@ApiModelProperty(notes="日程类型",allowEmptyValue=true,example="",allowableValues="")
	String rcType;

	
	@ApiModelProperty(notes="日程开始时间hh:mm",allowEmptyValue=true,example="",allowableValues="")
	String startTime;

	
	@ApiModelProperty(notes="日程结束时间hh:mm",allowEmptyValue=true,example="",allowableValues="")
	String endTime;

	
	@ApiModelProperty(notes="紧急程度",allowEmptyValue=true,example="",allowableValues="")
	String urgent;

	
	@ApiModelProperty(notes="提醒类型",allowEmptyValue=true,example="",allowableValues="")
	String remindType;

	
	@ApiModelProperty(notes="立即提醒",allowEmptyValue=true,example="",allowableValues="")
	String remindNowtime;

	
	@ApiModelProperty(notes="开始前提醒时间",allowEmptyValue=true,example="",allowableValues="")
	String remindBeforestart;

	
	@ApiModelProperty(notes="结束前提醒时间",allowEmptyValue=true,example="",allowableValues="")
	String remindBeforeend;

	
	@ApiModelProperty(notes="日程状态，0未删除，2已删除",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="是否循环0不循环、1循环,每次循环增加一条记录到exec表，本表记录不变化",allowEmptyValue=true,example="",allowableValues="")
	String circleable;

	
	@ApiModelProperty(notes="循环星期0、1、2、3、4、5、6分别代表周日到周六任意组合，逗号分割",allowEmptyValue=true,example="",allowableValues="")
	String circleDaysOfWeek;

	
	@ApiModelProperty(notes="循环类型week、month、date三种分别代表按星期、按月、指定日期进行循环",allowEmptyValue=true,example="",allowableValues="")
	String circleType;

	
	@ApiModelProperty(notes="循环日期1、2、3、4、5、6、...31分别代表每个月的1-31号任意组合，逗号分割",allowEmptyValue=true,example="",allowableValues="")
	String circleDaysOfMonth;

	
	@ApiModelProperty(notes="指定日期，yyy-MM-dd类型，逗号分割多个",allowEmptyValue=true,example="",allowableValues="")
	String circcleDates;

	
	@ApiModelProperty(notes="循环执行日期",allowEmptyValue=true,example="",allowableValues="")
	Date execCalcDate;

	
	@ApiModelProperty(notes="创建日期",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="下次更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date nextUpdateTime;

	/**
	 *日程id
	 **/
	public RcSchedule(String id) {
		this.id = id;
	}
    
    /**
     * rc_schedule
     **/
	public RcSchedule() {
	}

}