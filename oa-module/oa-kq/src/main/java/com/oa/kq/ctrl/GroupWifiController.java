package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.GroupWifi;
import com.oa.kq.service.GroupWifiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/kq/groupWifi")
@Api(tags = {"考勤组与wifi关系表-操作接口"})
public class GroupWifiController {

    static Logger logger = LoggerFactory.getLogger(GroupWifiController.class);

    @Autowired
    private GroupWifiService groupWifiService;

    @ApiOperation(value = "考勤组与wifi关系表-查询列表", notes = " ")
    @ApiEntityParams(GroupWifi.class)
    @ApiResponses({@ApiResponse(code = 200, response = GroupWifi.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listGroupWifi(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<GroupWifi> qw = QueryTools.initQueryWrapper(GroupWifi.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = groupWifiService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "考勤组与wifi关系表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupWifi.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addGroupWifi(@RequestBody GroupWifi groupWifi) {
        groupWifiService.save(groupWifi);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "考勤组与wifi关系表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delGroupWifi(@RequestBody GroupWifi groupWifi) {
        groupWifiService.removeById(groupWifi);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "考勤组与wifi关系表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupWifi.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editGroupWifi(@RequestBody GroupWifi groupWifi) {
        groupWifiService.updateById(groupWifi);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "考勤组与wifi关系表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = GroupWifi.class, props = {}, remark = "考勤组与wifi关系表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = GroupWifi.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        groupWifiService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "考勤组与wifi关系表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelGroupWifi(@RequestBody List<GroupWifi> groupWifis) {
        User user = LoginUtils.getCurrentUserInfo();
        if (groupWifis.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<GroupWifi> datasDb = groupWifiService.listByIds(groupWifis.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<GroupWifi> can = new ArrayList<>();
        List<GroupWifi> no = new ArrayList<>();
        for (GroupWifi data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            groupWifiService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "考勤组与wifi关系表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupWifi.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(GroupWifi groupWifi) {
        GroupWifi data = (GroupWifi) groupWifiService.getById(groupWifi);
        return Result.ok().setData(data);
    }

}
