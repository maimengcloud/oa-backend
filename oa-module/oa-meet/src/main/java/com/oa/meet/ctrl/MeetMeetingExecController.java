package com.oa.meet.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.meet.entity.MeetMeetingExec;
import com.oa.meet.entity.MeetMeetingExecAppendix;
import com.oa.meet.entity.MeetMeetingExecAttender;
import com.oa.meet.entity.MeetMeetingExecRecord;
import com.oa.meet.service.MeetMeetingExecAppendixService;
import com.oa.meet.service.MeetMeetingExecAttenderService;
import com.oa.meet.service.MeetMeetingExecRecordService;
import com.oa.meet.service.MeetMeetingExecService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value="/*/oa/meet/meetMeetingExec")
@Api(tags = {"会议执行明细表-操作接口"})
public class MeetMeetingExecController {

    static Logger logger = LoggerFactory.getLogger(MeetMeetingExecController.class);

    @Autowired
    private MeetMeetingExecService meetMeetingExecService;
    @Autowired
    MeetMeetingExecAppendixService meetMeetingExecAppendixService;
    @Autowired
    MeetMeetingExecAttenderService meetMeetingExecAttenderService;
    @Autowired
    MeetMeetingExecRecordService meetMeetingExecRecordService;

    @ApiOperation(value = "会议执行明细表-查询列表", notes = " ")
    @ApiEntityParams(MeetMeetingExec.class)
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMeetMeetingExec(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<MeetMeetingExec> qw = QueryTools.initQueryWrapper(MeetMeetingExec.class, params);
        IPage page = QueryTools.initPage(params);

        List<Map<String, Object>> datas = meetMeetingExecService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ApiOperation(value = "查询会议执行明细表信息列表", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/listExec", method = RequestMethod.GET)
    public Result listExecMeetMeetingExec(@RequestParam Map<String, Object> meetMeetingExec) {
        RequestUtils.transformArray(meetMeetingExec, "ids");
        List<Map<String, Object>> meetMeetingExecList = meetMeetingExecService.selectMeetMeetingExecListMapByWhere(meetMeetingExec);    //列出MeetMeetingExec列表

        return Result.ok().setData(meetMeetingExecList);
    }


    @ApiOperation(value = "会议执行明细表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMeetMeetingExec(@RequestBody MeetMeetingExec meetMeetingExec) {
        meetMeetingExecService.save(meetMeetingExec);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "会议执行明细表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMeetMeetingExec(@RequestBody MeetMeetingExec meetMeetingExec) {

        
            MeetMeetingExec meetMeetingExecDB = new MeetMeetingExec();
            meetMeetingExecDB.setId(meetMeetingExec.getId());
            meetMeetingExecService.deleteByPk(meetMeetingExec);
            MeetMeetingExecAppendix meetMeetingExecAppendix = new MeetMeetingExecAppendix();
            meetMeetingExecAppendix.setExecId(meetMeetingExec.getId());
//            meetMeetingExecAppendixService.delete("deleteExecByWhere",meetMeetingExecAppendix);
            meetMeetingExecAppendixService.deleteByWhere(meetMeetingExecAppendix);
            MeetMeetingExecAttender meetMeetingExecAttender = new MeetMeetingExecAttender();
            meetMeetingExecAttender.setExecId(meetMeetingExec.getId());
//            meetMeetingExecAttenderService.delete("deleteExecByWhere",meetMeetingExecAttender);
            meetMeetingExecAttenderService.deleteByWhere(meetMeetingExecAttender);
            MeetMeetingExecRecord meetMeetingExecRecord = new MeetMeetingExecRecord();
            meetMeetingExecRecord.setExecId(meetMeetingExec.getId());
//            meetMeetingExecRecordService.delete("deleteExecByWhere",meetMeetingExecRecord);
            meetMeetingExecRecordService.deleteByWhere(meetMeetingExecRecord);

        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "会议执行明细表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMeetMeetingExec(@RequestBody MeetMeetingExec meetMeetingExec) {
        meetMeetingExecService.updateById(meetMeetingExec);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "会议执行明细表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MeetMeetingExec.class, props = {}, remark = "会议执行明细表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        meetMeetingExecService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "会议执行明细表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMeetMeetingExec(@RequestBody List<MeetMeetingExec> meetMeetingExecs) {
        User user = LoginUtils.getCurrentUserInfo();
        if (meetMeetingExecs.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MeetMeetingExec> datasDb = meetMeetingExecService.listByIds(meetMeetingExecs.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MeetMeetingExec> can = new ArrayList<>();
        List<MeetMeetingExec> no = new ArrayList<>();
        for (MeetMeetingExec data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            meetMeetingExecService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "会议执行明细表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MeetMeetingExec meetMeetingExec) {
        MeetMeetingExec data = (MeetMeetingExec) meetMeetingExecService.getById(meetMeetingExec);
        return Result.ok().setData(data);
    }

}
