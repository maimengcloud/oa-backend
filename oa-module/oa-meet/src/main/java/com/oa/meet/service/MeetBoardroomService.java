package com.oa.meet.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.service.BaseService;
import com.oa.meet.entity.MeetBoardroom;
import com.oa.meet.entity.MeetBoardroomAppendix;
import com.oa.meet.entity.MeetBoardroomDevice;
import com.oa.meet.entity.MeetBoardroomVo;
import com.oa.meet.mapper.MeetBoardroomMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class MeetBoardroomService extends BaseService<MeetBoardroomMapper, MeetBoardroom> {
    static Logger logger = LoggerFactory.getLogger(MeetBoardroomService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    Tips tips = new Tips("成功新增一条数据");

    @Autowired
    MeetBoardroomAppendixService meetBoardroomAppendixService;

    @Autowired
    MeetBoardroomDeviceService meetBoardroomDeviceService;

    public Integer save(MeetBoardroomVo meetBoardroomVo) {

        if (StringUtils.isEmpty(meetBoardroomVo.getId())) {
            meetBoardroomVo.setId(this.createKey("id"));
        } else {
            MeetBoardroom meetBoardroomQuery = new MeetBoardroom(meetBoardroomVo.getId());
            if (this.countByWhere(meetBoardroomQuery) > 0) {
                tips.setErrMsg("编号重复，请修改编号再提交");
                throw new BizException(tips);
            }
        }
        MeetBoardroom meetBoardroom = new MeetBoardroom();
        BeanUtils.copyProperties(meetBoardroomVo, meetBoardroom);
        Integer i = this.insert(meetBoardroom);

        //保存设备
        if (meetBoardroomVo.getDevices() != null && meetBoardroomVo.getDevices().size() > 0) {
            meetBoardroomVo.getDevices().forEach((device) -> {
                device.setMeetBoardroomId(meetBoardroomVo.getId());

                if (StringUtils.isEmpty(device.getId())) {
                    device.setId(meetBoardroomDeviceService.createKey("id"));
                } else {
                    MeetBoardroomDevice meetBoardroomDeviceQuery = new MeetBoardroomDevice(device.getId());
                    if (meetBoardroomDeviceService.countByWhere(meetBoardroomDeviceQuery) > 0) {
                        tips.setErrMsg("会议室设备编号重复，请修改编号再提交");
                        throw new BizException(tips);
                    }
                }
                meetBoardroomDeviceService.insert(device);
            });
        }
        //保存附件
        if (meetBoardroomVo.getAppendixList() != null && meetBoardroomVo.getAppendixList().size() > 0) {
            meetBoardroomVo.getAppendixList().forEach((appendix) -> {
                appendix.setMeetBoardroomId(meetBoardroomVo.getId());
                meetBoardroomAppendixService.insert(appendix);
            });
        }
        return i;
    }

    public Integer removeMeetBoardroom(MeetBoardroom meetBoardroom) {
        int deleteByPk = this.deleteByPk(meetBoardroom);

        MeetBoardroomDevice meetBoardroomDevice = new MeetBoardroomDevice();
        meetBoardroomDevice.setMeetBoardroomId(meetBoardroom.getId());
        int delete = meetBoardroomDeviceService.deleteByWhere(meetBoardroomDevice);

        MeetBoardroomAppendix meetBoardroomAppendix = new MeetBoardroomAppendix();
        meetBoardroomAppendix.setMeetBoardroomId(meetBoardroom.getId());
        int deleteByWhere = meetBoardroomAppendixService.deleteByWhere(meetBoardroomAppendix);

        return deleteByPk;
    }

    public MeetBoardroomVo getMeetBoardroom(Map<String, Object> meetBoardroom) {

        MeetBoardroomVo meetBoardroomVo = new MeetBoardroomVo();

        MeetBoardroom meetBoardroomQuery = new MeetBoardroom();
        meetBoardroomQuery.setId(meetBoardroom.get("id").toString());
        MeetBoardroom selectOneObject = selectOneObject(meetBoardroomQuery);

        BeanUtils.copyProperties(selectOneObject, meetBoardroomVo);

        MeetBoardroomDevice meetBoardroomDevice = new MeetBoardroomDevice();
        meetBoardroomDevice.setMeetBoardroomId(meetBoardroomQuery.getId());
        List<MeetBoardroomDevice> meetBoardroomDevices = meetBoardroomDeviceService.selectListByWhere(meetBoardroomDevice);
        meetBoardroomVo.setDevices(meetBoardroomDevices);

        MeetBoardroomAppendix meetBoardroomAppendix = new MeetBoardroomAppendix();
        meetBoardroomAppendix.setMeetBoardroomId(meetBoardroomQuery.getId());
        List<MeetBoardroomAppendix> meetBoardroomAppendices = meetBoardroomAppendixService.selectListByWhere(meetBoardroomAppendix);
        meetBoardroomVo.setAppendixList(meetBoardroomAppendices);

        return meetBoardroomVo;
    }

    public Integer editMeetBoardroom(MeetBoardroomVo meetBoardroomVo) {
        MeetBoardroom meetBoardroom = new MeetBoardroom();
        BeanUtils.copyProperties(meetBoardroomVo, meetBoardroom);

        int i = this.updateByPk(meetBoardroom);

        //删除信息
        MeetBoardroomDevice meetBoardroomDevice = new MeetBoardroomDevice();
        meetBoardroomDevice.setMeetBoardroomId(meetBoardroom.getId());
        int delete = meetBoardroomDeviceService.deleteByWhere(meetBoardroomDevice);
        MeetBoardroomAppendix meetBoardroomAppendix = new MeetBoardroomAppendix();
        meetBoardroomAppendix.setMeetBoardroomId(meetBoardroom.getId());
        int deleteByWhere = meetBoardroomAppendixService.deleteByWhere(meetBoardroomAppendix);

        //保存设备
        if (meetBoardroomVo.getDevices() != null && meetBoardroomVo.getDevices().size() > 0) {
            meetBoardroomVo.getDevices().forEach((device) -> {
                device.setMeetBoardroomId(meetBoardroomVo.getId());

                if (StringUtils.isEmpty(device.getId())) {
                    device.setId(meetBoardroomDeviceService.createKey("id"));
                } else {
                    MeetBoardroomDevice meetBoardroomDeviceQuery = new MeetBoardroomDevice(device.getId());
                    if (meetBoardroomDeviceService.countByWhere(meetBoardroomDeviceQuery) > 0) {
                        tips.setErrMsg("会议室设备编号重复，请修改编号再提交");
                        throw new BizException(tips);
                    }
                }
                meetBoardroomDeviceService.insert(device);
            });
        }
        //保存附件
        if (meetBoardroomVo.getAppendixList() != null && meetBoardroomVo.getAppendixList().size() > 0) {
            meetBoardroomVo.getAppendixList().forEach((appendix) -> {
                appendix.setMeetBoardroomId(meetBoardroomVo.getId());
                meetBoardroomAppendixService.insert(appendix);
            });
        }

        return i;

    }
    /** 请在此类添加自定义函数 */

}

