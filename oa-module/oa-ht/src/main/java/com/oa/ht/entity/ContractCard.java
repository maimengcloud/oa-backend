package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_card")
@ApiModel(description="ht_contract_card")
public class ContractCard  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="合同id,主键",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	
	@ApiModelProperty(notes="合同起草id",allowEmptyValue=true,example="",allowableValues="")
	String draftId;

	
	@ApiModelProperty(notes="合同关联相对方id",allowEmptyValue=true,example="",allowableValues="")
	String cpId;

	
	@ApiModelProperty(notes="相对方名称",allowEmptyValue=true,example="",allowableValues="")
	String cpName;

	
	@ApiModelProperty(notes="相对方联系人名称",allowEmptyValue=true,example="",allowableValues="")
	String linkName;

	
	@ApiModelProperty(notes="相对方联系人电话",allowEmptyValue=true,example="",allowableValues="")
	String linkPhone;

	
	@ApiModelProperty(notes="相对方联系人地址",allowEmptyValue=true,example="",allowableValues="")
	String cpAddress;

	
	@ApiModelProperty(notes="相对方开户行",allowEmptyValue=true,example="",allowableValues="")
	String cpOpenBank;

	
	@ApiModelProperty(notes="相对方开户行账号",allowEmptyValue=true,example="",allowableValues="")
	String cpBankAccount;

	
	@ApiModelProperty(notes="合同名称",allowEmptyValue=true,example="",allowableValues="")
	String htName;

	
	@ApiModelProperty(notes="合同编号(合同上的编号)",allowEmptyValue=true,example="",allowableValues="")
	String htSn;

	
	@ApiModelProperty(notes="合同总金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal htGrossAmount;

	
	@ApiModelProperty(notes="合同开票金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal htKaiPiaoAmount;

	
	@ApiModelProperty(notes="合同已收金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal htReceivedAmount;

	
	@ApiModelProperty(notes="合同应收金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal htReceivableAmount;

	
	@ApiModelProperty(notes="合同已付金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal htPayedAmount;

	
	@ApiModelProperty(notes="合同应付金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal htPayableAmount;

	
	@ApiModelProperty(notes="合同附件列表，逗号分割多个",allowEmptyValue=true,example="",allowableValues="")
	String htFiles;

	
	@ApiModelProperty(notes="合同生效日期",allowEmptyValue=true,example="",allowableValues="")
	Date htEffectDate;

	
	@ApiModelProperty(notes="合同负责人id",allowEmptyValue=true,example="",allowableValues="")
	String htFzUserid;

	
	@ApiModelProperty(notes="合同负责人名称",allowEmptyValue=true,example="",allowableValues="")
	String htFzUsername;

	
	@ApiModelProperty(notes="合同类型主要分类",allowEmptyValue=true,example="",allowableValues="")
	String htParentType;

	
	@ApiModelProperty(notes="合同类型具体小类",allowEmptyValue=true,example="",allowableValues="")
	String htType;

	
	@ApiModelProperty(notes="合同签订日期",allowEmptyValue=true,example="",allowableValues="")
	Date htSignDate;

	
	@ApiModelProperty(notes="合同到期日期",allowEmptyValue=true,example="",allowableValues="")
	Date htExpireDate;

	
	@ApiModelProperty(notes="合同状态",allowEmptyValue=true,example="",allowableValues="")
	String htStatus;

	
	@ApiModelProperty(notes="合同需求说明",allowEmptyValue=true,example="",allowableValues="")
	String htDemand;

	
	@ApiModelProperty(notes="合同签约主体",allowEmptyValue=true,example="",allowableValues="")
	String htParty;

	
	@ApiModelProperty(notes="合同签约主体名称",allowEmptyValue=true,example="",allowableValues="")
	String htPartyName;

	
	@ApiModelProperty(notes="合同重要条款",allowEmptyValue=true,example="",allowableValues="")
	String htImportantClause;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="创建用户id",allowEmptyValue=true,example="",allowableValues="")
	String createUserId;

	
	@ApiModelProperty(notes="创建用户名称",allowEmptyValue=true,example="",allowableValues="")
	String createUserName;

	
	@ApiModelProperty(notes="相对方工商注册号-纳税人识别号",allowEmptyValue=true,example="",allowableValues="")
	String cpGszch;

	
	@ApiModelProperty(notes="开票类型1-增值税专用发票，2-增值税普通发票，3-其它",allowEmptyValue=true,example="",allowableValues="")
	String invoiceType;

	
	@ApiModelProperty(notes="税率",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal invoicePoint;

	
	@ApiModelProperty(notes="合同分类名称",allowEmptyValue=true,example="",allowableValues="")
	String htTypeName;

	
	@ApiModelProperty(notes="合同大类名称",allowEmptyValue=true,example="",allowableValues="")
	String htParentTypeName;

	
	@ApiModelProperty(notes="合同分类-暂时不用",allowEmptyValue=true,example="",allowableValues="")
	String mainType;

	
	@ApiModelProperty(notes="合同收票金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal htShouPiaoAmount;

	
	@ApiModelProperty(notes="合同成本总额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal htCbAmount;

	
	@ApiModelProperty(notes="成本中心编号",allowEmptyValue=true,example="",allowableValues="")
	String cbCenterId;

	
	@ApiModelProperty(notes="成本中心名称",allowEmptyValue=true,example="",allowableValues="")
	String cbCenterName;

	
	@ApiModelProperty(notes="当前流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="当前流程状态0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	
	@ApiModelProperty(notes="法定代表人",allowEmptyValue=true,example="",allowableValues="")
	String cpFddb;

	/**
	 *合同id
	 **/
	public ContractCard(String htId) {
		this.htId = htId;
	}
    
    /**
     * ht_contract_card
     **/
	public ContractCard() {
	}

}