package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_group_wifi")
@ApiModel(description="考勤组与wifi关系表")
public class GroupWifi  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="组id",allowEmptyValue=true,example="",allowableValues="")
	String groupId;

	
	@ApiModelProperty(notes="wifi名称",allowEmptyValue=true,example="",allowableValues="")
	String wifiName;

	
	@ApiModelProperty(notes="wifi访问点的mac地址",allowEmptyValue=true,example="",allowableValues="")
	String bssid;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *编号id
	 **/
	public GroupWifi(String id) {
		this.id = id;
	}
    
    /**
     * 考勤组与wifi关系表
     **/
	public GroupWifi() {
	}

}