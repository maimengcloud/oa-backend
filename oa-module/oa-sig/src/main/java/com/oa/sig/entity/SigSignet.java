package com.oa.sig.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("sig_signet")
@ApiModel(description="印章库")
public class SigSignet  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="印章主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="印章标识码",allowEmptyValue=true,example="",allowableValues="")
	String signetSn;

	
	@ApiModelProperty(notes="印章设备",allowEmptyValue=true,example="",allowableValues="")
	String deviceId;

	
	@ApiModelProperty(notes="印章类型公章0法人代表章1法人代表章",allowEmptyValue=true,example="",allowableValues="")
	String signetType;

	
	@ApiModelProperty(notes="印章简称",allowEmptyValue=true,example="",allowableValues="")
	String signetSimpleName;

	
	@ApiModelProperty(notes="印章全称",allowEmptyValue=true,example="",allowableValues="")
	String signetName;

	
	@ApiModelProperty(notes="备案编号",allowEmptyValue=true,example="",allowableValues="")
	String keepRecordId;

	
	@ApiModelProperty(notes="章面信息",allowEmptyValue=true,example="",allowableValues="")
	String signetContext;

	
	@ApiModelProperty(notes="印章状态0-启用1-停用",allowEmptyValue=true,example="",allowableValues="")
	String signetStatus;

	
	@ApiModelProperty(notes="所属主体",allowEmptyValue=true,example="",allowableValues="")
	String signetBranchId;

	
	@ApiModelProperty(notes="主题名称",allowEmptyValue=true,example="",allowableValues="")
	String signetBranchName;

	
	@ApiModelProperty(notes="流程状态",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	
	@ApiModelProperty(notes="流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="印章形态0电子印章1物理印章",allowEmptyValue=true,example="",allowableValues="")
	String signetForm;

	/**
	 *印章主键
	 **/
	public SigSignet(String id) {
		this.id = id;
	}
    
    /**
     * 印章库
     **/
	public SigSignet() {
	}

}