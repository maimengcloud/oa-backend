package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_skill")
@ApiModel(description="技能定义表")
public class HrSkill  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="技能编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="技能名",allowEmptyValue=true,example="",allowableValues="")
	String skillName;

	
	@ApiModelProperty(notes="机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="技能分组",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="是否公共0否1是",allowEmptyValue=true,example="",allowableValues="")
	String isPub;

	
	@ApiModelProperty(notes="技能描述",allowEmptyValue=true,example="",allowableValues="")
	String skillRemarks;

	/**
	 *技能编号
	 **/
	public HrSkill(String id) {
		this.id = id;
	}
    
    /**
     * 技能定义表
     **/
	public HrSkill() {
	}

}