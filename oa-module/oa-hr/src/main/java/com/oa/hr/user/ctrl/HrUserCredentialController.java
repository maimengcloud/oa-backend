package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserCredential;
import com.oa.hr.user.service.HrUserBaseInfoService;
import com.oa.hr.user.service.HrUserCredentialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/hr/user/hrUserCredential")
@Api(tags = {"员工资格证书表-操作接口"})
public class HrUserCredentialController {

    static Logger logger = LoggerFactory.getLogger(HrUserCredentialController.class);

    @Autowired
    private HrUserCredentialService hrUserCredentialService;
    @Autowired
    private HrUserBaseInfoService hrUserBaseInfoService;

    @ApiOperation(value = "员工资格证书表-查询列表", notes = " ")
    @ApiEntityParams(HrUserCredential.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrUserCredential.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrUserCredential(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("res.branchId", user.getBranchId());
        QueryWrapper<HrUserCredential> qw = QueryTools.initQueryWrapper(HrUserCredential.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = hrUserCredentialService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "员工资格证书表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserCredential.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrUserCredential(@RequestBody HrUserCredential hrUserCredential) {
        

        User user = LoginUtils.getCurrentUserInfo();
        hrUserCredential.setBranchId(user.getBranchId());
        if (StringUtils.isEmpty(hrUserCredential.getId())) {
            hrUserCredential.setId(hrUserCredentialService.createKey("id"));
        } else {
            HrUserCredential hrUserCredentialQuery = new HrUserCredential(hrUserCredential.getId());
            if (hrUserCredentialService.countByWhere(hrUserCredentialQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        hrUserCredentialService.insert(hrUserCredential);

        return Result.ok("add-ok", "添加成功！").setData(hrUserCredential);
    }

    @ApiOperation(value = "员工资格证书表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrUserCredential(@RequestBody HrUserCredential hrUserCredential) {
        

        hrUserCredentialService.deleteByPk(hrUserCredential);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "员工资格证书表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserCredential.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrUserCredential(@RequestBody HrUserCredential hrUserCredential) {
        

        hrUserCredentialService.updateByPk(hrUserCredential);

        return Result.ok("edit-ok", "修改成功！").setData(hrUserCredential);
    }


    //上传的excel文件
    @PostMapping("addCredentialByExcel")
    public Result addWorkedByExcel(MultipartFile file) {
        

        try {
            hrUserCredentialService.saveSubject(file, hrUserBaseInfoService, hrUserCredentialService);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return Result.ok("", "成功导入数据");
    }


    @ApiOperation(value = "员工资格证书表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrUserCredential.class, props = {}, remark = "员工资格证书表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserCredential.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        hrUserCredentialService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "员工资格证书表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrUserCredential(@RequestBody List<HrUserCredential> hrUserCredentials) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrUserCredentials.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrUserCredential> datasDb = hrUserCredentialService.listByIds(hrUserCredentials.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrUserCredential> can = new ArrayList<>();
        List<HrUserCredential> no = new ArrayList<>();
        for (HrUserCredential data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            hrUserCredentialService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "员工资格证书表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserCredential.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrUserCredential hrUserCredential) {
        HrUserCredential data = (HrUserCredential) hrUserCredentialService.getById(hrUserCredential);
        return Result.ok().setData(data);
    }

}
