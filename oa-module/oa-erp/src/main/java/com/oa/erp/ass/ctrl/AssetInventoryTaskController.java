package com.oa.erp.ass.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.erp.ass.entity.AssetInventoryTask;
import com.oa.erp.ass.entity.AssetMng;
import com.oa.erp.ass.service.AssetInventoryTaskService;
import com.oa.erp.ass.vo.AssetInventoryTaskVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/erp/ass/assetInventoryTask")
@Api(tags = {"ass_asset_inventory_task-操作接口"})
public class AssetInventoryTaskController {

    static Logger logger = LoggerFactory.getLogger(AssetInventoryTaskController.class);

    @Autowired
    private AssetInventoryTaskService assetInventoryTaskService;

    @ApiOperation(value = "ass_asset_inventory_task-查询列表", notes = " ")
    @ApiEntityParams(AssetInventoryTask.class)
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listAssetInventoryTask(@ApiIgnore @RequestParam Map<String, Object> params) {
        RequestUtils.transformArray(params, "taskIds");
        User user = LoginUtils.getCurrentUserInfo();
        params.put("reqBranchId", user.getBranchId());
        QueryWrapper<AssetInventoryTask> qw = QueryTools.initQueryWrapper(AssetInventoryTask.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> assetInventoryTaskList = assetInventoryTaskService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(assetInventoryTaskList).setTotal(page.getTotal());
    }


    @ApiOperation(value = "ass_asset_inventory_task-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addAssetInventoryTask(@RequestBody AssetInventoryTaskVo assetInventoryTaskVo) {


        if (StringUtils.isEmpty(assetInventoryTaskVo.getInventoryTask().getTaskId())) {
            assetInventoryTaskVo.getInventoryTask().setTaskId(assetInventoryTaskService.createKey("taskId"));
        } else {
            AssetInventoryTask assetInventoryTaskQuery = new AssetInventoryTask(assetInventoryTaskVo.getInventoryTask().getTaskId());
            if (assetInventoryTaskService.countByWhere(assetInventoryTaskQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
//            assetInventoryTaskService.insertAssetInventoryTask(assetInventoryTaskVo);
        assetInventoryTaskService.insert(assetInventoryTaskVo.getInventoryTask());

        return Result.ok("add-ok", "添加成功！").setData(assetInventoryTaskVo);
    }

    @ApiOperation(value = "获取盘点任务明细", notes = "获取盘点任务明细")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/getTaskDetail", method = RequestMethod.GET)
    public Result getTaskDetail(@RequestParam Map<String, Object> assetInventoryTask) {
        RequestUtils.transformArray(assetInventoryTask, "taskIds");
        QueryWrapper<AssetInventoryTask> qw = QueryTools.initQueryWrapper(AssetInventoryTask.class, assetInventoryTask);
        IPage page = QueryTools.initPage(assetInventoryTask);
        Map<String, Object> assetInventoryTaskList = assetInventoryTaskService.getTaskDetail(page, qw, assetInventoryTask);    //列出AssetInventoryTask列表

        return Result.ok("query-ok", "查询成功").setData(assetInventoryTaskList);
    }

    @ApiOperation(value = "获取我的盘点任务明细", notes = "获取盘点任务明细")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/getMyTaskDetail", method = RequestMethod.GET)
    public Result getMyTaskDetail(@RequestParam Map<String, Object> assetInventoryTask) {
        RequestUtils.transformArray(assetInventoryTask, "taskIds");
        QueryWrapper<AssetInventoryTask> qw = QueryTools.initQueryWrapper(AssetInventoryTask.class, assetInventoryTask);
        IPage page = QueryTools.initPage(assetInventoryTask);
        Map<String, Object> assetInventoryTaskList = assetInventoryTaskService.getMyTaskDetail(page, qw, assetInventoryTask);    //列出AssetInventoryTask列表

        return Result.ok("query-ok", "查询成功").setData(assetInventoryTaskList);
    }

    @ApiOperation(value = "获取能操作盘点任务的用户", notes = "获取盘点任务明细")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/getCanOperTaskUser", method = RequestMethod.GET)
    public Result getCanOperTaskUser(@RequestParam Map<String, Object> assetInventoryTask) {
        Map<String, Object> m = new HashMap<>();
        RequestUtils.transformArray(assetInventoryTask, "taskIds");
        QueryWrapper<AssetInventoryTask> qw = QueryTools.initQueryWrapper(AssetInventoryTask.class, assetInventoryTask);
        IPage page = QueryTools.initPage(assetInventoryTask);
        List<Map<String, Object>> assetInventoryTaskList = assetInventoryTaskService.getCanOperTaskUser(page, qw, assetInventoryTask);    //列出AssetInventoryTask列表
        return Result.ok("query-ok", "查询成功").setData(assetInventoryTaskList);
    }

    @ApiOperation(value = "资产盘点完成", notes = "资产盘点完成")
    @ApiResponses({@ApiResponse(code = 200, response = AssetMng.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/finishInventory", method = RequestMethod.GET)
    public Result finishInventory(@RequestParam Map<String, Object> assetMng) {
        RequestUtils.transformArray(assetMng, "ids");
        assetInventoryTaskService.finishInventory(assetMng);    //列出AssetMng列表
        return Result.ok("query-ok", "操作成功");
    }

    @ApiOperation(value = "开始资产盘点", notes = "开始资产盘点")
    @ApiResponses({@ApiResponse(code = 200, response = AssetMng.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/beginInventory", method = RequestMethod.GET)
    public Result beginInventory(@RequestParam Map<String, Object> assetMng) {
        RequestUtils.transformArray(assetMng, "ids");
        assetInventoryTaskService.beginInventory(assetMng);    //列出AssetMng列表
        return Result.ok("query-ok", "操作成功");
    }

    @ApiOperation(value = "ass_asset_inventory_task-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delAssetInventoryTask(@RequestBody AssetInventoryTask assetInventoryTask) {
//            assetInventoryTaskService.deleteAssetInventoryTask(assetInventoryTask);
        assetInventoryTaskService.deleteByWhere(assetInventoryTask);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "ass_asset_inventory_task-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editAssetInventoryTask(@RequestBody AssetInventoryTaskVo assetInventoryTask) {
//            assetInventoryTaskService.updateAssetInventoryTask(assetInventoryTask);
        assetInventoryTaskService.updateById(assetInventoryTask.getInventoryTask());
        return Result.ok("edit-ok", "修改成功！").setData(assetInventoryTask);
    }

    @ApiOperation(value = "ass_asset_inventory_task-批量修改某些字段", notes = "")
    @ApiEntityParams(value = AssetInventoryTask.class, props = {}, remark = "ass_asset_inventory_task", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        assetInventoryTaskService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "ass_asset_inventory_task-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelAssetInventoryTask(@RequestBody List<AssetInventoryTask> assetInventoryTasks) {
        User user = LoginUtils.getCurrentUserInfo();
        if (assetInventoryTasks.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<AssetInventoryTask> datasDb = assetInventoryTaskService.listByIds(assetInventoryTasks.stream().map(i -> i.getTaskId()).collect(Collectors.toList()));
        List<AssetInventoryTask> can = new ArrayList<>();
        List<AssetInventoryTask> no = new ArrayList<>();
        for (AssetInventoryTask data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            assetInventoryTaskService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }
        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getTaskId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ass_asset_inventory_task-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(AssetInventoryTask assetInventoryTask) {
        AssetInventoryTask data = (AssetInventoryTask) assetInventoryTaskService.getById(assetInventoryTask);
        return Result.ok().setData(data);
    }

}
