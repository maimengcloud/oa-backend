package com.oa.ht.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.ht.entity.ContractDraft;
import com.oa.ht.service.ContractDraftService;
import com.oa.ht.vo.ContractDraftAddVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/ht/contractDraft")
@Api(tags = {"ht_contract_draft-操作接口"})
public class ContractDraftController {

    static Logger logger = LoggerFactory.getLogger(ContractDraftController.class);

    @Autowired
    private ContractDraftService contractDraftService;

    @ApiOperation(value = "ht_contract_draft-查询列表", notes = " ")
    @ApiEntityParams(ContractDraft.class)
    @ApiResponses({@ApiResponse(code = 200, response = ContractDraft.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listContractDraft(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("reqUserId", user.getUserid());
        QueryWrapper<ContractDraft> qw = QueryTools.initQueryWrapper(ContractDraft.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> contractDraftList = contractDraftService.selectListMapByWhere(page, qw, params);
        //查询关联数据
        contractDraftService.selectLinkData(contractDraftList);
        return Result.ok("query-ok", "查询成功").setData(contractDraftList).setTotal(page.getTotal());
    }


    @ApiOperation(value = "ht_contract_draft-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractDraft.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addContractDraft(@RequestBody ContractDraftAddVo contractDraftAddVo) {
        if (StringUtils.isEmpty(contractDraftAddVo.getId())) {
            contractDraftAddVo.setId(contractDraftService.createKey("id"));
        } else {
            ContractDraft contractDraftQuery = new ContractDraft(contractDraftAddVo.getId());
            if (contractDraftService.countByWhere(contractDraftQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        contractDraftService.insertContractDraft(contractDraftAddVo);
        return Result.ok("add-ok", "添加成功！").setData(contractDraftAddVo);
    }

    @ApiOperation(value = "ht_contract_draft-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delContractDraft(@RequestBody ContractDraft contractDraft) {
        contractDraftService.removeById(contractDraft);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "ht_contract_draft-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractDraft.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editContractDraft(@RequestBody ContractDraftAddVo contractDraftAddVo) {
        contractDraftService.updateContractDraft(contractDraftAddVo);
        return Result.ok("edit-ok", "成功更新一条数据！").setData(contractDraftAddVo);
    }

    @ApiOperation(value = "ht_contract_draft-批量修改某些字段", notes = "")
    @ApiEntityParams(value = ContractDraft.class, props = {}, remark = "ht_contract_draft", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = ContractDraft.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        contractDraftService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "ht_contract_draft-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelContractDraft(@RequestBody List<ContractDraft> contractDrafts) {
        User user = LoginUtils.getCurrentUserInfo();
        if (contractDrafts.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<ContractDraft> datasDb = contractDraftService.listByIds(contractDrafts.stream().map(ContractDraft::getId).collect(Collectors.toList()));

        List<ContractDraft> can = new ArrayList<>();
        List<ContractDraft> no = new ArrayList<>();
        for (ContractDraft data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            contractDraftService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ht_contract_draft-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractDraft.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(ContractDraft contractDraft) {
        ContractDraft data = (ContractDraft) contractDraftService.getById(contractDraft);
        return Result.ok().setData(data);
    }

}
