package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.query.QueryTools;
import com.mdp.core.service.SequenceService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.GroupScheduleCycle;
import com.oa.kq.service.GroupScheduleCycleService;
import com.oa.utils.ExcelView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Controller
@RequestMapping(value = "/*/oa/kq/groupScheduleCycle")
@Api(tags = {"排班周期表-操作接口"})
public class GroupScheduleCycleController {

    static Logger logger = LoggerFactory.getLogger(GroupScheduleCycleController.class);

    @Autowired
    private GroupScheduleCycleService groupScheduleCycleService;

    @Autowired
    private SequenceService seqService;

    /**
     * 跳转到查询页面listGroupScheduleCycle.jsp
     */
    @RequestMapping(value="/listGroupScheduleCycle")
    public ModelAndView listGroupScheduleCycle(@ModelAttribute("groupScheduleCycle") GroupScheduleCycle groupScheduleCycle, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        mv.addObject("data",groupScheduleCycle);
        return mv;
    }


    /**
     * 跳转到新增页面addGroupScheduleCycle.jsp
     */
    @RequestMapping(value="/addGroupScheduleCycle")
    public ModelAndView addGroupScheduleCycle(){

        ModelAndView mv = new ModelAndView();
        GroupScheduleCycle groupScheduleCycle=new GroupScheduleCycle();
        mv.addObject("data",groupScheduleCycle);
        return mv;
    }

    /**
     * 跳转到修改页面editGroupScheduleCycle.jsp
     */
    @RequestMapping(value="/editGroupScheduleCycle")
    public ModelAndView editGroupScheduleCycle( @ModelAttribute("groupScheduleCycle") GroupScheduleCycle groupScheduleCycle, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        groupScheduleCycle = groupScheduleCycleService.selectOneObject(groupScheduleCycle);	//根据ID读取
        mv.addObject("data",groupScheduleCycle);
        return mv;
    }


    /**
     * 请求需带json后缀,如selectListGroupScheduleCycleByPage.json
     * 根据条件查询数据对象列表
     */
    @RequestMapping(value="/selectListGroupScheduleCycleByPage")
    public ModelAndView selectListGroupScheduleCycleByPage( @ModelAttribute("groupScheduleCycle") GroupScheduleCycle groupScheduleCycle, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();

        List<GroupScheduleCycle>	groupScheduleCycleList = groupScheduleCycleService.selectListByWhere(groupScheduleCycle);	//列出GroupScheduleCycle列表
        mv.addObject("data",groupScheduleCycleList);
        Tips tips=new Tips("查询成功");
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 新增一条数据
     */
    @RequestMapping(value="/insertGroupScheduleCycle")
    public ModelAndView insertGroupScheduleCycle( @ModelAttribute("groupScheduleCycle") GroupScheduleCycle groupScheduleCycle, BindingResult result, Model model) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功新增一条数据");
        if(StringUtils.isEmpty(groupScheduleCycle.getId())){
            groupScheduleCycle.setId(seqService.getTablePK("groupScheduleCycle", "id"));
        }
        groupScheduleCycleService.insert(groupScheduleCycle);
        mv.addObject("data",groupScheduleCycle);
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 根据主键删除1条数据
     */
    @RequestMapping(value="/deleteGroupScheduleCycleByPk")
    public ModelAndView deleteGroupScheduleCycleByPk(@ModelAttribute("groupScheduleCycle") GroupScheduleCycle groupScheduleCycle, BindingResult result){
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功删除一条数据");
        groupScheduleCycleService.deleteByPk(groupScheduleCycle);
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 根据主键修改一条数据
     */
    @RequestMapping(value="/updateGroupScheduleCycleByPk")
    public ModelAndView updateGroupScheduleCycleByPk( @ModelAttribute("groupScheduleCycle") GroupScheduleCycle groupScheduleCycle, BindingResult result, Model model) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功更新一条数据");
        groupScheduleCycleService.updateByPk(groupScheduleCycle);
        mv.addObject("data",groupScheduleCycle);
        mv.addObject("tips", tips);
        return mv;
    }




    /**
     * 批量删除
     */
    @RequestMapping(value="/batchDeleteGroupScheduleCycle")
    public ModelAndView batchDeleteGroupScheduleCycle(@RequestParam("id[]") String[] id) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功删除"+id.length+"条数据");
        List<GroupScheduleCycle> list=new ArrayList<GroupScheduleCycle>();
        for(int i=0;i<id.length;i++){
            GroupScheduleCycle groupScheduleCycle=new GroupScheduleCycle();
            groupScheduleCycle.setId(id[i]);
            list.add(groupScheduleCycle);
        }
        groupScheduleCycleService.batchDelete(list);
        mv.addObject("tips", tips);
        return mv;
    }

    /*
     * 导出到excel
     * @return
     */
    @RequestMapping(value="/exportGroupScheduleCycleToExcel")
    public ModelAndView exportGroupScheduleCycleToExcel( @ModelAttribute("groupScheduleCycle") GroupScheduleCycle groupScheduleCycle, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功导出数据");
        try{
            Map<String,Object> dataMap = new HashMap<String,Object>();
            List<String> titles = new ArrayList<String>();
            titles.add("编号id");
            titles.add("周六（填入班次id）");
            titles.add("组id");
            titles.add("周一（填入班次id）");
            titles.add("周二（填入班次id）");
            titles.add("周三（填入班次id）");
            titles.add("周四（填入班次id）");
            titles.add("周五（填入班次id）");
            titles.add("周日（填入班次id）");
            dataMap.put("titles", titles);
            List<GroupScheduleCycle> GroupScheduleCycleList = groupScheduleCycleService.selectListByWhere(groupScheduleCycle);
            List<Map<String,Object>> allList = new ArrayList<Map<String,Object>>();
            for(int i=0;i<GroupScheduleCycleList.size();i++){
                Map<String,Object> row = new HashMap<String,Object>();
                row.put("var1", GroupScheduleCycleList.get(i).getId());
                row.put("var2", GroupScheduleCycleList.get(i).getSaturday());
                row.put("var3", GroupScheduleCycleList.get(i).getGroupId());
                row.put("var4", GroupScheduleCycleList.get(i).getMonday());
                row.put("var5", GroupScheduleCycleList.get(i).getTuesday());
                row.put("var6", GroupScheduleCycleList.get(i).getWednesday());
                row.put("var7", GroupScheduleCycleList.get(i).getThursday());
                row.put("var8", GroupScheduleCycleList.get(i).getFriday());
                row.put("var9", GroupScheduleCycleList.get(i).getSunday());
                allList.add(row);
            }
            dataMap.put("varList", allList);
            ExcelView erv = new ExcelView();
            mv = new ModelAndView(erv,dataMap);
        } catch(Exception e){
            logger.error(e.toString(), e);
            tips.setErrMsg("导出数据异常");
        }
        mv.addObject("tips", tips);
        return mv;
    }

    @ResponseBody
    @ApiOperation(value = "排班周期表-查询列表", notes = " ")
    @ApiEntityParams(GroupScheduleCycle.class)
    @ApiResponses({@ApiResponse(code = 200, response = GroupScheduleCycle.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listGroupScheduleCycle(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        QueryWrapper<GroupScheduleCycle> qw = QueryTools.initQueryWrapper(GroupScheduleCycle.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = groupScheduleCycleService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ResponseBody
    @ApiOperation(value = "排班周期表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupScheduleCycle.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addGroupScheduleCycle(@RequestBody GroupScheduleCycle groupScheduleCycle) {
        groupScheduleCycleService.save(groupScheduleCycle);
        return Result.ok("add-ok", "添加成功！");
    }

    @ResponseBody
    @ApiOperation(value = "排班周期表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delGroupScheduleCycle(@RequestBody GroupScheduleCycle groupScheduleCycle) {
        groupScheduleCycleService.removeById(groupScheduleCycle);
        return Result.ok("del-ok", "删除成功！");
    }

    @ResponseBody
    @ApiOperation(value = "排班周期表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupScheduleCycle.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editGroupScheduleCycle(@RequestBody GroupScheduleCycle groupScheduleCycle) {
        groupScheduleCycleService.updateById(groupScheduleCycle);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ResponseBody
    @ApiOperation(value = "排班周期表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = GroupScheduleCycle.class, props = {}, remark = "排班周期表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = GroupScheduleCycle.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        groupScheduleCycleService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ResponseBody
    @ApiOperation(value = "排班周期表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelGroupScheduleCycle(@RequestBody List<GroupScheduleCycle> groupScheduleCycles) {
        User user = LoginUtils.getCurrentUserInfo();
        if (groupScheduleCycles.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<GroupScheduleCycle> datasDb = groupScheduleCycleService.listByIds(groupScheduleCycles.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<GroupScheduleCycle> can = new ArrayList<>();
        List<GroupScheduleCycle> no = new ArrayList<>();
        for (GroupScheduleCycle data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            groupScheduleCycleService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ResponseBody
    @ApiOperation(value = "排班周期表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupScheduleCycle.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(GroupScheduleCycle groupScheduleCycle) {
        GroupScheduleCycle data = (GroupScheduleCycle) groupScheduleCycleService.getById(groupScheduleCycle);
        return Result.ok().setData(data);
    }

}
