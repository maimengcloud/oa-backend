package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserAccessory;
import com.oa.hr.user.service.HrUserAccessoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value="/*/oa/hr/user/hrUserAccessory")
@Api(tags = {"hr_user_accessory-操作接口"})
public class HrUserAccessoryController {

    static Logger logger = LoggerFactory.getLogger(HrUserAccessoryController.class);

    @Autowired
    private HrUserAccessoryService hrUserAccessoryService;

    @ApiOperation(value = "hr_user_accessory-查询列表", notes = " ")
    @ApiEntityParams(HrUserAccessory.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrUserAccessory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrUserAccessory(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("res.branchId", user.getBranchId());
        QueryWrapper<HrUserAccessory> qw = QueryTools.initQueryWrapper(HrUserAccessory.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = hrUserAccessoryService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "hr_user_accessory-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserAccessory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrUserAccessory(@RequestBody HrUserAccessory hrUserAccessory) {
        
        
            User user = LoginUtils.getCurrentUserInfo();
            hrUserAccessory.setBranchId(user.getBranchId());
            if (StringUtils.isEmpty(hrUserAccessory.getId())) {
                hrUserAccessory.setId(hrUserAccessoryService.createKey("id"));
            } else {
                HrUserAccessory hrUserAccessoryQuery = new HrUserAccessory(hrUserAccessory.getId());
                if (hrUserAccessoryService.countByWhere(hrUserAccessoryQuery) > 0) {
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            hrUserAccessory.setUploadTime(new Date());
            hrUserAccessoryService.insert(hrUserAccessory);
        
        return Result.ok("add-ok", "添加成功！").setData(hrUserAccessory);
    }

    @ApiOperation(value = "hr_user_accessory-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrUserAccessory(@RequestBody HrUserAccessory hrUserAccessory) {
        
        
            hrUserAccessoryService.deleteByPk(hrUserAccessory);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "hr_user_accessory-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserAccessory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrUserAccessory(@RequestBody HrUserAccessory hrUserAccessory) {
        
        
            hrUserAccessoryService.updateByPk(hrUserAccessory);
        
        return Result.ok("edit-ok", "修改成功！").setData(hrUserAccessory);
    }

    @RequestMapping(value = "/delHrUserAccessoryByWhere", method = RequestMethod.POST)
    public Result delHrUserAccessoryByWhere(@RequestBody HrUserAccessory hrUserAccessory) {
        
        
            hrUserAccessoryService.deleteByWhere(hrUserAccessory);
        
        return Result.ok("", "成功删除一条数据");
    }

    @ApiOperation(value = "hr_user_accessory-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrUserAccessory.class, props = {}, remark = "hr_user_accessory", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserAccessory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        hrUserAccessoryService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "hr_user_accessory-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrUserAccessory(@RequestBody List<HrUserAccessory> hrUserAccessorys) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrUserAccessorys.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrUserAccessory> datasDb = hrUserAccessoryService.listByIds(hrUserAccessorys.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrUserAccessory> can = new ArrayList<>();
        List<HrUserAccessory> no = new ArrayList<>();
        for (HrUserAccessory data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            hrUserAccessoryService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "hr_user_accessory-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserAccessory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrUserAccessory hrUserAccessory) {
        HrUserAccessory data = (HrUserAccessory) hrUserAccessoryService.getById(hrUserAccessory);
        return Result.ok().setData(data);
    }

}
