package com.oa.ht.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.ht.entity.ContractProduct;
import com.oa.ht.entity.ContractProductBatchVo;
import com.oa.ht.service.ContractProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/ht/contractProduct")
@Api(tags = {"ht_contract_product-操作接口"})
public class ContractProductController {

    static Logger logger = LoggerFactory.getLogger(ContractProductController.class);

    @Autowired
    private ContractProductService contractProductService;

    @ApiOperation(value = "ht_contract_product-查询列表", notes = " ")
    @ApiEntityParams(ContractProduct.class)
    @ApiResponses({@ApiResponse(code = 200, response = ContractProduct.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listContractProduct(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "PIds");
        QueryWrapper<ContractProduct> qw = QueryTools.initQueryWrapper(ContractProduct.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = contractProductService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "ht_contract_product-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractProduct.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addContractProduct(@RequestBody ContractProduct contractProduct) {
        contractProductService.save(contractProduct);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "ht_contract_product-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delContractProduct(@RequestBody ContractProduct contractProduct) {
        contractProductService.removeById(contractProduct);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "ht_contract_product-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractProduct.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editContractProduct(@RequestBody ContractProduct contractProduct) {
        contractProductService.updateById(contractProduct);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "ht_contract_product-批量修改某些字段", notes = "")
    @ApiEntityParams(value = ContractProduct.class, props = {}, remark = "ht_contract_product", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = ContractProduct.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        contractProductService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "ht_contract_product-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelContractProduct(@RequestBody List<ContractProduct> contractProducts) {
        User user = LoginUtils.getCurrentUserInfo();
        if (contractProducts.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<ContractProduct> datasDb = contractProductService.listByIds(contractProducts.stream().map(ContractProduct::getId).collect(Collectors.toList()));

        List<ContractProduct> can = new ArrayList<>();
        List<ContractProduct> no = new ArrayList<>();
        for (ContractProduct data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            contractProductService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ht_contract_product-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractProduct.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(ContractProduct contractProduct) {
        ContractProduct data = (ContractProduct) contractProductService.getById(contractProduct);
        return Result.ok().setData(data);
    }

    @ApiOperation(value = "批量新增修改合同首付款条件", notes = "batchSave")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
    })
    @RequestMapping(value = "/batchSave", method = RequestMethod.POST)
    public Result batchSave(@RequestBody ContractProductBatchVo contractProducts) {

        contractProductService.batchSave(contractProducts);

        return Result.ok("update-ok", "更新成功");
    }
}
