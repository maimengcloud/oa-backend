package com.oa.om.ctrl;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import com.mdp.core.utils.ObjectTools;
import com.oa.om.OmTools;
import com.oa.om.entity.OmAppliancePurchase;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import static com.mdp.core.utils.BaseUtils.*;
import com.mdp.core.entity.LangTips;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;

import com.oa.om.service.OmApplianceRequireService;
import com.oa.om.entity.OmApplianceRequire;
/**
 * @author 唛盟开源 code-gen
 * @since 2024-8-21
 */
@RestController
@RequestMapping(value="/oa/om/omApplianceRequire")
@Api(tags={"办公用品申请-操作接口"})
public class OmApplianceRequireController {
	
	static Logger logger =LoggerFactory.getLogger(OmApplianceRequireController.class);
	
	@Autowired
	private OmApplianceRequireService omApplianceRequireService;

	@Operation( summary =  "办公用品申请-查询列表")
	@ApiEntityParams(OmApplianceRequire.class)
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listOmApplianceRequire(  @RequestParam Map<String,Object> params){
			User user=LoginUtils.getCurrentUserInfo();
			QueryWrapper<OmApplianceRequire> qw = QueryTools.initQueryWrapper(OmApplianceRequire.class , params);
			IPage page=QueryTools.initPage(params);
			List<Map<String,Object>> datas = omApplianceRequireService.selectListMapByWhere(page,qw,params);
			return Result.ok("query-ok","查询成功").setData(datas).setTotal(page.getTotal());
	}
	

	@Operation( summary =  "办公用品申请-新增")
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addOmApplianceRequire(@RequestBody OmApplianceRequire omApplianceRequire) {
        if(ObjectTools.isEmpty(omApplianceRequire.getReqNum())){
            omApplianceRequire.setReqNum(BigDecimal.ZERO);
        }
		 omApplianceRequireService.save(omApplianceRequire);
         return Result.ok("add-ok","添加成功！").setData(omApplianceRequire);
	}

	@Operation( summary =  "办公用品申请-删除")
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delOmApplianceRequire(@RequestBody OmApplianceRequire omApplianceRequire){
		omApplianceRequireService.removeById(omApplianceRequire);
        return Result.ok("del-ok","删除成功！");
	}

	@Operation( summary =  "办公用品申请-修改")
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editOmApplianceRequire(@RequestBody OmApplianceRequire omApplianceRequire) {
		omApplianceRequireService.updateById(omApplianceRequire);
        return Result.ok("edit-ok","修改成功！");
	}

    @Operation( summary =  "办公用品申请-批量修改某些字段" )
    @ApiEntityParams( value = OmApplianceRequire.class, props={ }, remark = "办公用品申请", paramType = "body" )
	@RequestMapping(value="/editSomeFields",method=RequestMethod.POST)
	public Result editSomeFields(   @RequestBody Map<String,Object> params) {
            User user= LoginUtils.getCurrentUserInfo();
            List<OmApplianceRequire> rows=this.omApplianceRequireService.listByIds((Collection) params.get("$pks"));
        for (OmApplianceRequire row : rows) {
            OmTools.validBizFlowStateIs(params,row.getBizFlowState());

        }
            omApplianceRequireService.editSomeFields(params);
            return Result.ok("edit-ok","更新成功");
	}

	@Operation( summary =  "办公用品申请-批量删除")
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelOmApplianceRequire(@RequestBody List<OmApplianceRequire> omApplianceRequires) {
	    User user= LoginUtils.getCurrentUserInfo();
        if(omApplianceRequires.size()<=0){
            return Result.error("batchDel-data-err-0","请上送待删除数据列表");
        }
         List<OmApplianceRequire> datasDb=omApplianceRequireService.listByIds(omApplianceRequires.stream().map(i-> i.getId() ).collect(Collectors.toList()));

        List<OmApplianceRequire> can=new ArrayList<>();
        List<OmApplianceRequire> no=new ArrayList<>();
        for (OmApplianceRequire data : datasDb) {
            if(true){
                can.add(data);
            }else{
                no.add(data);
            }
        }
        List<String> msgs=new ArrayList<>();
        if(can.size()>0){
            omApplianceRequireService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num","成功删除%s条数据.",can.size()));
        }

        if(no.size()>0){
            msgs.add(LangTips.transMsg("not-allow-del-num","以下%s条数据不能删除:【%s】",no.size(),no.stream().map(i-> i.getId() ).collect(Collectors.joining(","))));
        }
        if(can.size()>0){
             return Result.ok(msgs.stream().collect(Collectors.joining()));
        }else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
	} 

	@Operation( summary =  "办公用品申请-根据主键查询一条数据")
    @RequestMapping(value="/queryById",method=RequestMethod.GET)
    public Result queryById(OmApplianceRequire omApplianceRequire) {
        OmApplianceRequire data = (OmApplianceRequire) omApplianceRequireService.getById(omApplianceRequire);
        return Result.ok().setData(data);
    }

}
