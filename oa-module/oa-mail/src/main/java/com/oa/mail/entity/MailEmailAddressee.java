package com.oa.mail.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("mail_email_addressee")
@ApiModel(description="邮件收件人表")
public class MailEmailAddressee  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="邮件id",allowEmptyValue=true,example="",allowableValues="")
	String emailId;

	
	@ApiModelProperty(notes="收件人id",allowEmptyValue=true,example="",allowableValues="")
	String userId;

	
	@ApiModelProperty(notes="收件人名字",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="收件人邮箱",allowEmptyValue=true,example="",allowableValues="")
	String userEmail;

	
	@ApiModelProperty(notes="类型0用户,1客户,2联系人",allowEmptyValue=true,example="",allowableValues="")
	String type;

	
	@ApiModelProperty(notes="0未读,1已读",allowEmptyValue=true,example="",allowableValues="")
	String isRead;

	
	@ApiModelProperty(notes="0否,1是",allowEmptyValue=true,example="",allowableValues="")
	String isStar;

	
	@ApiModelProperty(notes="0收件人,1抄送人,2密送人",allowEmptyValue=true,example="",allowableValues="")
	String receiveType;

	/**
	 *主键
	 **/
	public MailEmailAddressee(String id) {
		this.id = id;
	}
    
    /**
     * 邮件收件人表
     **/
	public MailEmailAddressee() {
	}

}