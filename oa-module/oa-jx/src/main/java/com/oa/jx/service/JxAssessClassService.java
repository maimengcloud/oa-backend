package com.oa.jx.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.core.utils.BaseUtils;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.jx.entity.JxAssessClass;
import com.oa.jx.mapper.JxAssessClassMapper;
import com.oa.jx.vo.JxAssessClassList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class JxAssessClassService extends BaseService<JxAssessClassMapper, JxAssessClass> {
    static Logger logger = LoggerFactory.getLogger(JxAssessClassService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    /**
     * 请在此类添加自定义函数
     */
    public void saveAssessClassList(IPage page, QueryWrapper qw, JxAssessClassList jxAssessClassList) {
        //先查询
        Map<String, Object> map = new HashMap<>();
        map.put("branchId", LoginUtils.getCurrentUserInfo().getBranchId());
        List<Map<String, Object>> maps = this.selectListMapByWhere(page, qw, map);
        if (maps.size() > 0) {
            jxAssessClassList.getJxAssessClassList().forEach(item -> {
                this.updateByPk(item);
            });

        } else {
            jxAssessClassList.getJxAssessClassList().forEach(item -> {
                this.insert(item);
            });
        }

    }

    @Transactional
    public List<Map<String, Object>> getAssessClass(IPage page, QueryWrapper qw, Map<String, Object> jxAssessClass) {
        List<Map<String, Object>> maps = this.selectListMapByWhere(page, qw, jxAssessClass);
        if (maps.size() == 0) {
            for (int i = 0; i < 5; i++) {
                JxAssessClass assessClass = new JxAssessClass();
                assessClass.setBranchId(LoginUtils.getCurrentUserInfo().getBranchId());
                assessClass.setClassType(i + 1 + "");
                if (i == 0) {

                    assessClass.setName("优秀");
                    assessClass.setStartPoints(95);
                    assessClass.setEndPoints(100);
                }
                if (i == 1) {
                    assessClass.setName("良好");
                    assessClass.setStartPoints(85);
                    assessClass.setEndPoints(94);
                }
                if (i == 2) {
                    assessClass.setName("中等");
                    assessClass.setStartPoints(70);
                    assessClass.setEndPoints(84);
                }
                if (i == 3) {
                    assessClass.setName("合格");
                    assessClass.setStartPoints(50);
                    assessClass.setEndPoints(69);
                }
                if (i == 4) {
                    assessClass.setName("不合格");
                    assessClass.setStartPoints(1);
                    assessClass.setEndPoints(49);
                }
                Map<String, Object> map = new HashMap<>();
                map.putAll(BaseUtils.toMap(assessClass));
                this.insert(assessClass);
                maps.add(map);
            }
        }
        return maps;
    }

    public JxAssessClass calcClassByFinalPoints(BigDecimal finalPoints) {
        JxAssessClass jxAssessClass = new JxAssessClass();
        jxAssessClass.setBranchId(LoginUtils.getCurrentUserInfo().getBranchId());
        List<JxAssessClass> jxAssessClasses = this.selectListByWhere(jxAssessClass);
        if (jxAssessClasses == null || jxAssessClasses.size() == 0 || finalPoints == null) {
            return null;
        } else {
            for (JxAssessClass assessClass : jxAssessClasses) {
                if (assessClass.getStartPoints() != null) {
                    if (assessClass.getStartPoints() <= finalPoints.intValue() && finalPoints.intValue() <= assessClass.getEndPoints()) {
                        return assessClass;
                    }
                }
            }
        }
        return null;
    }

}

