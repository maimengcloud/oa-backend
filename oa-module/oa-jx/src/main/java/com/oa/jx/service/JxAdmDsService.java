package com.oa.jx.service;

//import com.mdp.core.dao.BaseDao;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.mdp.core.service.BaseService;
import com.oa.jx.entity.JxAdmDs;
import com.oa.jx.mapper.JxAdmDsMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class JxAdmDsService extends BaseService<JxAdmDsMapper, JxAdmDs> {

    //    @Autowired
//    BaseDao baseDao;

    /**
     * 请在此类添加自定义函数
     */
    @DS(value = "adm-ds")
    public List<Map<String, Object>> selectUserByUsername(Map<String, Object> params) {
//        return baseDao.selectList("JxAdmDs.selectUserByUsername", params);
        return baseMapper.selectUserByUsername(params);
    }

    @DS("adm-ds")
    List<Map<String, Object>> selectUserByWhere(Map<String, Object> jxAssessGrade) {
        return baseMapper.selectUserByWhere(jxAssessGrade);
    }


    @DS("adm-ds")
    public List<Map<String, Object>> selectDeptByUserid(Map<String, Object> userMap) {
        return baseMapper.selectDeptByUserid(userMap);
    }

    @DS("adm-ds")
    public List<String> selectUsersByDeptid(Map dept) {
        return baseMapper.selectUsersByDeptid(dept);
    }
}
