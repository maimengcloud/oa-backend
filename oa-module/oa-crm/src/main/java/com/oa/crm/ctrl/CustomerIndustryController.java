package com.oa.crm.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.crm.dto.CustomerIndustryTreeDto;
import com.oa.crm.entity.CustomerIndustry;
import com.oa.crm.service.CustomerIndustryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/crm/customerIndustry")
@Api(tags = {"客户行业表-操作接口"})
public class CustomerIndustryController {

    static Logger logger = LoggerFactory.getLogger(CustomerIndustryController.class);

    @Autowired
    private CustomerIndustryService customerIndustryService;

    @ApiOperation(value = "客户行业表-查询列表", notes = " ")
    @ApiEntityParams(CustomerIndustry.class)
    @ApiResponses({@ApiResponse(code = 200, response = CustomerIndustry.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCustomerIndustry(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CustomerIndustry> qw = QueryTools.initQueryWrapper(CustomerIndustry.class, params);
        IPage page = QueryTools.initPage(params);
//        List<CustomerIndustryTreeDto> customerIndustryList = customerIndustryService.selectCustomerIndustryTree();    //列出CustomerIndustry列表

        List<Map<String, Object>> data = customerIndustryService.selectListMapByWhere(page, qw, params);
        List<CustomerIndustryTreeDto> customerIndustryList = data.stream().map((value) -> {
            CustomerIndustryTreeDto customerIndustryTreeDto = new CustomerIndustryTreeDto();
            BeanUtils.copyProperties(value, customerIndustryTreeDto);
            return customerIndustryTreeDto;
        }).collect(Collectors.toList());

        return Result.ok("query-ok", "查询成功").setData(customerIndustryList).setTotal(page.getTotal());
    }


    @ApiOperation(value = "客户行业表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerIndustry.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCustomerIndustry(@RequestBody CustomerIndustry customerIndustry) {
        customerIndustryService.save(customerIndustry);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "客户行业表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCustomerIndustry(@RequestBody CustomerIndustry customerIndustry) {
        customerIndustryService.removeById(customerIndustry);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "客户行业表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerIndustry.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCustomerIndustry(@RequestBody CustomerIndustry customerIndustry) {
        customerIndustryService.updateById(customerIndustry);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "客户行业表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CustomerIndustry.class, props = {}, remark = "客户行业表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerIndustry.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        customerIndustryService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "客户行业表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCustomerIndustry(@RequestBody List<CustomerIndustry> customerIndustrys) {
        User user = LoginUtils.getCurrentUserInfo();
        if (customerIndustrys.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CustomerIndustry> datasDb = customerIndustryService.listByIds(customerIndustrys.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CustomerIndustry> can = new ArrayList<>();
        List<CustomerIndustry> no = new ArrayList<>();
        for (CustomerIndustry data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            customerIndustryService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "客户行业表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerIndustry.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CustomerIndustry customerIndustry) {
        CustomerIndustry data = (CustomerIndustry) customerIndustryService.getById(customerIndustry);
        return Result.ok().setData(data);
    }

}
