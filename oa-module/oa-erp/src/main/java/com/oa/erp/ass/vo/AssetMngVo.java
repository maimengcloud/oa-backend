package com.oa.erp.ass.vo;
import com.oa.erp.ass.entity.AssetMaintenanceConsumables;
import com.oa.erp.ass.entity.AssetMng;

import java.util.ArrayList;
import java.util.List;

public class AssetMngVo {

    //盘点任务Id
    private AssetMng assetMng;

    private List<AssetDetailVo> assetDetails = new ArrayList<>();

    //耗材: 维修单使用
    private List<AssetMaintenanceConsumables> haoCai = new ArrayList<>();

    public AssetMng getAssetMng() {
        return assetMng;
    }

    public void setAssetMng(AssetMng assetMng) {
        this.assetMng = assetMng;
    }

    public List<AssetDetailVo> getAssetDetails() {
        return assetDetails;
    }

    public void setAssetDetails(List<AssetDetailVo> assetDetails) {
        this.assetDetails = assetDetails;
    }

    public List<AssetMaintenanceConsumables> getHaoCai() {
        return haoCai;
    }

    public void setHaoCai(List<AssetMaintenanceConsumables> haoCai) {
        this.haoCai = haoCai;
    }

}
