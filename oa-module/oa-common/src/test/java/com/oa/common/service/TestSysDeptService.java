package  com.oa.common.service;

import java.util.*;
import java.text.SimpleDateFormat;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.mdp.core.utils.BaseUtils;
import org.springframework.beans.factory.annotation.Autowired; 
import  com.oa.common.service.SysDeptService;
import  com.oa.common.entity.SysDept;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * @author code-gen
 * @since 2024-10-18
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestSysDeptService  {

	@Autowired
	SysDeptService sysDeptService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("deptid","Kb2w","deptName","2817","pdeptid","W1Ds","deptType","muCf","state","kR7S","manager","GlW5","leader","iz3b","shortName","RDxy","displayDeptid","W7A8","orgType","hFsQ","managerName","K07J","leaderName","6FUo","branchId","1ApG","levelType","L644","idPath","2E6b","bizProcInstId","6DO4","bizFlowState","B","ltime",new Date("2024-10-18 9:30:10"),"isCbCenter","L","cpaType","dqs7","cpaBranchId","0rHd","relyType","x5XI","relyId","DMVB","relyStype","7r1T","relySid","knk7","icon","tza2");
		SysDept sysDept=BaseUtils.fromMap(p,SysDept.class);
		sysDeptService.save(sysDept);
		//Assert.assertEquals(1, result);
	}
	 
}
