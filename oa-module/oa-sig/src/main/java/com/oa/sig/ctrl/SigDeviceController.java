package com.oa.sig.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.sig.entity.SigDevice;
import com.oa.sig.service.SigDeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/sig/sigDevice")
@Api(tags = {"印章设备库-操作接口"})
public class SigDeviceController {

    static Logger logger = LoggerFactory.getLogger(SigDeviceController.class);

    @Autowired
    private SigDeviceService sigDeviceService;

    @ApiOperation(value = "印章设备库-查询列表", notes = " ")
    @ApiEntityParams(SigDevice.class)
    @ApiResponses({@ApiResponse(code = 200, response = SigDevice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listSigDevice(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<SigDevice> qw = QueryTools.initQueryWrapper(SigDevice.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = sigDeviceService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "印章设备库-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = SigDevice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addSigDevice(@RequestBody SigDevice sigDevice) {
        if (StringUtils.isEmpty(sigDevice.getId())) {
            sigDevice.setId(sigDeviceService.createKey("id"));
        } else {
            SigDevice sigDeviceQuery = new SigDevice(sigDevice.getId());
            if (sigDeviceService.countByWhere(sigDeviceQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        String branchName = LoginUtils.getCurrentUserInfo().getBranchName();
        sigDevice.setBranchId(branchId);
        sigDevice.setBranchName(branchName);
        sigDeviceService.insert(sigDevice);
        return Result.ok("add-ok", "添加成功！").setData(sigDevice);
    }

    @ApiOperation(value = "印章设备库-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delSigDevice(@RequestBody SigDevice sigDevice) {
        sigDeviceService.deleteByPk(sigDevice);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "印章设备库-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = SigDevice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editSigDevice(@RequestBody SigDevice sigDevice) {
        sigDeviceService.updateByPk(sigDevice);
        return Result.ok("edit-ok", "修改成功！").setData(sigDevice);
    }

    @ApiOperation(value = "印章设备库-批量修改某些字段", notes = "")
    @ApiEntityParams(value = SigDevice.class, props = {}, remark = "印章设备库", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = SigDevice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        sigDeviceService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "印章设备库-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelSigDevice(@RequestBody List<SigDevice> sigDevices) {
        User user = LoginUtils.getCurrentUserInfo();
        if (sigDevices.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<SigDevice> datasDb = sigDeviceService.listByIds(sigDevices.stream().map(i -> i.getId()).collect(Collectors.toList()));
        List<SigDevice> can = new ArrayList<>();
        List<SigDevice> no = new ArrayList<>();
        for (SigDevice data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            sigDeviceService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }
        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "印章设备库-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = SigDevice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(SigDevice sigDevice) {
        SigDevice data = sigDeviceService.getById(sigDevice);
        return Result.ok().setData(data);
    }

}
