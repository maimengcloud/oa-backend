package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_user_treatment")
@ApiModel(description="员工薪酬福利表")
public class HrUserTreatment  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="用户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="工资账户名",allowEmptyValue=true,example="",allowableValues="")
	String salaryAccountName;

	
	@ApiModelProperty(notes="工资银行",allowEmptyValue=true,example="",allowableValues="")
	String salaryBank;

	
	@ApiModelProperty(notes="工资账号",allowEmptyValue=true,example="",allowableValues="")
	String salaryAccount;

	
	@ApiModelProperty(notes="公积金账户",allowEmptyValue=true,example="",allowableValues="")
	String accumulationAccount;

	
	@ApiModelProperty(notes="社保号码",allowEmptyValue=true,example="",allowableValues="")
	String socialSecurity;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public HrUserTreatment(String id) {
		this.id = id;
	}
    
    /**
     * 员工薪酬福利表
     **/
	public HrUserTreatment() {
	}

}