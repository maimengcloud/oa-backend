package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserOvertime;
import com.oa.hr.user.service.HrUserOvertimeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value="/*/oa/hr/user/hrUserOvertime")
@Api(tags = {"员工加班表-操作接口"})
public class HrUserOvertimeController {

    static Logger logger = LoggerFactory.getLogger(HrUserOvertimeController.class);

    @Autowired
    private HrUserOvertimeService hrUserOvertimeService;

    @ApiOperation(value = "员工加班表-查询列表", notes = " ")
    @ApiEntityParams(HrUserOvertime.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrUserOvertime.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrUserOvertime(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("branchId", user.getBranchId());

        QueryWrapper<HrUserOvertime> qw = QueryTools.initQueryWrapper(HrUserOvertime.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = hrUserOvertimeService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "员工加班表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserOvertime.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrUserOvertime(@RequestBody HrUserOvertime hrUserOvertime) {
        
        
            User user= LoginUtils.getCurrentUserInfo();
            hrUserOvertime.setBranchId(user.getBranchId());
            if(StringUtils.isEmpty(hrUserOvertime.getId())) {
                hrUserOvertime.setId(hrUserOvertimeService.createKey("id"));
            }else{
                HrUserOvertime hrUserOvertimeQuery = new  HrUserOvertime(hrUserOvertime.getId());
                if(hrUserOvertimeService.countByWhere(hrUserOvertimeQuery)>0){
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            hrUserOvertimeService.insert(hrUserOvertime);
        
        return Result.ok("add-ok", "添加成功！").setData(hrUserOvertime);
    }

    @ApiOperation(value = "员工加班表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrUserOvertime(@RequestBody HrUserOvertime hrUserOvertime) {
        
        
            hrUserOvertimeService.deleteByPk(hrUserOvertime);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "员工加班表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserOvertime.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrUserOvertime(@RequestBody HrUserOvertime hrUserOvertime) {
        
        
            hrUserOvertimeService.updateByPk(hrUserOvertime);
        
        return Result.ok("edit-ok", "修改成功！").setData(hrUserOvertime);
    }

    @ApiOperation(value = "员工加班表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrUserOvertime.class, props = {}, remark = "员工加班表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserOvertime.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        hrUserOvertimeService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "员工加班表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrUserOvertime(@RequestBody List<HrUserOvertime> hrUserOvertimes) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrUserOvertimes.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrUserOvertime> datasDb = hrUserOvertimeService.listByIds(hrUserOvertimes.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrUserOvertime> can = new ArrayList<>();
        List<HrUserOvertime> no = new ArrayList<>();
        for (HrUserOvertime data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            hrUserOvertimeService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "员工加班表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserOvertime.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrUserOvertime hrUserOvertime) {
        HrUserOvertime data = (HrUserOvertime) hrUserOvertimeService.getById(hrUserOvertime);
        return Result.ok().setData(data);
    }

}
