package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_address")
@ApiModel(description="crm_customer_address")
public class CustomerAddress  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="地址id,主键",allowEmptyValue=true,example="",allowableValues="")
	String addressId;

	
	@ApiModelProperty(notes="客户id",allowEmptyValue=true,example="",allowableValues="")
	Integer curstomerId;

	
	@ApiModelProperty(notes="国家",allowEmptyValue=true,example="",allowableValues="")
	String country;

	
	@ApiModelProperty(notes="国家代码",allowEmptyValue=true,example="",allowableValues="")
	String countryCode;

	
	@ApiModelProperty(notes="省份",allowEmptyValue=true,example="",allowableValues="")
	String province;

	
	@ApiModelProperty(notes="省份代码",allowEmptyValue=true,example="",allowableValues="")
	String provinceCode;

	
	@ApiModelProperty(notes="城市",allowEmptyValue=true,example="",allowableValues="")
	String city;

	
	@ApiModelProperty(notes="城市代码",allowEmptyValue=true,example="",allowableValues="")
	String cityCode;

	
	@ApiModelProperty(notes="街道",allowEmptyValue=true,example="",allowableValues="")
	String street;

	
	@ApiModelProperty(notes="街道代码",allowEmptyValue=true,example="",allowableValues="")
	String streetCode;

	
	@ApiModelProperty(notes="区县",allowEmptyValue=true,example="",allowableValues="")
	String district;

	
	@ApiModelProperty(notes="区县code",allowEmptyValue=true,example="",allowableValues="")
	String districtCode;

	
	@ApiModelProperty(notes="邮政编码",allowEmptyValue=true,example="",allowableValues="")
	String zipCode;

	
	@ApiModelProperty(notes="电话",allowEmptyValue=true,example="",allowableValues="")
	String phone;

	
	@ApiModelProperty(notes="传真",allowEmptyValue=true,example="",allowableValues="")
	String fax;

	
	@ApiModelProperty(notes="电子邮件",allowEmptyValue=true,example="",allowableValues="")
	String email;

	
	@ApiModelProperty(notes="联系人id",allowEmptyValue=true,example="",allowableValues="")
	String contacterUserId;

	
	@ApiModelProperty(notes="联系人名称",allowEmptyValue=true,example="",allowableValues="")
	String contacterUserName;

	
	@ApiModelProperty(notes="地址类型",allowEmptyValue=true,example="",allowableValues="")
	String typeId;

	
	@ApiModelProperty(notes="地址类型名称",allowEmptyValue=true,example="",allowableValues="")
	String typeName;

	
	@ApiModelProperty(notes="附加地址",allowEmptyValue=true,example="",allowableValues="")
	String appendAddress;

	
	@ApiModelProperty(notes="地址",allowEmptyValue=true,example="",allowableValues="")
	String address;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *地址id
	 **/
	public CustomerAddress(String addressId) {
		this.addressId = addressId;
	}
    
    /**
     * crm_customer_address
     **/
	public CustomerAddress() {
	}

}