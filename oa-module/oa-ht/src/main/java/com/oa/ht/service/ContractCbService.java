package com.oa.ht.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.ht.entity.ContractCb;
import com.oa.ht.entity.ContractCbBatchVo;
import com.oa.ht.mapper.ContractCbMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class ContractCbService extends BaseService<ContractCbMapper, ContractCb> {
    static Logger logger = LoggerFactory.getLogger(ContractCbService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    @Autowired
    ContractLogService contractLogService;

    @Lazy
    @Autowired
    ContractCardService contractCardService;

    public List<ContractCb> selectCbByIds(Map<String, Object> query) {
        return baseMapper.selectCbByIds(query);
    }


    /**
     * 批量新增、修改、删除一体化
     */
    @Transactional
    public void batchSave(ContractCbBatchVo ContractCbsVo) {
        User user = LoginUtils.getCurrentUserInfo();
        String htId = ContractCbsVo.getHtId();
        ContractCb ContractCb = new ContractCb();
        ContractCb.setHtId(htId);
        if (ContractCbsVo.getContractCbs() == null || ContractCbsVo.getContractCbs().size() == 0) {
            this.deleteByWhere(ContractCb);
            this.updateContractCardCbAmount(htId);
            contractLogService.addLog(htId, user.getUserid(), user.getUsername(), "清空所有成本");
            return;
        }
        List<ContractCb> ContractCbsDb = this.selectListByWhere(ContractCb);

        if (ContractCbsDb == null || ContractCbsDb.size() == 0) {
            for (ContractCb condition : ContractCbsVo.getContractCbs()) {
                condition.setCbId(this.createKey("id"));
                condition.setHtId(htId);
            }
            this.batchInsert(ContractCbsVo.getContractCbs());
            contractLogService.addLog(htId, user.getUserid(), user.getUsername(), "新增" + ContractCbsVo.getContractCbs().size() + "条成本");
            this.updateContractCardCbAmount(htId);
            return;
        }


        Map<String, ContractCb> dbMap = new HashMap<>();
        Map<String, ContractCb> voMap = new HashMap<>();
        List<ContractCb> addList = new ArrayList<>();
        List<ContractCb> updateList = new ArrayList<>();
        List<ContractCb> delList = new ArrayList<>();

        for (ContractCb condition : ContractCbsDb) {
            dbMap.put(condition.getCbId(), condition);
        }

        for (ContractCb condition : ContractCbsVo.getContractCbs()) {
            voMap.put(condition.getCbId(), condition);
        }


        for (ContractCb condition : ContractCbsDb) {
            if (voMap.containsKey(condition.getCbId())) {
                updateList.add(voMap.get(condition.getCbId()));
            } else {
                delList.add(condition);
            }
        }

        for (ContractCb condition : ContractCbsVo.getContractCbs()) {
            if (StringUtils.hasText(condition.getCbId())) {
                if (!dbMap.containsKey(condition.getCbId())) {
                    condition.setHtId(htId);
                    addList.add(condition);
                }
            } else {
                condition.setCbId(this.createKey("id"));
                condition.setHtId(htId);
                addList.add(condition);
            }

        }

        if (delList.size() > 0) {
            this.batchDelete(delList);
            contractLogService.addLog(htId, user.getUserid(), user.getUsername(), "删除" + delList.size() + "条成本");

        }

        if (addList.size() > 0) {
            this.batchInsert(addList);
            contractLogService.addLog(htId, user.getUserid(), user.getUsername(), "新增" + addList.size() + "条成本");

        }

        if (updateList.size() > 0) {
            this.batchUpdate(updateList);
            contractLogService.addLog(htId, user.getUserid(), user.getUsername(), "修改" + updateList.size() + "条成本");

        }
        this.updateContractCardCbAmount(htId);
    }

    public void updateContractCardCbAmount(String htId) {
//        super.update("com.oa.ht.entity.ContractCard.updateContractCardCbAmount", htId);
        contractCardService.updateContractCardCbAmount(htId);
    }
}

