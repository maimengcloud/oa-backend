package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.query.QueryTools;
import com.mdp.core.service.SequenceService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.GroupOtherAttendance;
import com.oa.kq.service.GroupOtherAttendanceService;
import com.oa.utils.ExcelView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Controller
@RequestMapping(value = "/*/oa/kq/groupOtherAttendance")
@Api(tags = {"其它考勤人员表-操作接口"})
public class GroupOtherAttendanceController {

    static Logger logger = LoggerFactory.getLogger(GroupOtherAttendanceController.class);

    @Autowired
    private GroupOtherAttendanceService groupOtherAttendanceService;

    @Autowired
    private SequenceService seqService;

    /**
     * 跳转到查询页面listGroupOtherAttendance.jsp
     */
    @RequestMapping(value="/listGroupOtherAttendance")
    public ModelAndView listGroupOtherAttendance(@ModelAttribute("groupOtherAttendance") GroupOtherAttendance groupOtherAttendance, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        mv.addObject("data",groupOtherAttendance);
        return mv;
    }


    /**
     * 跳转到新增页面addGroupOtherAttendance.jsp
     */
    @RequestMapping(value="/addGroupOtherAttendance")
    public ModelAndView addGroupOtherAttendance(){

        ModelAndView mv = new ModelAndView();
        GroupOtherAttendance groupOtherAttendance=new GroupOtherAttendance();
        mv.addObject("data",groupOtherAttendance);
        return mv;
    }

    /**
     * 跳转到修改页面editGroupOtherAttendance.jsp
     */
    @RequestMapping(value="/editGroupOtherAttendance")
    public ModelAndView editGroupOtherAttendance( @ModelAttribute("groupOtherAttendance") GroupOtherAttendance groupOtherAttendance, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        groupOtherAttendance = groupOtherAttendanceService.selectOneObject(groupOtherAttendance);	//根据ID读取
        mv.addObject("data",groupOtherAttendance);
        return mv;
    }


    /**
     * 请求需带json后缀,如selectListGroupOtherAttendanceByPage.json
     * 根据条件查询数据对象列表
     */
    @RequestMapping(value="/selectListGroupOtherAttendanceByPage")
    public ModelAndView selectListGroupOtherAttendanceByPage( @ModelAttribute("groupOtherAttendance") GroupOtherAttendance groupOtherAttendance, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();

        List<GroupOtherAttendance>	groupOtherAttendanceList = groupOtherAttendanceService.selectListByWhere(groupOtherAttendance);	//列出GroupOtherAttendance列表
        mv.addObject("data",groupOtherAttendanceList);
        Tips tips=new Tips("查询成功");
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 新增一条数据
     */
    @RequestMapping(value="/insertGroupOtherAttendance")
    public ModelAndView insertGroupOtherAttendance( @ModelAttribute("groupOtherAttendance") GroupOtherAttendance groupOtherAttendance, BindingResult result, Model model) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功新增一条数据");
        if(StringUtils.isEmpty(groupOtherAttendance.getId())){
            groupOtherAttendance.setId(seqService.getTablePK("groupOtherAttendance", "id"));
        }
        groupOtherAttendanceService.insert(groupOtherAttendance);
        mv.addObject("data",groupOtherAttendance);
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 根据主键删除1条数据
     */
    @RequestMapping(value="/deleteGroupOtherAttendanceByPk")
    public ModelAndView deleteGroupOtherAttendanceByPk(@ModelAttribute("groupOtherAttendance") GroupOtherAttendance groupOtherAttendance, BindingResult result){
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功删除一条数据");
        groupOtherAttendanceService.deleteByPk(groupOtherAttendance);
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 根据主键修改一条数据
     */
    @RequestMapping(value="/updateGroupOtherAttendanceByPk")
    public ModelAndView updateGroupOtherAttendanceByPk( @ModelAttribute("groupOtherAttendance") GroupOtherAttendance groupOtherAttendance, BindingResult result, Model model) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功更新一条数据");
        groupOtherAttendanceService.updateByPk(groupOtherAttendance);
        mv.addObject("data",groupOtherAttendance);
        mv.addObject("tips", tips);
        return mv;
    }




    /**
     * 批量删除
     */
    @RequestMapping(value="/batchDeleteGroupOtherAttendance")
    public ModelAndView batchDeleteGroupOtherAttendance(@RequestParam("id[]") String[] id) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功删除"+id.length+"条数据");
        List<GroupOtherAttendance> list=new ArrayList<GroupOtherAttendance>();
        for(int i=0;i<id.length;i++){
            GroupOtherAttendance groupOtherAttendance=new GroupOtherAttendance();
            groupOtherAttendance.setId(id[i]);
            list.add(groupOtherAttendance);
        }
        groupOtherAttendanceService.batchDelete(list);
        mv.addObject("tips", tips);
        return mv;
    }

    /*
     * 导出到excel
     * @return
     */
    @RequestMapping(value="/exportGroupOtherAttendanceToExcel")
    public ModelAndView exportGroupOtherAttendanceToExcel( @ModelAttribute("groupOtherAttendance") GroupOtherAttendance groupOtherAttendance, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功导出数据");
        try{
            Map<String,Object> dataMap = new HashMap<String,Object>();
            List<String> titles = new ArrayList<String>();
            titles.add("组id");
            titles.add("其它考勤人员id");
            titles.add("编号id");
            dataMap.put("titles", titles);
            List<GroupOtherAttendance> GroupOtherAttendanceList = groupOtherAttendanceService.selectListByWhere(groupOtherAttendance);
            List<Map<String,Object>> allList = new ArrayList<Map<String,Object>>();
            for(int i=0;i<GroupOtherAttendanceList.size();i++){
                Map<String,Object> row = new HashMap<String,Object>();
                row.put("var1", GroupOtherAttendanceList.get(i).getGroupId());
                row.put("var2", GroupOtherAttendanceList.get(i).getUserid());
                row.put("var3", GroupOtherAttendanceList.get(i).getId());
                allList.add(row);
            }
            dataMap.put("varList", allList);
            ExcelView erv = new ExcelView();
            mv = new ModelAndView(erv,dataMap);
        } catch(Exception e){
            logger.error(e.toString(), e);
            tips.setErrMsg("导出数据异常");
        }
        mv.addObject("tips", tips);
        return mv;
    }

    @ResponseBody
    @ApiOperation(value = "其它考勤人员表-查询列表", notes = " ")
    @ApiEntityParams(GroupOtherAttendance.class)
    @ApiResponses({@ApiResponse(code = 200, response = GroupOtherAttendance.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listGroupOtherAttendance(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        QueryWrapper<GroupOtherAttendance> qw = QueryTools.initQueryWrapper(GroupOtherAttendance.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = groupOtherAttendanceService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ResponseBody
    @ApiOperation(value = "其它考勤人员表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupOtherAttendance.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addGroupOtherAttendance(@RequestBody GroupOtherAttendance groupOtherAttendance) {
        groupOtherAttendanceService.save(groupOtherAttendance);
        return Result.ok("add-ok", "添加成功！");
    }

    @ResponseBody
    @ApiOperation(value = "其它考勤人员表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delGroupOtherAttendance(@RequestBody GroupOtherAttendance groupOtherAttendance) {
        groupOtherAttendanceService.removeById(groupOtherAttendance);
        return Result.ok("del-ok", "删除成功！");
    }

    @ResponseBody
    @ApiOperation(value = "其它考勤人员表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupOtherAttendance.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editGroupOtherAttendance(@RequestBody GroupOtherAttendance groupOtherAttendance) {
        groupOtherAttendanceService.updateById(groupOtherAttendance);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ResponseBody
    @ApiOperation(value = "其它考勤人员表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = GroupOtherAttendance.class, props = {}, remark = "其它考勤人员表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = GroupOtherAttendance.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        groupOtherAttendanceService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ResponseBody
    @ApiOperation(value = "其它考勤人员表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelGroupOtherAttendance(@RequestBody List<GroupOtherAttendance> groupOtherAttendances) {
        User user = LoginUtils.getCurrentUserInfo();
        if (groupOtherAttendances.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<GroupOtherAttendance> datasDb = groupOtherAttendanceService.listByIds(groupOtherAttendances.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<GroupOtherAttendance> can = new ArrayList<>();
        List<GroupOtherAttendance> no = new ArrayList<>();
        for (GroupOtherAttendance data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            groupOtherAttendanceService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ResponseBody
    @ApiOperation(value = "其它考勤人员表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupOtherAttendance.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(GroupOtherAttendance groupOtherAttendance) {
        GroupOtherAttendance data = (GroupOtherAttendance) groupOtherAttendanceService.getById(groupOtherAttendance);
        return Result.ok().setData(data);
    }

}
