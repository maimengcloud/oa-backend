package com.oa.ht.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.ht.entity.Counterparty;
import com.oa.ht.service.CounterpartyService;
import com.oa.ht.vo.CounterpartyAddVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/ht/counterparty")
@Api(tags = {"ht_counterparty-操作接口"})
public class CounterpartyController {

    static Logger logger = LoggerFactory.getLogger(CounterpartyController.class);

    @Autowired
    private CounterpartyService counterpartyService;

    @ApiOperation(value = "ht_counterparty-查询列表", notes = " ")
    @ApiEntityParams(Counterparty.class)
    @ApiResponses({@ApiResponse(code = 200, response = Counterparty.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCounterparty(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "cpIds");
        QueryWrapper<Counterparty> qw = QueryTools.initQueryWrapper(Counterparty.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = counterpartyService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "ht_counterparty-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Counterparty.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCounterparty(@RequestBody CounterpartyAddVo counterpartyAddVo) {
        if (StringUtils.isEmpty(counterpartyAddVo.getCpId())) {
            counterpartyAddVo.setCpId(counterpartyService.createKey("cpId"));
        } else {
            Counterparty counterpartyQuery = new Counterparty(counterpartyAddVo.getCpId());
            if (counterpartyService.countByWhere(counterpartyQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        counterpartyService.insertCounterParty(counterpartyAddVo);
        return Result.ok("add-ok", "添加成功！").setData(counterpartyAddVo);
    }

    @ApiOperation(value = "ht_counterparty-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCounterparty(@RequestBody Counterparty counterparty) {
        counterpartyService.deleteByPk(counterparty);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "ht_counterparty-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Counterparty.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCounterparty(@RequestBody CounterpartyAddVo counterpartyAddVo) {
        counterpartyService.updateCounterparty(counterpartyAddVo);
        return Result.ok("edit-ok", "修改成功！").setData(counterpartyAddVo);
    }

    @ApiOperation(value = "ht_counterparty-批量修改某些字段", notes = "")
    @ApiEntityParams(value = Counterparty.class, props = {}, remark = "ht_counterparty", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = Counterparty.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        counterpartyService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "ht_counterparty-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCounterparty(@RequestBody List<Counterparty> counterpartys) {
        User user = LoginUtils.getCurrentUserInfo();
        if (counterpartys.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<Counterparty> datasDb = counterpartyService.listByIds(counterpartys.stream().map(i -> i.getCpId()).collect(Collectors.toList()));

        List<Counterparty> can = new ArrayList<>();
        List<Counterparty> no = new ArrayList<>();
        for (Counterparty data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            counterpartyService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getCpId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ht_counterparty-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Counterparty.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(Counterparty counterparty) {
        Counterparty data = (Counterparty) counterpartyService.getById(counterparty);
        return Result.ok().setData(data);
    }

}
