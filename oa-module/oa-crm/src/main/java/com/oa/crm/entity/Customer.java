package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer")
@ApiModel(description="客户信息表")
public class Customer  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="客户id,主键",allowEmptyValue=true,example="",allowableValues="")
	String customerId;

	
	@ApiModelProperty(notes="客户名称",allowEmptyValue=true,example="",allowableValues="")
	String customerName;

	
	@ApiModelProperty(notes="客户编码",allowEmptyValue=true,example="",allowableValues="")
	String customerCode;

	
	@ApiModelProperty(notes="客户简称",allowEmptyValue=true,example="",allowableValues="")
	String customerEngName;

	
	@ApiModelProperty(notes="客户地址",allowEmptyValue=true,example="",allowableValues="")
	String address;

	
	@ApiModelProperty(notes="gps定位",allowEmptyValue=true,example="",allowableValues="")
	String addressGps;

	
	@ApiModelProperty(notes="邮政编码",allowEmptyValue=true,example="",allowableValues="")
	String zipCode;

	
	@ApiModelProperty(notes="国家",allowEmptyValue=true,example="",allowableValues="")
	String country;

	
	@ApiModelProperty(notes="省份",allowEmptyValue=true,example="",allowableValues="")
	String province;

	
	@ApiModelProperty(notes="省份代码",allowEmptyValue=true,example="",allowableValues="")
	String provinceCode;

	
	@ApiModelProperty(notes="城市",allowEmptyValue=true,example="",allowableValues="")
	String city;

	
	@ApiModelProperty(notes="城市代码",allowEmptyValue=true,example="",allowableValues="")
	String cityCode;

	
	@ApiModelProperty(notes="区县",allowEmptyValue=true,example="",allowableValues="")
	String district;

	
	@ApiModelProperty(notes="区县代码",allowEmptyValue=true,example="",allowableValues="")
	String districtCode;

	
	@ApiModelProperty(notes="语言",allowEmptyValue=true,example="",allowableValues="")
	String language;

	
	@ApiModelProperty(notes="语言名称",allowEmptyValue=true,example="",allowableValues="")
	String languageName;

	
	@ApiModelProperty(notes="电话",allowEmptyValue=true,example="",allowableValues="")
	String phone;

	
	@ApiModelProperty(notes="传真",allowEmptyValue=true,example="",allowableValues="")
	String fax;

	
	@ApiModelProperty(notes="电子邮件",allowEmptyValue=true,example="",allowableValues="")
	String email;

	
	@ApiModelProperty(notes="网站",allowEmptyValue=true,example="",allowableValues="")
	String website;

	
	@ApiModelProperty(notes="介绍",allowEmptyValue=true,example="",allowableValues="")
	String introduction;

	
	@ApiModelProperty(notes="客户状态",allowEmptyValue=true,example="",allowableValues="")
	String customerStatus;

	
	@ApiModelProperty(notes="客户状态名称",allowEmptyValue=true,example="",allowableValues="")
	String customerStatusName;

	
	@ApiModelProperty(notes="客户类型",allowEmptyValue=true,example="",allowableValues="")
	String customerType;

	
	@ApiModelProperty(notes="客户类型名称",allowEmptyValue=true,example="",allowableValues="")
	String customerTypeName;

	
	@ApiModelProperty(notes="描述",allowEmptyValue=true,example="",allowableValues="")
	String description;

	
	@ApiModelProperty(notes="客户描述名称",allowEmptyValue=true,example="",allowableValues="")
	String descriptionName;

	
	@ApiModelProperty(notes="规模",allowEmptyValue=true,example="",allowableValues="")
	String size;

	
	@ApiModelProperty(notes="规模名称",allowEmptyValue=true,example="",allowableValues="")
	String sizeName;

	
	@ApiModelProperty(notes="来源",allowEmptyValue=true,example="",allowableValues="")
	String source;

	
	@ApiModelProperty(notes="来源名称",allowEmptyValue=true,example="",allowableValues="")
	String sourceName;

	
	@ApiModelProperty(notes="客户行业id",allowEmptyValue=true,example="",allowableValues="")
	String sectorId;

	
	@ApiModelProperty(notes="客户行业名称",allowEmptyValue=true,example="",allowableValues="")
	String sectorName;

	
	@ApiModelProperty(notes="客户经理id",allowEmptyValue=true,example="",allowableValues="")
	String managerUserId;

	
	@ApiModelProperty(notes="客户经理名称",allowEmptyValue=true,example="",allowableValues="")
	String managerUserName;

	
	@ApiModelProperty(notes="中介机构id",allowEmptyValue=true,example="",allowableValues="")
	String agentId;

	
	@ApiModelProperty(notes="中介机构名称",allowEmptyValue=true,example="",allowableValues="")
	String agentName;

	
	@ApiModelProperty(notes="上级单位id",allowEmptyValue=true,example="",allowableValues="")
	String parentId;

	
	@ApiModelProperty(notes="上级单位名称",allowEmptyValue=true,example="",allowableValues="")
	String parentName;

	
	@ApiModelProperty(notes="文档id",allowEmptyValue=true,example="",allowableValues="")
	String customerDocId;

	
	@ApiModelProperty(notes="文档名称",allowEmptyValue=true,example="",allowableValues="")
	String customerDocName;

	
	@ApiModelProperty(notes="背景资料id",allowEmptyValue=true,example="",allowableValues="")
	String customerIntroductiondocId;

	
	@ApiModelProperty(notes="背景资料名称",allowEmptyValue=true,example="",allowableValues="")
	String customerIntroductiondocName;

	
	@ApiModelProperty(notes="安全级别",allowEmptyValue=true,example="",allowableValues="")
	String securityLevel;

	
	@ApiModelProperty(notes="安全级别名称",allowEmptyValue=true,example="",allowableValues="")
	String securityLevelName;

	
	@ApiModelProperty(notes="客户价值",allowEmptyValue=true,example="",allowableValues="")
	String evaluation;

	
	@ApiModelProperty(notes="客户级别",allowEmptyValue=true,example="",allowableValues="")
	String rating;

	
	@ApiModelProperty(notes="门户状态",allowEmptyValue=true,example="",allowableValues="")
	String portalStatus;

	
	@ApiModelProperty(notes="信用额度",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal creditAmount;

	
	@ApiModelProperty(notes="信用期间",allowEmptyValue=true,example="",allowableValues="")
	String creditTime;

	
	@ApiModelProperty(notes="开户银行",allowEmptyValue=true,example="",allowableValues="")
	String bankName;

	
	@ApiModelProperty(notes="帐户",allowEmptyValue=true,example="",allowableValues="")
	String accountName;

	
	@ApiModelProperty(notes="银行帐号",allowEmptyValue=true,example="",allowableValues="")
	String accounts;

	
	@ApiModelProperty(notes="累计合同金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalContractAmount;

	
	@ApiModelProperty(notes="是否关注客户",allowEmptyValue=true,example="",allowableValues="")
	String isAttention;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="创建人Id",allowEmptyValue=true,example="",allowableValues="")
	String createUserId;

	
	@ApiModelProperty(notes="创建人姓名",allowEmptyValue=true,example="",allowableValues="")
	String createUserName;

	
	@ApiModelProperty(notes="最后更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date updateTime;

	
	@ApiModelProperty(notes="最后更新Id",allowEmptyValue=true,example="",allowableValues="")
	String updateUserId;

	
	@ApiModelProperty(notes="最后更新姓名",allowEmptyValue=true,example="",allowableValues="")
	String updateUserName;

	
	@ApiModelProperty(notes="最后联系客户时间",allowEmptyValue=true,example="",allowableValues="")
	Date lastContactTime;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *客户id
	 **/
	public Customer(String customerId) {
		this.customerId = customerId;
	}
    
    /**
     * 客户信息表
     **/
	public Customer() {
	}

}