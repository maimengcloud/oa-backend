package com.oa.erp.pur.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("pur_supplier")
@ApiModel(description="供应商-作废，使用相对方表")
public class Supplier  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="供应商名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="公司统一信用代码证",allowEmptyValue=true,example="",allowableValues="")
	String companyNo;

	
	@ApiModelProperty(notes="公司联系人",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="联系人名称",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="公司地址",allowEmptyValue=true,example="",allowableValues="")
	String companyAddress;

	
	@ApiModelProperty(notes="归属地区",allowEmptyValue=true,example="",allowableValues="")
	String districtId;

	
	@ApiModelProperty(notes="公司定位",allowEmptyValue=true,example="",allowableValues="")
	String addressGps;

	
	@ApiModelProperty(notes="归属机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="归属机构名称",allowEmptyValue=true,example="",allowableValues="")
	String branchName;

	
	@ApiModelProperty(notes="联系电话",allowEmptyValue=true,example="",allowableValues="")
	String userPhone;

	/**
	 *编号
	 **/
	public Supplier(String id) {
		this.id = id;
	}
    
    /**
     * 供应商-作废，使用相对方表
     **/
	public Supplier() {
	}

}