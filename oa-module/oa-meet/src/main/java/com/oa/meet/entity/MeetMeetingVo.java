package com.oa.meet.entity;

import io.swagger.annotations.ApiModel;

import java.util.List;

/**
 * 组织 com.qqkj  顶级模块 oa 大模块 meet  小模块 <br> 
 * 实体 MeetMeeting所有属性名: <br>
 *	id,name,categoryId,categoryName,requireUserid,requireUsername,roomId,roomName,customRoomName,startTime,endTime,reminders,remindersImmediate,remindersBeforeStart,remindersBeforeEnd,otherAttender,attenderNum,customerNum,isUsingReturn,isUsingSingin,singinMode,singinUserid,singinUsername,singinWifi,branchId,deptId,deptName,requireRemark,startRemark,createTime,status,approveSuggestion;<br>
 * 表 OA.meet_meeting meet_meeting的所有字段名: <br>
 *	id,name,category_id,category_name,require_userid,require_username,room_id,room_name,custom_room_name,start_time,end_time,reminders,reminders_immediate,reminders_before_start,reminders_before_end,other_attender,attender_num,customer_num,is_using_return,is_using_singin,singin_mode,singin_userid,singin_username,singin_wifi,branch_id,dept_id,dept_name,require_remark,start_remark,create_time,status,approve_suggestion;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
@ApiModel(description="MeetMeetingVo")
public class MeetMeetingVo extends MeetMeeting {

	List<MeetMeetingAttender> attenders;

	List<MeetMeetingAppendix> meetMeetingAppendix;

	public List<MeetMeetingAttender> getAttenders() {
		return attenders;
	}

	public void setAttenders(List<MeetMeetingAttender> attenders) {
		this.attenders = attenders;
	}




	public List<MeetMeetingAppendix> getMeetMeetingAppendix() {
		return meetMeetingAppendix;
	}

	public void setMeetMeetingAppendix(List<MeetMeetingAppendix> meetMeetingAppendix) {
		this.meetMeetingAppendix = meetMeetingAppendix;
	}


}