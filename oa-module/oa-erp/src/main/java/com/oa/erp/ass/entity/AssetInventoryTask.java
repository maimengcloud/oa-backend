package com.oa.erp.ass.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ass_asset_inventory_task")
@ApiModel(description="ass_asset_inventory_task")
public class AssetInventoryTask  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="盘点任务id,主键",allowEmptyValue=true,example="",allowableValues="")
	String taskId;

	
	@ApiModelProperty(notes="申请人姓名",allowEmptyValue=true,example="",allowableValues="")
	String reqUsername;

	
	@ApiModelProperty(notes="申请人部门",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptid;

	
	@ApiModelProperty(notes="申请人部门名称",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptName;

	
	@ApiModelProperty(notes="申请标题",allowEmptyValue=true,example="",allowableValues="")
	String reqTitle;

	
	@ApiModelProperty(notes="申请机构id",allowEmptyValue=true,example="",allowableValues="")
	String reqBranchId;

	
	@ApiModelProperty(notes="申请日期",allowEmptyValue=true,example="",allowableValues="")
	Date reqDate;

	
	@ApiModelProperty(notes="分配用户",allowEmptyValue=true,example="",allowableValues="")
	String allocationType;

	
	@ApiModelProperty(notes="资产类别id",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="资产类别名称",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	
	@ApiModelProperty(notes="仓库id",allowEmptyValue=true,example="",allowableValues="")
	String warehouseId;

	
	@ApiModelProperty(notes="仓库名称",allowEmptyValue=true,example="",allowableValues="")
	String warehouseName;

	
	@ApiModelProperty(notes="采购时间",allowEmptyValue=true,example="",allowableValues="")
	String purchasedateType;

	
	@ApiModelProperty(notes="开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	
	@ApiModelProperty(notes="结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="notStart未开始",allowEmptyValue=true,example="",allowableValues="")
	String taskStatus;

	
	@ApiModelProperty(notes="hold",allowEmptyValue=true,example="",allowableValues="")
	String reqStatus;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String reqReason;

	/**
	 *盘点任务id
	 **/
	public AssetInventoryTask(String taskId) {
		this.taskId = taskId;
	}
    
    /**
     * ass_asset_inventory_task
     **/
	public AssetInventoryTask() {
	}

}