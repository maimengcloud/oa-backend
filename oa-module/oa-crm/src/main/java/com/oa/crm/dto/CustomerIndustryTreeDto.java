package com.oa.crm.dto;

import com.google.common.collect.Lists;
import com.oa.crm.entity.CustomerIndustry;
import org.springframework.beans.BeanUtils;

import java.util.List;


public class CustomerIndustryTreeDto extends CustomerIndustry{

    private List<CustomerIndustryTreeDto> children = Lists.newArrayList();

    public static CustomerIndustryTreeDto adapt(CustomerIndustry source) {
        CustomerIndustryTreeDto customerIndustryTreeDto = new CustomerIndustryTreeDto();
        BeanUtils.copyProperties(source, customerIndustryTreeDto);
        return customerIndustryTreeDto;
    }

    public List<CustomerIndustryTreeDto> getChildren() {
        return children;
    }

    public void setChildren(List<CustomerIndustryTreeDto> children) {
        this.children = children;
    }

}
