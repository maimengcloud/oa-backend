package com.oa.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月7日
 */
@Data
@TableName("car_require_car_driver")
@ApiModel(description="司机汽车关联表")
public class CarRequireCarDriver  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="汽车编号",allowEmptyValue=true,example="",allowableValues="")
	String carId;

	
	@ApiModelProperty(notes="司机编号",allowEmptyValue=true,example="",allowableValues="")
	String driverId;

	
	@ApiModelProperty(notes="开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	
	@ApiModelProperty(notes="结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="状态1启用0禁用",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="新增时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="申请编号",allowEmptyValue=true,example="",allowableValues="")
	String requireId;

	
	@ApiModelProperty(notes="还车时间",allowEmptyValue=true,example="",allowableValues="")
	Date giveBackTime;

	
	@ApiModelProperty(notes="是否已还车0否1是",allowEmptyValue=true,example="",allowableValues="")
	String giveBacked;

	
	@ApiModelProperty(notes="还车用户编号",allowEmptyValue=true,example="",allowableValues="")
	String giveBackUserid;

	
	@ApiModelProperty(notes="还车人姓名",allowEmptyValue=true,example="",allowableValues="")
	String giveBackUsername;

	
	@ApiModelProperty(notes="还车说明",allowEmptyValue=true,example="",allowableValues="")
	String giveBackRemark;

	
	@ApiModelProperty(notes="是否来自于模板，是则保存id",allowEmptyValue=true,example="",allowableValues="")
	String isTpl;

	/**
	 *主键
	 **/
	public CarRequireCarDriver(String id) {
		this.id = id;
	}
    
    /**
     * 司机汽车关联表
     **/
	public CarRequireCarDriver() {
	}

}