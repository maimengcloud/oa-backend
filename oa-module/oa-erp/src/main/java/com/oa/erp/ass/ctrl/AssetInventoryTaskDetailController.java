package com.oa.erp.ass.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.erp.ass.entity.AssetInventoryTask;
import com.oa.erp.ass.entity.AssetInventoryTaskDetail;
import com.oa.erp.ass.service.AssetInventoryTaskDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/erp/ass/assetInventoryTaskDetail")
@Api(tags = {"ass_asset_inventory_task_detail-操作接口"})
public class AssetInventoryTaskDetailController {

    static Logger logger = LoggerFactory.getLogger(AssetInventoryTaskDetailController.class);

    @Autowired
    private AssetInventoryTaskDetailService assetInventoryTaskDetailService;

    @ApiOperation(value = "ass_asset_inventory_task_detail-查询列表", notes = " ")
    @ApiEntityParams(AssetInventoryTaskDetail.class)
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTaskDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listAssetInventoryTaskDetail(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<AssetInventoryTaskDetail> qw = QueryTools.initQueryWrapper(AssetInventoryTaskDetail.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> assetInventoryTaskDetailList = assetInventoryTaskDetailService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(assetInventoryTaskDetailList).setTotal(page.getTotal());
    }

    @ApiOperation(value = "根据状态获取盘点任务下各状态的数量", notes = "获取盘点任务明细")
    @ApiResponses({
            @ApiResponse(code = 200, response = AssetInventoryTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/getTaskCountByStatus", method = RequestMethod.GET)
    public Result getTaskCountByStatus(@RequestParam Map<String, Object> assetInventoryTask) {
        RequestUtils.transformArray(assetInventoryTask, "taskIds");
        long count = assetInventoryTaskDetailService.countByWhere((AssetInventoryTaskDetail) assetInventoryTask);
        return Result.ok("query-ok", "查询成功").setData(count);
    }


    @ApiOperation(value = "ass_asset_inventory_task_detail-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTaskDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addAssetInventoryTaskDetail(@RequestBody AssetInventoryTaskDetail assetInventoryTaskDetail) {
        assetInventoryTaskDetailService.save(assetInventoryTaskDetail);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "ass_asset_inventory_task_detail-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delAssetInventoryTaskDetail(@RequestBody AssetInventoryTaskDetail assetInventoryTaskDetail) {
        assetInventoryTaskDetailService.removeById(assetInventoryTaskDetail);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "ass_asset_inventory_task_detail-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTaskDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editAssetInventoryTaskDetail(@RequestBody AssetInventoryTaskDetail assetInventoryTaskDetail) {
        assetInventoryTaskDetailService.updateById(assetInventoryTaskDetail);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "ass_asset_inventory_task_detail-批量修改某些字段", notes = "")
    @ApiEntityParams(value = AssetInventoryTaskDetail.class, props = {}, remark = "ass_asset_inventory_task_detail", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTaskDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        assetInventoryTaskDetailService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "ass_asset_inventory_task_detail-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelAssetInventoryTaskDetail(@RequestBody List<AssetInventoryTaskDetail> assetInventoryTaskDetails) {
        User user = LoginUtils.getCurrentUserInfo();
        if (assetInventoryTaskDetails.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<AssetInventoryTaskDetail> datasDb = assetInventoryTaskDetailService.listByIds(assetInventoryTaskDetails.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<AssetInventoryTaskDetail> can = new ArrayList<>();
        List<AssetInventoryTaskDetail> no = new ArrayList<>();
        for (AssetInventoryTaskDetail data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            assetInventoryTaskDetailService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ass_asset_inventory_task_detail-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetInventoryTaskDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(AssetInventoryTaskDetail assetInventoryTaskDetail) {
        AssetInventoryTaskDetail data = (AssetInventoryTaskDetail) assetInventoryTaskDetailService.getById(assetInventoryTaskDetail);
        return Result.ok().setData(data);
    }

}
