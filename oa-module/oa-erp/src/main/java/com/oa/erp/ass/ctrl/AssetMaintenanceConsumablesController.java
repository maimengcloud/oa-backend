package com.oa.erp.ass.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.erp.ass.entity.AssetMaintenanceConsumables;
import com.oa.erp.ass.service.AssetMaintenanceConsumablesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/erp/ass/assetMaintenanceConsumables")
@Api(tags = {"资产维修/保养单配件耗材表-操作接口"})
public class AssetMaintenanceConsumablesController {

    static Logger logger = LoggerFactory.getLogger(AssetMaintenanceConsumablesController.class);

    @Autowired
    private AssetMaintenanceConsumablesService assetMaintenanceConsumablesService;

    @ApiOperation(value = "资产维修/保养单配件耗材表-查询列表", notes = " ")
    @ApiEntityParams(AssetMaintenanceConsumables.class)
    @ApiResponses({@ApiResponse(code = 200, response = AssetMaintenanceConsumables.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listAssetMaintenanceConsumables(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<AssetMaintenanceConsumables> qw = QueryTools.initQueryWrapper(AssetMaintenanceConsumables.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = assetMaintenanceConsumablesService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ApiOperation(value = "查询耗材通过修理单id", notes = "getAssetMaintenanceConsumablesByRequireId,条件之间是 and关系,模糊查询写法如 {studentName:'%才哥%'}")
    @ApiResponses({@ApiResponse(code = 200, response = AssetMaintenanceConsumables.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/getDataByRequireId", method = RequestMethod.GET)
    public Result getAssetMaintenanceConsumablesByRequireId(@RequestParam Map<String, Object> assetMaintenanceConsumables) {
        Map<String, Object> params = new HashMap<>();
        QueryWrapper<AssetMaintenanceConsumables> qw = QueryTools.initQueryWrapper(AssetMaintenanceConsumables.class, params);
        IPage page = QueryTools.initPage(params);
        params.put("requireId", assetMaintenanceConsumables.get("requireId"));
        List<Map<String, Object>> assetMaintenanceConsumablesList = assetMaintenanceConsumablesService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(assetMaintenanceConsumablesList);
    }

    @ApiOperation(value = "资产维修/保养单配件耗材表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetMaintenanceConsumables.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addAssetMaintenanceConsumables(@RequestBody AssetMaintenanceConsumables assetMaintenanceConsumables) {
        assetMaintenanceConsumablesService.save(assetMaintenanceConsumables);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "资产维修/保养单配件耗材表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delAssetMaintenanceConsumables(@RequestBody AssetMaintenanceConsumables assetMaintenanceConsumables) {
        assetMaintenanceConsumablesService.removeById(assetMaintenanceConsumables);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "资产维修/保养单配件耗材表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetMaintenanceConsumables.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editAssetMaintenanceConsumables(@RequestBody AssetMaintenanceConsumables assetMaintenanceConsumables) {
        assetMaintenanceConsumablesService.updateById(assetMaintenanceConsumables);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "资产维修/保养单配件耗材表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = AssetMaintenanceConsumables.class, props = {}, remark = "资产维修/保养单配件耗材表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = AssetMaintenanceConsumables.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        assetMaintenanceConsumablesService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "资产维修/保养单配件耗材表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelAssetMaintenanceConsumables(@RequestBody List<AssetMaintenanceConsumables> assetMaintenanceConsumabless) {
        User user = LoginUtils.getCurrentUserInfo();
        if (assetMaintenanceConsumabless.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<AssetMaintenanceConsumables> datasDb = assetMaintenanceConsumablesService.listByIds(assetMaintenanceConsumabless.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<AssetMaintenanceConsumables> can = new ArrayList<>();
        List<AssetMaintenanceConsumables> no = new ArrayList<>();
        for (AssetMaintenanceConsumables data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            assetMaintenanceConsumablesService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "资产维修/保养单配件耗材表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetMaintenanceConsumables.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(AssetMaintenanceConsumables assetMaintenanceConsumables) {
        AssetMaintenanceConsumables data = (AssetMaintenanceConsumables) assetMaintenanceConsumablesService.getById(assetMaintenanceConsumables);
        return Result.ok().setData(data);
    }

}
