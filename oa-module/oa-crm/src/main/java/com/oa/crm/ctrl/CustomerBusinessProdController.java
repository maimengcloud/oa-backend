package com.oa.crm.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.crm.entity.CustomerBusinessProd;
import com.oa.crm.service.CustomerBusinessProdService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/crm/customerBusinessProd")
@Api(tags = {"crm_customer_business_prod-操作接口"})
public class CustomerBusinessProdController {

    static Logger logger = LoggerFactory.getLogger(CustomerBusinessProdController.class);

    @Autowired
    private CustomerBusinessProdService customerBusinessProdService;

    @ApiOperation(value = "crm_customer_business_prod-查询列表", notes = " ")
    @ApiEntityParams(CustomerBusinessProd.class)
    @ApiResponses({@ApiResponse(code = 200, response = CustomerBusinessProd.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCustomerBusinessProd(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CustomerBusinessProd> qw = QueryTools.initQueryWrapper(CustomerBusinessProd.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = customerBusinessProdService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "crm_customer_business_prod-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerBusinessProd.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCustomerBusinessProd(@RequestBody CustomerBusinessProd customerBusinessProd) {
        customerBusinessProdService.save(customerBusinessProd);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "crm_customer_business_prod-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCustomerBusinessProd(@RequestBody CustomerBusinessProd customerBusinessProd) {
        customerBusinessProdService.removeById(customerBusinessProd);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "crm_customer_business_prod-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerBusinessProd.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCustomerBusinessProd(@RequestBody CustomerBusinessProd customerBusinessProd) {
        customerBusinessProdService.updateById(customerBusinessProd);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "crm_customer_business_prod-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CustomerBusinessProd.class, props = {}, remark = "crm_customer_business_prod", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerBusinessProd.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        customerBusinessProdService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "crm_customer_business_prod-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCustomerBusinessProd(@RequestBody List<CustomerBusinessProd> customerBusinessProds) {
        User user = LoginUtils.getCurrentUserInfo();
        if (customerBusinessProds.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CustomerBusinessProd> datasDb = customerBusinessProdService.listByIds(customerBusinessProds.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CustomerBusinessProd> can = new ArrayList<>();
        List<CustomerBusinessProd> no = new ArrayList<>();
        for (CustomerBusinessProd data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            customerBusinessProdService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "crm_customer_business_prod-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerBusinessProd.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CustomerBusinessProd customerBusinessProd) {
        CustomerBusinessProd data = (CustomerBusinessProd) customerBusinessProdService.getById(customerBusinessProd);
        return Result.ok().setData(data);
    }

}
