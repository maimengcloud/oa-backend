package com.oa.sig.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("sig_device")
@ApiModel(description="印章设备库")
public class SigDevice  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="印章设备编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="设备名称",allowEmptyValue=true,example="",allowableValues="")
	String deviceName;

	
	@ApiModelProperty(notes="归属机构",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="归属部门",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="归属部门",allowEmptyValue=true,example="",allowableValues="")
	String deptName;

	
	@ApiModelProperty(notes="归属机构名称",allowEmptyValue=true,example="",allowableValues="")
	String branchName;

	
	@ApiModelProperty(notes="设备标识码",allowEmptyValue=true,example="",allowableValues="")
	String deviceSn;

	
	@ApiModelProperty(notes="品牌编号",allowEmptyValue=true,example="",allowableValues="")
	String brandId;

	
	@ApiModelProperty(notes="品牌名称",allowEmptyValue=true,example="",allowableValues="")
	String brandName;

	
	@ApiModelProperty(notes="启用时间",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	
	@ApiModelProperty(notes="停用时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="状态0停用1启用",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="保管地址",allowEmptyValue=true,example="",allowableValues="")
	String address;

	
	@ApiModelProperty(notes="地址定位",allowEmptyValue=true,example="",allowableValues="")
	String addressGps;

	
	@ApiModelProperty(notes="设备类型0便携机1台式机2普通印章",allowEmptyValue=true,example="",allowableValues="")
	String deviceType;

	
	@ApiModelProperty(notes="流程状态",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	
	@ApiModelProperty(notes="流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="管理用户编号",allowEmptyValue=true,example="",allowableValues="")
	String admUserid;

	
	@ApiModelProperty(notes="管理员姓名",allowEmptyValue=true,example="",allowableValues="")
	String admUsername;

	
	@ApiModelProperty(notes="管理部门编号",allowEmptyValue=true,example="",allowableValues="")
	String admDeptid;

	
	@ApiModelProperty(notes="管理部门名称",allowEmptyValue=true,example="",allowableValues="")
	String admDeptName;

	
	@ApiModelProperty(notes="管理机构号",allowEmptyValue=true,example="",allowableValues="")
	String admBranchId;

	/**
	 *印章设备编号
	 **/
	public SigDevice(String id) {
		this.id = id;
	}
    
    /**
     * 印章设备库
     **/
	public SigDevice() {
	}

}