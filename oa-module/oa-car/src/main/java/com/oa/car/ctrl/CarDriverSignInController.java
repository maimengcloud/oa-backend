package com.oa.car.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.car.entity.CarDriverSignIn;
import com.oa.car.service.CarDriverSignInService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@RestController
@RequestMapping(value = "/*/oa/car/carDriverSignIn")
@Api(tags = {"car_driver_sign_in-操作接口"})
public class CarDriverSignInController {

    static Logger logger = LoggerFactory.getLogger(CarDriverSignInController.class);

    @Autowired
    private CarDriverSignInService carDriverSignInService;

    @ApiOperation(value = "car_driver_sign_in-查询列表", notes = " ")
    @ApiEntityParams(CarDriverSignIn.class)
    @ApiResponses({@ApiResponse(code = 200, response = CarDriverSignIn.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCarDriverSignIn(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CarDriverSignIn> qw = QueryTools.initQueryWrapper(CarDriverSignIn.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = carDriverSignInService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "car_driver_sign_in-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarDriverSignIn.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCarDriverSignIn(@RequestBody CarDriverSignIn carDriverSignIn) {
        

        if (StringUtils.isEmpty(carDriverSignIn.getAddressGps())) {
            return Result.error("打卡位置错误");
        }
        if (StringUtils.isEmpty(carDriverSignIn.getCreateTime())) {
            carDriverSignIn.setCreateTime(new Date());
        }


        if (StringUtils.isEmpty(carDriverSignIn.getId())) {
            carDriverSignIn.setId(carDriverSignInService.createKey("id"));
        } else {
            CarDriverSignIn carDriverSignInQuery = new CarDriverSignIn(carDriverSignIn.getId());
            if (carDriverSignInService.countByWhere(carDriverSignInQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        carDriverSignInService.insert(carDriverSignIn);
        return Result.ok("add-ok", "添加成功！").setData(carDriverSignIn);
    }

    @ApiOperation(value = "car_driver_sign_in-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCarDriverSignIn(@RequestBody CarDriverSignIn carDriverSignIn) {
        carDriverSignInService.removeById(carDriverSignIn);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "car_driver_sign_in-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarDriverSignIn.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCarDriverSignIn(@RequestBody CarDriverSignIn carDriverSignIn) {
        if (StringUtils.isEmpty(carDriverSignIn.getId())) {
            return Result.error("主键为空，不能修改");
        }
        carDriverSignInService.updateById(carDriverSignIn);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "car_driver_sign_in-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CarDriverSignIn.class, props = {}, remark = "car_driver_sign_in", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CarDriverSignIn.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        carDriverSignInService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "car_driver_sign_in-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCarDriverSignIn(@RequestBody List<CarDriverSignIn> carDriverSignIns) {
        User user = LoginUtils.getCurrentUserInfo();
        if (carDriverSignIns.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CarDriverSignIn> datasDb = carDriverSignInService.listByIds(carDriverSignIns.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CarDriverSignIn> can = new ArrayList<>();
        List<CarDriverSignIn> no = new ArrayList<>();
        for (CarDriverSignIn data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            carDriverSignInService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "car_driver_sign_in-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarDriverSignIn.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CarDriverSignIn carDriverSignIn) {
        CarDriverSignIn data = (CarDriverSignIn) carDriverSignInService.getById(carDriverSignIn);
        return Result.ok().setData(data);
    }

}
