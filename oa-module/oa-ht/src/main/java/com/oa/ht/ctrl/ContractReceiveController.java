package com.oa.ht.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.ht.entity.*;
import com.oa.ht.service.ContractConditionService;
import com.oa.ht.service.ContractReceiveInvoiceService;
import com.oa.ht.service.ContractReceiveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/ht/contractReceive")
@Api(tags = {"ht_contract_receive-操作接口"})
public class ContractReceiveController {

    static Logger logger = LoggerFactory.getLogger(ContractReceiveController.class);

    @Autowired
    private ContractReceiveService contractReceiveService;

    @Autowired
    private ContractConditionService contractConditionService;


    @Autowired
    private ContractReceiveInvoiceService contractReceiveInvoiceService;


    @ApiOperation(value = "ht_contract_receive-查询列表", notes = " ")
    @ApiEntityParams(ContractReceive.class)
    @ApiResponses({@ApiResponse(code = 200, response = ContractReceive.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listContractReceive(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<ContractReceive> qw = QueryTools.initQueryWrapper(ContractReceive.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = contractReceiveService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "ht_contract_receive-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractReceive.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addContractReceive(@RequestBody ContractReceiveVo contractReceive) {
        if (contractReceive.getAmount() == null || contractReceive.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            return Result.error("amount-is-zero", "收款金额不能为空，并且必须大于0");
        }
        if (StringUtils.isEmpty(contractReceive.getId())) {
            contractReceive.setId(contractReceiveService.createKey("id"));
        } else {
            ContractReceive contractReceiveQuery = new ContractReceive(contractReceive.getId());
            if (contractReceiveService.countByWhere(contractReceiveQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        if (!StringUtils.hasText(contractReceive.getConditionId())) {
            return Result.error("conditionId-is-null", "conditionId不能为空");
        }
        ContractCondition contractCondition = new ContractCondition(contractReceive.getConditionId());
        contractCondition = this.contractConditionService.selectOneObject(contractCondition);
        if (contractCondition == null) {
            return Result.error("condition-not-exists", "款项不存在");
        } else {
            if (contractCondition.getFinishAmount() == null) {
                contractCondition.setFinishAmount(BigDecimal.ZERO);
            }
            if ("2".equals(contractCondition.getConditionStatus())) {
                return Result.error("all-is-finish", "所有应收金额已完成，不能再收款");
            }
            if (contractCondition.getConditionAmount().compareTo(contractCondition.getFinishAmount()) <= 0) {
                return Result.error("all-is-finish", "所有应收金额已完成，不能再收款");
            }
            if (contractCondition.getConditionAmount().subtract(contractCondition.getFinishAmount()).compareTo(contractReceive.getAmount()) < 0) {
                return Result.error("amount-is-too-big", "本次收款金额大于未收款额，不能提交");
            }
        }
        BigDecimal allReceiveAmount = this.contractConditionService.getAllReceiveAmountByConditionId(contractCondition.getConditionId());
        if (allReceiveAmount.compareTo(contractCondition.getConditionAmount()) >= 0) {
            return Result.error("amount-is-too-big", "所有已收款及审核中，审核失败的收款总额已超出应收总额，不能提交");
        }
        if (allReceiveAmount.add(contractReceive.getAmount()).compareTo(contractCondition.getConditionAmount()) > 0) {
            return Result.error("amount-is-too-big", "本次收款金额大于未收款额，不能提交");
        }
        User user = LoginUtils.getCurrentUserInfo();
        contractReceive.setCreateUserId(user.getUserid());
        contractReceive.setCreateUserName(user.getUsername());
        contractReceive.setCreateTime(new Date());
        contractReceive.setBranchId(user.getBranchId());
        contractReceiveService.add(contractReceive);
        return Result.ok("add-ok", "添加成功！").setData(contractReceive);
    }

    @ApiOperation(value = "ht_contract_receive-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delContractReceive(@RequestBody ContractReceive contractReceive) {
        ContractReceive contractReceiveDb = this.contractReceiveService.selectOneObject(new ContractReceive(contractReceive.getId()));
        if ("1".equals(contractReceiveDb.getReceiveStatus())) {
            return Result.error("recceive-status-is-finish", "已完成收款，不能删除");
        }
        if ("1".equals(contractReceiveDb.getBizFlowState())) {
            return Result.error("BizFlowState-is-1", "审批中，不能删除");
        }
        contractReceiveService.deleteByPk(contractReceive);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "收款关联发票", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractPaymentInvoice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/linkInvoices", method = RequestMethod.POST)
    public Result receiveLinkInvoices(@RequestBody ReceiveLinkInvoicesVo linkInvoices) {
        this.contractReceiveInvoiceService.receiveLinkInvoices(linkInvoices);
        return Result.ok("", "成功更新一条数据");
    }

    @ApiOperation(value = "ht_contract_receive-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractReceive.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editContractReceive(@RequestBody ContractReceiveVo contractReceive) {
        ContractReceive contractReceiveDb = this.contractReceiveService.selectOneObject(new ContractReceive(contractReceive.getId()));
        if ("1".equals(contractReceiveDb.getReceiveStatus())) {
            return Result.error("recceive-status-is-finish", "已完成收款，不能再修改");
        }
        if ("1".equals(contractReceiveDb.getBizFlowState())) {
            return Result.error("BizFlowState-is-1", "审批中，不能修改");
        }
        ContractCondition contractCondition = new ContractCondition(contractReceive.getConditionId());
        contractCondition = this.contractConditionService.selectOneObject(contractCondition);
        if (contractCondition == null) {
            return Result.error("condition-not-exists", "款项不存在");
        } else {
            if (contractCondition.getFinishAmount() == null) {
                contractCondition.setFinishAmount(BigDecimal.ZERO);
            }
            if ("2".equals(contractCondition.getConditionStatus())) {
                return Result.error("all-is-finish", "所有应收金额已完成，不能再收款");
            }
            if (contractCondition.getConditionAmount().compareTo(contractCondition.getFinishAmount()) <= 0) {
                return Result.error("all-is-finish", "所有应收金额已完成，不能再收款");
            }
            if (contractCondition.getConditionAmount().subtract(contractCondition.getFinishAmount()).compareTo(contractReceive.getAmount()) < 0) {
                return Result.error("amount-is-too-big", "本次收款金额大于未收款额，不能提交");
            }
        }
        BigDecimal allReceiveAmount = this.contractConditionService.getAllReceiveAmountByConditionId(contractCondition.getConditionId());
        allReceiveAmount = allReceiveAmount.subtract(contractReceiveDb.getAmount()).add(contractReceive.getAmount());
        if (allReceiveAmount.compareTo(contractCondition.getConditionAmount()) > 0) {
            return Result.error("amount-is-too-big", "所有已收款及审核中，审核失败的收款总额已超出应收总额，不能提交");
        }
        contractReceive.setReceiveStatus(contractReceiveDb.getReceiveStatus());
        contractReceive.setBizFlowState(contractReceiveDb.getBizFlowState());
        contractReceiveService.editContractReceive(contractReceive);
        return Result.ok("edit-ok", "修改成功！").setData(contractReceive);
    }

    @ApiOperation(value = "ht_contract_receive-批量修改某些字段", notes = "")
    @ApiEntityParams(value = ContractReceive.class, props = {}, remark = "ht_contract_receive", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = ContractReceive.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        contractReceiveService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }


    //@AuditLog(firstMenu="合同管理",secondMenu="付款管理",func="processApprova",funcDesc="付款审批",operType= OperType.UPDATE)
    @RequestMapping(value = "/processApprova", method = RequestMethod.POST)
    public Result processApprova(@RequestBody Map<String, Object> flowVars) {
        this.contractReceiveService.processApprova(flowVars);
        logger.debug("procInstId=====" + flowVars.get("procInstId"));
        return Result.ok("", "成功提交审批");
    }

    @ApiOperation(value = "ht_contract_receive-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelContractReceive(@RequestBody List<ContractReceive> contractReceives) {
        User user = LoginUtils.getCurrentUserInfo();
        if (contractReceives.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<ContractReceive> datasDb = contractReceiveService.listByIds(contractReceives.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<ContractReceive> can = new ArrayList<>();
        List<ContractReceive> no = new ArrayList<>();
        for (ContractReceive data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            contractReceiveService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ht_contract_receive-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractReceive.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(ContractReceive contractReceive) {
        ContractReceive data = (ContractReceive) contractReceiveService.getById(contractReceive);
        return Result.ok().setData(data);
    }

}
