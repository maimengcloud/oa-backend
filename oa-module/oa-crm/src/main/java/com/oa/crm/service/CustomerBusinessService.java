package com.oa.crm.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.crm.entity.CustomerBusiness;
import com.oa.crm.enums.CustomerBusinessEnum;
import com.oa.crm.mapper.CustomerBusinessMapper;
import com.oa.crm.vo.CustomerBusinessAddVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class CustomerBusinessService extends BaseService<CustomerBusinessMapper, CustomerBusiness> {
    static Logger logger = LoggerFactory.getLogger(CustomerBusinessService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    public void isAttention(Map<String, Object> customerBusiness) {
        baseMapper.updateCustomerBusinessAttention(customerBusiness);
    }

    /**
     * 插入客户商机
     *
     * @param customerBusinessAddVo
     */
    @Transactional
    public void insertCustomerBusiness(CustomerBusinessAddVo customerBusinessAddVo) {

        User user = LoginUtils.getCurrentUserInfo();

        //1.添加客户商机
        CustomerBusiness customerBusiness = customerBusinessAddVo.getCustomerBusiness();
        customerBusiness.setBusinessCreateId(user.getUserid());
        customerBusiness.setBusinessCreateName(user.getUsername());
        customerBusiness.setBusinessCreateTime(new Date());
        customerBusiness.setBranchId(user.getBranchId());
        //归档状态
        customerBusiness.setArchiveStatus(CustomerBusinessEnum.ARCHIVE_STATUS_RUNNING.getCode());
        this.insert(customerBusiness);

        //2.添加关联产品


    }

    /**
     * 修改客户商机
     *
     * @param customerBusinessAddVo
     */
    @Transactional
    public void updateCustomerBusiness(CustomerBusinessAddVo customerBusinessAddVo) {

        CustomerBusiness customerBusiness = customerBusinessAddVo.getCustomerBusiness();

        //更新客户商机
        this.updateByPk(customerBusiness);


        //2.更新客户商机关联产品

    }

}

