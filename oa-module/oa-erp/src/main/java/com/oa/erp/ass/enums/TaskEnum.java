package com.oa.erp.ass.enums;

public enum TaskEnum {

    NOT_START("notStart", "未开始"),
    RUNNING("running", "运行中"),
    OVER("over", "完成"),
    ALLOCATION_LIABLE("1", "资产盘点权限-只能责任人进行盘点"),
    ALLOCATION_LIABLE_AND_SELECTUSER("2", "资产盘点权限-只能责任人和选中用户进行盘点"),
    ALLOCATION_SELECTUSER("3", "资产盘点权限-只能选中用户进行盘点");


    TaskEnum(String status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    private String status;

    private String desc;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
