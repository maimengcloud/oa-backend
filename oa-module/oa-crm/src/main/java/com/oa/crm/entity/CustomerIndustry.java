package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_industry")
@ApiModel(description="客户行业表")
public class CustomerIndustry  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="行业id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="上级id",allowEmptyValue=true,example="",allowableValues="")
	String parentId;

	
	@ApiModelProperty(notes="行业名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="层级",allowEmptyValue=true,example="",allowableValues="")
	String level;

	
	@ApiModelProperty(notes="排序",allowEmptyValue=true,example="",allowableValues="")
	Integer seq;

	/**
	 *行业id
	 **/
	public CustomerIndustry(String id) {
		this.id = id;
	}
    
    /**
     * 客户行业表
     **/
	public CustomerIndustry() {
	}

}