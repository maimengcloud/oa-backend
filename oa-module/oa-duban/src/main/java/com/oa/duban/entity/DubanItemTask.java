package com.oa.duban.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("duban_item_task")
@ApiModel(description="督办任务表")
public class DubanItemTask  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="督办主键",allowEmptyValue=true,example="",allowableValues="")
	String dubanId;

	
	@ApiModelProperty(notes="事项分类id",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="事项分类",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	
	@ApiModelProperty(notes="任务名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="任务开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	
	@ApiModelProperty(notes="任务结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="创建人id",allowEmptyValue=true,example="",allowableValues="")
	String createUserid;

	
	@ApiModelProperty(notes="创建人名称",allowEmptyValue=true,example="",allowableValues="")
	String createUsername;

	
	@ApiModelProperty(notes="主办单位id",allowEmptyValue=true,example="",allowableValues="")
	String hostDeptId;

	
	@ApiModelProperty(notes="主办单位",allowEmptyValue=true,example="",allowableValues="")
	String hostDeptName;

	
	@ApiModelProperty(notes="主办id",allowEmptyValue=true,example="",allowableValues="")
	String hostId;

	
	@ApiModelProperty(notes="主办",allowEmptyValue=true,example="",allowableValues="")
	String hostName;

	
	@ApiModelProperty(notes="任务描述",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="状态0已申报1进行中2已完成3已超时",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="进度",allowEmptyValue=true,example="",allowableValues="")
	String progress;

	/**
	 *主键
	 **/
	public DubanItemTask(String id) {
		this.id = id;
	}
    
    /**
     * 督办任务表
     **/
	public DubanItemTask() {
	}

}