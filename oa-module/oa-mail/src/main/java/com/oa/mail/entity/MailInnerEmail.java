package com.oa.mail.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("mail_inner_email")
@ApiModel(description="内部邮件表")
public class MailInnerEmail  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="主题",allowEmptyValue=true,example="",allowableValues="")
	String subject;

	
	@ApiModelProperty(notes="正文",allowEmptyValue=true,example="",allowableValues="")
	String content;

	
	@ApiModelProperty(notes="用户id",allowEmptyValue=true,example="",allowableValues="")
	String userId;

	
	@ApiModelProperty(notes="发用户名字",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="时间",allowEmptyValue=true,example="",allowableValues="")
	Date sendTime;

	
	@ApiModelProperty(notes="邮件类型,0草稿箱,1已发送,2收件箱,3垃圾箱",allowEmptyValue=true,example="",allowableValues="")
	String type;

	
	@ApiModelProperty(notes="是否为星标",allowEmptyValue=true,example="",allowableValues="")
	String isStar;

	
	@ApiModelProperty(notes="是否已读",allowEmptyValue=true,example="",allowableValues="")
	String isRead;

	
	@ApiModelProperty(notes="发送状态",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="接受类型",allowEmptyValue=true,example="",allowableValues="")
	String receiveType;

	
	@ApiModelProperty(notes="机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="收件人字符串",allowEmptyValue=true,example="",allowableValues="")
	String addressee;

	
	@ApiModelProperty(notes="抄送人字符串",allowEmptyValue=true,example="",allowableValues="")
	String cc;

	
	@ApiModelProperty(notes="密送人字符串",allowEmptyValue=true,example="",allowableValues="")
	String bcc;

	/**
	 *主键
	 **/
	public MailInnerEmail(String id) {
		this.id = id;
	}
    
    /**
     * 内部邮件表
     **/
	public MailInnerEmail() {
	}

}