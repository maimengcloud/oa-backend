package com.oa.jx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("jx_assess_grade")
@ApiModel(description="考核总分表")
public class JxAssessGrade  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="组织id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="方案总分",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalPoints;

	
	@ApiModelProperty(notes="考核评价",allowEmptyValue=true,example="",allowableValues="")
	String evaluate;

	
	@ApiModelProperty(notes="附件",allowEmptyValue=true,example="",allowableValues="")
	String accessory;

	
	@ApiModelProperty(notes="考核人id",allowEmptyValue=true,example="",allowableValues="")
	String assUserid;

	
	@ApiModelProperty(notes="年份",allowEmptyValue=true,example="",allowableValues="")
	String year;

	
	@ApiModelProperty(notes="月份",allowEmptyValue=true,example="",allowableValues="")
	String month;

	
	@ApiModelProperty(notes="季度：1-第一季度，2-第二季度，3-第三季度，4-第四季度",allowEmptyValue=true,example="",allowableValues="")
	String quarter;

	
	@ApiModelProperty(notes="半年度：1-上半年度，2-下半年度",allowEmptyValue=true,example="",allowableValues="")
	String semiAnnual;

	
	@ApiModelProperty(notes="年度",allowEmptyValue=true,example="",allowableValues="")
	String annual;

	
	@ApiModelProperty(notes="所属考核方案",allowEmptyValue=true,example="",allowableValues="")
	String schemeType;

	
	@ApiModelProperty(notes="自评总分",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal selfPoints;

	
	@ApiModelProperty(notes="自我评价",allowEmptyValue=true,example="",allowableValues="")
	String selfRemark;

	
	@ApiModelProperty(notes="核定总分",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal vouchPoints;

	
	@ApiModelProperty(notes="核定评论",allowEmptyValue=true,example="",allowableValues="")
	String vouchRemark;

	
	@ApiModelProperty(notes="是否已经自评",allowEmptyValue=true,example="",allowableValues="")
	String isSelf;

	
	@ApiModelProperty(notes="是否已经复评",allowEmptyValue=true,example="",allowableValues="")
	String isReEval;

	
	@ApiModelProperty(notes="是否已经核定",allowEmptyValue=true,example="",allowableValues="")
	String isVouch;

	
	@ApiModelProperty(notes="考核模板编号",allowEmptyValue=true,example="",allowableValues="")
	String templateId;

	
	@ApiModelProperty(notes="考核状态",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="考核人归属部门",allowEmptyValue=true,example="",allowableValues="")
	String assDeptid;

	
	@ApiModelProperty(notes="考核人归属部门名称",allowEmptyValue=true,example="",allowableValues="")
	String assDeptName;

	
	@ApiModelProperty(notes="考核人姓名",allowEmptyValue=true,example="",allowableValues="")
	String assUsername;

	
	@ApiModelProperty(notes="最终得分",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal finalPoints;

	
	@ApiModelProperty(notes="考核最终等级",allowEmptyValue=true,example="",allowableValues="")
	String classType;

	
	@ApiModelProperty(notes="核定人id",allowEmptyValue=true,example="",allowableValues="")
	String vouchId;

	
	@ApiModelProperty(notes="核定人名称",allowEmptyValue=true,example="",allowableValues="")
	String vouchName;

	
	@ApiModelProperty(notes="复评人id",allowEmptyValue=true,example="",allowableValues="")
	String reUserid;

	
	@ApiModelProperty(notes="复评人名称",allowEmptyValue=true,example="",allowableValues="")
	String reUsername;

	
	@ApiModelProperty(notes="复评时间",allowEmptyValue=true,example="",allowableValues="")
	Date reTime;

	
	@ApiModelProperty(notes="核定时间",allowEmptyValue=true,example="",allowableValues="")
	Date vouchTime;

	
	@ApiModelProperty(notes="自评时间",allowEmptyValue=true,example="",allowableValues="")
	Date selfTime;

	
	@ApiModelProperty(notes="方案执行主键",allowEmptyValue=true,example="",allowableValues="")
	String schemeExecId;

	
	@ApiModelProperty(notes="等级名称",allowEmptyValue=true,example="",allowableValues="")
	String classTypeName;

	
	@ApiModelProperty(notes="复评意见",allowEmptyValue=true,example="",allowableValues="")
	String reRemark;

	
	@ApiModelProperty(notes="方案编号",allowEmptyValue=true,example="",allowableValues="")
	String schemeId;

	
	@ApiModelProperty(notes="复评总分",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal rePoints;

	
	@ApiModelProperty(notes="员工确认状态0否1是",allowEmptyValue=true,example="",allowableValues="")
	String assCstatus;

	
	@ApiModelProperty(notes="确认时间",allowEmptyValue=true,example="",allowableValues="")
	Date assCtime;

	
	@ApiModelProperty(notes="复评人确认状态0否1是",allowEmptyValue=true,example="",allowableValues="")
	String reCstatus;

	
	@ApiModelProperty(notes="核定人确认状态0否1是",allowEmptyValue=true,example="",allowableValues="")
	String vouchCstatus;

	
	@ApiModelProperty(notes="核定人确认时间",allowEmptyValue=true,example="",allowableValues="")
	Date vouchCtime;

	
	@ApiModelProperty(notes="复评人确认时间",allowEmptyValue=true,example="",allowableValues="")
	Date reCtime;

	
	@ApiModelProperty(notes="执行方案名称",allowEmptyValue=true,example="",allowableValues="")
	String schemeExecName;

	/**
	 *id
	 **/
	public JxAssessGrade(String id) {
		this.id = id;
	}
    
    /**
     * 考核总分表
     **/
	public JxAssessGrade() {
	}

}