package com.oa.meet.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.meet.entity.MeetMeetingExecAttender;
import com.oa.meet.service.MeetMeetingExecAttenderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value="/*/oa/meet/meetMeetingExecAttender")
@Api(tags = {"会议参会人员表-操作接口"})
public class MeetMeetingExecAttenderController {

    static Logger logger = LoggerFactory.getLogger(MeetMeetingExecAttenderController.class);

    @Autowired
    private MeetMeetingExecAttenderService meetMeetingExecAttenderService;

    @ApiOperation(value = "会议参会人员表-查询列表", notes = " ")
    @ApiEntityParams(MeetMeetingExecAttender.class)
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMeetMeetingExecAttender(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");

        QueryWrapper<MeetMeetingExecAttender> qw = QueryTools.initQueryWrapper(MeetMeetingExecAttender.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = meetMeetingExecAttenderService.selectListMapByWhere(page, qw, params);


        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "会议参会人员表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMeetMeetingExecAttender(@RequestBody MeetMeetingExecAttender meetMeetingExecAttender) {

        
            if (StringUtils.isEmpty(meetMeetingExecAttender.getMeetingId())) {
                return Result.error("会议id必传");
            }
            if (StringUtils.isEmpty(meetMeetingExecAttender.getExecId())) {
                return Result.error("execId必传");
            }
            User user = LoginUtils.getCurrentUserInfo();
            if (user.getUserid() == null) {
                return Result.error("用户未登录");
            }
            MeetMeetingExecAttender meetMeetingExecAttenderQuery = new MeetMeetingExecAttender();
            meetMeetingExecAttenderQuery.setMeetingId(meetMeetingExecAttender.getMeetingId());
            meetMeetingExecAttenderQuery.setExecId(meetMeetingExecAttender.getExecId());
            meetMeetingExecAttenderQuery.setUserid(user.getUserid());
            List<MeetMeetingExecAttender> signList = meetMeetingExecAttenderService.selectListByWhere(meetMeetingExecAttenderQuery);
            MeetMeetingExecAttender meetMeetingAttenderUd = signList.get(0);
            if (signList.size() == 0) {
                MeetMeetingExecAttender meetMeetingAttenderDB = new MeetMeetingExecAttender();
                meetMeetingAttenderDB.setId(meetMeetingExecAttenderService.createKey("id"));
                meetMeetingAttenderDB.setMeetingId(meetMeetingExecAttender.getMeetingId());
                meetMeetingAttenderDB.setUserid(user.getUserid());
                meetMeetingAttenderDB.setUsername(user.getUsername());
                meetMeetingAttenderDB.setStatus("1");
                meetMeetingAttenderDB.setSignStatus("1");
                meetMeetingAttenderDB.setSigninTime(new Date());
                meetMeetingAttenderDB.setSignoutTime(null);
                meetMeetingAttenderDB.setAttType("0");
                meetMeetingExecAttenderService.insert(meetMeetingAttenderDB);
            } else {
                if (meetMeetingAttenderUd.getSigninTime() != null) {
                    return Result.error("用户已签到");
                }
                MeetMeetingExecAttender meetMeetingAttenderUpdate = new MeetMeetingExecAttender();
                meetMeetingAttenderUpdate.setId(meetMeetingAttenderUd.getId());
                meetMeetingAttenderUpdate.setSignStatus("1");
                meetMeetingAttenderUpdate.setSigninTime(new Date());
                meetMeetingAttenderUpdate.setSignoutTime(null);
                meetMeetingAttenderUpdate.setStatus("1");
                meetMeetingAttenderUpdate.setAttType("1");
                meetMeetingExecAttenderService.updateSomeFieldByPk(meetMeetingAttenderUpdate);
            }
        
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "新增一条meet_meeting_attender信息", notes = "addMeetMeetingAttender,主键如果为空，后台自动生成")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/signout", method = RequestMethod.POST)
    public Result outMeetMeetingAttender(@RequestBody MeetMeetingExecAttender meetMeetingAttenderAttender) {
        Tips tips = new Tips("成功修改一条数据");
        
            if (StringUtils.isEmpty(meetMeetingAttenderAttender.getMeetingId())) {
                return Result.error("会议id必传");
            }
            if (StringUtils.isEmpty(meetMeetingAttenderAttender.getExecId())) {
                return Result.error("execId必传");
            }
            MeetMeetingExecAttender meetMeetingAttenderQuery = new MeetMeetingExecAttender();
            User user = LoginUtils.getCurrentUserInfo();
            if (user.getUserid() == null) {
                return Result.error("用户未登录");
            }
            meetMeetingAttenderQuery.setMeetingId(meetMeetingAttenderAttender.getMeetingId());
            meetMeetingAttenderQuery.setUserid(user.getUserid());
            meetMeetingAttenderQuery.setExecId(meetMeetingAttenderAttender.getExecId());
            List<MeetMeetingExecAttender> signList = meetMeetingExecAttenderService.selectListByWhere(meetMeetingAttenderQuery);
            if (signList.size() == 0) {
                return Result.error("用户未签到");
            }
            MeetMeetingExecAttender meetMeetingAttenderDb = signList.get(0);
            if (!"1".equals(meetMeetingAttenderDb.getSignStatus())) {
                if ("2".equals(meetMeetingAttenderDb.getSignStatus())) {
                    tips.setErrMsg("用户已签退");
                } else {
                    tips.setErrMsg("用户未签到");
                }
                return Result.error();
            }
            MeetMeetingExecAttender meetMeetingAttenderUpdate = new MeetMeetingExecAttender();
            meetMeetingAttenderUpdate.setId(meetMeetingAttenderDb.getId());
            meetMeetingAttenderUpdate.setSignStatus("2");
            meetMeetingAttenderUpdate.setSignoutTime(new Date());
            meetMeetingExecAttenderService.updateSomeFieldByPk(meetMeetingAttenderUpdate);
        
        return Result.ok();
    }

    @ApiOperation(value = "新增一条会议参会人员表信息", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/addAttender", method = RequestMethod.POST)
    public Result addMeetMeetingExecAttender1(@RequestBody MeetMeetingExecAttender meetMeetingExecAttender) {

        
            boolean createPk = false;

            MeetMeetingExecAttender meetMeetingExecAttenderDB = new MeetMeetingExecAttender();
            meetMeetingExecAttenderDB.setStatus("1");
            meetMeetingExecAttenderDB.setId(meetMeetingExecAttender.getId());
            meetMeetingExecAttenderDB.setRemark(meetMeetingExecAttender.getRemark());
            meetMeetingExecAttenderService.updateSomeFieldByPk(meetMeetingExecAttenderDB);
            meetMeetingExecAttender.setStatus("1");
        
        return Result.ok().setData(meetMeetingExecAttender);
    }


    @ApiOperation(value = "会议参会人员表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMeetMeetingExecAttender(@RequestBody MeetMeetingExecAttender meetMeetingExecAttender) {
        meetMeetingExecAttenderService.removeById(meetMeetingExecAttender);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "会议参会人员表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMeetMeetingExecAttender(@RequestBody MeetMeetingExecAttender meetMeetingExecAttender) {

        
            if (StringUtils.isEmpty(meetMeetingExecAttender.getMeetingId())) {
                return Result.error("会议ID必传");
            }
            if (StringUtils.isEmpty(meetMeetingExecAttender.getExecId())) {
                return Result.error("会议执行id必传");
            }
            MeetMeetingExecAttender MeetMeetingExecAttenderDb = new MeetMeetingExecAttender();
            MeetMeetingExecAttenderDb.setExecId(meetMeetingExecAttender.getExecId());
            MeetMeetingExecAttenderDb.setMeetingId(meetMeetingExecAttender.getMeetingId());
            MeetMeetingExecAttenderDb.setStatus("2");
            MeetMeetingExecAttenderDb.setRemark(meetMeetingExecAttender.getRemark());
            MeetMeetingExecAttenderDb.setId(meetMeetingExecAttender.getId());
            meetMeetingExecAttenderService.updateSomeFieldByPk(MeetMeetingExecAttenderDb);
        
        return Result.ok("edit-ok", "修改成功！").setData(meetMeetingExecAttender);
    }

    @ApiOperation(value = "会议参会人员表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MeetMeetingExecAttender.class, props = {}, remark = "会议参会人员表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        meetMeetingExecAttenderService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "会议参会人员表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMeetMeetingExecAttender(@RequestBody List<MeetMeetingExecAttender> meetMeetingExecAttenders) {
        User user = LoginUtils.getCurrentUserInfo();
        if (meetMeetingExecAttenders.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MeetMeetingExecAttender> datasDb = meetMeetingExecAttenderService.listByIds(meetMeetingExecAttenders.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MeetMeetingExecAttender> can = new ArrayList<>();
        List<MeetMeetingExecAttender> no = new ArrayList<>();
        for (MeetMeetingExecAttender data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            meetMeetingExecAttenderService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "会议参会人员表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetMeetingExecAttender.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MeetMeetingExecAttender meetMeetingExecAttender) {
        MeetMeetingExecAttender data = (MeetMeetingExecAttender) meetMeetingExecAttenderService.getById(meetMeetingExecAttender);
        return Result.ok().setData(data);
    }

}
