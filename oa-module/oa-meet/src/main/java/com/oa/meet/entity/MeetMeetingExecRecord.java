package com.oa.meet.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("meet_meeting_exec_record")
@ApiModel(description="会议记录表")
public class MeetMeetingExecRecord  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="会议id",allowEmptyValue=true,example="",allowableValues="")
	String meetingId;

	
	@ApiModelProperty(notes="用户id",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="用户",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="用户头像",allowEmptyValue=true,example="",allowableValues="")
	String headerImage;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="内容",allowEmptyValue=true,example="",allowableValues="")
	String content;

	
	@ApiModelProperty(notes="会议执行编号",allowEmptyValue=true,example="",allowableValues="")
	String execId;

	/**
	 *主键
	 **/
	public MeetMeetingExecRecord(String id) {
		this.id = id;
	}
    
    /**
     * 会议记录表
     **/
	public MeetMeetingExecRecord() {
	}

}