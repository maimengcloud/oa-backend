package com.oa.crm.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.crm.entity.CustomerOpertion;
import com.oa.crm.mapper.CustomerOpertionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class CustomerOpertionService extends BaseService<CustomerOpertionMapper,CustomerOpertion> {
	static Logger logger =LoggerFactory.getLogger(CustomerOpertionService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}

	@Transactional
	public void addOperHistory(String customerId, String change, String operType, String ipAddress) {
		User user = LoginUtils.getCurrentUserInfo();
		CustomerOpertion customerOpertion = new CustomerOpertion();
		customerOpertion.setOperId(this.createKey("operId"));
		customerOpertion.setBranchId(user.getBranchId());
		customerOpertion.setCustomerId(customerId);
		customerOpertion.setOperUserId(user.getUserid());
		customerOpertion.setOperUserName(user.getUsername());
		customerOpertion.setOperDeptId(user.getDeptid());
		customerOpertion.setOperDeptName(user.getDeptName());
		customerOpertion.setChangeValue(change);
		customerOpertion.setOperType(operType);
		customerOpertion.setOperTime(new Date());
		customerOpertion.setIpAddress(ipAddress);
		this.insert(customerOpertion);
	}
}

