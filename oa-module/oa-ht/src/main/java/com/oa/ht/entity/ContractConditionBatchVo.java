package com.oa.ht.entity;

import java.util.List;

public class ContractConditionBatchVo {

    String htId;
    List<ContractCondition> contractConditions;


    public String getHtId() {
        return htId;
    }

    public void setHtId(String htId) {
        this.htId = htId;
    }

    public List<ContractCondition> getContractConditions() {
        return contractConditions;
    }

    public void setContractConditions(List<ContractCondition> contractConditions) {
        this.contractConditions = contractConditions;
    }
}
