package com.oa.car.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.car.entity.CarRouteDetailTpl;
import com.oa.car.service.CarRouteDetailTplService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@RestController
@RequestMapping(value = "/*/oa/car/carRouteDetailTpl")
@Api(tags = {"car_route_detail_tpl-操作接口"})
public class CarRouteDetailTplController {

    static Logger logger = LoggerFactory.getLogger(CarRouteDetailTplController.class);

    @Autowired
    private CarRouteDetailTplService carRouteDetailTplService;

    @ApiOperation(value = "car_route_detail_tpl-查询列表", notes = " ")
    @ApiEntityParams(CarRouteDetailTpl.class)
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteDetailTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCarRouteDetailTpl(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CarRouteDetailTpl> qw = QueryTools.initQueryWrapper(CarRouteDetailTpl.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = carRouteDetailTplService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "car_route_detail_tpl-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteDetailTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCarRouteDetailTpl(@RequestBody CarRouteDetailTpl carRouteDetailTpl) {


        if (!carRouteDetailTpl.getIsSignIn().equals("0") && !carRouteDetailTpl.getIsSignIn().equals("1")) {
            return Result.error("是否打卡输入有误，请重新输入");
        }


        if (StringUtils.isEmpty(carRouteDetailTpl.getId())) {
            carRouteDetailTpl.setId(carRouteDetailTplService.createKey("id"));
        } else {
            CarRouteDetailTpl carRouteDetailTplQuery = new CarRouteDetailTpl(carRouteDetailTpl.getId());
            if (carRouteDetailTplService.countByWhere(carRouteDetailTplQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        carRouteDetailTplService.insert(carRouteDetailTpl);


        return Result.ok("add-ok", "添加成功！").setData(carRouteDetailTpl);
    }

    @ApiOperation(value = "car_route_detail_tpl-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCarRouteDetailTpl(@RequestBody CarRouteDetailTpl carRouteDetailTpl) {


        carRouteDetailTplService.deleteByPk(carRouteDetailTpl);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "car_route_detail_tpl-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteDetailTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCarRouteDetailTpl(@RequestBody CarRouteDetailTpl carRouteDetailTpl) {


        if (!carRouteDetailTpl.getIsSignIn().equals("0") && !carRouteDetailTpl.getIsSignIn().equals("1")) {
            return Result.error("是否打卡输入有误，请重新输入");
        }


        carRouteDetailTplService.updateByPk(carRouteDetailTpl);


        return Result.ok("edit-ok", "修改成功！").setData(carRouteDetailTpl);
    }

    @ApiOperation(value = "car_route_detail_tpl-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CarRouteDetailTpl.class, props = {}, remark = "car_route_detail_tpl", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteDetailTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        carRouteDetailTplService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "car_route_detail_tpl-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCarRouteDetailTpl(@RequestBody List<CarRouteDetailTpl> carRouteDetailTpls) {
        User user = LoginUtils.getCurrentUserInfo();
        if (carRouteDetailTpls.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CarRouteDetailTpl> datasDb = carRouteDetailTplService.listByIds(carRouteDetailTpls.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CarRouteDetailTpl> can = new ArrayList<>();
        List<CarRouteDetailTpl> no = new ArrayList<>();
        for (CarRouteDetailTpl data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            carRouteDetailTplService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "car_route_detail_tpl-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteDetailTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CarRouteDetailTpl carRouteDetailTpl) {
        CarRouteDetailTpl data = (CarRouteDetailTpl) carRouteDetailTplService.getById(carRouteDetailTpl);
        return Result.ok().setData(data);
    }

    @RequestMapping(value = "/listAddressGPS", method = RequestMethod.GET)
    public Result selectAddressGPS(@RequestParam Map<String, Object> map) {
        QueryWrapper<CarRouteDetailTpl> qw = QueryTools.initQueryWrapper(CarRouteDetailTpl.class, map);
        IPage page = QueryTools.initPage(map);
//        List<Map<String, Object>> carRouteDetailTplList = carRouteDetailTplService.selectAddressGPS(map);    //列出CarRouteDetailTpl列表
        List<Map<String, Object>> carRouteDetailTplList = carRouteDetailTplService.selectListMapByWhereAndKeyWord(page, qw, map);    //列出CarRouteDetailTpl列表

        return Result.ok().setData(carRouteDetailTplList);
    }

}
