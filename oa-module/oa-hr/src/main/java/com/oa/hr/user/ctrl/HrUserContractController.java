package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserContract;
import com.oa.hr.user.service.HrUserBaseInfoService;
import com.oa.hr.user.service.HrUserContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/hr/user/hrUserContract")
@Api(tags = {"员工合同信息表-操作接口"})
public class HrUserContractController {

    static Logger logger = LoggerFactory.getLogger(HrUserContractController.class);

    @Autowired
    private HrUserContractService hrUserContractService;
    @Autowired
    private HrUserBaseInfoService hrUserBaseInfoService;

    @ApiOperation(value = "员工合同信息表-查询列表", notes = " ")
    @ApiEntityParams(HrUserContract.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrUserContract.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrUserContract(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("res.branchId", user.getBranchId());
        QueryWrapper<HrUserContract> qw = QueryTools.initQueryWrapper(HrUserContract.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = hrUserContractService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    //上传的excel文件
    @PostMapping("addContractByExcel")
    public Result addContractByExcel(MultipartFile file) {
        

        try {
            hrUserContractService.saveSubject(file, hrUserBaseInfoService, hrUserContractService);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return Result.ok("", "成功导入数据");
    }

    @ApiOperation(value = "员工合同信息表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserContract.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrUserContract(@RequestBody HrUserContract hrUserContract) {
        


        User user = LoginUtils.getCurrentUserInfo();
        hrUserContract.setBranchId(user.getBranchId());
        if (StringUtils.isEmpty(hrUserContract.getId())) {
            hrUserContract.setId(hrUserContractService.createKey("id"));
        } else {
            HrUserContract hrUserContractQuery = new HrUserContract(hrUserContract.getId());
            if (hrUserContractService.countByWhere(hrUserContractQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        hrUserContractService.insert(hrUserContract);

        return Result.ok("add-ok", "添加成功！").setData(hrUserContract);
    }

    @ApiOperation(value = "员工合同信息表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrUserContract(@RequestBody HrUserContract hrUserContract) {
        


        hrUserContractService.deleteByPk(hrUserContract);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "员工合同信息表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserContract.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrUserContract(@RequestBody HrUserContract hrUserContract) {
        

        hrUserContractService.updateByPk(hrUserContract);


        return Result.ok("edit-ok", "修改成功！").setData(hrUserContract);
    }

    @ApiOperation(value = "员工合同信息表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrUserContract.class, props = {}, remark = "员工合同信息表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserContract.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        hrUserContractService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "员工合同信息表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrUserContract(@RequestBody List<HrUserContract> hrUserContracts) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrUserContracts.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrUserContract> datasDb = hrUserContractService.listByIds(hrUserContracts.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrUserContract> can = new ArrayList<>();
        List<HrUserContract> no = new ArrayList<>();
        for (HrUserContract data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            hrUserContractService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "员工合同信息表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserContract.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrUserContract hrUserContract) {
        HrUserContract data = (HrUserContract) hrUserContractService.getById(hrUserContract);
        return Result.ok().setData(data);
    }

}
