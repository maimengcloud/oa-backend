package com.oa.erp.pur.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.erp.pur.entity.Brand;
import com.oa.erp.pur.service.BrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/erp/pur/brand")
@Api(tags = {"pur_brand-操作接口"})
public class BrandController {

    static Logger logger = LoggerFactory.getLogger(BrandController.class);

    @Autowired
    private BrandService brandService;

    @ApiOperation(value = "pur_brand-查询列表", notes = " ")
    @ApiEntityParams(Brand.class)
    @ApiResponses({@ApiResponse(code = 200, response = Brand.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listBrand(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<Brand> qw = QueryTools.initQueryWrapper(Brand.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = brandService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "pur_brand-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Brand.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addBrand(@RequestBody Brand brand) {
        if (StringUtils.isEmpty(brand.getId())) {
            brand.setId(brandService.createKey("id"));
        } else {
            Brand brandQuery = new Brand(brand.getId());
            if (brandService.countByWhere(brandQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        User user = LoginUtils.getCurrentUserInfo();
        brand.setBranchId(user.getBranchId());
        brandService.insert(brand);

        return Result.ok("add-ok", "添加成功！").setData(brand);
    }

    @ApiOperation(value = "pur_brand-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delBrand(@RequestBody Brand brand) {
        brandService.deleteByPk(brand);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "pur_brand-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Brand.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editBrand(@RequestBody Brand brand) {
        brandService.updateByPk(brand);
        return Result.ok("edit-ok", "修改成功！").setData(brand);
    }

    @ApiOperation(value = "pur_brand-批量修改某些字段", notes = "")
    @ApiEntityParams(value = Brand.class, props = {}, remark = "pur_brand", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = Brand.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        brandService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "pur_brand-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelBrand(@RequestBody List<Brand> brands) {
        User user = LoginUtils.getCurrentUserInfo();
        if (brands.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<Brand> datasDb = brandService.listByIds(brands.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<Brand> can = new ArrayList<>();
        List<Brand> no = new ArrayList<>();
        for (Brand data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            brandService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "pur_brand-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Brand.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(Brand brand) {
        Brand data = (Brand) brandService.getById(brand);
        return Result.ok().setData(data);
    }

}
