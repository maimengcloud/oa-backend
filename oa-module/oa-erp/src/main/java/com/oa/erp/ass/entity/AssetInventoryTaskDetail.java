package com.oa.erp.ass.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ass_asset_inventory_task_detail")
@ApiModel(description="ass_asset_inventory_task_detail")
public class AssetInventoryTaskDetail  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="资产卡片Id",allowEmptyValue=true,example="",allowableValues="")
	String cardId;

	
	@ApiModelProperty(notes="当前资产卡片的名称",allowEmptyValue=true,example="",allowableValues="")
	String nowCardName;

	
	@ApiModelProperty(notes="盘点任务状态，1待盘点",allowEmptyValue=true,example="",allowableValues="")
	String detailStatus;

	
	@ApiModelProperty(notes="盘点任务ID",allowEmptyValue=true,example="",allowableValues="")
	String taskId;

	
	@ApiModelProperty(notes="资产操作记录id",allowEmptyValue=true,example="",allowableValues="")
	String assMngId;

	/**
	 *id
	 **/
	public AssetInventoryTaskDetail(String id) {
		this.id = id;
	}
    
    /**
     * ass_asset_inventory_task_detail
     **/
	public AssetInventoryTaskDetail() {
	}

}