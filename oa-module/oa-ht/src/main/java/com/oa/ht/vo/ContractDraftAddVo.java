package com.oa.ht.vo;

import com.google.common.collect.Lists;
import com.oa.ht.entity.*;

import java.util.List;

public class ContractDraftAddVo extends ContractDraft {

    private List<ContractProduct> productList = Lists.newArrayList();

    private List<ContractCondition> conditionList = Lists.newArrayList();


    private List<ContractProject> projectList = Lists.newArrayList();

    private List<ContractCb> cbList = Lists.newArrayList();

    public List<ContractProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<ContractProduct> productList) {
        this.productList = productList;
    }

    public List<ContractCondition> getConditionList() {
        return conditionList;
    }

    public void setConditionList(List<ContractCondition> conditionList) {
        this.conditionList = conditionList;
    }

    public List<ContractCb> getCbList() {
        return cbList;
    }

    public void setCbList(List<ContractCb> cbList) {
        this.cbList = cbList;
    }

    public List<ContractProject> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<ContractProject> projectList) {
        this.projectList = projectList;
    }
}
