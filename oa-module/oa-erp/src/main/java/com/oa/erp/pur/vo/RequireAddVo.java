package com.oa.erp.pur.vo;

import com.oa.erp.pur.entity.Require;
import com.oa.erp.pur.entity.RequireDetail;

import java.util.ArrayList;
import java.util.List;

public class RequireAddVo {

    private Require purRequire;

    private List<RequireDetail> purRequireDetail = new ArrayList<>();

    public Require getPurRequire() {
        return purRequire;
    }

    public void setPurRequire(Require purRequire) {
        this.purRequire = purRequire;
    }

    public List<RequireDetail> getPurRequireDetail() {
        return purRequireDetail;
    }

    public void setPurRequireDetail(List<RequireDetail> purRequireDetail) {
        this.purRequireDetail = purRequireDetail;
    }

    @Override
    public String toString() {
        return "RequireAddVo{" +
                "purRequire=" + purRequire +
                ", purRequireDetail=" + purRequireDetail +
                '}';
    }
}
