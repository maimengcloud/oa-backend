package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_agenda")
@ApiModel(description="客户日程表")
public class CustomerAgenda  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="日程id,主键",allowEmptyValue=true,example="",allowableValues="")
	String agendaId;

	
	@ApiModelProperty(notes="日程标题",allowEmptyValue=true,example="",allowableValues="")
	String agendaText;

	
	@ApiModelProperty(notes="日程内容",allowEmptyValue=true,example="",allowableValues="")
	String agendaNav;

	
	@ApiModelProperty(notes="客户id",allowEmptyValue=true,example="",allowableValues="")
	String customerId;

	
	@ApiModelProperty(notes="客户名称",allowEmptyValue=true,example="",allowableValues="")
	String customerName;

	
	@ApiModelProperty(notes="客户联系人id",allowEmptyValue=true,example="",allowableValues="")
	String contacterId;

	
	@ApiModelProperty(notes="客户联系人名称",allowEmptyValue=true,example="",allowableValues="")
	String contacterName;

	
	@ApiModelProperty(notes="紧急程度",allowEmptyValue=true,example="",allowableValues="")
	String urgentLevel;

	
	@ApiModelProperty(notes="紧急程度名称",allowEmptyValue=true,example="",allowableValues="")
	String urgentLevelName;

	
	@ApiModelProperty(notes="开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date beginTime;

	
	@ApiModelProperty(notes="结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="提醒方式",allowEmptyValue=true,example="",allowableValues="")
	String tipType;

	
	@ApiModelProperty(notes="是否立即提醒",allowEmptyValue=true,example="",allowableValues="")
	String remindImmediately;

	
	@ApiModelProperty(notes="是否开始前提醒",allowEmptyValue=true,example="",allowableValues="")
	String remindBeforeStart;

	
	@ApiModelProperty(notes="开始前提醒小时",allowEmptyValue=true,example="",allowableValues="")
	String remindBeforeHour;

	
	@ApiModelProperty(notes="开始前提醒分钟",allowEmptyValue=true,example="",allowableValues="")
	String remindBeforeMinute;

	
	@ApiModelProperty(notes="是否结束前提醒",allowEmptyValue=true,example="",allowableValues="")
	String remindEndStart;

	
	@ApiModelProperty(notes="结束前提醒小时",allowEmptyValue=true,example="",allowableValues="")
	String remindEndHour;

	
	@ApiModelProperty(notes="结束前提醒分钟",allowEmptyValue=true,example="",allowableValues="")
	String remindEndMinute;

	
	@ApiModelProperty(notes="日程类型",allowEmptyValue=true,example="",allowableValues="")
	String workPlanType;

	
	@ApiModelProperty(notes="创建人Id",allowEmptyValue=true,example="",allowableValues="")
	String agendaCreateId;

	
	@ApiModelProperty(notes="创建人姓名",allowEmptyValue=true,example="",allowableValues="")
	String agendaCreateName;

	
	@ApiModelProperty(notes="创建人头像",allowEmptyValue=true,example="",allowableValues="")
	String agendaCreateImg;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date agendaCreateTime;

	
	@ApiModelProperty(notes="是否日程计划",allowEmptyValue=true,example="",allowableValues="")
	String isPlan;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="关联商机id",allowEmptyValue=true,example="",allowableValues="")
	String businessId;

	
	@ApiModelProperty(notes="关联商机名称",allowEmptyValue=true,example="",allowableValues="")
	String businessName;

	/**
	 *日程id
	 **/
	public CustomerAgenda(String agendaId) {
		this.agendaId = agendaId;
	}
    
    /**
     * 客户日程表
     **/
	public CustomerAgenda() {
	}

}