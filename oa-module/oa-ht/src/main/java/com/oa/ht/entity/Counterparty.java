package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_counterparty")
@ApiModel(description="ht_counterparty")
public class Counterparty  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="相对方id,主键",allowEmptyValue=true,example="",allowableValues="")
	String cpId;

	
	@ApiModelProperty(notes="相对方编号",allowEmptyValue=true,example="",allowableValues="")
	String cpSn;

	
	@ApiModelProperty(notes="向对方名称",allowEmptyValue=true,example="",allowableValues="")
	String cpName;

	
	@ApiModelProperty(notes="相对方简称",allowEmptyValue=true,example="",allowableValues="")
	String cpShort;

	
	@ApiModelProperty(notes="相对方级别",allowEmptyValue=true,example="",allowableValues="")
	String cpLevel;

	
	@ApiModelProperty(notes="相对放类型",allowEmptyValue=true,example="",allowableValues="")
	String cpType;

	
	@ApiModelProperty(notes="相对方类型名称",allowEmptyValue=true,example="",allowableValues="")
	String cpTypeName;

	
	@ApiModelProperty(notes="1",allowEmptyValue=true,example="",allowableValues="")
	String cpStatus;

	
	@ApiModelProperty(notes="相对放行业",allowEmptyValue=true,example="",allowableValues="")
	String cpIndustry;

	
	@ApiModelProperty(notes="相对方行业名称",allowEmptyValue=true,example="",allowableValues="")
	String cpIndustryName;

	
	@ApiModelProperty(notes="相对方国家地区",allowEmptyValue=true,example="",allowableValues="")
	String cpAddress;

	
	@ApiModelProperty(notes="相对方区域",allowEmptyValue=true,example="",allowableValues="")
	String cpRegion;

	
	@ApiModelProperty(notes="相对方区域名称",allowEmptyValue=true,example="",allowableValues="")
	String cpRegionName;

	
	@ApiModelProperty(notes="相对方网址",allowEmptyValue=true,example="",allowableValues="")
	String cpWebsite;

	
	@ApiModelProperty(notes="相对方简介",allowEmptyValue=true,example="",allowableValues="")
	String cpDesc;

	
	@ApiModelProperty(notes="相对方法定代表",allowEmptyValue=true,example="",allowableValues="")
	String cpFddb;

	
	@ApiModelProperty(notes="相对方注册信息",allowEmptyValue=true,example="",allowableValues="")
	String cpZcxx;

	
	@ApiModelProperty(notes="相对方工商注册号",allowEmptyValue=true,example="",allowableValues="")
	String cpGszch;

	
	@ApiModelProperty(notes="相对方注册资本",allowEmptyValue=true,example="",allowableValues="")
	String cpZczb;

	
	@ApiModelProperty(notes="相对方注册时间",allowEmptyValue=true,example="",allowableValues="")
	String cpZcsj;

	
	@ApiModelProperty(notes="单位状态",allowEmptyValue=true,example="",allowableValues="")
	String cpDwzt;

	
	@ApiModelProperty(notes="单位状态名称",allowEmptyValue=true,example="",allowableValues="")
	String cpDwztName;

	
	@ApiModelProperty(notes="相对方单位电话",allowEmptyValue=true,example="",allowableValues="")
	String cpPhone;

	
	@ApiModelProperty(notes="相对方邮箱",allowEmptyValue=true,example="",allowableValues="")
	String cpEmail;

	
	@ApiModelProperty(notes="相对方传真",allowEmptyValue=true,example="",allowableValues="")
	String cpFax;

	
	@ApiModelProperty(notes="相对方省",allowEmptyValue=true,example="",allowableValues="")
	String cpProvince;

	
	@ApiModelProperty(notes="相对方省代码",allowEmptyValue=true,example="",allowableValues="")
	String cpProvinceCode;

	
	@ApiModelProperty(notes="相对方市",allowEmptyValue=true,example="",allowableValues="")
	String cpCity;

	
	@ApiModelProperty(notes="相对方市代码",allowEmptyValue=true,example="",allowableValues="")
	String cpCityCode;

	
	@ApiModelProperty(notes="相对方区",allowEmptyValue=true,example="",allowableValues="")
	String cpDistrict;

	
	@ApiModelProperty(notes="相对方区代码",allowEmptyValue=true,example="",allowableValues="")
	String cpDistrictCode;

	
	@ApiModelProperty(notes="相对方负责人",allowEmptyValue=true,example="",allowableValues="")
	String cpFzr;

	
	@ApiModelProperty(notes="相对方负责人手机",allowEmptyValue=true,example="",allowableValues="")
	String cpFzrPhone;

	
	@ApiModelProperty(notes="相对方结算方式",allowEmptyValue=true,example="",allowableValues="")
	String cpClearWay;

	
	@ApiModelProperty(notes="向对方结算方式名称",allowEmptyValue=true,example="",allowableValues="")
	String cpClearWayName;

	
	@ApiModelProperty(notes="相对方开户行",allowEmptyValue=true,example="",allowableValues="")
	String cpOpenBank;

	
	@ApiModelProperty(notes="相对方币种",allowEmptyValue=true,example="",allowableValues="")
	String cpCurrency;

	
	@ApiModelProperty(notes="相对方账号",allowEmptyValue=true,example="",allowableValues="")
	String cpBankAccount;

	
	@ApiModelProperty(notes="机构账号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="创建用户",allowEmptyValue=true,example="",allowableValues="")
	String createUserId;

	
	@ApiModelProperty(notes="创建用户名称",allowEmptyValue=true,example="",allowableValues="")
	String createUserName;

	/**
	 *相对方id
	 **/
	public Counterparty(String cpId) {
		this.cpId = cpId;
	}
    
    /**
     * ht_counterparty
     **/
	public Counterparty() {
	}

}