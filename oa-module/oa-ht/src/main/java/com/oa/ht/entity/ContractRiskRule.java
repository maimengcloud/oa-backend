package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_risk_rule")
@ApiModel(description="ht_contract_risk_rule")
public class ContractRiskRule  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="rule_id,主键",allowEmptyValue=true,example="",allowableValues="")
	String ruleId;

	
	@ApiModelProperty(notes="风险名称",allowEmptyValue=true,example="",allowableValues="")
	String ruleName;

	
	@ApiModelProperty(notes="风险说明",allowEmptyValue=true,example="",allowableValues="")
	String ruleDesc;

	
	@ApiModelProperty(notes="branch_id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *rule_id
	 **/
	public ContractRiskRule(String ruleId) {
		this.ruleId = ruleId;
	}
    
    /**
     * ht_contract_risk_rule
     **/
	public ContractRiskRule() {
	}

}