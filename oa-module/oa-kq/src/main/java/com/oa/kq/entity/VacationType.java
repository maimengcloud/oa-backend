package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_vacation_type")
@ApiModel(description="请假类型表")
public class VacationType  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="ID,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="假期类型1-事假，2-病假，3-年假，4-婚假,5-哺乳假，6-丧假，7-外勤",allowEmptyValue=true,example="",allowableValues="")
	String vacationType;

	
	@ApiModelProperty(notes="请假规则描述：比如妇乳期只能女员工享有",allowEmptyValue=true,example="",allowableValues="")
	String vacationRule;

	
	@ApiModelProperty(notes="组织架构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="可请总时长，单位小时",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalHours;

	
	@ApiModelProperty(notes="性别要求0-男性可请，1-女性可请，2-全部",allowEmptyValue=true,example="",allowableValues="")
	String sexRule;

	
	@ApiModelProperty(notes="假期名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="入职年限要求开始月数",allowEmptyValue=true,example="",allowableValues="")
	Integer beginEmpMonths;

	
	@ApiModelProperty(notes="入职年限要求结束月数",allowEmptyValue=true,example="",allowableValues="")
	Integer endEmpMonths;

	
	@ApiModelProperty(notes="工龄要求开始月数",allowEmptyValue=true,example="",allowableValues="")
	Integer beginWorkingMonths;

	
	@ApiModelProperty(notes="工龄要求结束月数",allowEmptyValue=true,example="",allowableValues="")
	Integer endWorkingMonths;

	
	@ApiModelProperty(notes="启用0否1是",allowEmptyValue=true,example="",allowableValues="")
	String status;

	/**
	 *ID
	 **/
	public VacationType(String id) {
		this.id = id;
	}
    
    /**
     * 请假类型表
     **/
	public VacationType() {
	}

}