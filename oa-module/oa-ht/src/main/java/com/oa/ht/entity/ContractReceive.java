package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_receive")
@ApiModel(description="ht_contract_receive")
public class ContractReceive  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="合同id",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	
	@ApiModelProperty(notes="收款款项id",allowEmptyValue=true,example="",allowableValues="")
	String conditionId;

	
	@ApiModelProperty(notes="合同名称",allowEmptyValue=true,example="",allowableValues="")
	String htName;

	
	@ApiModelProperty(notes="合同编码",allowEmptyValue=true,example="",allowableValues="")
	String htSn;

	
	@ApiModelProperty(notes="到款金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal amount;

	
	@ApiModelProperty(notes="到款日期",allowEmptyValue=true,example="",allowableValues="")
	Date receiveDate;

	
	@ApiModelProperty(notes="到款银行",allowEmptyValue=true,example="",allowableValues="")
	String bank;

	
	@ApiModelProperty(notes="到款账号",allowEmptyValue=true,example="",allowableValues="")
	String bankAccount;

	
	@ApiModelProperty(notes="付款单位",allowEmptyValue=true,example="",allowableValues="")
	String office;

	
	@ApiModelProperty(notes="对应收款项",allowEmptyValue=true,example="",allowableValues="")
	String receiveType;

	
	@ApiModelProperty(notes="对应收款金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal receiveAmount;

	
	@ApiModelProperty(notes="匹配状态",allowEmptyValue=true,example="",allowableValues="")
	String matchStatus;

	
	@ApiModelProperty(notes="创建用户id",allowEmptyValue=true,example="",allowableValues="")
	String createUserId;

	
	@ApiModelProperty(notes="创建用户名称",allowEmptyValue=true,example="",allowableValues="")
	String createUserName;

	
	@ApiModelProperty(notes="创建用户时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="批次机构",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="收款状态0-待收款1已收款",allowEmptyValue=true,example="",allowableValues="")
	String receiveStatus;

	
	@ApiModelProperty(notes="当前流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="当前流程状态0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	/**
	 *id
	 **/
	public ContractReceive(String id) {
		this.id = id;
	}
    
    /**
     * ht_contract_receive
     **/
	public ContractReceive() {
	}

}