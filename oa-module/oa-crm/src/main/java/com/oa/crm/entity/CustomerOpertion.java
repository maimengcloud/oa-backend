package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_opertion")
@ApiModel(description="crm_customer_opertion")
public class CustomerOpertion  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="操作记录id,主键",allowEmptyValue=true,example="",allowableValues="")
	String operId;

	
	@ApiModelProperty(notes="客户id",allowEmptyValue=true,example="",allowableValues="")
	String customerId;

	
	@ApiModelProperty(notes="操作人",allowEmptyValue=true,example="",allowableValues="")
	String operUserId;

	
	@ApiModelProperty(notes="操作人名称",allowEmptyValue=true,example="",allowableValues="")
	String operUserName;

	
	@ApiModelProperty(notes="操作人部门id",allowEmptyValue=true,example="",allowableValues="")
	String operDeptId;

	
	@ApiModelProperty(notes="操作人部门名称",allowEmptyValue=true,example="",allowableValues="")
	String operDeptName;

	
	@ApiModelProperty(notes="ip地址",allowEmptyValue=true,example="",allowableValues="")
	String ipAddress;

	
	@ApiModelProperty(notes="create",allowEmptyValue=true,example="",allowableValues="")
	String operType;

	
	@ApiModelProperty(notes="操作时间",allowEmptyValue=true,example="",allowableValues="")
	Date operTime;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="变化",allowEmptyValue=true,example="",allowableValues="")
	String changeValue;

	/**
	 *操作记录id
	 **/
	public CustomerOpertion(String operId) {
		this.operId = operId;
	}
    
    /**
     * crm_customer_opertion
     **/
	public CustomerOpertion() {
	}

}