package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_evaluation")
@ApiModel(description="crm_customer_evaluation")
public class CustomerEvaluation  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="客户id",allowEmptyValue=true,example="",allowableValues="")
	String customerId;

	
	@ApiModelProperty(notes="模板id",allowEmptyValue=true,example="",allowableValues="")
	String temId;

	
	@ApiModelProperty(notes="1差",allowEmptyValue=true,example="",allowableValues="")
	String level;

	
	@ApiModelProperty(notes="等级名称",allowEmptyValue=true,example="",allowableValues="")
	String levelName;

	
	@ApiModelProperty(notes="总分",allowEmptyValue=true,example="",allowableValues="")
	String score;

	/**
	 *id
	 **/
	public CustomerEvaluation(String id) {
		this.id = id;
	}
    
    /**
     * crm_customer_evaluation
     **/
	public CustomerEvaluation() {
	}

}