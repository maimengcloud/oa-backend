package com.oa.matter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("sxk_matter_inspect_item")
@ApiModel(description="检查项目")
public class MatterInspectItem  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="序号",allowEmptyValue=true,example="",allowableValues="")
	String orderNumber;

	
	@ApiModelProperty(notes="检查项目名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="事项基本信息id",allowEmptyValue=true,example="",allowableValues="")
	String infoId;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public MatterInspectItem(String id) {
		this.id = id;
	}
    
    /**
     * 检查项目
     **/
	public MatterInspectItem() {
	}

}