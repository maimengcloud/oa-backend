package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_risk")
@ApiModel(description="ht_contract_risk")
public class ContractRisk  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="risk_id,主键",allowEmptyValue=true,example="",allowableValues="")
	String riskId;

	
	@ApiModelProperty(notes="合同id",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	
	@ApiModelProperty(notes="风险相对方id",allowEmptyValue=true,example="",allowableValues="")
	String cpId;

	
	@ApiModelProperty(notes="相对方名称",allowEmptyValue=true,example="",allowableValues="")
	String cpName;

	
	@ApiModelProperty(notes="关联风险规则id",allowEmptyValue=true,example="",allowableValues="")
	String ruleId;

	
	@ApiModelProperty(notes="风险名称",allowEmptyValue=true,example="",allowableValues="")
	String riskName;

	
	@ApiModelProperty(notes="风险内容",allowEmptyValue=true,example="",allowableValues="")
	String riskNav;

	
	@ApiModelProperty(notes="处理策略",allowEmptyValue=true,example="",allowableValues="")
	String riskMethod;

	
	@ApiModelProperty(notes="预计时间",allowEmptyValue=true,example="",allowableValues="")
	Date yujiTime;

	
	@ApiModelProperty(notes="风险创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="风险创建人id",allowEmptyValue=true,example="",allowableValues="")
	String createUserId;

	
	@ApiModelProperty(notes="风险创建人名称",allowEmptyValue=true,example="",allowableValues="")
	String createUserName;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *risk_id
	 **/
	public ContractRisk(String riskId) {
		this.riskId = riskId;
	}
    
    /**
     * ht_contract_risk
     **/
	public ContractRisk() {
	}

}