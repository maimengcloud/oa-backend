package com.oa.erp.pur.vo;

import com.oa.erp.pur.entity.StockIn;
import com.oa.erp.pur.entity.StockInDetail;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StockInAddVo  {

    //资产入库信息
    private StockIn stockIn;

    //资产卡片信息
    private List<StockInDetail> stockInDetails = new ArrayList<>();

    //资产
    private List<Map<String, Object>> order = new ArrayList<>();

    public StockIn getStockIn() {
        return stockIn;
    }

    public void setStockIn(StockIn stockIn) {
        this.stockIn = stockIn;
    }

    public List<StockInDetail> getStockInDetails() {
        return stockInDetails;
    }

    public void setStockInDetails(List<StockInDetail> stockInDetails) {
        this.stockInDetails = stockInDetails;
    }

    public List<Map<String, Object>> getOrder() {
        return order;
    }

    public void setOrder(List<Map<String, Object>> order) {
        this.order = order;
    }
}
