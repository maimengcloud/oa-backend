package com.oa.erp.ass.enums;

/**
 * 单据的枚举
 */
public enum MngStatusEnum {

    HOLD("hold", "暂存"),
    SUBMIT("submit", "提交");



    MngStatusEnum(String status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    private String status;

    private String desc;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
