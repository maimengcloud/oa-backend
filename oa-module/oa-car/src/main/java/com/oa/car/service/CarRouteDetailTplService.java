package com.oa.car.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.service.BaseService;
import com.oa.car.entity.CarRouteDetailTpl;
import com.oa.car.mapper.CarRouteDetailTplMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@Service
public class CarRouteDetailTplService extends BaseService<CarRouteDetailTplMapper, CarRouteDetailTpl> {
    static Logger logger = LoggerFactory.getLogger(CarRouteDetailTplService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    /**
     * 请在此类添加自定义函数
     */
    public List<CarRouteDetailTpl> selectListByWhereAndRouteId(Map<String, Object> carRouteDetailTpl) {
        return baseMapper.selectListMapByWhereAndRouteId(carRouteDetailTpl);
    }

    public List<Map<String, Object>> selectListMapByWhereAndKeyWord(IPage page, QueryWrapper ew, Map<String, Object> carRouteDetailTpl) {

        return baseMapper.selectListMapByWhereAndKeyWord(page, ew, carRouteDetailTpl);

    }

    @Transactional
    public boolean save(CarRouteDetailTpl carRouteDetailTpl) {
        Tips tips = new Tips();

        if (StringUtils.isEmpty(carRouteDetailTpl.getId())) {
            carRouteDetailTpl.setId(this.createKey("id"));
        } else {
            CarRouteDetailTpl carRouteDetailTplQuery = new CarRouteDetailTpl(carRouteDetailTpl.getId());
            if (this.countByWhere(carRouteDetailTplQuery) > 0) {
                throw new BizException("详细路线id重复");
            }
        }
        this.insert(carRouteDetailTpl);
        return true;

    }
}

