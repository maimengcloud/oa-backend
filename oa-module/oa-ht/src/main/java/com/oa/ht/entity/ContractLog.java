package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_log")
@ApiModel(description="ht_contract_log")
public class ContractLog  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="操作编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="合同编号",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	
	@ApiModelProperty(notes="操作描述",allowEmptyValue=true,example="",allowableValues="")
	String opRemark;

	
	@ApiModelProperty(notes="操作人编号",allowEmptyValue=true,example="",allowableValues="")
	String opUserid;

	
	@ApiModelProperty(notes="操作人姓名",allowEmptyValue=true,example="",allowableValues="")
	String opUsername;

	
	@ApiModelProperty(notes="操作时间",allowEmptyValue=true,example="",allowableValues="")
	Date opTime;

	
	@ApiModelProperty(notes="操作者机器ip",allowEmptyValue=true,example="",allowableValues="")
	String opIp;

	/**
	 *操作编号
	 **/
	public ContractLog(String id) {
		this.id = id;
	}
    
    /**
     * ht_contract_log
     **/
	public ContractLog() {
	}

}