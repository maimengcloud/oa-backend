package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_agenda_doc")
@ApiModel(description="crm_customer_agenda_doc")
public class CustomerAgendaDoc  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="日程id",allowEmptyValue=true,example="",allowableValues="")
	String agendaId;

	
	@ApiModelProperty(notes="文档id",allowEmptyValue=true,example="",allowableValues="")
	String docId;

	
	@ApiModelProperty(notes="文档名称",allowEmptyValue=true,example="",allowableValues="")
	String docName;

	
	@ApiModelProperty(notes="链接",allowEmptyValue=true,example="",allowableValues="")
	String docUrl;

	
	@ApiModelProperty(notes="doc:文档",allowEmptyValue=true,example="",allowableValues="")
	String docType;

	/**
	 *id
	 **/
	public CustomerAgendaDoc(String id) {
		this.id = id;
	}
    
    /**
     * crm_customer_agenda_doc
     **/
	public CustomerAgendaDoc() {
	}

}