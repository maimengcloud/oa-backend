package com.oa.ht.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.ht.entity.ContractCb;
import com.oa.ht.entity.ContractCbBatchVo;
import com.oa.ht.service.ContractCbService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/ht/contractCb")
@Api(tags = {"ht_contract_cb-操作接口"})
public class ContractCbController {

    static Logger logger = LoggerFactory.getLogger(ContractCbController.class);

    @Autowired
    private ContractCbService contractCbService;

    @ApiOperation(value = "ht_contract_cb-查询列表", notes = " ")
    @ApiEntityParams(ContractCb.class)
    @ApiResponses({@ApiResponse(code = 200, response = ContractCb.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listContractCb(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "cbIds");
        QueryWrapper<ContractCb> qw = QueryTools.initQueryWrapper(ContractCb.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = contractCbService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ApiOperation(value = "批量新增修改合同首付款条件", notes = "batchSave")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchSave", method = RequestMethod.POST)
    public Result batchSave(@RequestBody ContractCbBatchVo contractCbs) {
        contractCbService.batchSave(contractCbs);
        return Result.ok("update-ok", "成功更新");
    }


    @ApiOperation(value = "ht_contract_cb-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractCb.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addContractCb(@RequestBody ContractCb contractCb) {
        contractCbService.save(contractCb);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "ht_contract_cb-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delContractCb(@RequestBody ContractCb contractCb) {
        contractCbService.removeById(contractCb);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "ht_contract_cb-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractCb.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editContractCb(@RequestBody ContractCb contractCb) {
        contractCbService.updateById(contractCb);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "ht_contract_cb-批量修改某些字段", notes = "")
    @ApiEntityParams(value = ContractCb.class, props = {}, remark = "ht_contract_cb", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = ContractCb.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        contractCbService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "ht_contract_cb-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelContractCb(@RequestBody List<ContractCb> contractCbs) {
        User user = LoginUtils.getCurrentUserInfo();
        if (contractCbs.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<ContractCb> datasDb = contractCbService.listByIds(contractCbs.stream().map(i -> i.getCbId()).collect(Collectors.toList()));

        List<ContractCb> can = new ArrayList<>();
        List<ContractCb> no = new ArrayList<>();
        for (ContractCb data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            contractCbService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getCbId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ht_contract_cb-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractCb.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(ContractCb contractCb) {
        ContractCb data = (ContractCb) contractCbService.getById(contractCb);
        return Result.ok().setData(data);
    }

}
