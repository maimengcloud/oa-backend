package com.oa.erp.pur.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.erp.pur.entity.RequireDetailDisassemble;
import com.oa.erp.pur.service.RequireDetailDisassembleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/oa/erp/pur/requireDetailDisassemble")
@Api(tags = {"采购需求明细拆分-操作接口"})
public class RequireDetailDisassembleController {

    static Logger logger = LoggerFactory.getLogger(RequireDetailDisassembleController.class);

    @Autowired
    private RequireDetailDisassembleService requireDetailDisassembleService;

    @ApiOperation(value = "采购需求明细拆分-查询列表", notes = " ")
    @ApiEntityParams(RequireDetailDisassemble.class)
    @ApiResponses({
            @ApiResponse(code = 200, response = RequireDetailDisassemble.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
    })
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listRequireDetailDisassemble(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        QueryWrapper<RequireDetailDisassemble> qw = QueryTools.initQueryWrapper(RequireDetailDisassemble.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = requireDetailDisassembleService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "采购需求明细拆分-新增", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = RequireDetailDisassemble.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addRequireDetailDisassemble(@RequestBody RequireDetailDisassemble requireDetailDisassemble) {
        requireDetailDisassembleService.save(requireDetailDisassemble);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "采购需求明细拆分-删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
    })
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delRequireDetailDisassemble(@RequestBody RequireDetailDisassemble requireDetailDisassemble) {
        requireDetailDisassembleService.removeById(requireDetailDisassemble);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "采购需求明细拆分-修改", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = RequireDetailDisassemble.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editRequireDetailDisassemble(@RequestBody RequireDetailDisassemble requireDetailDisassemble) {
        requireDetailDisassembleService.updateById(requireDetailDisassemble);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "采购需求明细拆分-批量修改某些字段", notes = "")
    @ApiEntityParams(value = RequireDetailDisassemble.class, props = {}, remark = "采购需求明细拆分", paramType = "body")
    @ApiResponses({
            @ApiResponse(code = 200, response = RequireDetailDisassemble.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        requireDetailDisassembleService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "采购需求明细拆分-批量删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
    })
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelRequireDetailDisassemble(@RequestBody List<RequireDetailDisassemble> requireDetailDisassembles) {
        User user = LoginUtils.getCurrentUserInfo();
        if (requireDetailDisassembles.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<RequireDetailDisassemble> datasDb = requireDetailDisassembleService.listByIds(requireDetailDisassembles.stream().map(i -> i.getAssetSn()).collect(Collectors.toList()));

        List<RequireDetailDisassemble> can = new ArrayList<>();
        List<RequireDetailDisassemble> no = new ArrayList<>();
        for (RequireDetailDisassemble data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            requireDetailDisassembleService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getAssetSn()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "采购需求明细拆分-根据主键查询一条数据", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = RequireDetailDisassemble.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(RequireDetailDisassemble requireDetailDisassemble) {
        RequireDetailDisassemble data = (RequireDetailDisassemble) requireDetailDisassembleService.getById(requireDetailDisassemble);
        return Result.ok().setData(data);
    }

}
