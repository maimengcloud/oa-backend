package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_work_overtime_info")
@ApiModel(description="加班表")
public class WorkOvertimeInfo  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="ID,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="员工id",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="员工名称",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="加班类型",allowEmptyValue=true,example="",allowableValues="")
	String workOvertimeType;

	
	@ApiModelProperty(notes="开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date beginTime;

	
	@ApiModelProperty(notes="结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="加班小时数",allowEmptyValue=true,example="",allowableValues="")
	String workOvertimeHours;

	
	@ApiModelProperty(notes="加班原因",allowEmptyValue=true,example="",allowableValues="")
	String reasonWorkOvertime;

	
	@ApiModelProperty(notes="组织id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="部门id",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="部门名称",allowEmptyValue=true,example="",allowableValues="")
	String deptName;

	
	@ApiModelProperty(notes="申请时间",allowEmptyValue=true,example="",allowableValues="")
	Date applyTime;

	
	@ApiModelProperty(notes="附件",allowEmptyValue=true,example="",allowableValues="")
	String accessory;

	
	@ApiModelProperty(notes="当前流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="当前流程状态",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	
	@ApiModelProperty(notes="标题",allowEmptyValue=true,example="",allowableValues="")
	String title;

	/**
	 *ID
	 **/
	public WorkOvertimeInfo(String id) {
		this.id = id;
	}
    
    /**
     * 加班表
     **/
	public WorkOvertimeInfo() {
	}

}