package com.oa.matter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("sxk_matter_info")
@ApiModel(description="事项库-事项基本信息")
public class MatterInfo  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="专业名称",allowEmptyValue=true,example="",allowableValues="")
	String majorName;

	
	@ApiModelProperty(notes="业务名称",allowEmptyValue=true,example="",allowableValues="")
	String businessName;

	
	@ApiModelProperty(notes="公司手册",allowEmptyValue=true,example="",allowableValues="")
	String companyHandbook;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public MatterInfo(String id) {
		this.id = id;
	}
    
    /**
     * 事项库-事项基本信息
     **/
	public MatterInfo() {
	}

}