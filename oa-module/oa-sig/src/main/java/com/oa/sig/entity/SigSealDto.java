package com.oa.sig.entity;

import java.util.List;

/**
 * 接受用印申请信息
 */
public class SigSealDto extends SigSeal{
    private List<SigSealSignet> sigSealSignets ;

    public List<SigSealSignet> getSigSealSignets() {
        return sigSealSignets;
    }

    public void setSigSealSignets(List<SigSealSignet> sigSealSignets) {
        this.sigSealSignets = sigSealSignets;
    }
}

