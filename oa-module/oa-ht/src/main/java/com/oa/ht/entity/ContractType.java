package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_type")
@ApiModel(description="ht_contract_type")
public class ContractType  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="合同类型id,主键",allowEmptyValue=true,example="",allowableValues="")
	String typeId;

	
	@ApiModelProperty(notes="合同上级id",allowEmptyValue=true,example="",allowableValues="")
	String typeParentId;

	
	@ApiModelProperty(notes="合同类型名称",allowEmptyValue=true,example="",allowableValues="")
	String typeName;

	
	@ApiModelProperty(notes="合同类型名称",allowEmptyValue=true,example="",allowableValues="")
	String typeDesc;

	
	@ApiModelProperty(notes="合同层级关系",allowEmptyValue=true,example="",allowableValues="")
	String typeLevel;

	/**
	 *合同类型id
	 **/
	public ContractType(String typeId) {
		this.typeId = typeId;
	}
    
    /**
     * ht_contract_type
     **/
	public ContractType() {
	}

}