package com.oa.sig.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.sig.entity.SigSealSignet;
import com.oa.sig.service.SigSealSignetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/sig/sigSealSignet")
@Api(tags = {"sig_seal_signet-操作接口"})
public class SigSealSignetController {

    static Logger logger = LoggerFactory.getLogger(SigSealSignetController.class);

    @Autowired
    private SigSealSignetService sigSealSignetService;

    @ApiOperation(value = "sig_seal_signet-查询列表", notes = " ")
    @ApiEntityParams(SigSealSignet.class)
    @ApiResponses({@ApiResponse(code = 200, response = SigSealSignet.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listSigSealSignet(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<SigSealSignet> qw = QueryTools.initQueryWrapper(SigSealSignet.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = sigSealSignetService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "sig_seal_signet-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = SigSealSignet.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addSigSealSignet(@RequestBody SigSealSignet sigSealSignet) {
        sigSealSignetService.save(sigSealSignet);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "sig_seal_signet-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delSigSealSignet(@RequestBody SigSealSignet sigSealSignet) {
        sigSealSignetService.removeById(sigSealSignet);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "sig_seal_signet-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = SigSealSignet.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editSigSealSignet(@RequestBody SigSealSignet sigSealSignet) {
        sigSealSignetService.updateById(sigSealSignet);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "sig_seal_signet-批量修改某些字段", notes = "")
    @ApiEntityParams(value = SigSealSignet.class, props = {}, remark = "sig_seal_signet", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = SigSealSignet.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        sigSealSignetService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "sig_seal_signet-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelSigSealSignet(@RequestBody List<SigSealSignet> sigSealSignets) {
        User user = LoginUtils.getCurrentUserInfo();
        if (sigSealSignets.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<SigSealSignet> datasDb = sigSealSignetService.listByIds(sigSealSignets.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<SigSealSignet> can = new ArrayList<>();
        List<SigSealSignet> no = new ArrayList<>();
        for (SigSealSignet data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            sigSealSignetService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "sig_seal_signet-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = SigSealSignet.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(SigSealSignet sigSealSignet) {
        SigSealSignet data = (SigSealSignet) sigSealSignetService.getById(sigSealSignet);
        return Result.ok().setData(data);
    }

}
