package com.oa.erp.pur.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.erp.pur.entity.Order;
import com.oa.erp.pur.entity.OrderDetail;
import com.oa.erp.pur.entity.RequireDetail;
import com.oa.erp.pur.mapper.OrderDetailMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class OrderDetailService extends BaseService<OrderDetailMapper, OrderDetail> {
    static Logger logger = LoggerFactory.getLogger(OrderDetailService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    @Autowired
    private OrderService orderService;


    @Autowired
    private RequireDetailService requireDetailService;


    @Transactional
    public void joinToOrder(List<OrderDetail> orderDetailsInsert, List<OrderDetail> orderDetailsUpdate, List<RequireDetail> details) {
        if (orderDetailsInsert != null && !orderDetailsInsert.isEmpty()) {
            this.saveOrUpdateBatch(orderDetailsInsert);
        }
        if (orderDetailsUpdate != null && !orderDetailsUpdate.isEmpty()) {
            this.saveOrUpdateBatch(orderDetailsUpdate);
        }
        if (details != null && !details.isEmpty()) {
            this.requireDetailService.batchUpdateOrderId(details);
        }

    }

    @Transactional
    public void unJoinToOrder(List<RequireDetail> details, List<OrderDetail> values) {
        baseMapper.batchUpdateStockNum(values);
        this.requireDetailService.batchUpdateOrderIdToNull(details);

    }

    public void batchDeleteDetailByPurOrderId(List<Order> orders) {
        baseMapper.batchDeleteDetailByPurOrderId(orders);
//        baseMapper.deleteBatchIds(orders);
    }

    public List<OrderDetail> selectDetailByPurOrderIds(List<String> idList) {
        return baseMapper.selectDetailByPurOrderIds(idList);
    }
}

