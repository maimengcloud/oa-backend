package com.oa.jx.vo;

import java.util.List;

public class BatchGradeVo {

    List<String> ids;
    String command;

     String reUserid;

     String reUsername;

     String vouchId;
     String vouchName;

     String status;

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getReUserid() {
        return reUserid;
    }

    public void setReUserid(String reUserid) {
        this.reUserid = reUserid;
    }

    public String getReUsername() {
        return reUsername;
    }

    public void setReUsername(String reUsername) {
        this.reUsername = reUsername;
    }

    public String getVouchId() {
        return vouchId;
    }

    public void setVouchId(String vouchId) {
        this.vouchId = vouchId;
    }

    public String getVouchName() {
        return vouchName;
    }

    public void setVouchName(String vouchName) {
        this.vouchName = vouchName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
