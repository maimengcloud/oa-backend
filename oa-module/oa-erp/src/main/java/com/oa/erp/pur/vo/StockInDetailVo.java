package com.oa.erp.pur.vo;

import com.oa.erp.pur.entity.StockInDetail;

public class StockInDetailVo extends StockInDetail {

    //采购单资产编码
    private String yuanAssetSn;

    //仓库名称
    private String warehouseName;

    //分类名称
    private String categoryName;

    public String getYuanAssetSn() {
        return yuanAssetSn;
    }

    public void setYuanAssetSn(String yuanAssetSn) {
        this.yuanAssetSn = yuanAssetSn;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
