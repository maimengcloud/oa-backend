package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_holidays")
@ApiModel(description="节假日表")
public class Holidays  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="开始日期",allowEmptyValue=true,example="",allowableValues="")
	Date beginDate;

	
	@ApiModelProperty(notes="年号",allowEmptyValue=true,example="",allowableValues="")
	String year;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="放假天数",allowEmptyValue=true,example="",allowableValues="")
	String number;

	
	@ApiModelProperty(notes="节假日名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date endDate;

	
	@ApiModelProperty(notes="月份",allowEmptyValue=true,example="",allowableValues="")
	String month;

	/**
	 *id
	 **/
	public Holidays(String id) {
		this.id = id;
	}
    
    /**
     * 节假日表
     **/
	public Holidays() {
	}

}