package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_group")
@ApiModel(description="考勤组表")
public class Group  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="组名",allowEmptyValue=true,example="",allowableValues="")
	String groupName;

	
	@ApiModelProperty(notes="考勤方式012（固定，排班，自由）",allowEmptyValue=true,example="",allowableValues="")
	String kqType;

	
	@ApiModelProperty(notes="是否允许外勤打卡（0不允许1允许)",allowEmptyValue=true,example="",allowableValues="")
	String isOut;

	
	@ApiModelProperty(notes="考勤地点有效范围",allowEmptyValue=true,example="",allowableValues="")
	String effectiveRange;

	
	@ApiModelProperty(notes="部门编号",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *编号id
	 **/
	public Group(String id) {
		this.id = id;
	}
    
    /**
     * 考勤组表
     **/
	public Group() {
	}

}