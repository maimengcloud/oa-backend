package com.oa.hr.user.listenter;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.mdp.core.err.BizException;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.hr.user.entity.HrUserBaseInfo;
import com.oa.hr.user.service.HrUserBaseInfoService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HrUserBaseInfoExcelListener extends AnalysisEventListener<HrUserBaseInfo> {

    //因为SubjectExcelListener不能交给spring进行管理，需要自己new，不能注入其他对象
    public HrUserBaseInfoService hrUserBaseInfoService;
    public List<HrUserBaseInfo> hrUserBaseInfoList = new ArrayList<HrUserBaseInfo>();
    List<Map<String, Object>> userList;
    Map<String, Object> uidMap = new HashMap<String, Object>();

    public HrUserBaseInfoExcelListener() {
    }

    public HrUserBaseInfoExcelListener(HrUserBaseInfoService hrUserBaseInfoService) {
        this.hrUserBaseInfoService = hrUserBaseInfoService;
    }

    //读取excel内容，一行一行进行读取
    @Override
    @Transactional
    public void invoke(HrUserBaseInfo hrUserBaseInfo, AnalysisContext analysisContext) {
        if (!StringUtils.isEmpty(hrUserBaseInfo)) {
            hrUserBaseInfoList.add(hrUserBaseInfo);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

        //验证员工是否存在
        String userErrorMsg = this.validateUserExist();
        //验证档案是否已经创建
        String recordErrorMsg = this.validateRecordExist();
        //验证用户信息是否正确
        String userInfoErrorMsg = this.validateUserInfoExist();

        if (!StringUtils.isEmpty(userErrorMsg) ||
                !StringUtils.isEmpty(recordErrorMsg) ||
                !StringUtils.isEmpty(userInfoErrorMsg)) {
            throw new BizException(userErrorMsg + recordErrorMsg + userInfoErrorMsg);
        }
        //保存到数据库
        //以上验证都没问题就增加员工档案
        for (int i = 0; i < hrUserBaseInfoList.size(); i++) {
            HrUserBaseInfo hrUserBaseInfo = hrUserBaseInfoList.get(i);
            hrUserBaseInfo.setId(hrUserBaseInfoService.createKey("id"));
            hrUserBaseInfoService.insert(hrUserBaseInfo);
        }

    }

    //验证用户是否存在
    public String validateUserExist() {
        String errMsg = "";
        for (int i = 0; i < hrUserBaseInfoList.size(); i++) {
            String userid = hrUserBaseInfoList.get(i).getUserid();
            Map user = (Map) uidMap.get(userid);
            if (StringUtils.isEmpty(user)) {
                errMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行用户不存在" + "</span></br>";
                hrUserBaseInfoList.remove(i);
            }
        }
        return errMsg;
    }

    //验证用户档案是否存在
    public String validateRecordExist() {
        String errMsg = "";
        List<Map<String, Object>> maps = hrUserBaseInfoService.selectListMapByWhere(null, null, null);
        Map<String, Map> baseInfoMap = new HashMap<String, Map>();
        for (int i = 0; i < maps.size(); i++) {
            String userid = (String) maps.get(i).get("userid");
            baseInfoMap.put(userid, maps.get(i));
        }

        for (int i = 0; i < hrUserBaseInfoList.size(); i++) {
            String userid = hrUserBaseInfoList.get(i).getUserid();
            Map user = (Map) baseInfoMap.get(userid);
            if (!StringUtils.isEmpty(user)) {
                errMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行用户档案已经存在" + "</span></br>";
                hrUserBaseInfoList.remove(i);
            }
        }
        return errMsg;
    }

    //验证用户信息是否正确
    public String validateUserInfoExist() {
        String errMsg = "";
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        for (int i = 0; i < hrUserBaseInfoList.size(); i++) {
            String userid = hrUserBaseInfoList.get(i).getUserid();
            String branchId1 = hrUserBaseInfoList.get(i).getBranchId();
            String account = hrUserBaseInfoList.get(i).getAccount();
            Map user = (Map) uidMap.get(userid);
            String displayUserid = (String) user.get("displayUserid");
            if (!branchId.equals(branchId1)) {
                errMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行【组织机构号不正确】" + "</span></br>";
            }
            if (!displayUserid.equals(account)) {
                errMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行【系统账号不正确】" + "</span></br>";
            }
        }
        return errMsg;
    }
}
