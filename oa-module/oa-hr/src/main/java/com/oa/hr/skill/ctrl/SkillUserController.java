package com.oa.hr.skill.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.skill.entity.HrSkillUser;
import com.oa.hr.skill.entity.HrSkillUserVo;
import com.oa.hr.skill.service.SkillUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController("oa.hr.skill.hrSkillUserController")
@RequestMapping(value = "/*/oa/hr/skill/hrSkillUser")
@Api(tags = {"技能定义表-操作接口"})
public class SkillUserController {

    static Logger logger = LoggerFactory.getLogger(SkillUserController.class);

    @Autowired
    private SkillUserService skillUserService;

    @ApiOperation(value = "技能定义表-查询列表", notes = " ")
    @ApiEntityParams(HrSkillUser.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrSkillUser.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrSkillUser(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<HrSkillUser> qw = QueryTools.initQueryWrapper(HrSkillUser.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = skillUserService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ApiOperation(value = "根据主键列表批量删除hr_skill_user信息", notes = "batchDelHrSkillUser,仅需要上传主键字段")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchEdit", method = RequestMethod.POST)
    public Result batchEdit(@RequestBody HrSkillUserVo hrSkillUserVo) {
        skillUserService.batchEdit(hrSkillUserVo);
        return Result.ok("", "更新成功");
    }

    @ApiOperation(value = "技能定义表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrSkillUser.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrSkillUser(@RequestBody HrSkillUser hrSkillUser) {
        skillUserService.save(hrSkillUser);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "技能定义表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrSkillUser(@RequestBody HrSkillUser hrSkillUser) {
        skillUserService.removeById(hrSkillUser);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "技能定义表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrSkillUser.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrSkillUser(@RequestBody HrSkillUser hrSkillUser) {
        skillUserService.updateById(hrSkillUser);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "技能定义表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrSkillUser.class, props = {}, remark = "技能定义表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrSkillUser.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        skillUserService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "技能定义表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrSkillUser(@RequestBody List<HrSkillUser> hrSkillUsers) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrSkillUsers.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrSkillUser> datasDb = skillUserService.listByIds(hrSkillUsers.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrSkillUser> can = new ArrayList<>();
        List<HrSkillUser> no = new ArrayList<>();
        for (HrSkillUser data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            skillUserService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "技能定义表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrSkillUser.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrSkillUser hrSkillUser) {
        HrSkillUser data = (HrSkillUser) skillUserService.getById(hrSkillUser);
        return Result.ok().setData(data);
    }

}
