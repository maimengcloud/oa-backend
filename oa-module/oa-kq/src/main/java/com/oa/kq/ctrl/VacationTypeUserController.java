package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.BaseUtils;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.VacationTypeUser;
import com.oa.kq.service.VacationTypeUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mdp.core.utils.BaseUtils.map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/kq/vacationTypeUser")
@Api(tags = {"员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得-操作接口"})
public class VacationTypeUserController {

    static Logger logger = LoggerFactory.getLogger(VacationTypeUserController.class);

    @Autowired
    private VacationTypeUserService vacationTypeUserService;

    @ApiOperation(value = "员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得-查询列表", notes = " ")
    @ApiEntityParams(VacationTypeUser.class)
    @ApiResponses({@ApiResponse(code = 200, response = VacationTypeUser.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listVacationTypeUser(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<VacationTypeUser> qw = QueryTools.initQueryWrapper(VacationTypeUser.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = vacationTypeUserService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = VacationTypeUser.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addVacationTypeUser(@RequestBody VacationTypeUser vacationTypeUser) {
        QueryWrapper<VacationTypeUser> qw = QueryTools.initQueryWrapper(VacationTypeUser.class, map());
        IPage page = QueryTools.initPage(BaseUtils.toMap(vacationTypeUser));

        if (StringUtils.isEmpty(vacationTypeUser.getId())) {
            vacationTypeUser.setId(vacationTypeUserService.createKey("id"));
        } else {
            VacationTypeUser vacationTypeUserQuery = new VacationTypeUser(vacationTypeUser.getId());
            if (vacationTypeUserService.countByWhere(vacationTypeUserQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("userid", vacationTypeUser.getUserid());
        map.put("vacationId", vacationTypeUser.getVacationId());
        map.put("year", vacationTypeUser.getYear());
        List<Map<String, Object>> maps = vacationTypeUserService.selectListMapByWhere(page, qw, map);
        if (maps.size() > 0) {
            String vacationType = (String) maps.get(0).get("vacationType");
            return Result.error("员工今年" + vacationType + "已经创建");
        }
        vacationTypeUserService.insert(vacationTypeUser);
        return Result.ok("add-ok", "添加成功！").setData(vacationTypeUser);
    }

    @ApiOperation(value = "员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delVacationTypeUser(@RequestBody VacationTypeUser vacationTypeUser) {
        vacationTypeUserService.deleteByPk(vacationTypeUser);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = VacationTypeUser.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editVacationTypeUser(@RequestBody VacationTypeUser vacationTypeUser) {
        vacationTypeUserService.updateByPk(vacationTypeUser);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得-批量修改某些字段", notes = "")
    @ApiEntityParams(value = VacationTypeUser.class, props = {}, remark = "员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = VacationTypeUser.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        vacationTypeUserService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelVacationTypeUser(@RequestBody List<VacationTypeUser> vacationTypeUsers) {
        User user = LoginUtils.getCurrentUserInfo();
        if (vacationTypeUsers.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<VacationTypeUser> datasDb = vacationTypeUserService.listByIds(vacationTypeUsers.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<VacationTypeUser> can = new ArrayList<>();
        List<VacationTypeUser> no = new ArrayList<>();
        for (VacationTypeUser data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            vacationTypeUserService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = VacationTypeUser.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(VacationTypeUser vacationTypeUser) {
        VacationTypeUser data = (VacationTypeUser) vacationTypeUserService.getById(vacationTypeUser);
        return Result.ok().setData(data);
    }

}
