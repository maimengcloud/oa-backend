package com.oa.mail.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * 组织 com.qqkj  顶级模块 oa 大模块 mail  小模块 <br> 
 * 实体 MailEmail所有属性名: <br>
 *	id,subject,content,userId,username,sendEmail,sendTime,type,isStar,isRead,isOuter,status;<br>
 * 表 OA.mail_email mail_email的所有字段名: <br>
 *	id,subject,content,user_id,username,send_email,send_time,type,is_star,is_read,is_outer,status;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
@ApiModel(description="mail_email")
public class MailEmailVo extends MailEmail {

	@ApiModelProperty(notes="收件人",allowEmptyValue=true,example="",allowableValues="")
	List<MailEmailAddressee> addresseeList;

	@ApiModelProperty(notes="抄送人",allowEmptyValue=true,example="",allowableValues="")
	List<MailEmailAddressee> ccList;

	@ApiModelProperty(notes="密送人",allowEmptyValue=true,example="",allowableValues="")
	List<MailEmailAddressee> bccList;

	@ApiModelProperty(notes="附件列表",allowEmptyValue=true,example="",allowableValues="")
	List<MailEmailAppendix> appendixList;

	public List<MailEmailAppendix> getAppendixList() {
		return appendixList;
	}

	public void setAppendixList(List<MailEmailAppendix> appendixList) {
		this.appendixList = appendixList;
	}

	public List<MailEmailAddressee> getAddresseeList() {
		return addresseeList;
	}

	public void setAddresseeList(List<MailEmailAddressee> addresseeList) {
		this.addresseeList = addresseeList;
	}

	public List<MailEmailAddressee> getCcList() {
		return ccList;
	}

	public void setCcList(List<MailEmailAddressee> ccList) {
		this.ccList = ccList;
	}

	public List<MailEmailAddressee> getBccList() {
		return bccList;
	}

	public void setBccList(List<MailEmailAddressee> bccList) {
		this.bccList = bccList;
	}
}