package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_user_contract")
@ApiModel(description="员工合同信息表")
public class HrUserContract  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="用户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="合同名称",allowEmptyValue=true,example="",allowableValues="")
	String contractName;

	
	@ApiModelProperty(notes="合同开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	
	@ApiModelProperty(notes="合同到期时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="合同类型---",allowEmptyValue=true,example="",allowableValues="")
	String contractType;

	
	@ApiModelProperty(notes="合同状态---0-正常，1已到期，2-已解除",allowEmptyValue=true,example="",allowableValues="")
	String contractStatus;

	
	@ApiModelProperty(notes="签订标识---0-首次签订",allowEmptyValue=true,example="",allowableValues="")
	String identification;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="合同办理时间",allowEmptyValue=true,example="",allowableValues="")
	Date processsingTime;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public HrUserContract(String id) {
		this.id = id;
	}
    
    /**
     * 员工合同信息表
     **/
	public HrUserContract() {
	}

}