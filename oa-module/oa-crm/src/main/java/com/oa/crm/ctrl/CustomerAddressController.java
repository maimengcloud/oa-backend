package com.oa.crm.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.crm.entity.CustomerAddress;
import com.oa.crm.service.CustomerAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/crm/customerAddress")
@Api(tags = {"crm_customer_address-操作接口"})
public class CustomerAddressController {

    static Logger logger = LoggerFactory.getLogger(CustomerAddressController.class);

    @Autowired
    private CustomerAddressService customerAddressService;

    @ApiOperation(value = "crm_customer_address-查询列表", notes = " ")
    @ApiEntityParams(CustomerAddress.class)
    @ApiResponses({@ApiResponse(code = 200, response = CustomerAddress.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCustomerAddress(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "addressIds");
        QueryWrapper<CustomerAddress> qw = QueryTools.initQueryWrapper(CustomerAddress.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = customerAddressService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "crm_customer_address-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerAddress.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCustomerAddress(@RequestBody CustomerAddress customerAddress) {


        if (StringUtils.isEmpty(customerAddress.getAddressId())) {
            customerAddress.setAddressId(customerAddressService.createKey("addressId"));
        } else {
            CustomerAddress customerAddressQuery = new CustomerAddress(customerAddress.getAddressId());
            if (customerAddressService.countByWhere(customerAddressQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        customerAddressService.insert(customerAddress);


        return Result.ok("add-ok", "添加成功！").setData(customerAddress);
    }

    @ApiOperation(value = "crm_customer_address-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCustomerAddress(@RequestBody CustomerAddress customerAddress) {


        customerAddressService.deleteByPk(customerAddress);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "crm_customer_address-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerAddress.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCustomerAddress(@RequestBody CustomerAddress customerAddress) {


        customerAddressService.updateByPk(customerAddress);

        return Result.ok("edit-ok", "修改成功！").setData(customerAddress);
    }

    @ApiOperation(value = "crm_customer_address-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CustomerAddress.class, props = {}, remark = "crm_customer_address", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerAddress.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        customerAddressService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "crm_customer_address-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCustomerAddress(@RequestBody List<CustomerAddress> customerAddresss) {
        User user = LoginUtils.getCurrentUserInfo();
        if (customerAddresss.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CustomerAddress> datasDb = customerAddressService.listByIds(customerAddresss.stream().map(i -> i.getAddressId()).collect(Collectors.toList()));

        List<CustomerAddress> can = new ArrayList<>();
        List<CustomerAddress> no = new ArrayList<>();
        for (CustomerAddress data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            customerAddressService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getAddressId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "crm_customer_address-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerAddress.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CustomerAddress customerAddress) {
        CustomerAddress data = (CustomerAddress) customerAddressService.getById(customerAddress);
        return Result.ok().setData(data);
    }

    //查询地址数据
    @ApiOperation(value = "获取客户地址树", notes = "获取客户地址树")
    @RequestMapping(value = "/getAddressTree", method = RequestMethod.GET)
    public Result getAddressTree(@RequestParam Map<String, Object> customerAddress) {
        List<Map<String, Object>> customerAddressList = customerAddressService.getAddressTree(customerAddress);    //列出CustomerAddress列表

        return Result.ok().setData(customerAddressList);
    }
}
