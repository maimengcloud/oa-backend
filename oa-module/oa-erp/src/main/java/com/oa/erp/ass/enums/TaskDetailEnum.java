package com.oa.erp.ass.enums;

public enum TaskDetailEnum {

    INVENTORY_STAY("1", "待盘点"),
    INVENTORY_NORMAL("2", "正常"),
    INVENTORY_NOT("3", "未盘点"),
    INVENTORY_PROFIT("4", "盘盈"),
    INVENTORY_LOSSES("5", "盘亏");


    TaskDetailEnum(String status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    private String status;

    private String desc;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


}
