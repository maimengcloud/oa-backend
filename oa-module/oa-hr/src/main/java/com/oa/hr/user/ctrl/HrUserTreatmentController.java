package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserTreatment;
import com.oa.hr.user.service.HrUserTreatmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value="/*/oa/hr/user/hrUserTreatment")
@Api(tags = {"员工薪酬福利表-操作接口"})
public class HrUserTreatmentController {

    static Logger logger = LoggerFactory.getLogger(HrUserTreatmentController.class);

    @Autowired
    private HrUserTreatmentService hrUserTreatmentService;

    @ApiOperation(value = "员工薪酬福利表-查询列表", notes = " ")
    @ApiEntityParams(HrUserTreatment.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrUserTreatment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrUserTreatment(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("branchId", user.getBranchId());
        QueryWrapper<HrUserTreatment> qw = QueryTools.initQueryWrapper(HrUserTreatment.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = hrUserTreatmentService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @RequestMapping(value = "/editHrUserTreatmentByUserid", method = RequestMethod.POST)
    public Result editHrUserTreatmentByUserid(@RequestBody HrUserTreatment hrUserTreatment) {
        
        
            User user = LoginUtils.getCurrentUserInfo();
            //根据userid查询
            HrUserTreatment treatment = hrUserTreatmentService.selectByUserid(hrUserTreatment.getUserid(), user.getBranchId());
            //不为空就修改，否则就新增
            if (treatment != null) {
                hrUserTreatment.setId(treatment.getId());
                hrUserTreatmentService.updateByPk(hrUserTreatment);
            } else {
                hrUserTreatment.setId(hrUserTreatmentService.createKey("id"));
                hrUserTreatment.setBranchId(user.getBranchId());
                hrUserTreatmentService.insert(hrUserTreatment);
            }
        
        return Result.ok().setData(hrUserTreatment);
    }

    @ApiOperation(value = "员工薪酬福利表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserTreatment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrUserTreatment(@RequestBody HrUserTreatment hrUserTreatment) {
        
        
            User user = LoginUtils.getCurrentUserInfo();
            hrUserTreatment.setBranchId(user.getBranchId());
            if (StringUtils.isEmpty(hrUserTreatment.getId())) {
                hrUserTreatment.setId(hrUserTreatmentService.createKey("id"));
            } else {
                HrUserTreatment hrUserTreatmentQuery = new HrUserTreatment(hrUserTreatment.getId());
                if (hrUserTreatmentService.countByWhere(hrUserTreatmentQuery) > 0) {
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            hrUserTreatmentService.insert(hrUserTreatment);
        
        return Result.ok("add-ok", "添加成功！").setData(hrUserTreatment);
    }

    @ApiOperation(value = "员工薪酬福利表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrUserTreatment(@RequestBody HrUserTreatment hrUserTreatment) {
        
        
            hrUserTreatmentService.deleteByPk(hrUserTreatment);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "员工薪酬福利表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserTreatment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrUserTreatment(@RequestBody HrUserTreatment hrUserTreatment) {
        
        
            hrUserTreatmentService.updateByPk(hrUserTreatment);
        
        return Result.ok("edit-ok", "修改成功！").setData(hrUserTreatment);
    }

    @ApiOperation(value = "员工薪酬福利表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrUserTreatment.class, props = {}, remark = "员工薪酬福利表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserTreatment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        hrUserTreatmentService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "员工薪酬福利表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrUserTreatment(@RequestBody List<HrUserTreatment> hrUserTreatments) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrUserTreatments.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrUserTreatment> datasDb = hrUserTreatmentService.listByIds(hrUserTreatments.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrUserTreatment> can = new ArrayList<>();
        List<HrUserTreatment> no = new ArrayList<>();
        for (HrUserTreatment data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            hrUserTreatmentService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "员工薪酬福利表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserTreatment.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrUserTreatment hrUserTreatment) {
        HrUserTreatment data = (HrUserTreatment) hrUserTreatmentService.getById(hrUserTreatment);
        return Result.ok().setData(data);
    }

}
