package com.oa.ht.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.ht.entity.ContractPaymentInvoice;
import com.oa.ht.entity.ContractReceiveInvoice;
import com.oa.ht.entity.ReceiveLinkInvoicesVo;
import com.oa.ht.service.ContractReceiveInvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mdp.core.utils.BaseUtils.map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/ht/contractReceiveInvoice")
@Api(tags = {"合同收款与开票关系-操作接口"})
public class ContractReceiveInvoiceController {

    static Logger logger = LoggerFactory.getLogger(ContractReceiveInvoiceController.class);

    @Autowired
    private ContractReceiveInvoiceService contractReceiveInvoiceService;

    @ApiOperation(value = "合同收款与开票关系-查询列表", notes = " ")
    @ApiEntityParams(ContractReceiveInvoice.class)
    @ApiResponses({@ApiResponse(code = 200, response = ContractReceiveInvoice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listContractReceiveInvoice(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "receiveIdsinvoiceIds");
        QueryWrapper<ContractReceiveInvoice> qw = QueryTools.initQueryWrapper(ContractReceiveInvoice.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = contractReceiveInvoiceService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "收款关联发票", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractPaymentInvoice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/linkInvoices", method = RequestMethod.POST)
    public Result receiveLinkInvoices(@RequestBody ReceiveLinkInvoicesVo linkInvoices) {
        this.contractReceiveInvoiceService.receiveLinkInvoices(linkInvoices);
        return Result.ok("", "成功更新一条数据");
    }


    @ApiOperation(value = "合同收款与开票关系-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractReceiveInvoice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addContractReceiveInvoice(@RequestBody ContractReceiveInvoice contractReceiveInvoice) {
        contractReceiveInvoiceService.save(contractReceiveInvoice);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "合同收款与开票关系-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delContractReceiveInvoice(@RequestBody ContractReceiveInvoice contractReceiveInvoice) {
        contractReceiveInvoiceService.removeById(contractReceiveInvoice);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "合同收款与开票关系-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractReceiveInvoice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editContractReceiveInvoice(@RequestBody ContractReceiveInvoice contractReceiveInvoice) {
        contractReceiveInvoiceService.updateById(contractReceiveInvoice);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "合同收款与开票关系-批量修改某些字段", notes = "")
    @ApiEntityParams(value = ContractReceiveInvoice.class, props = {}, remark = "合同收款与开票关系", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = ContractReceiveInvoice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        contractReceiveInvoiceService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "合同收款与开票关系-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelContractReceiveInvoice(@RequestBody List<ContractReceiveInvoice> contractReceiveInvoices) {
        User user = LoginUtils.getCurrentUserInfo();
        if (contractReceiveInvoices.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<ContractReceiveInvoice> datasDb = contractReceiveInvoiceService.listByIds(contractReceiveInvoices.stream().map(i -> map("receiveId", i.getReceiveId(), "invoiceId", i.getInvoiceId())).collect(Collectors.toList()));

        List<ContractReceiveInvoice> can = new ArrayList<>();
        List<ContractReceiveInvoice> no = new ArrayList<>();
        for (ContractReceiveInvoice data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            contractReceiveInvoiceService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getReceiveId() + " " + i.getInvoiceId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "合同收款与开票关系-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractReceiveInvoice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(ContractReceiveInvoice contractReceiveInvoice) {
        ContractReceiveInvoice data = (ContractReceiveInvoice) contractReceiveInvoiceService.getById(contractReceiveInvoice);
        return Result.ok().setData(data);
    }

}
