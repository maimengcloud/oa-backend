package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserLeave;
import com.oa.hr.user.service.HrUserLeaveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value="/*/oa/hr/user/hrUserLeave")
@Api(tags = {"员工请假表（作废，用考勤中的请假表）-操作接口"})
public class HrUserLeaveController {

    static Logger logger = LoggerFactory.getLogger(HrUserLeaveController.class);

    @Autowired
    private HrUserLeaveService hrUserLeaveService;

    @ApiOperation(value = "员工请假表（作废，用考勤中的请假表）-查询列表", notes = " ")
    @ApiEntityParams(HrUserLeave.class)
    @ApiResponses({@ApiResponse(code = 200, response = HrUserLeave.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHrUserLeave(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("branchId", user.getBranchId());

        QueryWrapper<HrUserLeave> qw = QueryTools.initQueryWrapper(HrUserLeave.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = hrUserLeaveService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "员工请假表（作废，用考勤中的请假表）-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserLeave.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHrUserLeave(@RequestBody HrUserLeave hrUserLeave) {
        
        
            User user = LoginUtils.getCurrentUserInfo();
            hrUserLeave.setBranchId(user.getBranchId());
            if (StringUtils.isEmpty(hrUserLeave.getId())) {
                hrUserLeave.setId(hrUserLeaveService.createKey("id"));
            } else {
                HrUserLeave hrUserLeaveQuery = new HrUserLeave(hrUserLeave.getId());
                if (hrUserLeaveService.countByWhere(hrUserLeaveQuery) > 0) {
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            hrUserLeaveService.insert(hrUserLeave);
        
        return Result.ok("add-ok", "添加成功！").setData(hrUserLeave);
    }

    @ApiOperation(value = "员工请假表（作废，用考勤中的请假表）-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHrUserLeave(@RequestBody HrUserLeave hrUserLeave) {
        
        
            hrUserLeaveService.deleteByPk(hrUserLeave);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "员工请假表（作废，用考勤中的请假表）-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserLeave.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHrUserLeave(@RequestBody HrUserLeave hrUserLeave) {
        
        
            hrUserLeaveService.updateByPk(hrUserLeave);
        
        return Result.ok("edit-ok", "修改成功！").setData(hrUserLeave);
    }

    @ApiOperation(value = "员工请假表（作废，用考勤中的请假表）-批量修改某些字段", notes = "")
    @ApiEntityParams(value = HrUserLeave.class, props = {}, remark = "员工请假表（作废，用考勤中的请假表）", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserLeave.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        hrUserLeaveService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "员工请假表（作废，用考勤中的请假表）-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHrUserLeave(@RequestBody List<HrUserLeave> hrUserLeaves) {
        User user = LoginUtils.getCurrentUserInfo();
        if (hrUserLeaves.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<HrUserLeave> datasDb = hrUserLeaveService.listByIds(hrUserLeaves.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<HrUserLeave> can = new ArrayList<>();
        List<HrUserLeave> no = new ArrayList<>();
        for (HrUserLeave data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            hrUserLeaveService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "员工请假表（作废，用考勤中的请假表）-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = HrUserLeave.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(HrUserLeave hrUserLeave) {
        HrUserLeave data = (HrUserLeave) hrUserLeaveService.getById(hrUserLeave);
        return Result.ok().setData(data);
    }

}
