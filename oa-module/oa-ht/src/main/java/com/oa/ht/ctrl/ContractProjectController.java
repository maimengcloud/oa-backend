package com.oa.ht.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.ht.entity.ContractProject;
import com.oa.ht.entity.ContractProjectBatchVo;
import com.oa.ht.service.ContractProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/ht/contractProject")
@Api(tags = {"项目合同关联关系表-操作接口"})
public class ContractProjectController {

    static Logger logger = LoggerFactory.getLogger(ContractProjectController.class);

    @Autowired
    private ContractProjectService contractProjectService;

    @ApiOperation(value = "项目合同关联关系表-查询列表", notes = " ")
    @ApiEntityParams(ContractProject.class)
    @ApiResponses({@ApiResponse(code = 200, response = ContractProject.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listContractProject(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<ContractProject> qw = QueryTools.initQueryWrapper(ContractProject.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = contractProjectService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "项目合同关联关系表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractProject.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addContractProject(@RequestBody ContractProject contractProject) {
        contractProjectService.save(contractProject);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "项目合同关联关系表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delContractProject(@RequestBody ContractProject contractProject) {
        contractProjectService.removeById(contractProject);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "项目合同关联关系表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractProject.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editContractProject(@RequestBody ContractProject contractProject) {
        contractProjectService.updateById(contractProject);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "项目合同关联关系表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = ContractProject.class, props = {}, remark = "项目合同关联关系表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = ContractProject.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        contractProjectService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "项目合同关联关系表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelContractProject(@RequestBody List<ContractProject> contractProjects) {
        User user = LoginUtils.getCurrentUserInfo();
        if (contractProjects.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<ContractProject> datasDb = contractProjectService.listByIds(contractProjects.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<ContractProject> can = new ArrayList<>();
        List<ContractProject> no = new ArrayList<>();
        for (ContractProject data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            contractProjectService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "项目合同关联关系表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractProject.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(ContractProject contractProject) {
        ContractProject data = (ContractProject) contractProjectService.getById(contractProject);
        return Result.ok().setData(data);
    }


    @ApiOperation(value = "批量新增修改合同首付款条件", notes = "batchSave")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
    })
    @RequestMapping(value = "/batchSave", method = RequestMethod.POST)
    public Result batchSave(@RequestBody ContractProjectBatchVo contractProjects) {
        contractProjectService.batchSave(contractProjects);
        return Result.ok("update-ok", "成功更新");
    }

}
