package com.oa.hr.skill.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.micro.client.CallBizService;
import com.oa.hr.skill.entity.HrSkill;
import com.oa.hr.skill.entity.HrSkillUser;
import com.oa.hr.skill.entity.HrSkillUserVo;
import com.oa.hr.skill.mapper.SkillUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */

@Service("oa.hr.skill.hrSkillUserService")
public class SkillUserService extends BaseService<SkillUserMapper,HrSkillUser> {
	static Logger logger =LoggerFactory.getLogger(SkillUserService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}

	@Autowired
	private SkillService skillService;

	@Autowired
	CallBizService callBizService;

	public void batchEdit(HrSkillUserVo hrSkillUserVo) {
		HrSkillUser hrSkillUserDelete=new HrSkillUser();
		hrSkillUserDelete.setUserid(hrSkillUserVo.getUserid());
		this.deleteByWhere(hrSkillUserDelete);
		//后保存
		if(hrSkillUserVo.getSkillIds()!=null && hrSkillUserVo.getSkillIds().size()>0){
			List<String> skillIds=hrSkillUserVo.getSkillIds();
			List<HrSkillUser> hrSkillUserList=new ArrayList<>();
			for (int i = skillIds.size() - 1; i >= 0; i--) {
				String skillId = skillIds.get(i);
				HrSkillUser hrSkillUser = new HrSkillUser();
				hrSkillUser.setUserid(hrSkillUserVo.getUserid());
				hrSkillUser.setSkillId(skillId);
				hrSkillUserList.add(hrSkillUser);
			}
			super.batchInsert(hrSkillUserList);
		}
		new Thread(){
			@Override
			public void run() {
				
					String url = "/sys/sys/userSkill/batchEdit";
					List<HrSkill> skills=new ArrayList<>();
					if(hrSkillUserVo.getSkillIds()!=null && hrSkillUserVo.getSkillIds().size()>0){
						skills = skillService.selectListByIds(hrSkillUserVo.getSkillIds());
					}
					callBizService.postForTips(url, map("userid", hrSkillUserVo.getUserid(), "skills", skills));
				
			}
		}.start();

	}
}

