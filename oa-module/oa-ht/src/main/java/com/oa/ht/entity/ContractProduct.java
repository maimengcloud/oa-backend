package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_product")
@ApiModel(description="ht_contract_product")
public class ContractProduct  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="合同产品id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="合同id",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	
	@ApiModelProperty(notes="产品id",allowEmptyValue=true,example="",allowableValues="")
	String productId;

	
	@ApiModelProperty(notes="产品名称",allowEmptyValue=true,example="",allowableValues="")
	String productName;

	
	@ApiModelProperty(notes="产品规格型号",allowEmptyValue=true,example="",allowableValues="")
	String productSpec;

	
	@ApiModelProperty(notes="产品类型",allowEmptyValue=true,example="",allowableValues="")
	String productType;

	
	@ApiModelProperty(notes="产品单位",allowEmptyValue=true,example="",allowableValues="")
	String productMeasUnit;

	
	@ApiModelProperty(notes="产品单价",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal productPrice;

	
	@ApiModelProperty(notes="产品数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal productNum;

	
	@ApiModelProperty(notes="产品总价",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal productGrossAmount;

	
	@ApiModelProperty(notes="是否第三方采购",allowEmptyValue=true,example="",allowableValues="")
	String isThirdParty;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *合同产品id
	 **/
	public ContractProduct(String id) {
		this.id = id;
	}
    
    /**
     * ht_contract_product
     **/
	public ContractProduct() {
	}

}