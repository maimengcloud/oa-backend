package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_attendance_define")
@ApiModel(description="擎勤科技考勤记录表")
public class AttendanceDefine  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="用户id",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="签到时间",allowEmptyValue=true,example="",allowableValues="")
	Date workTime;

	
	@ApiModelProperty(notes="上下班状态（上班0，下班1",allowEmptyValue=true,example="",allowableValues="")
	String state;

	
	@ApiModelProperty(notes="流程状态(0,1)",allowEmptyValue=true,example="",allowableValues="")
	String lcState;

	
	@ApiModelProperty(notes="是否已打卡",allowEmptyValue=true,example="",allowableValues="")
	String isAtt;

	
	@ApiModelProperty(notes="打卡状态0正常1迟到2严重迟到3旷工4早退5缺卡",allowEmptyValue=true,example="",allowableValues="")
	String dkState;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public AttendanceDefine(String id) {
		this.id = id;
	}
    
    /**
     * 擎勤科技考勤记录表
     **/
	public AttendanceDefine() {
	}

}