package com.oa.crm.vo;

import com.oa.crm.entity.CustomerAgenda;
import com.oa.crm.entity.CustomerAgendaDoc;
import com.oa.crm.entity.CustomerAgendaUser;

import java.util.List;

public class CustomerAgendaAddVo {

    private CustomerAgenda customerAgenda;

    private List<CustomerAgendaDoc> customerAgendaDocList;

    private List<CustomerAgendaUser> customerAgendaUsers;

    public CustomerAgenda getCustomerAgenda() {
        return customerAgenda;
    }

    public void setCustomerAgenda(CustomerAgenda customerAgenda) {
        this.customerAgenda = customerAgenda;
    }

    public List<CustomerAgendaDoc> getCustomerAgendaDocList() {
        return customerAgendaDocList;
    }

    public void setCustomerAgendaDocList(List<CustomerAgendaDoc> customerAgendaDocList) {
        this.customerAgendaDocList = customerAgendaDocList;
    }

    public List<CustomerAgendaUser> getCustomerAgendaUsers() {
        return customerAgendaUsers;
    }

    public void setCustomerAgendaUsers(List<CustomerAgendaUser> customerAgendaUsers) {
        this.customerAgendaUsers = customerAgendaUsers;
    }
}
