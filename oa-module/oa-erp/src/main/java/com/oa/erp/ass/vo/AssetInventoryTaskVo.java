package com.oa.erp.ass.vo;

import com.oa.erp.ass.entity.AssetInventoryTask;
import com.oa.erp.ass.entity.AssetInventoryTaskDetail;
import com.oa.erp.ass.entity.AssetInventoryTaskUser;

import java.util.ArrayList;
import java.util.List;

/**
 * 盘点任务
 */
public class    AssetInventoryTaskVo {

    private AssetInventoryTask inventoryTask;

    private List<AssetInventoryTaskUser> inventoryUserDatas = new ArrayList<>();

    private List<AssetInventoryTaskDetail> inventoryTaskDetails = new ArrayList<>();

    public AssetInventoryTask getInventoryTask() {
        return inventoryTask;
    }

    public void setInventoryTask(AssetInventoryTask inventoryTask) {
        this.inventoryTask = inventoryTask;
    }

    public List<AssetInventoryTaskUser> getInventoryUserDatas() {
        return inventoryUserDatas;
    }

    public void setInventoryUserDatas(List<AssetInventoryTaskUser> inventoryUserDatas) {
        this.inventoryUserDatas = inventoryUserDatas;
    }

    public List<AssetInventoryTaskDetail> getInventoryTaskDetails() {
        return inventoryTaskDetails;
    }

    public void setInventoryTaskDetails(List<AssetInventoryTaskDetail> inventoryTaskDetails) {
        this.inventoryTaskDetails = inventoryTaskDetails;
    }

    @Override
    public String toString() {
        return "AssetInventoryTaskVo{" +
                "inventoryTask=" + inventoryTask +
                ", inventoryUserDatas=" + inventoryUserDatas +
                ", inventoryTaskDetails=" + inventoryTaskDetails +
                '}';
    }
}
