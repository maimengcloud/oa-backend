package com.oa.mail.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("mail_account")
@ApiModel(description="邮件附件表")
public class MailAccount  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="邮箱",allowEmptyValue=true,example="",allowableValues="")
	String email;

	
	@ApiModelProperty(notes="密码或授权码",allowEmptyValue=true,example="",allowableValues="")
	String password;

	
	@ApiModelProperty(notes="收件服务器类型",allowEmptyValue=true,example="",allowableValues="")
	String receiveServerType;

	
	@ApiModelProperty(notes="收件服务器类型",allowEmptyValue=true,example="",allowableValues="")
	String receiveServer;

	
	@ApiModelProperty(notes="是否使用SSL",allowEmptyValue=true,example="",allowableValues="")
	String receiveIsSsl;

	
	@ApiModelProperty(notes="端口",allowEmptyValue=true,example="",allowableValues="")
	String receivePort;

	
	@ApiModelProperty(notes="SMTP服务器",allowEmptyValue=true,example="",allowableValues="")
	String sendServer;

	
	@ApiModelProperty(notes="是否使用SSL",allowEmptyValue=true,example="",allowableValues="")
	String sendIsSsl;

	
	@ApiModelProperty(notes="端口",allowEmptyValue=true,example="",allowableValues="")
	String sendPort;

	
	@ApiModelProperty(notes="昵称",allowEmptyValue=true,example="",allowableValues="")
	String nickName;

	
	@ApiModelProperty(notes="是否使用昵称",allowEmptyValue=true,example="",allowableValues="")
	String isUseNick;

	
	@ApiModelProperty(notes="是否默认",allowEmptyValue=true,example="",allowableValues="")
	String isDefault;

	
	@ApiModelProperty(notes="备注说明",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *主键
	 **/
	public MailAccount(String id) {
		this.id = id;
	}
    
    /**
     * 邮件附件表
     **/
	public MailAccount() {
	}

}