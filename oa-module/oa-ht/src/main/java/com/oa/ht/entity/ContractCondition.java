package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_condition")
@ApiModel(description="ht_contract_condition")
public class ContractCondition  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="回款条件/收款条件,主键",allowEmptyValue=true,example="",allowableValues="")
	String conditionId;

	
	@ApiModelProperty(notes="合同id",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	
	@ApiModelProperty(notes="款项类别-来自字典表amountType",allowEmptyValue=true,example="",allowableValues="")
	String conditionType;

	
	@ApiModelProperty(notes="款项条件",allowEmptyValue=true,example="",allowableValues="")
	String conditionTiaoJian;

	
	@ApiModelProperty(notes="比例",allowEmptyValue=true,example="",allowableValues="")
	Integer conditionRatio;

	
	@ApiModelProperty(notes="金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal conditionAmount;

	
	@ApiModelProperty(notes="状态0-初始，1-待付款/待收款，2-已付款/已收款，4-客户永久拒绝付款，5-我方永久拒绝付款",allowEmptyValue=true,example="",allowableValues="")
	String conditionStatus;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String conditionRemark;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="款项方向0-付款1-收款",allowEmptyValue=true,example="",allowableValues="")
	String dct;

	
	@ApiModelProperty(notes="预计收付款日期",allowEmptyValue=true,example="",allowableValues="")
	Date forecastDate;

	
	@ApiModelProperty(notes="已开票/已收票金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal invoicedAmount;

	
	@ApiModelProperty(notes="已完成款项（已收款/已付款）",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal finishAmount;

	
	@ApiModelProperty(notes="完成时间",allowEmptyValue=true,example="",allowableValues="")
	Date finishDate;

	
	@ApiModelProperty(notes="已申请的付款/已申请的收款",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal requireAmount;

	/**
	 *回款条件/收款条件
	 **/
	public ContractCondition(String conditionId) {
		this.conditionId = conditionId;
	}
    
    /**
     * ht_contract_condition
     **/
	public ContractCondition() {
	}

}