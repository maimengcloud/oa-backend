package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_vacation_type_user")
@ApiModel(description="员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得")
public class VacationTypeUser  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="请假类型id",allowEmptyValue=true,example="",allowableValues="")
	String vacationId;

	
	@ApiModelProperty(notes="员工id",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="已请假期",allowEmptyValue=true,example="",allowableValues="")
	String remainingHoliday;

	
	@ApiModelProperty(notes="组织机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="员工名称",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="年份",allowEmptyValue=true,example="",allowableValues="")
	String year;

	/**
	 *id
	 **/
	public VacationTypeUser(String id) {
		this.id = id;
	}
    
    /**
     * 员工请假剩余假期表（此表应该删除），员工剩余假期通过即时统计获得
     **/
	public VacationTypeUser() {
	}

}