package com.oa.jx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("jx_assess_scheme")
@ApiModel(description="考核方案")
public class JxAssessScheme  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="所属考核方案",allowEmptyValue=true,example="",allowableValues="")
	String schemeType;

	
	@ApiModelProperty(notes="计划开始天数",allowEmptyValue=true,example="",allowableValues="")
	String planStartDay;

	
	@ApiModelProperty(notes="计划开始类型（1-之前；2-之后）",allowEmptyValue=true,example="",allowableValues="")
	String planStartType;

	
	@ApiModelProperty(notes="说明",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="计划结束天数",allowEmptyValue=true,example="",allowableValues="")
	String planEndDay;

	
	@ApiModelProperty(notes="计划结束类型（1-之前；2-之后）",allowEmptyValue=true,example="",allowableValues="")
	String planEndType;

	
	@ApiModelProperty(notes="组织机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="是否启动方案（1-启动；0-不启动）",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="是否提醒考核（1-提醒；0-不提醒）",allowEmptyValue=true,example="",allowableValues="")
	String remindStatus;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createdAt;

	
	@ApiModelProperty(notes="更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date updatedAt;

	
	@ApiModelProperty(notes="方案名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="模板编号",allowEmptyValue=true,example="",allowableValues="")
	String templateId;

	
	@ApiModelProperty(notes="模板名称",allowEmptyValue=true,example="",allowableValues="")
	String templateName;

	
	@ApiModelProperty(notes="是否自动初始化0否1是",allowEmptyValue=true,example="",allowableValues="")
	String autoInit;

	
	@ApiModelProperty(notes="模板总分",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalPoints;

	/**
	 *id
	 **/
	public JxAssessScheme(String id) {
		this.id = id;
	}
    
    /**
     * 考核方案
     **/
	public JxAssessScheme() {
	}

}