package com.oa.car.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.car.entity.CarDriverSignIn;
import com.oa.car.entity.CarRequireRoute;
import com.oa.car.mapper.CarRequireRouteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@Service
public class CarRequireRouteService extends BaseService<CarRequireRouteMapper, CarRequireRoute> {
    static Logger logger = LoggerFactory.getLogger(CarRequireRouteService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    @Autowired
    CarRequireRouteDetailService carRequireRouteDetailService;

    @Autowired
    CarDriverSignInService carDriverSignInService;

    /**
     * 请在此类添加自定义函数
     */
    public CarRequireRoute insertOne(CarRequireRoute carRequireRoute) {
        if (StringUtils.isEmpty(carRequireRoute.getId())) {
            carRequireRoute.setId(this.createKey("id"));
        } else {
            CarRequireRoute carRequireRouteQuery = new CarRequireRoute(carRequireRoute.getId());
            if (this.countByWhere(carRequireRouteQuery) > 0) {
                return null;
            }
        }
        this.insert(carRequireRoute);
        return carRequireRoute;
    }

    public List<Map<String, Object>> selectAddressGPS(IPage page, QueryWrapper qw, Map<String, Object> map) {

        //获取总路线列表
        List<Map<String, Object>> list = baseMapper.selectAddressGPS(map);

        //循环获取路线列表
        list.forEach((route) -> {
            CarDriverSignIn selectObj = new CarDriverSignIn();
            selectObj.setRequireId(route.get("requireId").toString());

            List<CarDriverSignIn> carDriverSignIns = carDriverSignInService.selectListByWhere(selectObj);
            if (carDriverSignIns != null && carDriverSignIns.size() > 0) {
                route.put("signIns", carDriverSignIns);
            }

            String hasDetail = (String) route.get("hasDetail");
            if (!StringUtils.isEmpty(hasDetail) && hasDetail.equals("1")) {
                Map<String, Object> hashMap = new HashMap<>();
                hashMap.put("routeId", route.get("id"));
//                List<Map<String, Object>> childList = carRequireRouteDetailService.selectAddressGPS(hashMap);
                List<Map<String, Object>> childList = carRequireRouteDetailService.selectListMapByWhere(page, qw, hashMap);
                route.put("subRoute", childList);
            }
        });

        return list;

    }
}