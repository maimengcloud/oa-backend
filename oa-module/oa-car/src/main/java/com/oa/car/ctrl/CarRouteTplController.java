package com.oa.car.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.car.entity.CarRouteTpl;
import com.oa.car.entity.CarRouteTplVo;
import com.oa.car.service.CarRouteTplService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@RestController
@RequestMapping(value = "/*/oa/car/carRouteTpl")
@Api(tags = {"car_route_tpl-操作接口"})
public class CarRouteTplController {

    static Logger logger = LoggerFactory.getLogger(CarRouteTplController.class);

    @Autowired
    private CarRouteTplService carRouteTplService;

    @ApiOperation(value = "car_route_tpl-查询列表", notes = " ")
    @ApiEntityParams(CarRouteTpl.class)
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCarRouteTpl(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();//获取登录的机构号
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        params.put("branchId", branchId);
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CarRouteTpl> qw = QueryTools.initQueryWrapper(CarRouteTpl.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = carRouteTplService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "car_route_tpl-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCarRouteTpl(@RequestBody CarRouteTplVo carRouteTplVo) {
        


        carRouteTplService.save(carRouteTplVo);

        return Result.ok("add-ok", "添加成功！").setData(carRouteTplVo);
    }

    @ApiOperation(value = "car_route_tpl-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCarRouteTpl(@RequestBody CarRouteTpl carRouteTpl) {
        


        carRouteTplService.deleteMessage(carRouteTpl);


        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "car_route_tpl-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCarRouteTpl(@RequestBody CarRouteTplVo carRouteTplVo) {
        carRouteTplService.updateMessage(carRouteTplVo);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "car_route_tpl-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CarRouteTpl.class, props = {}, remark = "car_route_tpl", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        carRouteTplService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "car_route_tpl-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCarRouteTpl(@RequestBody List<CarRouteTpl> carRouteTpls) {
        User user = LoginUtils.getCurrentUserInfo();
        if (carRouteTpls.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CarRouteTpl> datasDb = carRouteTplService.listByIds(carRouteTpls.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CarRouteTpl> can = new ArrayList<>();
        List<CarRouteTpl> no = new ArrayList<>();
        for (CarRouteTpl data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            carRouteTplService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "car_route_tpl-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CarRouteTpl carRouteTpl) {
        CarRouteTpl data = (CarRouteTpl) carRouteTplService.getById(carRouteTpl);
        return Result.ok().setData(data);
    }

}
