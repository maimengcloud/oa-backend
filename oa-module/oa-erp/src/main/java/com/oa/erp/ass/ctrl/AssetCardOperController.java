package com.oa.erp.ass.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.erp.ass.entity.AssetCardOper;
import com.oa.erp.ass.service.AssetCardOperService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/erp/ass/assetCardOper")
@Api(tags = {"ass_asset_card_oper-操作接口"})
public class AssetCardOperController {

    static Logger logger = LoggerFactory.getLogger(AssetCardOperController.class);

    @Autowired
    private AssetCardOperService assetCardOperService;

    @ApiOperation(value = "ass_asset_card_oper-查询列表", notes = " ")
    @ApiEntityParams(AssetCardOper.class)
    @ApiResponses({@ApiResponse(code = 200, response = AssetCardOper.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listAssetCardOper(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<AssetCardOper> qw = QueryTools.initQueryWrapper(AssetCardOper.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = assetCardOperService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @RequestMapping(value = "/listWithMng", method = RequestMethod.GET)
    public Result listWithMng(@RequestParam Map<String, Object> assetDetailOper) {
        RequestUtils.transformArray(assetDetailOper, "ids");
//        PageUtils.startPage(assetDetailOper);
        QueryWrapper<AssetCardOper> qw = QueryTools.initQueryWrapper(AssetCardOper.class, assetDetailOper);
        IPage page = QueryTools.initPage(assetDetailOper);
        List<Map<String, Object>> assetDetailOperList = assetCardOperService.selectListWithMng(page, qw, assetDetailOper);
//        PageUtils.responePage(m, assetDetailOperList);
        return Result.ok().setData(assetDetailOperList);
    }

    @RequestMapping(value = "/getDetailAllProcInstIds", method = RequestMethod.GET)
    public Result getDetailAllProcInstIds(@RequestParam Map<String, Object> assetDetailOper) {
        RequestUtils.transformArray(assetDetailOper, "ids");
        List<String> datas = assetCardOperService.getDetailAllProcInstIds(assetDetailOper);
        return Result.ok("query-ok", "查询成功").setData(datas);
    }

    @ApiOperation(value = "ass_asset_card_oper-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetCardOper.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addAssetCardOper(@RequestBody AssetCardOper assetCardOper) {
        assetCardOperService.save(assetCardOper);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "ass_asset_card_oper-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delAssetCardOper(@RequestBody AssetCardOper assetCardOper) {
        assetCardOperService.removeById(assetCardOper);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "ass_asset_card_oper-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetCardOper.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editAssetCardOper(@RequestBody AssetCardOper assetCardOper) {
        assetCardOperService.updateById(assetCardOper);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "ass_asset_card_oper-批量修改某些字段", notes = "")
    @ApiEntityParams(value = AssetCardOper.class, props = {}, remark = "ass_asset_card_oper", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = AssetCardOper.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        assetCardOperService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "ass_asset_card_oper-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelAssetCardOper(@RequestBody List<AssetCardOper> assetCardOpers) {
        User user = LoginUtils.getCurrentUserInfo();
        if (assetCardOpers.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<AssetCardOper> datasDb = assetCardOperService.listByIds(assetCardOpers.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<AssetCardOper> can = new ArrayList<>();
        List<AssetCardOper> no = new ArrayList<>();
        for (AssetCardOper data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            assetCardOperService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ass_asset_card_oper-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = AssetCardOper.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(AssetCardOper assetCardOper) {
        AssetCardOper data = (AssetCardOper) assetCardOperService.getById(assetCardOper);
        return Result.ok().setData(data);
    }

}
