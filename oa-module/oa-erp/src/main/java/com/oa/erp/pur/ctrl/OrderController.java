package com.oa.erp.pur.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.audit.log.client.annotation.AuditLog;
import com.mdp.audit.log.client.annotation.OperType;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.erp.pur.entity.Order;
import com.oa.erp.pur.entity.OrderDetail;
import com.oa.erp.pur.service.OrderService;
import com.oa.erp.pur.vo.OrderAddVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/erp/pur/order")
@Api(tags = {"pur_order-操作接口"})
public class OrderController {

    static Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @ApiOperation(value = "pur_order-查询列表", notes = " ")
    @ApiEntityParams(Order.class)
    @ApiResponses({@ApiResponse(code = 200, response = Order.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listOrder(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<Order> qw = QueryTools.initQueryWrapper(Order.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = orderService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "pur_order-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Order.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addOrder(@RequestBody OrderAddVo orderAddVo) {
        if (StringUtils.isEmpty(orderAddVo.getOrder().getId())) {
            orderAddVo.getOrder().setId(orderService.createKey("id"));
        } else {
            Order orderQuery = new Order(orderAddVo.getOrder().getId());
            if (orderService.countByWhere(orderQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        orderService.insertOrder(orderAddVo);
        return Result.ok("add-ok", "添加成功！").setData(orderAddVo);
    }

    @ApiOperation(value = "pur_order-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delOrder(@RequestBody Order order) {
        orderService.deleteOrder(order);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "pur_order-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Order.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editOrder(@RequestBody OrderAddVo orderAddVo) {
        if (!StringUtils.hasText(orderAddVo.getOrder().getId())) {
            return Result.error("order.id-0", "采购单编号id不能为空");
        }
        List<OrderDetail> details = orderAddVo.getOrderDetailList();
        if (details == null || details.size() == 0) {
            return Result.error("orderDetailList-0", "采购单明细清单不能为空");
        }
        orderService.updateOrder(orderAddVo);
        return Result.ok("edit-ok", "修改成功！").setData(orderAddVo);
    }

    @AuditLog(firstMenu = "资产管理", secondMenu = "资产采购", func = "processApprova", funcDesc = "资产采购入库流程", operType = OperType.UPDATE)
    @RequestMapping(value = "/processApprova", method = RequestMethod.POST)
    public Result processApprova(@RequestBody Map<String, Object> flowVars) {
        QueryWrapper<Order> qw = QueryTools.initQueryWrapper(Order.class, flowVars);
        IPage page = QueryTools.initPage(flowVars);
        this.orderService.processApprova(page, qw, flowVars);
        logger.debug("procInstId=====" + flowVars.get("procInstId"));
        return Result.ok("", "").setTotal(page.getTotal());
    }

    @ApiOperation(value = "pur_order-批量修改某些字段", notes = "")
    @ApiEntityParams(value = Order.class, props = {}, remark = "pur_order", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = Order.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        orderService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "pur_order-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelOrder(@RequestBody List<Order> orders) {
        User user = LoginUtils.getCurrentUserInfo();
        if (orders.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<Order> datasDb = orderService.listByIds(orders.stream().map(i -> i.getId()).collect(Collectors.toList()));
        List<Order> can = new ArrayList<>();
        List<Order> no = new ArrayList<>();
        for (Order data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            orderService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }
        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "pur_order-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Order.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(Order order) {
        Order data = (Order) orderService.getById(order);
        return Result.ok().setData(data);
    }

}
