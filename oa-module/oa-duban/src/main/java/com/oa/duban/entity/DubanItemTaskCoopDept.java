package com.oa.duban.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("duban_item_task_coop_dept")
@ApiModel(description="任务协助单位表")
public class DubanItemTaskCoopDept  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="任务id",allowEmptyValue=true,example="",allowableValues="")
	String dubanItemTaskId;

	
	@ApiModelProperty(notes="协助单位id",allowEmptyValue=true,example="",allowableValues="")
	String deptId;

	
	@ApiModelProperty(notes="协助单位",allowEmptyValue=true,example="",allowableValues="")
	String deptName;

	/**
	 *主键
	 **/
	public DubanItemTaskCoopDept(String id) {
		this.id = id;
	}
    
    /**
     * 任务协助单位表
     **/
	public DubanItemTaskCoopDept() {
	}

}