package com.oa.rc.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.core.utils.BaseUtils;
import com.oa.rc.entity.RcSchedule;
import com.oa.rc.mapper.RcScheduleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mdp.core.utils.BaseUtils.toMap;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class RcScheduleService extends BaseService<RcScheduleMapper, RcSchedule> {
    static Logger logger = LoggerFactory.getLogger(RcScheduleService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    public List<RcSchedule> selectRcShedule(Map<String, Object> params) {
        return baseMapper.selectRcShedule(params);
    }

    public int upDateRcSchedule(RcSchedule rcSchedule) {
        return baseMapper.upDateRcSchedule(rcSchedule);
    }

    public List<RcSchedule> selectListMapByStatus() {
        RcSchedule rc = new RcSchedule();
        rc.setStatus("0");
        /*
         * return this.selectList(statement("selectListMapByStatus"),rc);
         */
        List<Map<String, Object>> selectListMapByWhere = baseMapper.selectListMapByWhere(null, null, toMap(rc));
        return selectListMapByWhere.stream().map(
                (e) -> {
                    return BaseUtils.fromMap(e, RcSchedule.class);
                }
        ).collect(Collectors.toList());
    }


    public List<Map<String, Object>> selectListRcScheduleByDate(IPage page, QueryWrapper<RcSchedule> qw, Map<String, Object> params) {
        /*
         * return this.selectList("selectListRcScheduleByDate", params);
         */
        return baseMapper.selectListMapByWhere(page, qw, params);
    }

    public List<Map<String, Object>> selectListRcScheduleByMonth(Map<String, Object> params) {
        /*
         * return this.selectList("selectListRcScheduleByMonth", params);
         */
        return baseMapper.selectListMapByWhere(null, null, params);
    }

    public void rcScheduleUpdateSomeFieldByPk(RcSchedule rcSchedule) {
        baseMapper.rcScheduleUpdateSomeFieldByPk(rcSchedule);
    }

    public List<Map<String, Object>> selectListScheduleMapByWhere(Map<String, Object> params1) {
       return baseMapper.selectListScheduleMapByWhere(params1);
    }
    /** 请在此类添加自定义函数 */
}

