package com.oa.erp.pur.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("pur_require_detail")
@ApiModel(description="pur_require_detail")
public class RequireDetail  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="资产编码",allowEmptyValue=true,example="",allowableValues="")
	String assetSn;

	
	@ApiModelProperty(notes="资产类别",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="资产序列号",allowEmptyValue=true,example="",allowableValues="")
	String assetNo;

	
	@ApiModelProperty(notes="规格型号",allowEmptyValue=true,example="",allowableValues="")
	String spec;

	
	@ApiModelProperty(notes="计量单位",allowEmptyValue=true,example="",allowableValues="")
	String measUnit;

	
	@ApiModelProperty(notes="预估价格",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal estimatePrice;

	
	@ApiModelProperty(notes="资产原值",allowEmptyValue=true,example="",allowableValues="")
	String orignUnitAmount;

	
	@ApiModelProperty(notes="使用期限",allowEmptyValue=true,example="",allowableValues="")
	String servLife;

	
	@ApiModelProperty(notes="组织机构代码",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="责任人",allowEmptyValue=true,example="",allowableValues="")
	String liableUserid;

	
	@ApiModelProperty(notes="采购人",allowEmptyValue=true,example="",allowableValues="")
	String purchaseUserid;

	
	@ApiModelProperty(notes="供应商编号",allowEmptyValue=true,example="",allowableValues="")
	String supplierId;

	
	@ApiModelProperty(notes="所属父资产",allowEmptyValue=true,example="",allowableValues="")
	String passetSn;

	
	@ApiModelProperty(notes="所属父资产资产编码",allowEmptyValue=true,example="",allowableValues="")
	String passetName;

	
	@ApiModelProperty(notes="保修期（月）",allowEmptyValue=true,example="",allowableValues="")
	String defectsLiabilityPeriod;

	
	@ApiModelProperty(notes="入库日期",allowEmptyValue=true,example="",allowableValues="")
	Date warehouseDate;

	
	@ApiModelProperty(notes="资产状态0闲置1在用2借用3维修4报废",allowEmptyValue=true,example="",allowableValues="")
	String assetStatus;

	
	@ApiModelProperty(notes="配置说明",allowEmptyValue=true,example="",allowableValues="")
	String confDesc;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="资产图片",allowEmptyValue=true,example="",allowableValues="")
	String assetImageUrl;

	
	@ApiModelProperty(notes="创建人",allowEmptyValue=true,example="",allowableValues="")
	String createUserid;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="最后修改人",allowEmptyValue=true,example="",allowableValues="")
	String lastEditUserid;

	
	@ApiModelProperty(notes="最后修改时间",allowEmptyValue=true,example="",allowableValues="")
	Date lastEditTime;

	
	@ApiModelProperty(notes="入库单号-入库后回填",allowEmptyValue=true,example="",allowableValues="")
	String receiptNo;

	
	@ApiModelProperty(notes="供应商",allowEmptyValue=true,example="",allowableValues="")
	String supplierName;

	
	@ApiModelProperty(notes="资产名称",allowEmptyValue=true,example="",allowableValues="")
	String assetName;

	
	@ApiModelProperty(notes="数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal stockNum;

	
	@ApiModelProperty(notes="仓库编号",allowEmptyValue=true,example="",allowableValues="")
	String warehouseId;

	
	@ApiModelProperty(notes="产品编号",allowEmptyValue=true,example="",allowableValues="")
	String productId;

	
	@ApiModelProperty(notes="产品编码",allowEmptyValue=true,example="",allowableValues="")
	String productSn;

	
	@ApiModelProperty(notes="品牌编号",allowEmptyValue=true,example="",allowableValues="")
	String brandId;

	
	@ApiModelProperty(notes="品牌名称",allowEmptyValue=true,example="",allowableValues="")
	String brandName;

	
	@ApiModelProperty(notes="申购单号",allowEmptyValue=true,example="",allowableValues="")
	String requireId;

	
	@ApiModelProperty(notes="建议到货时间",allowEmptyValue=true,example="",allowableValues="")
	Date suggestArrivalDate;

	
	@ApiModelProperty(notes="采购单中明细编号-加入采购单后回填",allowEmptyValue=true,example="",allowableValues="")
	String orderDetailId;

	
	@ApiModelProperty(notes="采购订单编号-加入采购单后回填",allowEmptyValue=true,example="",allowableValues="")
	String orderId;

	
	@ApiModelProperty(notes="资产明细编号",allowEmptyValue=true,example="",allowableValues="")
	String assetDetailId;

	
	@ApiModelProperty(notes="分类名称",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	/**
	 *编号
	 **/
	public RequireDetail(String id) {
		this.id = id;
	}
    
    /**
     * pur_require_detail
     **/
	public RequireDetail() {
	}

}