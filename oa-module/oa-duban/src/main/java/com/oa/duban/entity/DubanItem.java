package com.oa.duban.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("duban_item")
@ApiModel(description="督办表")
public class DubanItem  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="督办字号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="事项名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="事项分类id",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="事项分类",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	
	@ApiModelProperty(notes="创建人id",allowEmptyValue=true,example="",allowableValues="")
	String createUserid;

	
	@ApiModelProperty(notes="创建人名称",allowEmptyValue=true,example="",allowableValues="")
	String createUsername;

	
	@ApiModelProperty(notes="责任人id",allowEmptyValue=true,example="",allowableValues="")
	String dutyId;

	
	@ApiModelProperty(notes="责任人名称",allowEmptyValue=true,example="",allowableValues="")
	String dutyName;

	
	@ApiModelProperty(notes="牵头部门id",allowEmptyValue=true,example="",allowableValues="")
	String betweenDeptid;

	
	@ApiModelProperty(notes="牵头部门名称",allowEmptyValue=true,example="",allowableValues="")
	String betweenDeptName;

	
	@ApiModelProperty(notes="牵头人id||中间人id",allowEmptyValue=true,example="",allowableValues="")
	String betweenId;

	
	@ApiModelProperty(notes="牵头人名称",allowEmptyValue=true,example="",allowableValues="")
	String betweenName;

	
	@ApiModelProperty(notes="归属机构",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="计划开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	
	@ApiModelProperty(notes="计划结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="主办单位id",allowEmptyValue=true,example="",allowableValues="")
	String hostDeptId;

	
	@ApiModelProperty(notes="主办单位",allowEmptyValue=true,example="",allowableValues="")
	String hostDeptName;

	
	@ApiModelProperty(notes="主办id",allowEmptyValue=true,example="",allowableValues="")
	String hostId;

	
	@ApiModelProperty(notes="主办",allowEmptyValue=true,example="",allowableValues="")
	String hostName;

	
	@ApiModelProperty(notes="任务描述",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="状态0已申报1进行中2已完成3已超时",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="进度",allowEmptyValue=true,example="",allowableValues="")
	String progress;

	/**
	 *督办字号
	 **/
	public DubanItem(String id) {
		this.id = id;
	}
    
    /**
     * 督办表
     **/
	public DubanItem() {
	}

}