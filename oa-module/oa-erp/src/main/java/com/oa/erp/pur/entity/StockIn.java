package com.oa.erp.pur.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("pur_stock_in")
@ApiModel(description="pur_stock_in")
public class StockIn  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="入库单号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="入库单标题",allowEmptyValue=true,example="",allowableValues="")
	String title;

	
	@ApiModelProperty(notes="申请人编号",allowEmptyValue=true,example="",allowableValues="")
	String reqUserid;

	
	@ApiModelProperty(notes="申请人姓名",allowEmptyValue=true,example="",allowableValues="")
	String reqUsername;

	
	@ApiModelProperty(notes="申请日期",allowEmptyValue=true,example="",allowableValues="")
	Date reqDate;

	
	@ApiModelProperty(notes="申请人机构id",allowEmptyValue=true,example="",allowableValues="")
	String reqBranchId;

	
	@ApiModelProperty(notes="是否生成资产卡片",allowEmptyValue=true,example="",allowableValues="")
	String isAssetCard;

	
	@ApiModelProperty(notes="资产卡片是否按数量拆分",allowEmptyValue=true,example="",allowableValues="")
	String cardDisByNum;

	
	@ApiModelProperty(notes="入库单状态",allowEmptyValue=true,example="",allowableValues="")
	String stockStatus;

	
	@ApiModelProperty(notes="对应流程实例ID",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="流程业务key",allowEmptyValue=true,example="",allowableValues="")
	String bizKey;

	
	@ApiModelProperty(notes="最后审批意见",allowEmptyValue=true,example="",allowableValues="")
	String commentMsg;

	
	@ApiModelProperty(notes="最后审批人",allowEmptyValue=true,example="",allowableValues="")
	String assignee;

	
	@ApiModelProperty(notes="最后审批人名称",allowEmptyValue=true,example="",allowableValues="")
	String assigneeName;

	
	@ApiModelProperty(notes="最后审批意见0不同意1同意",allowEmptyValue=true,example="",allowableValues="")
	String agree;

	
	@ApiModelProperty(notes="流程最后更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowLastTime;

	
	@ApiModelProperty(notes="流程结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowEndTime;

	
	@ApiModelProperty(notes="流程当前节点编号",allowEmptyValue=true,example="",allowableValues="")
	String actId;

	
	@ApiModelProperty(notes="当前任务名称",allowEmptyValue=true,example="",allowableValues="")
	String taskName;

	
	@ApiModelProperty(notes="0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	
	@ApiModelProperty(notes="总入库数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalInNum;

	
	@ApiModelProperty(notes="总采购量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalStockNum;

	
	@ApiModelProperty(notes="申购单位编号",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptid;

	
	@ApiModelProperty(notes="申购单位名称",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptName;

	
	@ApiModelProperty(notes="申购事由",allowEmptyValue=true,example="",allowableValues="")
	String reqReason;

	/**
	 *入库单号
	 **/
	public StockIn(String id) {
		this.id = id;
	}
    
    /**
     * pur_stock_in
     **/
	public StockIn() {
	}

}