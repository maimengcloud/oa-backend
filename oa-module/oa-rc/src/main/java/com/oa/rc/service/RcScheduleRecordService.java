package com.oa.rc.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.rc.entity.RcSchedule;
import com.oa.rc.entity.RcScheduleRecord;
import com.oa.rc.mapper.RcScheduleRecordMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class RcScheduleRecordService extends BaseService<RcScheduleRecordMapper, RcScheduleRecord> {
    static Logger logger = LoggerFactory.getLogger(RcScheduleRecordService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }


    /**
     * 该方法在日程新建后执行，生成对应的日志信息
     * author：z1rar1
     **/

    public void insertRcScheduleRecord(RcSchedule rcSchedule, RcScheduleRecord rcScheduleRecord, String operationType) throws UnknownHostException {
        //判断是否修改数据，未修改则退出该方法
        if (operationType == "修改") {

//			baseMapper.selectOneDetail(rcSchedule.getId());
//			String detail = ((RcScheduleRecord)this.selectOne("selectOneDetail", rcSchedule.getId())).getOperationDetail();

            RcScheduleRecord record = new RcScheduleRecord();
            record.setScheduleId(rcSchedule.getId());

            String detail = baseMapper.selectOneDetail(record).getOperationDetail();
            if (rcSchedule.toString().equals(detail)) {
                return;
            }


        }
        rcScheduleRecord.setScheduleId(rcSchedule.getId());
        rcScheduleRecord.setOperatorId(rcSchedule.getCreateUserid());
        rcScheduleRecord.setOperatorName(rcSchedule.getCreateUsername());
        rcScheduleRecord.setOperationTime(new java.util.Date());
        rcScheduleRecord.setModules("工作模块");
        rcScheduleRecord.setRecordProject("日程信息");
        rcScheduleRecord.setRcTitle(rcSchedule.getTitle());
        rcScheduleRecord.setOperatorIp(java.net.Inet4Address.getLocalHost().getHostAddress());
        rcScheduleRecord.setScheduleId(rcSchedule.getId());
        rcScheduleRecord.setOperationType(operationType);
        rcScheduleRecord.setOperationDetail(rcSchedule.toString());
//        this.getDao().insert(rcScheduleRecord);
        baseMapper.insert(rcScheduleRecord);
    }


    public RcScheduleRecord setRcScheduleRecord(RcSchedule rcSchedule, String operationType) throws UnknownHostException {
        RcScheduleRecord rcScheduleRecord = new RcScheduleRecord();
        //判断是否修改数据，未修改则退出   该方法
        rcScheduleRecord.setId(this.createKey("id"));
        rcScheduleRecord.setScheduleId(rcSchedule.getId());
        rcScheduleRecord.setOperatorId(rcSchedule.getCreateUserid());
        rcScheduleRecord.setOperatorName(rcSchedule.getCreateUsername());
        rcScheduleRecord.setOperationTime(new java.util.Date());
        rcScheduleRecord.setModules("工作模块");
        rcScheduleRecord.setRecordProject("日程信息");
        rcScheduleRecord.setRcTitle(rcSchedule.getTitle());
        rcScheduleRecord.setOperatorIp(java.net.Inet4Address.getLocalHost().getHostAddress());
        rcScheduleRecord.setScheduleId(rcSchedule.getId());
        rcScheduleRecord.setOperationType(operationType);
        rcScheduleRecord.setOperationDetail(rcSchedule.toString());
//        this.getDao().insert(rcScheduleRecord);
        baseMapper.insert(rcScheduleRecord);
        return rcScheduleRecord;
    }

    /** 请在此类添加自定义函数 */
}

