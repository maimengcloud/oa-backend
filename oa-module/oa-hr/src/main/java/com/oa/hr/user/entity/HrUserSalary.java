package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_user_salary")
@ApiModel(description="员工工资记录表")
public class HrUserSalary  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="员工id",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="工资,单位是元",allowEmptyValue=true,example="",allowableValues="")
	String salary;

	
	@ApiModelProperty(notes="是否是当前工资",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="调整时间",allowEmptyValue=true,example="",allowableValues="")
	Date adjustmentTime;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public HrUserSalary(String id) {
		this.id = id;
	}
    
    /**
     * 员工工资记录表
     **/
	public HrUserSalary() {
	}

}