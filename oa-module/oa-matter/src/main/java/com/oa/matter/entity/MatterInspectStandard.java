package com.oa.matter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("sxk_matter_inspect_standard")
@ApiModel(description="事项检查依据")
public class MatterInspectStandard  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="依据名称（检查依据）",allowEmptyValue=true,example="",allowableValues="")
	String accordingName;

	
	@ApiModelProperty(notes="依据条款（检查依据）",allowEmptyValue=true,example="",allowableValues="")
	String clause;

	
	@ApiModelProperty(notes="检查内容id",allowEmptyValue=true,example="",allowableValues="")
	String contentId;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="检查标准",allowEmptyValue=true,example="",allowableValues="")
	String standard;

	/**
	 *id
	 **/
	public MatterInspectStandard(String id) {
		this.id = id;
	}
    
    /**
     * 事项检查依据
     **/
	public MatterInspectStandard() {
	}

}