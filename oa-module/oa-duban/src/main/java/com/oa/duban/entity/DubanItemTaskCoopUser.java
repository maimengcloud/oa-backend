package com.oa.duban.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("duban_item_task_coop_user")
@ApiModel(description="任务协助表")
public class DubanItemTaskCoopUser  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="任务id",allowEmptyValue=true,example="",allowableValues="")
	String dubanItemTaskId;

	
	@ApiModelProperty(notes="协助用户id",allowEmptyValue=true,example="",allowableValues="")
	String userId;

	
	@ApiModelProperty(notes="协助用户",allowEmptyValue=true,example="",allowableValues="")
	String username;

	/**
	 *主键
	 **/
	public DubanItemTaskCoopUser(String id) {
		this.id = id;
	}
    
    /**
     * 任务协助表
     **/
	public DubanItemTaskCoopUser() {
	}

}