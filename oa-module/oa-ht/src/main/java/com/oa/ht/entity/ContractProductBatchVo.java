package com.oa.ht.entity;

import java.util.List;

public class ContractProductBatchVo {

    String htId;
    List<ContractProduct> contractProducts;


    public String getHtId() {
        return htId;
    }

    public void setHtId(String htId) {
        this.htId = htId;
    }

    public List<ContractProduct> getContractProducts() {
        return contractProducts;
    }

    public void setContractProducts(List<ContractProduct> contractProducts) {
        this.contractProducts = contractProducts;
    }
}
