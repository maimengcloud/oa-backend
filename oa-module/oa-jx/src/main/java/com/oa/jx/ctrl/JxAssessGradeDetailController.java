package com.oa.jx.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.jx.entity.JxAssessGradeDetail;
import com.oa.jx.service.JxAssessGradeDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mdp.core.utils.BaseUtils.map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/jx/jxAssessGradeDetail")
@Api(tags = {"考核分数详情表-操作接口"})
public class JxAssessGradeDetailController {

    static Logger logger = LoggerFactory.getLogger(JxAssessGradeDetailController.class);

    @Autowired
    private JxAssessGradeDetailService jxAssessGradeDetailService;

    @ApiOperation(value = "考核分数详情表-查询列表", notes = " ")
    @ApiEntityParams(JxAssessGradeDetail.class)
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGradeDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listJxAssessGradeDetail(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<JxAssessGradeDetail> qw = QueryTools.initQueryWrapper(JxAssessGradeDetail.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = jxAssessGradeDetailService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "考核分数详情表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGradeDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addJxAssessGradeDetail(@RequestBody JxAssessGradeDetail jxAssessGradeDetail) {
        jxAssessGradeDetailService.save(jxAssessGradeDetail);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "考核分数详情表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delJxAssessGradeDetail(@RequestBody JxAssessGradeDetail jxAssessGradeDetail) {
        jxAssessGradeDetailService.removeById(jxAssessGradeDetail);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "考核分数详情表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGradeDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editJxAssessGradeDetail(@RequestBody JxAssessGradeDetail jxAssessGradeDetail) {
        jxAssessGradeDetailService.updateById(jxAssessGradeDetail);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "考核分数详情表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = JxAssessGradeDetail.class, props = {}, remark = "考核分数详情表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGradeDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        jxAssessGradeDetailService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "考核分数详情表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelJxAssessGradeDetail(@RequestBody List<JxAssessGradeDetail> jxAssessGradeDetails) {
        User user = LoginUtils.getCurrentUserInfo();
        if (jxAssessGradeDetails.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<JxAssessGradeDetail> datasDb = jxAssessGradeDetailService.listByIds(jxAssessGradeDetails.stream().map(i -> map("gradeId", i.getGradeId(), "contentId", i.getContentId())).collect(Collectors.toList()));

        List<JxAssessGradeDetail> can = new ArrayList<>();
        List<JxAssessGradeDetail> no = new ArrayList<>();
        for (JxAssessGradeDetail data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            jxAssessGradeDetailService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getGradeId() + " " + i.getContentId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "考核分数详情表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessGradeDetail.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(JxAssessGradeDetail jxAssessGradeDetail) {
        JxAssessGradeDetail data = (JxAssessGradeDetail) jxAssessGradeDetailService.getById(jxAssessGradeDetail);
        return Result.ok().setData(data);
    }

}
