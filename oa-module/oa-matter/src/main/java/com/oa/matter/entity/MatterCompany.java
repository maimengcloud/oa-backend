package com.oa.matter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("sxk_matter_company")
@ApiModel(description="事项与公司关联表")
public class MatterCompany  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="事项id",allowEmptyValue=true,example="",allowableValues="")
	String matterInfoId;

	
	@ApiModelProperty(notes="公司id",allowEmptyValue=true,example="",allowableValues="")
	String companyId;

	/**
	 *id
	 **/
	public MatterCompany(String id) {
		this.id = id;
	}
    
    /**
     * 事项与公司关联表
     **/
	public MatterCompany() {
	}

}