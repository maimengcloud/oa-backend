package com.oa.meet.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("meet_meeting_course")
@ApiModel(description="会议课程关联表")
public class MeetMeetingCourse  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="会议编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String meetingId;

	
	@ApiModelProperty(notes="课程编号",allowEmptyValue=true,example="",allowableValues="")
	String courseId;

	
	@ApiModelProperty(notes="主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	/**
	 *会议编号
	 **/
	public MeetMeetingCourse(String meetingId) {
		this.meetingId = meetingId;
	}
    
    /**
     * 会议课程关联表
     **/
	public MeetMeetingCourse() {
	}

}