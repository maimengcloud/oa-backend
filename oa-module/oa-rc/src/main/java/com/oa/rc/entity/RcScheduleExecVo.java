package com.oa.rc.entity;

import io.swagger.annotations.ApiModel;

import java.util.List;
import java.util.Map;

/**
 * 组织 com.qqkj  顶级模块 oa 大模块 rc  小模块 <br> 
 * 实体 RcSchedule所有属性名: <br>
 *	id,createUserid,createUsername,receiveUserid,receiveUsername,title,description,rcType,startTime,endTime,urgent,remindType,remindNowtime,remindBeforestart,remindBeforeend,status,branchId;<br>
 * 表 OA.rc_schedule rc_schedule的所有字段名: <br>
 *	id,create_userid,create_username,receive_userid,receive_username,title,description,rc_type,start_time,end_time,urgent,remind_type,remind_nowtime,remind_beforestart,remind_beforeend,status,branch_id;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 *{
 *     scheduleId:xxxx,
 *     receiveUsers:[{
 *         userid:xxxx,
 *         username:xxxx
 *     }]
 *}
 */


@ApiModel(description="rc_schedule_share_vo")
public class RcScheduleExecVo implements java.io.Serializable  {
	String scheduleId;
	List<Map<String,Object>> users;

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public List<Map<String, Object>> getUsers() {
		return users;
	}

	public void setReceiveUsers(List<Map<String, Object>> receiveUsers) {
		this.users = receiveUsers;
	}
}