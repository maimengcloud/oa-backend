package com.oa.matter.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.matter.entity.MatterInspectStandard;
import com.oa.matter.service.MatterInspectStandardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value="/*/oa/matter/matterInspectStandard")
@Api(tags = {"事项检查依据-操作接口"})
public class MatterInspectStandardController {

    static Logger logger = LoggerFactory.getLogger(MatterInspectStandardController.class);

    @Autowired
    private MatterInspectStandardService matterInspectStandardService;

    @ApiOperation(value = "事项检查依据-查询列表", notes = " ")
    @ApiEntityParams(MatterInspectStandard.class)
    @ApiResponses({@ApiResponse(code = 200, response = MatterInspectStandard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMatterInspectStandard(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        QueryWrapper<MatterInspectStandard> qw = QueryTools.initQueryWrapper(MatterInspectStandard.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = matterInspectStandardService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "事项检查依据-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MatterInspectStandard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMatterInspectStandard(@RequestBody MatterInspectStandard matterInspectStandard) {
        matterInspectStandardService.save(matterInspectStandard);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "事项检查依据-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMatterInspectStandard(@RequestBody MatterInspectStandard matterInspectStandard) {
        matterInspectStandardService.removeById(matterInspectStandard);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "事项检查依据-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MatterInspectStandard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMatterInspectStandard(@RequestBody MatterInspectStandard matterInspectStandard) {
        matterInspectStandardService.updateById(matterInspectStandard);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "事项检查依据-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MatterInspectStandard.class, props = {}, remark = "事项检查依据", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MatterInspectStandard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        matterInspectStandardService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "事项检查依据-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMatterInspectStandard(@RequestBody List<MatterInspectStandard> matterInspectStandards) {
        User user = LoginUtils.getCurrentUserInfo();
        if (matterInspectStandards.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MatterInspectStandard> datasDb = matterInspectStandardService.listByIds(matterInspectStandards.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MatterInspectStandard> can = new ArrayList<>();
        List<MatterInspectStandard> no = new ArrayList<>();
        for (MatterInspectStandard data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            matterInspectStandardService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "事项检查依据-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MatterInspectStandard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MatterInspectStandard matterInspectStandard) {
        MatterInspectStandard data = (MatterInspectStandard) matterInspectStandardService.getById(matterInspectStandard);
        return Result.ok().setData(data);
    }

}
