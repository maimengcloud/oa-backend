package com.oa.crm.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.crm.entity.CustomerAgenda;
import com.oa.crm.service.CustomerAgendaService;
import com.oa.crm.vo.CustomerAgendaAddVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/crm/customerAgenda")
@Api(tags = {"客户日程表-操作接口"})
public class CustomerAgendaController {

    static Logger logger = LoggerFactory.getLogger(CustomerAgendaController.class);

    @Autowired
    private CustomerAgendaService customerAgendaService;

    @ApiOperation(value = "客户日程表-查询列表", notes = " ")
    @ApiEntityParams(CustomerAgenda.class)
    @ApiResponses({@ApiResponse(code = 200, response = CustomerAgenda.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCustomerAgenda(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();

        RequestUtils.transformArray(params, "agendaIds");
        QueryWrapper<CustomerAgenda> qw = QueryTools.initQueryWrapper(CustomerAgenda.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> customerAgendaList = customerAgendaService.selectListMapByWhere(page, qw, params);
        //查询相关的附件
        List<Map<String, Object>> customerAgendaList2 = customerAgendaService.selectAgendaDoc(customerAgendaList);

        return Result.ok("query-ok", "查询成功").setData(customerAgendaList2).setTotal(page.getTotal());
    }


    @ApiOperation(value = "客户日程表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerAgenda.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCustomerAgenda(@RequestBody CustomerAgendaAddVo customerAgendaAddVo) {


        if (StringUtils.isEmpty(customerAgendaAddVo.getCustomerAgenda().getAgendaId())) {
            customerAgendaAddVo.getCustomerAgenda().setAgendaId(customerAgendaService.createKey("agendaId"));
        } else {
            CustomerAgenda customerAgendaQuery = new CustomerAgenda(customerAgendaAddVo.getCustomerAgenda().getAgendaId());
            if (customerAgendaService.countByWhere(customerAgendaQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        customerAgendaService.insertCustomerAgenda(customerAgendaAddVo);


        return Result.ok("add-ok", "添加成功！").setData(customerAgendaAddVo);
    }

    @ApiOperation(value = "客户日程表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCustomerAgenda(@RequestBody CustomerAgenda customerAgenda) {
        customerAgendaService.removeById(customerAgenda);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "客户日程表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerAgenda.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCustomerAgenda(@RequestBody CustomerAgenda customerAgenda) {
        customerAgendaService.updateById(customerAgenda);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "客户日程表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CustomerAgenda.class, props = {}, remark = "客户日程表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerAgenda.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        customerAgendaService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "客户日程表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCustomerAgenda(@RequestBody List<CustomerAgenda> customerAgendas) {
        User user = LoginUtils.getCurrentUserInfo();
        if (customerAgendas.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CustomerAgenda> datasDb = customerAgendaService.listByIds(customerAgendas.stream().map(i -> i.getAgendaId()).collect(Collectors.toList()));

        List<CustomerAgenda> can = new ArrayList<>();
        List<CustomerAgenda> no = new ArrayList<>();
        for (CustomerAgenda data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            customerAgendaService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getAgendaId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "客户日程表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerAgenda.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CustomerAgenda customerAgenda) {
        CustomerAgenda data = (CustomerAgenda) customerAgendaService.getById(customerAgenda);
        return Result.ok().setData(data);
    }

}
