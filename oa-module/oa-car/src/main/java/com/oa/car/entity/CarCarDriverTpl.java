package com.oa.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月7日
 */
@Data
@TableName("car_car_driver_tpl")
@ApiModel(description="司机汽车关联表")
public class CarCarDriverTpl  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="汽车编号",allowEmptyValue=true,example="",allowableValues="")
	String carId;

	
	@ApiModelProperty(notes="司机编号",allowEmptyValue=true,example="",allowableValues="")
	String driverId;

	
	@ApiModelProperty(notes="开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	
	@ApiModelProperty(notes="结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	
	@ApiModelProperty(notes="状态1启用0禁用",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="新增时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="归属机构",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *主键
	 **/
	public CarCarDriverTpl(String id) {
		this.id = id;
	}
    
    /**
     * 司机汽车关联表
     **/
	public CarCarDriverTpl() {
	}

}