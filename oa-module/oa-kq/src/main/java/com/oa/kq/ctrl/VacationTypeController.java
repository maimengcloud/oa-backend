package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.VacationType;
import com.oa.kq.entity.VacationTypeUser;
import com.oa.kq.service.VacationTypeService;
import com.oa.kq.service.VacationTypeUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/kq/vacationType")
@Api(tags = {"请假类型表-操作接口"})
public class VacationTypeController {

    static Logger logger = LoggerFactory.getLogger(VacationTypeController.class);

    @Autowired
    private VacationTypeService vacationTypeService;

    @Autowired
    private VacationTypeUserService vacationTypeUserService;

    @ApiOperation(value = "请假类型表-查询列表", notes = " ")
    @ApiEntityParams(VacationType.class)
    @ApiResponses({@ApiResponse(code = 200, response = VacationType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listVacationType(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<VacationType> qw = QueryTools.initQueryWrapper(VacationType.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = vacationTypeService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "请假类型表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = VacationType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addVacationType(@RequestBody VacationType vacationType) {
        Tips tips = new Tips("成功新增一条数据");
        User user = LoginUtils.getCurrentUserInfo();
        if (StringUtils.isEmpty(vacationType.getId())) {
            vacationType.setId(vacationTypeService.createKey("id"));
        } else {
            VacationType kqVacationTypeQuery = new VacationType(vacationType.getId());
            if (vacationTypeService.countByWhere(kqVacationTypeQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        vacationType.setBranchId(user.getUserid());
        vacationTypeService.insert(vacationType);
        return Result.ok("add-ok", "添加成功！").setData(vacationType);
    }

    @ApiOperation(value = "请假类型表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delVacationType(@RequestBody VacationType vacationType) {
        vacationTypeService.deleteByPk(vacationType);
        VacationTypeUser vacationTypeUser = new VacationTypeUser();
        vacationTypeUser.setVacationId(vacationType.getId());
        vacationTypeUserService.deleteByWhere(vacationTypeUser);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "请假类型表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = VacationType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editVacationType(@RequestBody VacationType vacationType) {
        vacationTypeService.updateByPk(vacationType);
        return Result.ok("edit-ok", "修改成功！").setData(vacationType);
    }

    @ApiOperation(value = "请假类型表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = VacationType.class, props = {}, remark = "请假类型表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = VacationType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        vacationTypeService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "请假类型表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelVacationType(@RequestBody List<VacationType> vacationTypes) {
        User user = LoginUtils.getCurrentUserInfo();
        if (vacationTypes.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<VacationType> datasDb = vacationTypeService.listByIds(vacationTypes.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<VacationType> can = new ArrayList<>();
        List<VacationType> no = new ArrayList<>();
        for (VacationType data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            vacationTypeService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "请假类型表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = VacationType.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(VacationType vacationType) {
        VacationType data = (VacationType) vacationTypeService.getById(vacationType);
        return Result.ok().setData(data);
    }

}
