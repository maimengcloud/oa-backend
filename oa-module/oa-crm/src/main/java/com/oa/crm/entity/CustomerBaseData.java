package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_base_data")
@ApiModel(description="crm_customer_base_data")
public class CustomerBaseData  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="简称",allowEmptyValue=true,example="",allowableValues="")
	String text;

	
	@ApiModelProperty(notes="描述",allowEmptyValue=true,example="",allowableValues="")
	String describe;

	
	@ApiModelProperty(notes="status:客户状态.",allowEmptyValue=true,example="",allowableValues="")
	String type;

	/**
	 *id
	 **/
	public CustomerBaseData(String id) {
		this.id = id;
	}
    
    /**
     * crm_customer_base_data
     **/
	public CustomerBaseData() {
	}

}