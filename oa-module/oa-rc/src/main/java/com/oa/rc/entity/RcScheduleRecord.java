package com.oa.rc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("rc_schedule_record")
@ApiModel(description="rc_schedule_record")
public class RcScheduleRecord  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="日志id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="操作者id",allowEmptyValue=true,example="",allowableValues="")
	String operatorId;

	
	@ApiModelProperty(notes="操作者名字",allowEmptyValue=true,example="",allowableValues="")
	String operatorName;

	
	@ApiModelProperty(notes="操作时间",allowEmptyValue=true,example="",allowableValues="")
	Date operationTime;

	
	@ApiModelProperty(notes="日志模块",allowEmptyValue=true,example="",allowableValues="")
	String modules;

	
	@ApiModelProperty(notes="操作类型",allowEmptyValue=true,example="",allowableValues="")
	String operationType;

	
	@ApiModelProperty(notes="操作项目",allowEmptyValue=true,example="",allowableValues="")
	String recordProject;

	
	@ApiModelProperty(notes="日程标题",allowEmptyValue=true,example="",allowableValues="")
	String rcTitle;

	
	@ApiModelProperty(notes="操作者ip",allowEmptyValue=true,example="",allowableValues="")
	String operatorIp;

	
	@ApiModelProperty(notes="日程编号",allowEmptyValue=true,example="",allowableValues="")
	String scheduleId;

	
	@ApiModelProperty(notes="操作详情",allowEmptyValue=true,example="",allowableValues="")
	String operationDetail;

	/**
	 *日志id
	 **/
	public RcScheduleRecord(String id) {
		this.id = id;
	}
    
    /**
     * rc_schedule_record
     **/
	public RcScheduleRecord() {
	}

}