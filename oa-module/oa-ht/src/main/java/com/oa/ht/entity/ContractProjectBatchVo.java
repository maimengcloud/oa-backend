package com.oa.ht.entity;

import java.util.List;

public class ContractProjectBatchVo {

    String htId;
    List<ContractProject> contractProjects;


    public String getHtId() {
        return htId;
    }

    public void setHtId(String htId) {
        this.htId = htId;
    }

    public List<ContractProject> getContractProjects() {
        return contractProjects;
    }

    public void setContractProjects(List<ContractProject> contractProjects) {
        this.contractProjects = contractProjects;
    }
}
