package com.oa.mail.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("mail_email")
@ApiModel(description="邮件表")
public class MailEmail  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="主题",allowEmptyValue=true,example="",allowableValues="")
	String subject;

	
	@ApiModelProperty(notes="正文",allowEmptyValue=true,example="",allowableValues="")
	String content;

	
	@ApiModelProperty(notes="发件用户id",allowEmptyValue=true,example="",allowableValues="")
	String userId;

	
	@ApiModelProperty(notes="发件用户名字",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="发件邮箱地址",allowEmptyValue=true,example="",allowableValues="")
	String sendEmail;

	
	@ApiModelProperty(notes="时间",allowEmptyValue=true,example="",allowableValues="")
	Date sendTime;

	
	@ApiModelProperty(notes="邮件类型,0草稿箱,1已发送,2收件箱,3垃圾箱.如果是内部邮件则不用管",allowEmptyValue=true,example="",allowableValues="")
	String type;

	
	@ApiModelProperty(notes="是否为星标",allowEmptyValue=true,example="",allowableValues="")
	String isStar;

	
	@ApiModelProperty(notes="是否已读",allowEmptyValue=true,example="",allowableValues="")
	String isRead;


	@ApiModelProperty(notes="是否外部0否1是",allowEmptyValue=true,example="",allowableValues="")
	String isOuter;

	
	@ApiModelProperty(notes="发送状态",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="机构号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="关联邮箱",allowEmptyValue=true,example="",allowableValues="")
	String receiveEmail;

	
	@ApiModelProperty(notes="收件人字符串",allowEmptyValue=true,example="",allowableValues="")
	String addressee;

	
	@ApiModelProperty(notes="抄送人字符串",allowEmptyValue=true,example="",allowableValues="")
	String cc;

	
	@ApiModelProperty(notes="密送人字符串",allowEmptyValue=true,example="",allowableValues="")
	String bcc;

	
	@ApiModelProperty(notes="外部收件的数量",allowEmptyValue=true,example="",allowableValues="")
	Integer currentNum;

	/**
	 *主键
	 **/
	public MailEmail(String id) {
		this.id = id;
	}
    
    /**
     * 邮件表
     **/
	public MailEmail() {
	}

}