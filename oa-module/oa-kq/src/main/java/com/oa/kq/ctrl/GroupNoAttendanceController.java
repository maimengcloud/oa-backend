package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.query.QueryTools;
import com.mdp.core.service.SequenceService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.GroupNoAttendance;
import com.oa.kq.service.GroupNoAttendanceService;
import com.oa.utils.ExcelView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Controller
@RequestMapping(value = "/*/oa/kq/groupNoAttendance")
@Api(tags = {"无需考勤人员表-操作接口"})
public class GroupNoAttendanceController {

    static Logger logger = LoggerFactory.getLogger(GroupNoAttendanceController.class);

    @Autowired
    private GroupNoAttendanceService groupNoAttendanceService;

    @Autowired
    private SequenceService seqService;


    /**
     * 跳转到查询页面listGroupNoAttendance.jsp
     */
    @RequestMapping(value="/listGroupNoAttendance")
    public ModelAndView listGroupNoAttendance(@ModelAttribute("groupNoAttendance") GroupNoAttendance groupNoAttendance, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        mv.addObject("data",groupNoAttendance);
        return mv;
    }


    /**
     * 跳转到新增页面addGroupNoAttendance.jsp
     */
    @RequestMapping(value="/addGroupNoAttendance")
    public ModelAndView addGroupNoAttendance(){

        ModelAndView mv = new ModelAndView();
        GroupNoAttendance groupNoAttendance=new GroupNoAttendance();
        mv.addObject("data",groupNoAttendance);
        return mv;
    }

    /**
     * 跳转到修改页面editGroupNoAttendance.jsp
     */
    @RequestMapping(value="/editGroupNoAttendance")
    public ModelAndView editGroupNoAttendance( @ModelAttribute("groupNoAttendance") GroupNoAttendance groupNoAttendance, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        groupNoAttendance = groupNoAttendanceService.selectOneObject(groupNoAttendance);	//根据ID读取
        mv.addObject("data",groupNoAttendance);
        return mv;
    }


    /**
     * 请求需带json后缀,如selectListGroupNoAttendanceByPage.json
     * 根据条件查询数据对象列表
     */
    @RequestMapping(value="/selectListGroupNoAttendanceByPage")
    public ModelAndView selectListGroupNoAttendanceByPage( @ModelAttribute("groupNoAttendance") GroupNoAttendance groupNoAttendance, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        List<GroupNoAttendance>	groupNoAttendanceList = groupNoAttendanceService.selectListByWhere(groupNoAttendance);	//列出GroupNoAttendance列表
        mv.addObject("data",groupNoAttendanceList);
        Tips tips=new Tips("查询成功");
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 新增一条数据
     */
    @RequestMapping(value="/insertGroupNoAttendance")
    public ModelAndView insertGroupNoAttendance( @ModelAttribute("groupNoAttendance") GroupNoAttendance groupNoAttendance, BindingResult result, Model model) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功新增一条数据");
        if(StringUtils.isEmpty(groupNoAttendance.getId())){
            groupNoAttendance.setId(seqService.getTablePK("groupNoAttendance", "id"));
        }
        groupNoAttendanceService.insert(groupNoAttendance);
        mv.addObject("data",groupNoAttendance);
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 根据主键删除1条数据
     */
    @RequestMapping(value="/deleteGroupNoAttendanceByPk")
    public ModelAndView deleteGroupNoAttendanceByPk(@ModelAttribute("groupNoAttendance") GroupNoAttendance groupNoAttendance, BindingResult result){
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功删除一条数据");
        groupNoAttendanceService.deleteByPk(groupNoAttendance);
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 根据主键修改一条数据
     */
    @RequestMapping(value="/updateGroupNoAttendanceByPk")
    public ModelAndView updateGroupNoAttendanceByPk( @ModelAttribute("groupNoAttendance") GroupNoAttendance groupNoAttendance, BindingResult result, Model model) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功更新一条数据");
        groupNoAttendanceService.updateByPk(groupNoAttendance);
        mv.addObject("data",groupNoAttendance);
        mv.addObject("tips", tips);
        return mv;
    }




    /**
     * 批量删除
     */
    @RequestMapping(value="/batchDeleteGroupNoAttendance")
    public ModelAndView batchDeleteGroupNoAttendance(@RequestParam("id[]") String[] id) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功删除"+id.length+"条数据");
        List<GroupNoAttendance> list=new ArrayList<GroupNoAttendance>();
        for(int i=0;i<id.length;i++){
            GroupNoAttendance groupNoAttendance=new GroupNoAttendance();
            groupNoAttendance.setId(id[i]);
            list.add(groupNoAttendance);
        }
        groupNoAttendanceService.batchDelete(list);
        mv.addObject("tips", tips);
        return mv;
    }

    /*
     * 导出到excel
     * @return
     */
    @RequestMapping(value="/exportGroupNoAttendanceToExcel")
    public ModelAndView exportGroupNoAttendanceToExcel(@ModelAttribute("groupNoAttendance") GroupNoAttendance groupNoAttendance, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功导出数据");
        try{
            Map<String,Object> dataMap = new HashMap<String,Object>();
            List<String> titles = new ArrayList<String>();
            titles.add("组id");
            titles.add("无需考勤人员表");
            titles.add("编号id");
            dataMap.put("titles", titles);
            List<GroupNoAttendance> GroupNoAttendanceList = groupNoAttendanceService.selectListByWhere(groupNoAttendance);
            List<Map<String,Object>> allList = new ArrayList<Map<String,Object>>();
            for(int i=0;i<GroupNoAttendanceList.size();i++){
                Map<String,Object> row = new HashMap<String,Object>();
                row.put("var1", GroupNoAttendanceList.get(i).getGroupId());
                row.put("var2", GroupNoAttendanceList.get(i).getUserid());
                row.put("var3", GroupNoAttendanceList.get(i).getId());
                allList.add(row);
            }
            dataMap.put("varList", allList);
            ExcelView erv = new ExcelView();
            mv = new ModelAndView(erv,dataMap);
        } catch(Exception e){
            logger.error(e.toString(), e);
            tips.setErrMsg("导出数据异常");
        }
        mv.addObject("tips", tips);
        return mv;
    }


    @ApiOperation(value = "无需考勤人员表-查询列表", notes = " ")
    @ApiEntityParams(GroupNoAttendance.class)
    @ApiResponses({@ApiResponse(code = 200, response = GroupNoAttendance.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listGroupNoAttendance(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        QueryWrapper<GroupNoAttendance> qw = QueryTools.initQueryWrapper(GroupNoAttendance.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = groupNoAttendanceService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ResponseBody
    @ApiOperation(value = "无需考勤人员表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupNoAttendance.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addGroupNoAttendance(@RequestBody GroupNoAttendance groupNoAttendance) {
        groupNoAttendanceService.save(groupNoAttendance);
        return Result.ok("add-ok", "添加成功！");
    }

    @ResponseBody
    @ApiOperation(value = "无需考勤人员表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delGroupNoAttendance(@RequestBody GroupNoAttendance groupNoAttendance) {
        groupNoAttendanceService.removeById(groupNoAttendance);
        return Result.ok("del-ok", "删除成功！");
    }

    @ResponseBody
    @ApiOperation(value = "无需考勤人员表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupNoAttendance.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editGroupNoAttendance(@RequestBody GroupNoAttendance groupNoAttendance) {
        groupNoAttendanceService.updateById(groupNoAttendance);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ResponseBody
    @ApiOperation(value = "无需考勤人员表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = GroupNoAttendance.class, props = {}, remark = "无需考勤人员表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = GroupNoAttendance.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        groupNoAttendanceService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ResponseBody
    @ApiOperation(value = "无需考勤人员表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelGroupNoAttendance(@RequestBody List<GroupNoAttendance> groupNoAttendances) {
        User user = LoginUtils.getCurrentUserInfo();
        if (groupNoAttendances.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<GroupNoAttendance> datasDb = groupNoAttendanceService.listByIds(groupNoAttendances.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<GroupNoAttendance> can = new ArrayList<>();
        List<GroupNoAttendance> no = new ArrayList<>();
        for (GroupNoAttendance data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            groupNoAttendanceService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ResponseBody
    @ApiOperation(value = "无需考勤人员表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupNoAttendance.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(GroupNoAttendance groupNoAttendance) {
        GroupNoAttendance data = (GroupNoAttendance) groupNoAttendanceService.getById(groupNoAttendance);
        return Result.ok().setData(data);
    }

}
