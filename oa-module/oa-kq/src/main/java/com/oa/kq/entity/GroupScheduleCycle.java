package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_group_schedule_cycle")
@ApiModel(description="排班周期表")
public class GroupScheduleCycle  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="组id",allowEmptyValue=true,example="",allowableValues="")
	String groupId;

	
	@ApiModelProperty(notes="周一（填入班次id）",allowEmptyValue=true,example="",allowableValues="")
	String monday;

	
	@ApiModelProperty(notes="周二（填入班次id）",allowEmptyValue=true,example="",allowableValues="")
	String tuesday;

	
	@ApiModelProperty(notes="周三（填入班次id）",allowEmptyValue=true,example="",allowableValues="")
	String wednesday;

	
	@ApiModelProperty(notes="周四（填入班次id）",allowEmptyValue=true,example="",allowableValues="")
	String thursday;

	
	@ApiModelProperty(notes="周五（填入班次id）",allowEmptyValue=true,example="",allowableValues="")
	String friday;

	
	@ApiModelProperty(notes="周六（填入班次id）",allowEmptyValue=true,example="",allowableValues="")
	String saturday;

	
	@ApiModelProperty(notes="周日（填入班次id）",allowEmptyValue=true,example="",allowableValues="")
	String sunday;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *编号id
	 **/
	public GroupScheduleCycle(String id) {
		this.id = id;
	}
    
    /**
     * 排班周期表
     **/
	public GroupScheduleCycle() {
	}

}