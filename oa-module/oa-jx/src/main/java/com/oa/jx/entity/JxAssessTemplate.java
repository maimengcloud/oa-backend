package com.oa.jx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("jx_assess_template")
@ApiModel(description="考核模板表")
public class JxAssessTemplate  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="组织id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="考核模板名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="总分",allowEmptyValue=true,example="",allowableValues="")
	Integer totalPoints;

	
	@ApiModelProperty(notes="考核模板说明",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date ctime;

	
	@ApiModelProperty(notes="模板状态0-未启用1启用",allowEmptyValue=true,example="",allowableValues="")
	String tstatus;

	
	@ApiModelProperty(notes="所属考核方案",allowEmptyValue=true,example="",allowableValues="")
	String schemeType;

	/**
	 *id
	 **/
	public JxAssessTemplate(String id) {
		this.id = id;
	}
    
    /**
     * 考核模板表
     **/
	public JxAssessTemplate() {
	}

}