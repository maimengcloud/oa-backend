package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.audit.log.client.annotation.AuditLog;
import com.mdp.audit.log.client.annotation.OperType;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.WorkOvertimeInfo;
import com.oa.kq.service.WorkOvertimeInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/kq/workOvertimeInfo")
@Api(tags = {"加班表-操作接口"})
public class WorkOvertimeInfoController {

    static Logger logger = LoggerFactory.getLogger(WorkOvertimeInfoController.class);

    @Autowired
    private WorkOvertimeInfoService workOvertimeInfoService;

    @ApiOperation(value = "加班表-查询列表", notes = " ")
    @ApiEntityParams(WorkOvertimeInfo.class)
    @ApiResponses({@ApiResponse(code = 200, response = WorkOvertimeInfo.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listWorkOvertimeInfo(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "idsbranchIds");
        QueryWrapper<WorkOvertimeInfo> qw = QueryTools.initQueryWrapper(WorkOvertimeInfo.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = workOvertimeInfoService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "加班表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = WorkOvertimeInfo.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addWorkOvertimeInfo(@RequestBody WorkOvertimeInfo workOvertimeInfo) {
        if (StringUtils.isEmpty(workOvertimeInfo.getId())) {
            workOvertimeInfo.setId(workOvertimeInfoService.createKey("id"));
        } else {
            WorkOvertimeInfo workOvertimeInfoQuery = new WorkOvertimeInfo(workOvertimeInfo.getId());
            if (workOvertimeInfoService.countByWhere(workOvertimeInfoQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        if (StringUtils.isEmpty(workOvertimeInfo.getBranchId())) {
            workOvertimeInfo.setBranchId(workOvertimeInfoService.createKey("branchId"));
        } else {
            WorkOvertimeInfo workOvertimeInfoQuery = new WorkOvertimeInfo(workOvertimeInfo.getBranchId());
            if (workOvertimeInfoService.countByWhere(workOvertimeInfoQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        workOvertimeInfoService.insert(workOvertimeInfo);
        return Result.ok("add-ok", "添加成功！").setData(workOvertimeInfo);
    }

    @ApiOperation(value = "加班表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delWorkOvertimeInfo(@RequestBody WorkOvertimeInfo workOvertimeInfo) {
        workOvertimeInfoService.deleteByPk(workOvertimeInfo);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "加班表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = WorkOvertimeInfo.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editWorkOvertimeInfo(@RequestBody WorkOvertimeInfo workOvertimeInfo) {
        workOvertimeInfoService.updateByPk(workOvertimeInfo);
        return Result.ok("edit-ok", "修改成功！");
    }

    @AuditLog(firstMenu = "办公平台", secondMenu = "请假申请", func = "processApprova", funcDesc = "请假审核流程", operType = OperType.UPDATE)
    @RequestMapping(value = "/processApprova", method = RequestMethod.POST)
    public Result processApprova(@RequestBody Map<String, Object> flowVars) {
        QueryWrapper<WorkOvertimeInfo> qw = QueryTools.initQueryWrapper(WorkOvertimeInfo.class, flowVars);
        IPage page = QueryTools.initPage(flowVars);
        this.workOvertimeInfoService.processApprova(page, qw, flowVars);
        logger.debug("procInstId=====" + flowVars.get("procInstId"));
        return Result.ok();
    }

    @ApiOperation(value = "加班表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = WorkOvertimeInfo.class, props = {}, remark = "加班表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = WorkOvertimeInfo.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        workOvertimeInfoService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "加班表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelWorkOvertimeInfo(@RequestBody List<WorkOvertimeInfo> workOvertimeInfos) {
        User user = LoginUtils.getCurrentUserInfo();
        if (workOvertimeInfos.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<WorkOvertimeInfo> datasDb = workOvertimeInfoService.listByIds(workOvertimeInfos.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<WorkOvertimeInfo> can = new ArrayList<>();
        List<WorkOvertimeInfo> no = new ArrayList<>();
        for (WorkOvertimeInfo data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            workOvertimeInfoService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "加班表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = WorkOvertimeInfo.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(WorkOvertimeInfo workOvertimeInfo) {
        WorkOvertimeInfo data = (WorkOvertimeInfo) workOvertimeInfoService.getById(workOvertimeInfo);
        return Result.ok().setData(data);
    }

}
