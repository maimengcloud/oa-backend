package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_user_family_info")
@ApiModel(description="员工家庭信息表")
public class HrUserFamilyInfo  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="家庭关系",allowEmptyValue=true,example="",allowableValues="")
	String familyTies;

	
	@ApiModelProperty(notes="用户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="任职单位",allowEmptyValue=true,example="",allowableValues="")
	String jobUnits;

	
	@ApiModelProperty(notes="职业",allowEmptyValue=true,example="",allowableValues="")
	String occupation;

	
	@ApiModelProperty(notes="联系方式",allowEmptyValue=true,example="",allowableValues="")
	String contactInformation;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="姓名",allowEmptyValue=true,example="",allowableValues="")
	String familyName;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public HrUserFamilyInfo(String id) {
		this.id = id;
	}
    
    /**
     * 员工家庭信息表
     **/
	public HrUserFamilyInfo() {
	}

}