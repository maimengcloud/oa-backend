package com.oa.car.entity;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author amao
 * @date 2020/5/9 16:19
 */
public class CarRouteTplVo extends CarRouteTpl {

    @ApiModelProperty(notes="新建申请子路线",allowEmptyValue=true,example="",allowableValues="")
    List<CarRouteDetailTpl> carRouteDetailTplsNew;

    public List<CarRouteDetailTpl> getCarRouteDetailTplsNew() {
        return carRouteDetailTplsNew;
    }

    public void setCarRouteDetailTplsNew(List<CarRouteDetailTpl> carRouteDetailTplsNew) {
        this.carRouteDetailTplsNew = carRouteDetailTplsNew;
    }
}
