package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.WorkOvertimeProcessApprova;
import com.oa.kq.service.WorkOvertimeProcessApprovaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/kq/workOvertimeProcessApprova")
@Api(tags = {"员工请假审核流程-操作接口"})
public class WorkOvertimeProcessApprovaController {

    static Logger logger = LoggerFactory.getLogger(WorkOvertimeProcessApprovaController.class);

    @Autowired
    private WorkOvertimeProcessApprovaService workOvertimeProcessApprovaService;

    @ApiOperation(value = "员工请假审核流程-查询列表", notes = " ")
    @ApiEntityParams(WorkOvertimeProcessApprova.class)
    @ApiResponses({
            @ApiResponse(code = 200, response = WorkOvertimeProcessApprova.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
    })
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listWorkOvertimeProcessApprova(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        QueryWrapper<WorkOvertimeProcessApprova> qw = QueryTools.initQueryWrapper(WorkOvertimeProcessApprova.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = workOvertimeProcessApprovaService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "员工请假审核流程-新增", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = WorkOvertimeProcessApprova.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addWorkOvertimeProcessApprova(@RequestBody WorkOvertimeProcessApprova workOvertimeProcessApprova) {
        workOvertimeProcessApprovaService.save(workOvertimeProcessApprova);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "员工请假审核流程-删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
    })
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delWorkOvertimeProcessApprova(@RequestBody WorkOvertimeProcessApprova workOvertimeProcessApprova) {
        workOvertimeProcessApprovaService.removeById(workOvertimeProcessApprova);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "员工请假审核流程-修改", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = WorkOvertimeProcessApprova.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editWorkOvertimeProcessApprova(@RequestBody WorkOvertimeProcessApprova workOvertimeProcessApprova) {
        workOvertimeProcessApprovaService.updateById(workOvertimeProcessApprova);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "员工请假审核流程-批量修改某些字段", notes = "")
    @ApiEntityParams(value = WorkOvertimeProcessApprova.class, props = {}, remark = "员工请假审核流程", paramType = "body")
    @ApiResponses({
            @ApiResponse(code = 200, response = WorkOvertimeProcessApprova.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        workOvertimeProcessApprovaService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "员工请假审核流程-批量删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
    })
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelWorkOvertimeProcessApprova(@RequestBody List<WorkOvertimeProcessApprova> workOvertimeProcessApprovas) {
        User user = LoginUtils.getCurrentUserInfo();
        if (workOvertimeProcessApprovas.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<WorkOvertimeProcessApprova> datasDb = workOvertimeProcessApprovaService.listByIds(workOvertimeProcessApprovas.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<WorkOvertimeProcessApprova> can = new ArrayList<>();
        List<WorkOvertimeProcessApprova> no = new ArrayList<>();
        for (WorkOvertimeProcessApprova data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            workOvertimeProcessApprovaService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "员工请假审核流程-根据主键查询一条数据", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = WorkOvertimeProcessApprova.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(WorkOvertimeProcessApprova workOvertimeProcessApprova) {
        WorkOvertimeProcessApprova data = (WorkOvertimeProcessApprova) workOvertimeProcessApprovaService.getById(workOvertimeProcessApprova);
        return Result.ok().setData(data);
    }

}
