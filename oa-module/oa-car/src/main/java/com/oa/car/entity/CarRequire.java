package com.oa.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月7日
 */
@Data
@TableName("car_require")
@ApiModel(description="car_require")
public class CarRequire  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="申请编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="申请用车开始时间",allowEmptyValue=true,example="",allowableValues="")
	Date reqStartTime;

	
	@ApiModelProperty(notes="申请用车结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date reqEndTime;

	
	@ApiModelProperty(notes="申请部门编号",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptid;

	
	@ApiModelProperty(notes="申请部门名称",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptName;

	
	@ApiModelProperty(notes="申请机构号",allowEmptyValue=true,example="",allowableValues="")
	String reqBranchId;

	
	@ApiModelProperty(notes="申请人编号",allowEmptyValue=true,example="",allowableValues="")
	String reqUserid;

	
	@ApiModelProperty(notes="申请人姓名",allowEmptyValue=true,example="",allowableValues="")
	String reqUsername;

	
	@ApiModelProperty(notes="申请人电话",allowEmptyValue=true,example="",allowableValues="")
	String reqPhoneno;

	
	@ApiModelProperty(notes="申请事由",allowEmptyValue=true,example="",allowableValues="")
	String reqReason;

	
	@ApiModelProperty(notes="申请时间",allowEmptyValue=true,example="",allowableValues="")
	Date reqTime;

	
	@ApiModelProperty(notes="申请状态",allowEmptyValue=true,example="",allowableValues="")
	String reqStatus;

	
	@ApiModelProperty(notes="审批状态",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	
	@ApiModelProperty(notes="流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="目的地地址",allowEmptyValue=true,example="",allowableValues="")
	String destAddress;

	
	@ApiModelProperty(notes="目的地定位",allowEmptyValue=true,example="",allowableValues="")
	String destGps;

	
	@ApiModelProperty(notes="申请标题",allowEmptyValue=true,example="",allowableValues="")
	String reqTitle;

	
	@ApiModelProperty(notes="是否规划路线",allowEmptyValue=true,example="",allowableValues="")
	String hasRoute;

	
	@ApiModelProperty(notes="机构名称",allowEmptyValue=true,example="",allowableValues="")
	String reqBranchName;

	/**
	 *申请编号
	 **/
	public CarRequire(String id) {
		this.id = id;
	}
    
    /**
     * car_require
     **/
	public CarRequire() {
	}

}