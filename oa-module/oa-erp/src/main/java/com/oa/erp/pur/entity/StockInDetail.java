package com.oa.erp.pur.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("pur_stock_in_detail")
@ApiModel(description="pur_stock_in_detail")
public class StockInDetail  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
    @TableIds
	
    @ApiModelProperty(notes="资产编码,主键",allowEmptyValue=true,example="",allowableValues="")
    String assetSn;
    @TableIds
	
    @ApiModelProperty(notes="入库单号,主键",allowEmptyValue=true,example="",allowableValues="")
    String receiptNo;

	
	@ApiModelProperty(notes="资产类别",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="资产类别名称",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	
	@ApiModelProperty(notes="资产序列号",allowEmptyValue=true,example="",allowableValues="")
	String assetNo;

	
	@ApiModelProperty(notes="规格型号",allowEmptyValue=true,example="",allowableValues="")
	String spec;

	
	@ApiModelProperty(notes="计量单位",allowEmptyValue=true,example="",allowableValues="")
	String measUnit;

	
	@ApiModelProperty(notes="资产原值",allowEmptyValue=true,example="",allowableValues="")
	String orignUnitAmount;

	
	@ApiModelProperty(notes="使用期限",allowEmptyValue=true,example="",allowableValues="")
	String servLife;

	
	@ApiModelProperty(notes="组织机构代码",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="使用部门",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="存放地点",allowEmptyValue=true,example="",allowableValues="")
	String storeAddress;

	
	@ApiModelProperty(notes="责任人",allowEmptyValue=true,example="",allowableValues="")
	String personLiableUserid;

	
	@ApiModelProperty(notes="采购人",allowEmptyValue=true,example="",allowableValues="")
	String purchaseUserid;

	
	@ApiModelProperty(notes="采购人名称",allowEmptyValue=true,example="",allowableValues="")
	String purchaseUsername;

	
	@ApiModelProperty(notes="采购日期",allowEmptyValue=true,example="",allowableValues="")
	Date purchaseDate;

	
	@ApiModelProperty(notes="供应商编号",allowEmptyValue=true,example="",allowableValues="")
	String supplierId;

	
	@ApiModelProperty(notes="所属父资产",allowEmptyValue=true,example="",allowableValues="")
	String passetSn;

	
	@ApiModelProperty(notes="所属父资产资产编码",allowEmptyValue=true,example="",allowableValues="")
	String passetName;

	
	@ApiModelProperty(notes="保修期（月）",allowEmptyValue=true,example="",allowableValues="")
	String defectsLiabilityPeriod;

	
	@ApiModelProperty(notes="入库日期",allowEmptyValue=true,example="",allowableValues="")
	String warehouseDate;

	
	@ApiModelProperty(notes="资产状态0闲置1在用2借用3维修4报废",allowEmptyValue=true,example="",allowableValues="")
	String assetStatus;

	
	@ApiModelProperty(notes="开始使用日期",allowEmptyValue=true,example="",allowableValues="")
	String startUseDate;

	
	@ApiModelProperty(notes="盘点任务",allowEmptyValue=true,example="",allowableValues="")
	String inventoryTask;

	
	@ApiModelProperty(notes="配置说明",allowEmptyValue=true,example="",allowableValues="")
	String confDesc;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="资产图片",allowEmptyValue=true,example="",allowableValues="")
	String assetImageUrl;

	
	@ApiModelProperty(notes="创建人",allowEmptyValue=true,example="",allowableValues="")
	String createUserid;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	String createTime;

	
	@ApiModelProperty(notes="最后修改人",allowEmptyValue=true,example="",allowableValues="")
	String lastEditUserid;

	
	@ApiModelProperty(notes="最后修改时间",allowEmptyValue=true,example="",allowableValues="")
	String lastEditTime;

	
	@ApiModelProperty(notes="供应商",allowEmptyValue=true,example="",allowableValues="")
	String supplierName;

	
	@ApiModelProperty(notes="资产名称",allowEmptyValue=true,example="",allowableValues="")
	String assetName;

	
	@ApiModelProperty(notes="数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal stockNum;

	
	@ApiModelProperty(notes="入库数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal inNum;

	
	@ApiModelProperty(notes="仓库编号",allowEmptyValue=true,example="",allowableValues="")
	String warehouseId;

	
	@ApiModelProperty(notes="仓库名称",allowEmptyValue=true,example="",allowableValues="")
	String warehouseName;

	
	@ApiModelProperty(notes="产品编号",allowEmptyValue=true,example="",allowableValues="")
	String productId;

	
	@ApiModelProperty(notes="产品编码",allowEmptyValue=true,example="",allowableValues="")
	String productSn;

	
	@ApiModelProperty(notes="品牌编号",allowEmptyValue=true,example="",allowableValues="")
	String brandId;

	
	@ApiModelProperty(notes="品牌名称",allowEmptyValue=true,example="",allowableValues="")
	String brandName;

	
	@ApiModelProperty(notes="采购单编号",allowEmptyValue=true,example="",allowableValues="")
	String purOrderId;

	
	@ApiModelProperty(notes="采购明细编号",allowEmptyValue=true,example="",allowableValues="")
	String orderDetailId;

	
	@ApiModelProperty(notes="资产明细编号",allowEmptyValue=true,example="",allowableValues="")
	String assetDetailId;

	/**
	 *资产编码,入库单号
	 **/
	public StockInDetail(String assetSn,String receiptNo) {
		this.assetSn = assetSn;
		this.receiptNo = receiptNo;
	}
    
    /**
     * pur_stock_in_detail
     **/
	public StockInDetail() {
	}

}