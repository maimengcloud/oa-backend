package com.oa.erp.pur.vo;

public class DetailLinkVo {
    String orderDetailId;
    String requireDetailId;

    public String getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(String orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getRequireDetailId() {
        return requireDetailId;
    }

    public void setRequireDetailId(String requireDetailId) {
        this.requireDetailId = requireDetailId;
    }
}
