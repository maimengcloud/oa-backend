package com.oa.erp.ass.dto;

import com.google.common.collect.Lists;
import com.oa.erp.ass.entity.AssetCategory;
import org.springframework.beans.BeanUtils;

import java.util.List;

public class CategoryTreeDto extends AssetCategory {

    private List<CategoryTreeDto> children = Lists.newArrayList();

    public static CategoryTreeDto adapt(AssetCategory assetCategory) {
        CategoryTreeDto categoryTreeDto = new CategoryTreeDto();
        BeanUtils.copyProperties(assetCategory, categoryTreeDto);
        return categoryTreeDto;
    }

    public List<CategoryTreeDto> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryTreeDto> children) {
        this.children = children;
    }
}
