package com.oa.crm.dto;

import com.google.common.collect.Lists;
import com.oa.crm.entity.Region;
import org.springframework.beans.BeanUtils;

import java.util.List;

public class RegionTreeDto extends Region{

    private List<RegionTreeDto> children = Lists.newArrayList();

    public static RegionTreeDto adapt(Region source) {
        RegionTreeDto regionTreeDto = new RegionTreeDto();
        BeanUtils.copyProperties(source, regionTreeDto);
        return regionTreeDto;
    }

    public List<RegionTreeDto> getChildren() {
        return children;
    }

    public void setChildren(List<RegionTreeDto> children) {
        this.children = children;
    }
}
