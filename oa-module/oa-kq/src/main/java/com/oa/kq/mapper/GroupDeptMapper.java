package com.oa.kq.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.safe.client.entity.User;
import com.oa.kq.entity.Dept;
import com.oa.kq.entity.Group;
import com.oa.kq.entity.GroupDept;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
public interface GroupDeptMapper extends BaseMapper<GroupDept> {

    /**
     * 自定义查询，支持多表关联
     * @param page 分页条件
     * @param ew 一定要，并且必须加@Param("ew")注解
     * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    List<Map<String,Object>> selectListMapByWhere(IPage page, @Param("ew") QueryWrapper ew,@Param("ext") Map<String,Object> ext);

    void batchDeleteByDeptid(List<GroupDept> outOfGroupDeptList);

    void deleteJoinDeptByGroupId(String groupId);

    List<Group> selectAllGroup(Dept dept);

    List<Dept> selectDeptByGroupId(String id);

    List<User> selectKQUserByGroupId(String groupId);

    List<User> selectOtherGroupUserByGroupId(String groupId);

    List<Dept> selectOtherGroupDeptByGroupId(String groupId);

    List<Dept> selectGroupsDept(Dept rootDept);

    List<Group> selectOtherGroupByGroupId(String groupId);
}

