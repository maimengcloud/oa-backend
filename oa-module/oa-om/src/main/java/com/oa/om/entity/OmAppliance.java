package  com.oa.om.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import com.mdp.core.dao.annotation.TableIds;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;

/**
 * @author 唛盟开源 code-gen
 * @since 2024-8-22
 */
@Data
@TableName("om_appliance")
@Schema(description="办公用品")
public class OmAppliance  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@Schema(description="主键,主键")
	String id;

	
	@Schema(description="名称")
	String name;

	
	@Schema(description="规格")
	String standard;

	
	@Schema(description="分类id")
	String categoryId;

	
	@Schema(description="分类名称")
	String categoryName;

	
	@Schema(description="备注")
	String remark;

	
	@Schema(description="库存量-根据出入库流水表汇总")
	BigDecimal stock;

	
	@Schema(description="状态0-初始，1-启用，2-拟停用，3-已停用，4-已报废")
	String status;

	
	@Schema(description="单位")
	String unit;

	
	@Schema(description="是否警示")
	String isWarn;

	
	@Schema(description="最高警示数量")
	BigDecimal highWarn;

	
	@Schema(description="最低警示数量")
	BigDecimal lowWarn;

	
	@Schema(description="最高采购数量")
	BigDecimal highPurchase;

	
	@Schema(description="最低采购数量")
	BigDecimal lowPurchase;

	
	@Schema(description="单价")
	BigDecimal price;

	
	@Schema(description="最高单价")
	BigDecimal highPrice;

	
	@Schema(description="最低单价")
	BigDecimal lowPrice;

	
	@Schema(description="图片地址")
	String imageUrl;

	
	@Schema(description="机构号")
	String branchId;

	
	@Schema(description="警示标签")
	String warnTag;

	
	@Schema(description="责任部门编号")
	String deptid;

	
	@Schema(description="存储地址")
	String storageAddress;

	
	@Schema(description="仓库地址-字典om_warehouse")
	String warehouseId;

	
	@Schema(description="管理人员编号")
	String linkUserid;

	
	@Schema(description="管理人员姓名")
	String linkUsername;

	
	@Schema(description="联系电话")
	String linkPhoneno;

	/**
	 *主键
	 **/
	public OmAppliance(String id) {
		this.id = id;
	}
    
    /**
     * 办公用品
     **/
	public OmAppliance() {
	}

}