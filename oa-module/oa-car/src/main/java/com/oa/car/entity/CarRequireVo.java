package com.oa.car.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * 组织 com.qqkj  顶级模块 oa 大模块 car  小模块 <br> 
 * 实体 CarRequire所有属性名: <br>
 *	reqStartTime,reqEndTime,id,reqDeptid,reqDeptName,reqBranchId,reqUserid,reqUsername,reqPhoneno,reqReason,reqTime,reqStatus,bizFlowState,bizProcInstId,destAddress,destGps,reqTitle,hasRoute,reqBranchName;<br>
 * 表 OA.car_require car_require的所有字段名: <br>
 *	req_start_time,req_end_time,id,req_deptid,req_dept_name,req_branch_id,req_userid,req_username,req_phoneno,req_reason,req_time,req_status,biz_flow_state,biz_proc_inst_id,dest_address,dest_gps,req_title,has_route,req_branch_name;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
@ApiModel(description="car_require")
public class CarRequireVo extends CarRequire {

	private static final long serialVersionUID = 1L;


	@ApiModelProperty(notes="汽车司机模板id",allowEmptyValue=true,example="",allowableValues="")
	String carDriverTplId;
	@ApiModelProperty(notes="汽车司机模板名称",allowEmptyValue=true,example="",allowableValues="")
	String carDriverTplName;

	@ApiModelProperty(notes="汽车id",allowEmptyValue=true,example="",allowableValues="")
	String carId;

	@ApiModelProperty(notes="汽车名称",allowEmptyValue=true,example="",allowableValues="")
	String carName;

	@ApiModelProperty(notes="司机id",allowEmptyValue=true,example="",allowableValues="")
	String driverId;

	@ApiModelProperty(notes="司机名称",allowEmptyValue=true,example="",allowableValues="")
	String driverName;

	@ApiModelProperty(notes="其他申请费用信息",allowEmptyValue=true,example="",allowableValues="")
	List<CarRequireExpenseDetail> carExpenseDetailTpls;

	@ApiModelProperty(notes="总路线信息",allowEmptyValue=true,example="",allowableValues="")
	CarRouteTpl carRouteTpl;

	@ApiModelProperty(notes="新建申请路线",allowEmptyValue=true,example="",allowableValues="")
	List<CarRequireRoute> carRouteTplsNew;

	@ApiModelProperty(notes="新建申请子路线",allowEmptyValue=true,example="",allowableValues="")
	List<CarRequireRouteDetail> carRouteDetailTplsNew;

	public CarRouteTpl getCarRouteTpl() {
		return carRouteTpl;
	}

	public void setCarRouteTpl(CarRouteTpl carRouteTpl) {
		this.carRouteTpl = carRouteTpl;
	}

	public String getCarDriverTplId() {
		return carDriverTplId;
	}

	public void setCarDriverTplId(String carDriverTplId) {
		this.carDriverTplId = carDriverTplId;
	}

	public String getCarDriverTplName() {
		return carDriverTplName;
	}

	public void setCarDriverTplName(String carDriverTplName) {
		this.carDriverTplName = carDriverTplName;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public List<CarRequireExpenseDetail> getCarExpenseDetailTpls() {
		return carExpenseDetailTpls;
	}

	public void setCarExpenseDetailTpls(List<CarRequireExpenseDetail> carExpenseDetailTpls) {
		this.carExpenseDetailTpls = carExpenseDetailTpls;
	}

	public List<CarRequireRoute> getCarRouteTplsNew() {
		return carRouteTplsNew;
	}

	public void setCarRouteTplsNew(List<CarRequireRoute> carRouteTplsNew) {
		this.carRouteTplsNew = carRouteTplsNew;
	}

	public List<CarRequireRouteDetail> getCarRouteDetailTplsNew() {
		return carRouteDetailTplsNew;
	}

	public void setCarRouteDetailTplsNew(List<CarRequireRouteDetail> carRouteDetailTplsNew) {
		this.carRouteDetailTplsNew = carRouteDetailTplsNew;
	}
}