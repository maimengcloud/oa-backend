package com.oa.crm.enums;

public enum  CustomerBusinessEnum {

    ARCHIVE_STATUS_RUNNING("1", "归档状态-进行中"),
    ARCHIVE_STATUS_SUCCESS("2", "归档状态-成功"),
    ARCHIVE_STATUS_FAULIT("3", "归档状态-失败");

    CustomerBusinessEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;

    private String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
