package com.oa.ht.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.ht.entity.ContractProduct;
import com.oa.ht.entity.ContractProductBatchVo;
import com.oa.ht.mapper.ContractProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class ContractProductService extends BaseService<ContractProductMapper,ContractProduct> {
	static Logger logger =LoggerFactory.getLogger(ContractProductService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}


	@Autowired
	ContractLogService contractLogService;

	public List<ContractProduct> selectProByIds(Map<String, Object> query) {

		return baseMapper.selectProByIds(query);
	}

	/**
	 * 批量新增、修改、删除一体化
	 */
	public void batchSave(ContractProductBatchVo contractProductsVo) {
		User user= LoginUtils.getCurrentUserInfo();
		String htId=contractProductsVo.getHtId();
		ContractProduct contractProduct=new ContractProduct();
		contractProduct.setHtId(htId);
		if(contractProductsVo.getContractProducts()==null || contractProductsVo.getContractProducts().size()==0){
			this.deleteByWhere(contractProduct);
			contractLogService.addLog(htId,user.getUserid(),user.getUsername(),"清空所有产品");
			return;
		}
		List<ContractProduct> contractProductsDb=this.selectListByWhere(contractProduct);

		if(contractProductsDb==null || contractProductsDb.size()==0){
			for (ContractProduct condition : contractProductsVo.getContractProducts()) {
				condition.setHtId(htId);
				condition.setId(this.createKey("id"));
			}
			this.batchInsert(contractProductsVo.getContractProducts());
			contractLogService.addLog(htId,user.getUserid(),user.getUsername(),"新增"+contractProductsVo.getContractProducts().size()+"条产品");

			return;
		}


		Map<String,ContractProduct> dbMap=new HashMap<>();
		Map<String,ContractProduct> voMap=new HashMap<>();
		List<ContractProduct> addList=new ArrayList<>();
		List<ContractProduct> updateList=new ArrayList<>();
		List<ContractProduct> delList=new ArrayList<>();

		for (ContractProduct condition : contractProductsDb) {
			dbMap.put(condition.getId(),condition);
		}

		for (ContractProduct condition : contractProductsVo.getContractProducts()) {
			voMap.put(condition.getId(),condition);
		}


		for (ContractProduct condition : contractProductsDb) {
			if(voMap.containsKey(condition.getId())){
				updateList.add(voMap.get(condition.getId()));
			}else{
				delList.add(condition);
			}
		}

		for (ContractProduct condition : contractProductsVo.getContractProducts()) {
			if(StringUtils.hasText(condition.getId())){
				if(!dbMap.containsKey(condition.getId())){

					condition.setHtId(htId);
					addList.add(condition);
				}
			}else{
				condition.setId(this.createKey("id"));
				condition.setHtId(htId);
				addList.add(condition);
			}

		}

		if(delList.size()>0){
			this.batchDelete(delList);
			contractLogService.addLog(htId,user.getUserid(),user.getUsername(),"删除"+delList.size()+"条产品");

		}

		if(addList.size()>0){
			this.batchInsert(addList);
			contractLogService.addLog(htId,user.getUserid(),user.getUsername(),"新增"+addList.size()+"条产品");

		}

		if(updateList.size()>0){
			this.batchUpdate(updateList);
			contractLogService.addLog(htId,user.getUserid(),user.getUsername(),"修改"+updateList.size()+"条产品");

		}

	}
}

