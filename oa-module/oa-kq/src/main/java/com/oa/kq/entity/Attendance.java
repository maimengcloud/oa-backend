package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_attendance")
@ApiModel(description="擎勤科技考勤记录表")
public class Attendance  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="用户id",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="签到经度",allowEmptyValue=true,example="",allowableValues="")
	String workLongitude;

	
	@ApiModelProperty(notes="签到纬度",allowEmptyValue=true,example="",allowableValues="")
	String workLatitude;

	
	@ApiModelProperty(notes="签到地址",allowEmptyValue=true,example="",allowableValues="")
	String workAddress;

	
	@ApiModelProperty(notes="签到时间",allowEmptyValue=true,example="",allowableValues="")
	Date workTime;

	
	@ApiModelProperty(notes="上下班状态（上班0，下班1",allowEmptyValue=true,example="",allowableValues="")
	String state;

	
	@ApiModelProperty(notes="流程状态(0,1)",allowEmptyValue=true,example="",allowableValues="")
	String lcState;

	
	@ApiModelProperty(notes="是否外勤（是1，否0）",allowEmptyValue=true,example="",allowableValues="")
	String outState;

	
	@ApiModelProperty(notes="外勤备注信息",allowEmptyValue=true,example="",allowableValues="")
	String outRemarks;

	
	@ApiModelProperty(notes="外勤照片",allowEmptyValue=true,example="",allowableValues="")
	String outImg;

	
	@ApiModelProperty(notes="本次打卡时间0正常1迟到2严重迟到3旷工4早退5缺卡",allowEmptyValue=true,example="",allowableValues="")
	String ontimeState;

	
	@ApiModelProperty(notes="定义表id",allowEmptyValue=true,example="",allowableValues="")
	String defineId;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="考勤规则时间",allowEmptyValue=true,example="",allowableValues="")
	String scheduleTime;

	
	@ApiModelProperty(notes="按钮的位置(0表示第一段上班，1表示第一段下班，2表示第二段上班，3表示第二段下班，4表示第三段上班，5表示第三段下班)",allowEmptyValue=true,example="",allowableValues="")
	String btnPoi;

	/**
	 *编号id
	 **/
	public Attendance(String id) {
		this.id = id;
	}
    
    /**
     * 擎勤科技考勤记录表
     **/
	public Attendance() {
	}

}