package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_year_date_all")
@ApiModel(description="全年日期表")
public class YearDateAll  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="组织id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="年份",allowEmptyValue=true,example="",allowableValues="")
	String repYear;

	
	@ApiModelProperty(notes="0:工作日；1：周末休息日；2：法定休息日；",allowEmptyValue=true,example="",allowableValues="")
	String dbStatus;

	
	@ApiModelProperty(notes="月份",allowEmptyValue=true,example="",allowableValues="")
	String month;

	
	@ApiModelProperty(notes="标签",allowEmptyValue=true,example="",allowableValues="")
	String tag;

	
	@ApiModelProperty(notes="开始日期",allowEmptyValue=true,example="",allowableValues="")
	String startTime;

	
	@ApiModelProperty(notes="结束日期",allowEmptyValue=true,example="",allowableValues="")
	String endTime;

	/**
	 *id
	 **/
	public YearDateAll(String id) {
		this.id = id;
	}
    
    /**
     * 全年日期表
     **/
	public YearDateAll() {
	}

}