package com.oa.meet.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("meet_meeting")
@ApiModel(description="会议表")
public class MeetMeeting  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="会议名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="会议分类id",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="会议分类",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	
	@ApiModelProperty(notes="申请人id",allowEmptyValue=true,example="",allowableValues="")
	String requireUserid;

	
	@ApiModelProperty(notes="申请人",allowEmptyValue=true,example="",allowableValues="")
	String requireUsername;

	
	@ApiModelProperty(notes="会议室id",allowEmptyValue=true,example="",allowableValues="")
	String roomId;

	
	@ApiModelProperty(notes="会议室名称",allowEmptyValue=true,example="",allowableValues="")
	String roomName;

	
	@ApiModelProperty(notes="自定义会议地点",allowEmptyValue=true,example="",allowableValues="")
	String customRoomName;

	
	@ApiModelProperty(notes="开始时间hh:mm",allowEmptyValue=true,example="",allowableValues="")
	String startTime;

	
	@ApiModelProperty(notes="结束时间hh:mm",allowEmptyValue=true,example="",allowableValues="")
	String endTime;

	
	@ApiModelProperty(notes="提醒方式",allowEmptyValue=true,example="",allowableValues="")
	String reminders;

	
	@ApiModelProperty(notes="立刻提醒",allowEmptyValue=true,example="",allowableValues="")
	String remindersImmediate;

	
	@ApiModelProperty(notes="开始前提醒",allowEmptyValue=true,example="",allowableValues="")
	Date remindersBeforeStart;

	
	@ApiModelProperty(notes="结束前提醒",allowEmptyValue=true,example="",allowableValues="")
	Date remindersBeforeEnd;

	
	@ApiModelProperty(notes="其他参加",allowEmptyValue=true,example="",allowableValues="")
	String otherAttender;

	
	@ApiModelProperty(notes="参会人数",allowEmptyValue=true,example="",allowableValues="")
	Integer attenderNum;

	
	@ApiModelProperty(notes="参会客户数",allowEmptyValue=true,example="",allowableValues="")
	Integer customerNum;

	
	@ApiModelProperty(notes="是否启用回执,0否1是,会议参与人手动参加回执",allowEmptyValue=true,example="",allowableValues="")
	String isUsingReturn;

	
	@ApiModelProperty(notes="是否启用会议签到,0否1是",allowEmptyValue=true,example="",allowableValues="")
	String isUsingSignin;

	
	@ApiModelProperty(notes="签到模式：0-人员签到,1-扫码签到,2-WIFI签到",allowEmptyValue=true,example="",allowableValues="")
	String signinMode;

	
	@ApiModelProperty(notes="签到模式1：签到人员,所选人员为统一签到人员！默认为会议申请人！",allowEmptyValue=true,example="",allowableValues="")
	String signinUserid;

	
	@ApiModelProperty(notes="签到模式1：签到人员",allowEmptyValue=true,example="",allowableValues="")
	String signinUsername;

	
	@ApiModelProperty(notes="签到模式2：签到WIFI",allowEmptyValue=true,example="",allowableValues="")
	String signinWifi;

	
	@ApiModelProperty(notes="归属机构",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="单位id",allowEmptyValue=true,example="",allowableValues="")
	String deptId;

	
	@ApiModelProperty(notes="单位",allowEmptyValue=true,example="",allowableValues="")
	String deptName;

	
	@ApiModelProperty(notes="申请备注",allowEmptyValue=true,example="",allowableValues="")
	String requireRemark;

	
	@ApiModelProperty(notes="会议开始备注",allowEmptyValue=true,example="",allowableValues="")
	String startRemark;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="状态0待审批,1审批中,2已批准,3已拒绝,4已开始,5已结束",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="审批意见",allowEmptyValue=true,example="",allowableValues="")
	String approveSuggestion;

	
	@ApiModelProperty(notes="是否循环0不循环、1循环，每次循环copy一份参会人员表到exec_attender表，增加一条会议执行记录到meeting_exec表",allowEmptyValue=true,example="",allowableValues="")
	String circleable;

	
	@ApiModelProperty(notes="循环星期0、1、2、3、4、5、6分别代表周日到周六任意组合，逗号分割",allowEmptyValue=true,example="",allowableValues="")
	String circleDaysOfWeek;

	
	@ApiModelProperty(notes="循环类型week、month、date三种分别代表按星期、按月、指定日期进行循环",allowEmptyValue=true,example="",allowableValues="")
	String circleType;

	
	@ApiModelProperty(notes="循环星期1、2、3、4、5、6、...31分别代表每个月的1-31号任意组合，逗号分割",allowEmptyValue=true,example="",allowableValues="")
	String circleDaysOfMonth;

	
	@ApiModelProperty(notes="指定日期，yyy-MM-dd类型，逗号分割多个",allowEmptyValue=true,example="",allowableValues="")
	String circcleDates;

	
	@ApiModelProperty(notes="循环执行日期",allowEmptyValue=true,example="",allowableValues="")
	Date execCalcDate;

	
	@ApiModelProperty(notes="提醒内容",allowEmptyValue=true,example="",allowableValues="")
	String content;

	
	@ApiModelProperty(notes="附件数目",allowEmptyValue=true,example="",allowableValues="")
	Integer fileCnt;

	
	@ApiModelProperty(notes="下次更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date nextUpdateTime;

	/**
	 *主键
	 **/
	public MeetMeeting(String id) {
		this.id = id;
	}
    
    /**
     * 会议表
     **/
	public MeetMeeting() {
	}

}