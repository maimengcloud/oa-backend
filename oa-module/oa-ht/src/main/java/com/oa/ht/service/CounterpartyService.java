package com.oa.ht.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.collect.Lists;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.ht.entity.Counterparty;
import com.oa.ht.entity.CounterpartyLinkman;
import com.oa.ht.mapper.CounterpartyMapper;
import com.oa.ht.vo.CounterpartyAddVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class CounterpartyService extends BaseService<CounterpartyMapper,Counterparty> {
	static Logger logger =LoggerFactory.getLogger(CounterpartyService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}
	@Autowired
	private CounterpartyLinkmanService counterpartyLinkmanService;

	/**
	 * 添加相对人
	 * @param counterpartyAddVo
	 */
	@Transactional
	public void insertCounterParty(CounterpartyAddVo counterpartyAddVo) {

		User user = LoginUtils.getCurrentUserInfo();
		Counterparty counterparty = new Counterparty();
		BeanUtils.copyProperties(counterpartyAddVo, counterparty);
		counterparty.setCreateUserId(user.getUserid());
		counterparty.setCreateUserName(user.getUsername());
		counterparty.setCreateTime(new Date());
		counterparty.setBranchId(user.getBranchId());
		this.insert(counterparty);

		//添加联系人
		List<CounterpartyLinkman> linkmanList = new ArrayList<>();
		if(!CollectionUtils.isEmpty(counterpartyAddVo.getLinkmanList())) {
			counterpartyAddVo.getLinkmanList().forEach(item -> {
				item.setLinkId(counterpartyLinkmanService.createKey("linkId"));
				item.setCpId(counterparty.getCpId());
				item.setBranchId(user.getBranchId());
				linkmanList.add(item);
			});
			counterpartyLinkmanService.batchInsert(linkmanList);
		}

		//添加操作记录

	}

	/**
	 * 查询相关联系人
	 * @param counterpartyList
	 */
	public List<Map<String, Object>> selectLinkMan(List<Map<String, Object>> counterpartyList) {

		List<String> cpIds = Lists.newArrayList();

		for (Map<String, Object> stringObjectMap : counterpartyList) {
			cpIds.add((String) stringObjectMap.get("cpId"));
		}

		if(CollectionUtils.isEmpty(cpIds)) return counterpartyList;

		//查询联系人
		List<CounterpartyLinkman> linkmanList = counterpartyLinkmanService.selectLinkManByCpIds(cpIds);

		//返回数据
		for (Map<String, Object> stringObjectMap : counterpartyList) {
			List<CounterpartyLinkman> counterpartyLinkmen = new ArrayList<>();
			for (CounterpartyLinkman linkman : linkmanList) {
				if(stringObjectMap.get("cpId").equals(linkman.getCpId())) {
					counterpartyLinkmen.add(linkman);
				}
			}
			stringObjectMap.put("linkmanList", counterpartyLinkmen);
		}
		return counterpartyList;
	}

	/**
	 * 更新相对方信息
	 * @param counterpartyAddVo
	 */
	@Transactional
	public void updateCounterparty(CounterpartyAddVo counterpartyAddVo) {
		User user = LoginUtils.getCurrentUserInfo();
		Counterparty counterparty = new Counterparty();
		BeanUtils.copyProperties(counterpartyAddVo, counterparty);
		//1.更新向对方信息
		this.updateByPk(counterparty);

		//2.删除联系人信息
		CounterpartyLinkman counterpartyLinkman = new CounterpartyLinkman();
		counterpartyLinkman.setCpId(counterpartyAddVo.getCpId());
		counterpartyLinkmanService.deleteByWhere(counterpartyLinkman);

		//3.添加联系人信息
		List<CounterpartyLinkman> linkmanList = new ArrayList<>();
		if(!CollectionUtils.isEmpty(counterpartyAddVo.getLinkmanList())) {
			counterpartyAddVo.getLinkmanList().forEach(item -> {
				item.setLinkId(counterpartyLinkmanService.createKey("linkId"));
				item.setCpId(counterparty.getCpId());
				item.setBranchId(user.getBranchId());
				linkmanList.add(item);
			});
			counterpartyLinkmanService.batchInsert(linkmanList);
		}
	}
	/** 请在此类添加自定义函数 */


}

