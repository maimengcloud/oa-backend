package com.oa.hr.user.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.hr.user.entity.HrUserSkill;
import com.oa.hr.user.service.HrUserSkillService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value="/oa/hr/user/hrUserSkill")
@Api(tags={"员工技能表-操作接口"})
public class HrUserSkillController {
	
	static Logger logger =LoggerFactory.getLogger(HrUserSkillController.class);
	
	@Autowired
	private HrUserSkillService hrUserSkillService;

	@ApiOperation( value = "员工技能表-查询列表",notes=" ")
	@ApiEntityParams(HrUserSkill.class)
	@ApiResponses({
		@ApiResponse(code = 200,response=HrUserSkill.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listHrUserSkill(@ApiIgnore @RequestParam Map<String,Object> params){
			User user=LoginUtils.getCurrentUserInfo();
			QueryWrapper<HrUserSkill> qw = QueryTools.initQueryWrapper(HrUserSkill.class , params);
			IPage page=QueryTools.initPage(params);
			List<Map<String,Object>> datas = hrUserSkillService.selectListMapByWhere(page,qw,params);
			return Result.ok("query-ok","查询成功").setData(datas).setTotal(page.getTotal());
	}
	

	@ApiOperation( value = "员工技能表-新增",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200,response=HrUserSkill.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addHrUserSkill(@RequestBody HrUserSkill hrUserSkill) {
		 hrUserSkillService.save(hrUserSkill);
         return Result.ok("add-ok","添加成功！");
	}

	@ApiOperation( value = "员工技能表-删除",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
	}) 
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delHrUserSkill(@RequestBody HrUserSkill hrUserSkill){
		hrUserSkillService.removeById(hrUserSkill);
        return Result.ok("del-ok","删除成功！");
	}

	@ApiOperation( value = "员工技能表-修改",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200,response=HrUserSkill.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	}) 
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editHrUserSkill(@RequestBody HrUserSkill hrUserSkill) {
		hrUserSkillService.updateById(hrUserSkill);
        return Result.ok("edit-ok","修改成功！");
	}

    @ApiOperation( value = "员工技能表-批量修改某些字段",notes="")
    @ApiEntityParams( value = HrUserSkill.class, props={ }, remark = "员工技能表", paramType = "body" )
	@ApiResponses({
			@ApiResponse(code = 200,response=HrUserSkill.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
	})
	@RequestMapping(value="/editSomeFields",method=RequestMethod.POST)
	public Result editSomeFields( @ApiIgnore @RequestBody Map<String,Object> params) {
            User user= LoginUtils.getCurrentUserInfo();
            hrUserSkillService.editSomeFields(params);
            return Result.ok("edit-ok","更新成功");
	}

	@ApiOperation( value = "员工技能表-批量删除",notes=" ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
	}) 
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelHrUserSkill(@RequestBody List<HrUserSkill> hrUserSkills) {
	    User user= LoginUtils.getCurrentUserInfo();
        if(hrUserSkills.size()<=0){
            return Result.error("batchDel-data-err-0","请上送待删除数据列表");
        }
         List<HrUserSkill> datasDb=hrUserSkillService.listByIds(hrUserSkills.stream().map(i-> i.getId() ).collect(Collectors.toList()));

        List<HrUserSkill> can=new ArrayList<>();
        List<HrUserSkill> no=new ArrayList<>();
        for (HrUserSkill data : datasDb) {
            if(true){
                can.add(data);
            }else{
                no.add(data);
            }
        }
        List<String> msgs=new ArrayList<>();
        if(can.size()>0){
            hrUserSkillService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num","成功删除%s条数据.",can.size()));
        }

        if(no.size()>0){
            msgs.add(LangTips.transMsg("not-allow-del-num","以下%s条数据不能删除:【%s】",no.size(),no.stream().map(i-> i.getId() ).collect(Collectors.joining(","))));
        }
        if(can.size()>0){
             return Result.ok(msgs.stream().collect(Collectors.joining()));
        }else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
	} 

	@ApiOperation( value = "员工技能表-根据主键查询一条数据",notes=" ")
     @ApiResponses({
            @ApiResponse(code = 200,response=HrUserSkill.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value="/queryById",method=RequestMethod.GET)
    public Result queryById(HrUserSkill hrUserSkill) {
        HrUserSkill data = (HrUserSkill) hrUserSkillService.getById(hrUserSkill);
        return Result.ok().setData(data);
    }

}
