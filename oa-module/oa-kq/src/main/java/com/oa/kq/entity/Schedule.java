package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_schedule")
@ApiModel(description="排班时间表")
public class Schedule  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="班次名称（用户自定义）",allowEmptyValue=true,example="",allowableValues="")
	String scheduleName;

	
	@ApiModelProperty(notes="上班时间09:30",allowEmptyValue=true,example="",allowableValues="")
	String workTimea;

	
	@ApiModelProperty(notes="下班时间。",allowEmptyValue=true,example="",allowableValues="")
	String releaseTimea;

	
	@ApiModelProperty(notes="上班弹性时间（分钟）",allowEmptyValue=true,example="",allowableValues="")
	String flexibleTime;

	
	@ApiModelProperty(notes="严重迟到时间（分钟）",allowEmptyValue=true,example="",allowableValues="")
	String lateTime;

	
	@ApiModelProperty(notes="旷工时间（分钟）",allowEmptyValue=true,example="",allowableValues="")
	String absenteeismTime;

	
	@ApiModelProperty(notes="部门id（）",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="上班时间",allowEmptyValue=true,example="",allowableValues="")
	String workTimeb;

	
	@ApiModelProperty(notes="下班时间。",allowEmptyValue=true,example="",allowableValues="")
	String releaseTimeb;

	
	@ApiModelProperty(notes="上班时间",allowEmptyValue=true,example="",allowableValues="")
	String workTimec;

	
	@ApiModelProperty(notes="下班时间。",allowEmptyValue=true,example="",allowableValues="")
	String releaseTimec;

	
	@ApiModelProperty(notes="排班方式（1一天一次，2一天两次，3一天三次）",allowEmptyValue=true,example="",allowableValues="")
	String scheduleType;

	
	@ApiModelProperty(notes="下班是否需要打卡（0不需要，1需要）",allowEmptyValue=true,example="",allowableValues="")
	String isNeedatt;

	
	@ApiModelProperty(notes="午休开始时间。一天一次班才有的",allowEmptyValue=true,example="",allowableValues="")
	String noonBreakBegin;

	
	@ApiModelProperty(notes="午休结束时间",allowEmptyValue=true,example="",allowableValues="")
	String noonBreakEnd;

	
	@ApiModelProperty(notes="合计工作时间",allowEmptyValue=true,example="",allowableValues="")
	String countTime;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *编号id
	 **/
	public Schedule(String id) {
		this.id = id;
	}
    
    /**
     * 排班时间表
     **/
	public Schedule() {
	}

}