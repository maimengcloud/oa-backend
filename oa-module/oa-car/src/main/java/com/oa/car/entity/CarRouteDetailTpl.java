package com.oa.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author code-gen
 * @since 2023年10月7日
 */
@Data
@TableName("car_route_detail_tpl")
@ApiModel(description="car_route_detail_tpl")
public class CarRouteDetailTpl  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="出发地址",allowEmptyValue=true,example="",allowableValues="")
	String fromAddress;

	
	@ApiModelProperty(notes="目的地址",allowEmptyValue=true,example="",allowableValues="")
	String destAddress;

	
	@ApiModelProperty(notes="出发地址定位",allowEmptyValue=true,example="",allowableValues="")
	String fromAddressGps;

	
	@ApiModelProperty(notes="目的地址定位",allowEmptyValue=true,example="",allowableValues="")
	String destAddressGps;

	
	@ApiModelProperty(notes="是否打卡",allowEmptyValue=true,example="",allowableValues="")
	String isSignIn;

	
	@ApiModelProperty(notes="序号",allowEmptyValue=true,example="",allowableValues="")
	Integer seqNum;

	
	@ApiModelProperty(notes="线路名称",allowEmptyValue=true,example="",allowableValues="")
	String detailName;

	
	@ApiModelProperty(notes="距离米",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal distance;

	
	@ApiModelProperty(notes="路线编号",allowEmptyValue=true,example="",allowableValues="")
	String routeId;

	/**
	 *主键
	 **/
	public CarRouteDetailTpl(String id) {
		this.id = id;
	}
    
    /**
     * car_route_detail_tpl
     **/
	public CarRouteDetailTpl() {
	}

}