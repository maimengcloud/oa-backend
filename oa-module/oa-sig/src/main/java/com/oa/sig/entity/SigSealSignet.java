package com.oa.sig.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("sig_seal_signet")
@ApiModel(description="sig_seal_signet")
public class SigSealSignet  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="申请单编号",allowEmptyValue=true,example="",allowableValues="")
	String sealId;

	
	@ApiModelProperty(notes="材料名称",allowEmptyValue=true,example="",allowableValues="")
	String docName;

	
	@ApiModelProperty(notes="材料保存地址/下载地址/上传地址",allowEmptyValue=true,example="",allowableValues="")
	String docFileUrl;

	
	@ApiModelProperty(notes="印章编号",allowEmptyValue=true,example="",allowableValues="")
	String signetId;

	
	@ApiModelProperty(notes="印章类型公章0法人代表章1法人代表章",allowEmptyValue=true,example="",allowableValues="")
	String signetType;

	
	@ApiModelProperty(notes="所属主体",allowEmptyValue=true,example="",allowableValues="")
	String signetBranchId;

	
	@ApiModelProperty(notes="用印份数",allowEmptyValue=true,example="",allowableValues="")
	Integer sealCount;

	
	@ApiModelProperty(notes="是否加盖骑缝章",allowEmptyValue=true,example="",allowableValues="")
	String pageSeal;

	/**
	 *主键
	 **/
	public SigSealSignet(String id) {
		this.id = id;
	}
    
    /**
     * sig_seal_signet
     **/
	public SigSealSignet() {
	}

}