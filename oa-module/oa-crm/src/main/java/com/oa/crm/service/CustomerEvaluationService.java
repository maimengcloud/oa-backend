package com.oa.crm.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.crm.entity.CustomerEvaluation;
import com.oa.crm.mapper.CustomerEvaluationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class CustomerEvaluationService extends BaseService<CustomerEvaluationMapper, CustomerEvaluation> {
    static Logger logger = LoggerFactory.getLogger(CustomerEvaluationService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    @Autowired
    private CustomerEvaluationTemService customerEvaluationTemService;

    @Autowired
    private CustomerService customerService;

    /**
     * 查询客户价值
     *
     * @param customerEvaluation 客户价值
     * @return
     */
    public List<Map<String, Object>> selectCustomerEvaluationList(IPage page, QueryWrapper queryWrapper, Map<String, Object> customerEvaluation) {
        //1.查询客户价值模板
        List<Map<String, Object>> templateList = customerEvaluationTemService.selectListMapByWhere(null, null, null);

        //2.查询客户价值
        List<Map<String, Object>> customerEvaluationList = this.selectListMapByWhere(page, queryWrapper, customerEvaluation);

        if (CollectionUtils.isEmpty(customerEvaluationList)) {
            return templateList;
        }
        for (Map<String, Object> template : templateList) {
            for (Map<String, Object> customerMap : customerEvaluationList) {
                if (template.get("temId").equals(customerMap.get("temId"))) {
                    template.put("id", customerMap.get("id"));
                    template.put("customerId", customerMap.get("customerId"));
                    template.put("level", customerMap.get("level"));
                    template.put("value", customerMap.get("level"));
                    template.put("levelName", customerMap.get("levelName"));
                    template.put("score", customerMap.get("score"));
                }
            }
        }
        return templateList;
    }

    /**
     * 更新客户价值
     *
     * @param customerEvaluationList
     */
    @Transactional
    public void insertEvaluationList(List<CustomerEvaluation> customerEvaluationList) {

        String customerId = customerEvaluationList.get(0).getCustomerId();

        //1.删除客户评价数据
        CustomerEvaluation customerEvaluation = new CustomerEvaluation();
        customerEvaluation.setCustomerId(customerId);
        this.deleteByWhere(customerEvaluation);
        //2.添加数据
        for (CustomerEvaluation evaluation : customerEvaluationList) {
            evaluation.setId(this.createKey("id"));
        }
        this.batchInsert(customerEvaluationList);

        //3.修改主数据
        //--1.计算客户登记
        String level = this.countCustomerLevel(customerEvaluationList);
        Map<String, Object> editParams = new HashMap<>();
        editParams.put("evaluation", level);
        editParams.put("customerId", customerId);
        customerService.updateCustomerEvaluation(editParams);
    }

    //计算客户等级
    private String countCustomerLevel(List<CustomerEvaluation> customerEvaluationList) {
        BigDecimal score = new BigDecimal(0);
        //1.计算客户评价总分
        for (CustomerEvaluation customerEvaluation : customerEvaluationList) {
            score = (score.add(new BigDecimal(customerEvaluation.getScore())));
        }
        double ns = score.doubleValue();
        if (ns >= 0 && ns <= 1) {
            return "差";
        } else if (ns > 1 && ns <= 2) {
            return "一般";
        } else if (ns > 2 && ns <= 3) {
            return "中等";
        } else if (ns > 3 && ns <= 4) {
            return "良好";
        } else if (ns > 4 && ns <= 5) {
            return "优秀";
        }
        return "";
    }

    /** 请在此类添加自定义函数 */

}

