package com.oa.sig.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("sig_seal_process_approva")
@ApiModel(description="用章审批流程")
public class SigSealProcessApprova  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String procInstId;

	
	@ApiModelProperty(notes="审批状态同意1不同意0",allowEmptyValue=true,example="",allowableValues="")
	String agree;

	
	@ApiModelProperty(notes="审批人",allowEmptyValue=true,example="",allowableValues="")
	String assignee;

	
	@ApiModelProperty(notes="流程标题",allowEmptyValue=true,example="",allowableValues="")
	String mainTitle;

	
	@ApiModelProperty(notes="审批节点编号",allowEmptyValue=true,example="",allowableValues="")
	String actId;

	
	@ApiModelProperty(notes="审批环节",allowEmptyValue=true,example="",allowableValues="")
	String taskName;

	
	@ApiModelProperty(notes="审批意见",allowEmptyValue=true,example="",allowableValues="")
	String commentMsg;

	
	@ApiModelProperty(notes="事件类型create/assignment/complete/delete/PROCESS_CREATED/PROCESS_COMPLETE/PROCESS_CANCELLED",allowEmptyValue=true,example="",allowableValues="")
	String eventName;

	
	@ApiModelProperty(notes="业务主键发起时上送，原样返回",allowEmptyValue=true,example="",allowableValues="")
	String bizKey;

	
	@ApiModelProperty(notes="流程key，可以根据该key找到对应的流程模型也代表审批事项，就是审什么内容",allowEmptyValue=true,example="",allowableValues="")
	String modelKey;

	
	@ApiModelProperty(notes="最后更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowLastTime;

	
	@ApiModelProperty(notes="流程审批机构号",allowEmptyValue=true,example="",allowableValues="")
	String flowBranchId;

	
	@ApiModelProperty(notes="0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String flowState;

	
	@ApiModelProperty(notes="启动人",allowEmptyValue=true,example="",allowableValues="")
	String startUserid;

	
	@ApiModelProperty(notes="流程定义编号带版本的",allowEmptyValue=true,example="",allowableValues="")
	String procDefId;

	
	@ApiModelProperty(notes="模型名称，也代表审批事项，就是审什么内容",allowEmptyValue=true,example="",allowableValues="")
	String modelName;

	
	@ApiModelProperty(notes="结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowEndTime;

	
	@ApiModelProperty(notes="执行人",allowEmptyValue=true,example="",allowableValues="")
	String assigneeName;

	
	@ApiModelProperty(notes="用章申请编号",allowEmptyValue=true,example="",allowableValues="")
	String sigSealId;

	/**
	 *主键
	 **/
	public SigSealProcessApprova(String id) {
		this.id = id;
	}
    
    /**
     * 用章审批流程
     **/
	public SigSealProcessApprova() {
	}

}