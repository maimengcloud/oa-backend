package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_agenda_about")
@ApiModel(description="crm_customer_agenda_about")
public class CustomerAgendaAbout  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="日程相关交流id,主键",allowEmptyValue=true,example="",allowableValues="")
	String aboutId;

	
	@ApiModelProperty(notes="日程id",allowEmptyValue=true,example="",allowableValues="")
	String agendaId;

	
	@ApiModelProperty(notes="提交人id",allowEmptyValue=true,example="",allowableValues="")
	String submitUserId;

	
	@ApiModelProperty(notes="提交人名称",allowEmptyValue=true,example="",allowableValues="")
	String submitUserName;

	
	@ApiModelProperty(notes="提交日期",allowEmptyValue=true,example="",allowableValues="")
	Date submitUserTime;

	
	@ApiModelProperty(notes="富文本",allowEmptyValue=true,example="",allowableValues="")
	String richText;

	/**
	 *日程相关交流id
	 **/
	public CustomerAgendaAbout(String aboutId) {
		this.aboutId = aboutId;
	}
    
    /**
     * crm_customer_agenda_about
     **/
	public CustomerAgendaAbout() {
	}

}