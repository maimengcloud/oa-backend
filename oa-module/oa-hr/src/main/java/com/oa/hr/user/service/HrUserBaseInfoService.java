package com.oa.hr.user.service;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.common.service.SysUserService;
import com.oa.hr.user.entity.HrUserBaseInfo;
import com.oa.hr.user.listenter.HrUserBaseInfoExcelListener;
import com.oa.hr.user.mapper.HrUserBaseInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class HrUserBaseInfoService extends BaseService<HrUserBaseInfoMapper, HrUserBaseInfo> {
    static Logger logger = LoggerFactory.getLogger(HrUserBaseInfoService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    @Autowired
    private HrUserTreatmentService hrUserTreatmentService;
    @Autowired
    private HrSkillService hrSkillService;

    @Autowired
    SysUserService sysUserService;


    /**
     * 请在此类添加自定义函数
     */
    public List<Map<String, Object>> selectListUserInfo(IPage page, QueryWrapper queryWrapper, Map<String, Object> hrUserBaseInfo) {

        return this.selectListMapByWhere(page, queryWrapper, hrUserBaseInfo);
    }

    public Map<String, Object> getEmployeeInfo(IPage page, QueryWrapper qw, Map<String, Object> empMap) {
        Map<String, Object> map = new HashMap<>();
        String userid = (String) empMap.get("userid");
        //员工基本信息
        List<Map<String, Object>> baseInfoMap = this.selectListMapByWhere(page, qw, empMap);
        //员工技能
        List<Map<String, Object>> hrSkills = hrSkillService.selectSkillByUserid(empMap);
        map.put("baseInfoMap", baseInfoMap);
        map.put("hrSkills", hrSkills);
        return map;
    }

    public void saveSubject(MultipartFile file, HrUserBaseInfoService hrUserBaseInfoService) throws IOException {

        //文件输入流
        InputStream in = file.getInputStream();
        //调用方法进行读取
        EasyExcel.read(in, HrUserBaseInfo.class, new HrUserBaseInfoExcelListener(hrUserBaseInfoService)).sheet().doRead();

    }


    public List<Map<String, Object>> selectListBaseInfoByDeriveType(Map<String, Object> hrUserBaseInfo) {
        return baseMapper.selectListBaseInfoByDeriveType(hrUserBaseInfo);
    }


    public List<Map<String, Object>> selectListNewWorker(Map<String, Object> hrUserBaseInfo) {

        return baseMapper.selectListNewWorker(hrUserBaseInfo);
    }
}

