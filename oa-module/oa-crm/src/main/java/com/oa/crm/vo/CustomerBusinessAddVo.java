package com.oa.crm.vo;

import com.oa.crm.entity.CustomerBusiness;
import com.oa.crm.entity.CustomerBusinessProd;

import java.util.List;

public class CustomerBusinessAddVo {

    private CustomerBusiness customerBusiness;

    private List<CustomerBusinessProd> businessProdList;

    public CustomerBusiness getCustomerBusiness() {
        return customerBusiness;
    }

    public void setCustomerBusiness(CustomerBusiness customerBusiness) {
        this.customerBusiness = customerBusiness;
    }

    public List<CustomerBusinessProd> getBusinessProdList() {
        return businessProdList;
    }

    public void setBusinessProdList(List<CustomerBusinessProd> businessProdList) {
        this.businessProdList = businessProdList;
    }
}
