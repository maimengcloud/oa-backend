package com.oa.erp.pur.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("pur_order_detail")
@ApiModel(description="pur_order_detail")
public class OrderDetail  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="明细编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="资产编码",allowEmptyValue=true,example="",allowableValues="")
	String assetSn;

	
	@ApiModelProperty(notes="资产类别",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="资产序列号",allowEmptyValue=true,example="",allowableValues="")
	String assetNo;

	
	@ApiModelProperty(notes="规格型号",allowEmptyValue=true,example="",allowableValues="")
	String spec;

	
	@ApiModelProperty(notes="计量单位",allowEmptyValue=true,example="",allowableValues="")
	String measUnit;

	
	@ApiModelProperty(notes="资产原值",allowEmptyValue=true,example="",allowableValues="")
	String orignUnitAmount;

	
	@ApiModelProperty(notes="使用期限",allowEmptyValue=true,example="",allowableValues="")
	String servLife;

	
	@ApiModelProperty(notes="组织机构代码",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="使用部门",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="采购人",allowEmptyValue=true,example="",allowableValues="")
	String purchaseUserid;

	
	@ApiModelProperty(notes="采购人名称",allowEmptyValue=true,example="",allowableValues="")
	String purchaseUsername;

	
	@ApiModelProperty(notes="采购时间",allowEmptyValue=true,example="",allowableValues="")
	Date purchaseDate;

	
	@ApiModelProperty(notes="供应商编号",allowEmptyValue=true,example="",allowableValues="")
	String supplierId;

	
	@ApiModelProperty(notes="所属父资产",allowEmptyValue=true,example="",allowableValues="")
	String passetSn;

	
	@ApiModelProperty(notes="所属父资产资产编码",allowEmptyValue=true,example="",allowableValues="")
	String passetName;

	
	@ApiModelProperty(notes="保修期（月）",allowEmptyValue=true,example="",allowableValues="")
	String defectsLiabilityPeriod;

	
	@ApiModelProperty(notes="资产状态0闲置1在用2借用3维修4报废",allowEmptyValue=true,example="",allowableValues="")
	String assetStatus;

	
	@ApiModelProperty(notes="配置说明",allowEmptyValue=true,example="",allowableValues="")
	String confDesc;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="资产图片",allowEmptyValue=true,example="",allowableValues="")
	String assetImageUrl;

	
	@ApiModelProperty(notes="创建人",allowEmptyValue=true,example="",allowableValues="")
	String createUserid;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="最后修改人",allowEmptyValue=true,example="",allowableValues="")
	String lastEditUserid;

	
	@ApiModelProperty(notes="最后修改时间",allowEmptyValue=true,example="",allowableValues="")
	Date lastEditTime;

	
	@ApiModelProperty(notes="供应商",allowEmptyValue=true,example="",allowableValues="")
	String supplierName;

	
	@ApiModelProperty(notes="资产名称",allowEmptyValue=true,example="",allowableValues="")
	String assetName;

	
	@ApiModelProperty(notes="采购数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal stockNum;

	
	@ApiModelProperty(notes="到达数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal reachNum;

	
	@ApiModelProperty(notes="产品编号",allowEmptyValue=true,example="",allowableValues="")
	String productId;

	
	@ApiModelProperty(notes="产品编码",allowEmptyValue=true,example="",allowableValues="")
	String productSn;

	
	@ApiModelProperty(notes="品牌编号",allowEmptyValue=true,example="",allowableValues="")
	String brandId;

	
	@ApiModelProperty(notes="品牌名称",allowEmptyValue=true,example="",allowableValues="")
	String brandName;

	
	@ApiModelProperty(notes="采购单编号",allowEmptyValue=true,example="",allowableValues="")
	String purOrderId;

	
	@ApiModelProperty(notes="资产明细编号",allowEmptyValue=true,example="",allowableValues="")
	String assetDetailId;

	
	@ApiModelProperty(notes="分类名称",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	/**
	 *明细编号
	 **/
	public OrderDetail(String id) {
		this.id = id;
	}
    
    /**
     * pur_order_detail
     **/
	public OrderDetail() {
	}

}