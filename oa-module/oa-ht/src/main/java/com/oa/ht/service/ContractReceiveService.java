package com.oa.ht.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.err.BizException;
import com.mdp.core.service.BaseService;
import com.mdp.core.utils.BaseUtils;
import com.oa.ht.entity.*;
import com.oa.ht.mapper.ContractReceiveMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class ContractReceiveService extends BaseService<ContractReceiveMapper, ContractReceive> {
    static Logger logger = LoggerFactory.getLogger(ContractReceiveService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    @Autowired
    ContractReceiveInvoiceService contractReceiveInvoiceService;
    @Autowired
    ContractConditionService contractConditionService;

    @Autowired
    ContractLogService contractLogService;

    @Autowired
    ContractCommonProcessApprovaService commonProcessApprovaService;

    @Autowired
    ContractCardService contractCardService;

    @Transactional
    public void add(ContractReceiveVo contractReceive) {
        ContractCondition contractCondition = new ContractCondition();
        contractCondition.setConditionId(contractReceive.getConditionId());
        contractCondition = contractConditionService.selectOneObject(contractCondition);
        if (contractCondition == null) {
            throw new BizException("收款条件为空");
        }
        if (!"1".equals(contractCondition.getDct())) {
            throw new BizException("请选择收款条件，而不是收款条件");
        }
        if ("2".equals(contractCondition.getConditionStatus())) {
            throw new BizException("该款项已完成收款，不能继续收款");
        }
        contractReceive.setReceiveStatus("0");
        contractReceive.setBizFlowState("0");
        this.insert(contractReceive);
        if (contractReceive.getInvoiceIds() != null && contractReceive.getInvoiceIds().size() > 0) {
            List<ContractReceiveInvoice> links = new ArrayList<>();
            for (String invoiceId : contractReceive.getInvoiceIds()) {
                ContractReceiveInvoice contractReceiveInvoice = new ContractReceiveInvoice();
                contractReceiveInvoice.setReceiveId(contractReceive.getId());
                contractReceiveInvoice.setHtId(contractReceive.getHtId());
                contractReceiveInvoice.setInvoiceId(invoiceId);
                links.add(contractReceiveInvoice);
            }
            contractReceiveInvoiceService.batchInsert(links);
        }

        this.contractConditionService.updateRequireReceiveAmountByConditionId(contractReceive.getConditionId());
        /**
         * 移到审批流中判断
         BigDecimal allReceiveAmount=contractCondition.getFinishAmount();
         if(allReceiveAmount==null||allReceiveAmount.compareTo(BigDecimal.ZERO)==0){
         allReceiveAmount=contractReceive.getReceiveAmount();
         }else{
         allReceiveAmount=allReceiveAmount.add(contractReceive.getReceiveAmount());
         }
         if(allReceiveAmount!=null && allReceiveAmount.compareTo(contractCondition.getConditionAmount())==0){
         ContractCondition contractConditionUpdate=new ContractCondition();
         contractConditionUpdate.setConditionId(contractCondition.getConditionId());
         contractConditionUpdate.setConditionStatus("2");
         contractConditionUpdate.setFinishAmount(allReceiveAmount);
         contractConditionUpdate.setFinishDate(new Date());
         contractConditionService.updateSomeFieldByPk(contractConditionUpdate);
         }else{
         ContractCondition contractConditionUpdate=new ContractCondition();
         contractConditionUpdate.setConditionId(contractCondition.getConditionId());
         contractConditionUpdate.setFinishAmount(allReceiveAmount);
         contractConditionService.updateSomeFieldByPk(contractConditionUpdate);
         }

         updateContractCardHtReceivedAmount(contractReceive.getHtId());
         **/

    }

    private void updateContractCardHtReceivedAmount(String htId) {
//		super.update("com.oa.ht.entity.ContractCard.updateContractCardHtReceivedAmount",htId);
        contractCardService.updateContractCardHtReceivedAmount(htId);
    }

    @Transactional
    public void editContractReceive(ContractReceiveVo contractReceive) {
        this.updateSomeFieldByPk(contractReceive);
        this.contractConditionService.updateRequireReceiveAmountByConditionId(contractReceive.getConditionId());
        ContractReceiveInvoice del = new ContractReceiveInvoice();
        del.setReceiveId(contractReceive.getId());
        contractReceiveInvoiceService.deleteByWhere(del);
        if (contractReceive.getInvoiceIds() != null && contractReceive.getInvoiceIds().size() > 0) {
            List<ContractReceiveInvoice> links = new ArrayList<>();
            for (String invoiceId : contractReceive.getInvoiceIds()) {
                ContractReceiveInvoice contractReceiveInvoice = new ContractReceiveInvoice();
                contractReceiveInvoice.setReceiveId(contractReceive.getId());
                contractReceiveInvoice.setHtId(contractReceive.getHtId());
                contractReceiveInvoice.setInvoiceId(invoiceId);
                links.add(contractReceiveInvoice);
            }
            contractReceiveInvoiceService.batchInsert(links);
        }
    }

    public int deleteByPk(ContractReceive contractReceive) {
        int i = super.deleteByPk(contractReceive);
        this.contractConditionService.updateRequireReceiveAmountByConditionId(contractReceive.getConditionId());
        return i;
    }

    /**
     * 流程审批过程中回调该接口，更新业务数据
     * 如果发起流程时上送了restUrl，则无论流程中是否配置了监听器都会在流程发生以下事件时推送数据过来
     * eventName: PROCESS_STARTED 流程启动完成 全局
     * PROCESS_COMPLETED 流程正常结束 全局
     * PROCESS_CANCELLED 流程删除 全局
     * create 人工任务启动
     * complete 人工任务完成
     * assignment 人工任务分配给了具体的人
     * delete 人工任务被删除
     * TASK_COMPLETED_FORM_DATA_UPDATE 人工任务提交完成后，智能表单数据更新
     * <p>
     * 其中 create/complete/assignment/delete事件是需要在模型中人工节点上配置了委托代理表达式 ${taskBizCallListener}才会推送过来。
     * 在人工任务节点上配置 任务监听器  建议事件为 complete,其它assignment/create/complete/delete也可以，一般建议在complete,委托代理表达式 ${taskBizCallListener}
     *
     * @param flowVars {flowBranchId,agree,procInstId,startUserid,assignee,actId,taskName,mainTitle,branchId,bizKey,commentMsg,eventName,modelKey} 等
     * @return 如果tips.isOk==false，将影响流程提交
     **/
    @Transactional
    public void processApprova(Map<String, Object> flowVars) {
        String eventName = (String) flowVars.get("eventName");
        String agree = (String) flowVars.get("agree");
        String branchId = (String) flowVars.get("branchId");
        String receiveId = (String) flowVars.get("receiveId");//收款编号
        String htId = (String) flowVars.get("htId");//收款编号
        String bizKey = (String) flowVars.get("bizKey");
        if ("contract_receive_process_approva".equals(bizKey)) {
        } else {
            throw new BizException("不支持的业务,请上送业务编码【bizKey】参数");
        }

        if ("complete".equals(eventName)) {
            if ("1".equals(agree)) {
                this.updateFlowStateByProcInst(null, flowVars);
            } else {
                this.updateFlowStateByProcInst(null, flowVars);
            }
        } else {
            if ("PROCESS_STARTED".equals(eventName)) {
                if (StringUtils.isEmpty(receiveId)) {
                    throw new BizException("请上送收款编号-receiveId");
                }
                if (StringUtils.isEmpty(branchId)) {
                    throw new BizException("请上送branchId");
                }
                ContractReceive contractReceive = this.selectOneObject(new ContractReceive(receiveId));
                if (contractReceive == null) {
                    throw new BizException("没有找到对应收款单,收款编号为【" + receiveId + "】");
                } else {
                    if ("1".equals(contractReceive.getBizFlowState())) {
                        throw new BizException("该合同正在审批中，不能再发起审批");
                    }
                }

                flowVars.put("id", this.createKey("id"));
                flowVars.put("contractId", contractReceive.getHtId());
                flowVars.put("contractName", contractReceive.getHtName());
                ContractReceive update = new ContractReceive();
                update.setId(contractReceive.getId());
                update.setBizFlowState("1");
                update.setBizProcInstId((String) flowVars.get("procInstId"));
                this.updateSomeFieldByPk(update);
                flowVars.put("flowState", "1");
                flowVars.put("flowLastTime", new Date());
                commonProcessApprovaService.insert(BaseUtils.fromMap(flowVars, ContractCommonProcessApprova.class));
                this.contractLogService.addLog(contractReceive.getHtId(), (String) flowVars.get("startUserid"), (String) flowVars.get("startUsername"), "合同收款审核流程启动");
            } else if ("PROCESS_COMPLETED".equals(eventName)) {
                //流程表、订单表更新
                flowVars.put("flowEndTime", 1);//传入任意数值，表示flowEndTime需要修改
                if ("1".equals(agree)) {
                    flowVars.put("receiveStatus", "1");
                    this.updateFlowStateByProcInst("2", flowVars);
                    this.updateContractCardHtReceivedAmount(htId);
                    ContractCondition contractConditionDb = this.contractConditionService.getConditionByReceiveId(receiveId);
                    this.contractConditionService.updateContractConditionReceiveFinishAmountAndStatus(contractConditionDb);
                    this.contractLogService.addLog(htId, (String) flowVars.get("startUserid"), (String) flowVars.get("startUsername"), "合同收款审批通过");
                } else {
                    this.contractLogService.addLog(htId, (String) flowVars.get("startUserid"), (String) flowVars.get("startUsername"), "合同收款审核流程已被拒绝");

                    this.updateFlowStateByProcInst("3", flowVars);
                }
            } else if ("PROCESS_CANCELLED".equals(eventName)) {
                //cancel为流程取消状态
                flowVars.put("flowEndTime", 1);//传入任意数值，表示flowEndTime需要修改
                this.contractLogService.addLog(htId, (String) flowVars.get("startUserid"), (String) flowVars.get("startUsername"), "合同收款审核流程取消");

                this.updateFlowStateByProcInst("4", flowVars);
            }
        }

    }

    /**
     * 0初始1审批中2审批通过3审批不通过4流程取消或者删除
     *
     * @param flowState 0初始1审批中2审批通过3审批不通过4流程取消或者删除
     * @param flowVars
     */
    public void updateFlowStateByProcInst(String flowState, Map<String, Object> flowVars) {
        flowVars.put("flowState", flowState);
        flowVars.put("bizFlowState", flowState);
        if ("1".equals(flowState)) {
            flowVars.put("bizProcInstId", flowVars.get("procInstId"));
        }
        commonProcessApprovaService.contractReceiveUpdateProcessApprova(flowVars);
    }
}

