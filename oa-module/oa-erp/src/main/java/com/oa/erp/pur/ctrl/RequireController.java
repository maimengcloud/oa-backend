package com.oa.erp.pur.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.audit.log.client.annotation.AuditLog;
import com.mdp.audit.log.client.annotation.OperType;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.erp.pur.entity.Require;
import com.oa.erp.pur.service.RequireService;
import com.oa.erp.pur.vo.RequireAddVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/erp/pur/require")
@Api(tags = {"资产申购单-操作接口"})
public class RequireController {

    static Logger logger = LoggerFactory.getLogger(RequireController.class);

    @Autowired
    private RequireService requireService;

    @ApiOperation(value = "资产申购单-查询列表", notes = " ")
    @ApiEntityParams(Require.class)
    @ApiResponses({@ApiResponse(code = 200, response = Require.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listRequire(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<Require> qw = QueryTools.initQueryWrapper(Require.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = requireService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "资产申购单-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Require.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addRequire(@RequestBody RequireAddVo requireAddVo) {


        if (StringUtils.isEmpty(requireAddVo.getPurRequire().getId())) {
            requireAddVo.getPurRequire().setId(requireService.createKey("id"));
        } else {
            Require requireQuery = new Require(requireAddVo.getPurRequire().getId());
            if (requireService.countByWhere(requireQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        requireService.insertRequire(requireAddVo);

        return Result.ok("add-ok", "添加成功！").setData(requireAddVo);
    }

    @ApiOperation(value = "资产申购单-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delRequire(@RequestBody Require require) {


        requireService.deleteRequire(require);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "资产申购单-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Require.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editRequire(@RequestBody RequireAddVo requireAddVo) {


        requireService.updateRequire(requireAddVo);

        return Result.ok("edit-ok", "修改成功！").setData(requireAddVo);
    }


    //资产申购审批流程
    @AuditLog(firstMenu = "资产管理", secondMenu = "资产申购", func = "processApprova", funcDesc = "资产申购流程", operType = OperType.UPDATE)
    @RequestMapping(value = "/processApprova", method = RequestMethod.POST)
    public Result processApprova(@RequestBody Map<String, Object> flowVars) {
        QueryWrapper<Require> qw = QueryTools.initQueryWrapper(Require.class, flowVars);
        IPage page = QueryTools.initPage(flowVars);


        this.requireService.processApprova(page, qw, flowVars);
        logger.debug("procInstId=====" + flowVars.get("procInstId"));

        return Result.ok("", "成功提交审批").setTotal(page.getTotal());
    }

    @ApiOperation(value = "资产申购单-批量修改某些字段", notes = "")
    @ApiEntityParams(value = Require.class, props = {}, remark = "资产申购单", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = Require.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        requireService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "资产申购单-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelRequire(@RequestBody List<Require> requires) {
        User user = LoginUtils.getCurrentUserInfo();
        if (requires.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<Require> datasDb = requireService.listByIds(requires.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<Require> can = new ArrayList<>();
        List<Require> no = new ArrayList<>();
        for (Require data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            requireService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "资产申购单-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Require.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(Require require) {
        Require data = (Require) requireService.getById(require);
        return Result.ok().setData(data);
    }

}
