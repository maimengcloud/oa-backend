package com.oa.erp.ass.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ass_asset_mng")
@ApiModel(description="资产出租/出借单")
public class AssetMng  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键（采购申请单）,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="申请人编号",allowEmptyValue=true,example="",allowableValues="")
	String reqUserid;

	
	@ApiModelProperty(notes="申请人姓名",allowEmptyValue=true,example="",allowableValues="")
	String reqUsername;

	
	@ApiModelProperty(notes="申请单位编号",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptid;

	
	@ApiModelProperty(notes="申请单位名称",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptName;

	
	@ApiModelProperty(notes="申请机构编号",allowEmptyValue=true,example="",allowableValues="")
	String reqBranchId;

	
	@ApiModelProperty(notes="申请机构名称",allowEmptyValue=true,example="",allowableValues="")
	String reqBranchName;

	
	@ApiModelProperty(notes="申请标题",allowEmptyValue=true,example="",allowableValues="")
	String reqTitle;

	
	@ApiModelProperty(notes="申请日期",allowEmptyValue=true,example="",allowableValues="")
	Date reqDate;

	
	@ApiModelProperty(notes="申请事由",allowEmptyValue=true,example="",allowableValues="")
	String reqReason;

	
	@ApiModelProperty(notes="维修/借用开始日期",allowEmptyValue=true,example="",allowableValues="")
	Date startDate;

	
	@ApiModelProperty(notes="维修/借用结束日期",allowEmptyValue=true,example="",allowableValues="")
	Date endDate;

	
	@ApiModelProperty(notes="共维修/借用天数",allowEmptyValue=true,example="",allowableValues="")
	Integer totalDay;

	
	@ApiModelProperty(notes="操作方式lease租用reallocation调拨maint维修use领用return归还change变更dispose处置inventory盘点",allowEmptyValue=true,example="",allowableValues="")
	String opType;

	
	@ApiModelProperty(notes="维修费用合计",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal maintTotalFeeAmount;

	
	@ApiModelProperty(notes="是否需要维修配件/耗材0否1是",allowEmptyValue=true,example="",allowableValues="")
	String maintIsConsumables;

	
	@ApiModelProperty(notes="资产处置总资产原值",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal dispTotalOrignAmount;

	
	@ApiModelProperty(notes="资产处置报废变卖值合计",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal dispTotalScrapSaleAmount;

	
	@ApiModelProperty(notes="资产处置同时处置附加资产",allowEmptyValue=true,example="",allowableValues="")
	String dispAddAssets;

	
	@ApiModelProperty(notes="资产出租借入部门",allowEmptyValue=true,example="",allowableValues="")
	String inDeptId;

	
	@ApiModelProperty(notes="资产出租借入部门名称",allowEmptyValue=true,example="",allowableValues="")
	String inDeptName;

	
	@ApiModelProperty(notes="资产出租借出部门",allowEmptyValue=true,example="",allowableValues="")
	String outDeptId;

	
	@ApiModelProperty(notes="资产出租借出部门名称",allowEmptyValue=true,example="",allowableValues="")
	String outDeptName;

	
	@ApiModelProperty(notes="资产出租借入机构id",allowEmptyValue=true,example="",allowableValues="")
	String inBranchId;

	
	@ApiModelProperty(notes="资产出租借入机构名称",allowEmptyValue=true,example="",allowableValues="")
	String inBranchName;

	
	@ApiModelProperty(notes="资产出租借出机构编号",allowEmptyValue=true,example="",allowableValues="")
	String outBranchId;

	
	@ApiModelProperty(notes="资产出租借出机构名称",allowEmptyValue=true,example="",allowableValues="")
	String outBranchName;

	
	@ApiModelProperty(notes="暂存:",allowEmptyValue=true,example="",allowableValues="")
	String mngStatus;

	
	@ApiModelProperty(notes="盘点任务",allowEmptyValue=true,example="",allowableValues="")
	String inventoryTask;

	
	@ApiModelProperty(notes="对应流程实例ID",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="流程业务key",allowEmptyValue=true,example="",allowableValues="")
	String bizKey;

	
	@ApiModelProperty(notes="最后审批意见",allowEmptyValue=true,example="",allowableValues="")
	String commentMsg;

	
	@ApiModelProperty(notes="最后审批人",allowEmptyValue=true,example="",allowableValues="")
	String assignee;

	
	@ApiModelProperty(notes="最后审批人名称",allowEmptyValue=true,example="",allowableValues="")
	String assigneeName;

	
	@ApiModelProperty(notes="最后审批意见0不同意1同意",allowEmptyValue=true,example="",allowableValues="")
	String agree;

	
	@ApiModelProperty(notes="流程最后更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowLastTime;

	
	@ApiModelProperty(notes="流程结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowEndTime;

	
	@ApiModelProperty(notes="流程当前节点编号",allowEmptyValue=true,example="",allowableValues="")
	String actId;

	
	@ApiModelProperty(notes="当前任务名称",allowEmptyValue=true,example="",allowableValues="")
	String taskName;

	
	@ApiModelProperty(notes="0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	/**
	 *主键（采购申请单）
	 **/
	public AssetMng(String id) {
		this.id = id;
	}
    
    /**
     * 资产出租/出借单
     **/
	public AssetMng() {
	}

}