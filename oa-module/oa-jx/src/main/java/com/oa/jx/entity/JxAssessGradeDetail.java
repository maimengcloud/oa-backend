package com.oa.jx.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("jx_assess_grade_detail")
@ApiModel(description="考核分数详情表")
public class JxAssessGradeDetail  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
    @TableIds
	
    @ApiModelProperty(notes="总分表id,主键",allowEmptyValue=true,example="",allowableValues="")
    String gradeId;
    @TableIds
	
    @ApiModelProperty(notes="考核项主键,主键",allowEmptyValue=true,example="",allowableValues="")
    String contentId;

	
	@ApiModelProperty(notes="复评分",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal rePoints;

	
	@ApiModelProperty(notes="自评分",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal selfPoints;

	
	@ApiModelProperty(notes="核定分",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal vouchPoints;

	
	@ApiModelProperty(notes="指标分解描述",allowEmptyValue=true,example="",allowableValues="")
	String selfDesc;

	/**
	 *总分表id,考核项主键
	 **/
	public JxAssessGradeDetail(String gradeId,String contentId) {
		this.gradeId = gradeId;
		this.contentId = contentId;
	}
    
    /**
     * 考核分数详情表
     **/
	public JxAssessGradeDetail() {
	}

}