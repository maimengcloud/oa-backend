package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.query.QueryTools;
import com.mdp.core.service.SequenceService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.Group;
import com.oa.kq.entity.GroupSchedule;
import com.oa.kq.service.GroupScheduleService;
import com.oa.utils.ExcelView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Controller
@RequestMapping(value = "/*/oa/kq/groupSchedule")
@Api(tags = {"组与班次关系表-操作接口"})
public class GroupScheduleController {

    static Logger logger = LoggerFactory.getLogger(GroupScheduleController.class);

    @Autowired
    private GroupScheduleService groupScheduleService;

    @Autowired
    private SequenceService seqService;


    /**
     * 获取所有使用该班次的考勤组
     */
    @ResponseBody
    @RequestMapping(value="/getAllGroupSchedule")
    public List<Group> getAllGroupSchedule(String schId){
        return groupScheduleService.selectGroupBySch(schId);
    }

    /**
     * 跳转到查询页面listGroupSchedule.jsp
     */
    @RequestMapping(value="/listGroupSchedule")
    public ModelAndView listGroupSchedule(@ModelAttribute("groupSchedule") GroupSchedule groupSchedule, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        mv.addObject("data",groupSchedule);
        return mv;
    }


    /**
     * 跳转到新增页面addGroupSchedule.jsp
     */
    @RequestMapping(value="/addGroupSchedule")
    public ModelAndView addGroupSchedule(){

        ModelAndView mv = new ModelAndView();
        GroupSchedule groupSchedule=new GroupSchedule();
        mv.addObject("data",groupSchedule);
        return mv;
    }

    /**
     * 跳转到修改页面editGroupSchedule.jsp
     */
    @RequestMapping(value="/editGroupSchedule")
    public ModelAndView editGroupSchedule( @ModelAttribute("groupSchedule") GroupSchedule groupSchedule, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        groupSchedule = groupScheduleService.selectOneObject(groupSchedule);	//根据ID读取
        mv.addObject("data",groupSchedule);
        return mv;
    }


    /**
     * 请求需带json后缀,如selectListGroupScheduleByPage.json
     * 根据条件查询数据对象列表
     */
    @RequestMapping(value="/selectListGroupScheduleByPage")
    public ModelAndView selectListGroupScheduleByPage( @ModelAttribute("groupSchedule") GroupSchedule groupSchedule, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();

        List<GroupSchedule>	groupScheduleList = groupScheduleService.selectListByWhere(groupSchedule);	//列出GroupSchedule列表
        mv.addObject("data",groupScheduleList);
        Tips tips=new Tips("查询成功");
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 新增一条数据
     */
    @RequestMapping(value="/insertGroupSchedule")
    public ModelAndView insertGroupSchedule( @ModelAttribute("groupSchedule") GroupSchedule groupSchedule, BindingResult result, Model model) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功新增一条数据");
        if(StringUtils.isEmpty(groupSchedule.getId())){
            groupSchedule.setId(seqService.getTablePK("groupSchedule", "id"));
        }
        groupScheduleService.insert(groupSchedule);
        mv.addObject("data",groupSchedule);
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 根据主键删除1条数据
     */
    @RequestMapping(value="/deleteGroupScheduleByPk")
    public ModelAndView deleteGroupScheduleByPk(@ModelAttribute("groupSchedule") GroupSchedule groupSchedule, BindingResult result){
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功删除一条数据");
        groupScheduleService.deleteByPk(groupSchedule);
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 根据主键修改一条数据
     */
    @RequestMapping(value="/updateGroupScheduleByPk")
    public ModelAndView updateGroupScheduleByPk( @ModelAttribute("groupSchedule") GroupSchedule groupSchedule, BindingResult result, Model model) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功更新一条数据");
        groupScheduleService.updateByPk(groupSchedule);
        mv.addObject("data",groupSchedule);
        mv.addObject("tips", tips);
        return mv;
    }




    /**
     * 批量删除
     */
    @RequestMapping(value="/batchDeleteGroupSchedule")
    public ModelAndView batchDeleteGroupSchedule(@RequestParam("id[]") String[] id) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功删除"+id.length+"条数据");
        List<GroupSchedule> list=new ArrayList<GroupSchedule>();
        for(int i=0;i<id.length;i++){
            GroupSchedule groupSchedule=new GroupSchedule();
            groupSchedule.setId(id[i]);
            list.add(groupSchedule);
        }
        groupScheduleService.batchDelete(list);
        mv.addObject("tips", tips);
        return mv;
    }

    /*
     * 导出到excel
     * @return
     */
    @RequestMapping(value="/exportGroupScheduleToExcel")
    public ModelAndView exportGroupScheduleToExcel(@ModelAttribute("groupSchedule") GroupSchedule groupSchedule, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功导出数据");
        try{
            Map<String,Object> dataMap = new HashMap<String,Object>();
            List<String> titles = new ArrayList<String>();
            titles.add("编号");
            titles.add("组id");
            titles.add("班次id");
            dataMap.put("titles", titles);
            List<GroupSchedule> GroupScheduleList = groupScheduleService.selectListByWhere(groupSchedule);
            List<Map<String,Object>> allList = new ArrayList<Map<String,Object>>();
            for(int i=0;i<GroupScheduleList.size();i++){
                Map<String,Object> row = new HashMap<String,Object>();
                row.put("var1", GroupScheduleList.get(i).getId());
                row.put("var2", GroupScheduleList.get(i).getGroupId());
                row.put("var3", GroupScheduleList.get(i).getScheduleId());
                allList.add(row);
            }
            dataMap.put("varList", allList);
            ExcelView erv = new ExcelView();
            mv = new ModelAndView(erv,dataMap);
        } catch(Exception e){
            logger.error(e.toString(), e);
            tips.setErrMsg("导出数据异常");
        }
        mv.addObject("tips", tips);
        return mv;
    }

    @ResponseBody
    @ApiOperation(value = "组与班次关系表-查询列表", notes = " ")
    @ApiEntityParams(GroupSchedule.class)
    @ApiResponses({@ApiResponse(code = 200, response = GroupSchedule.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listGroupSchedule(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        QueryWrapper<GroupSchedule> qw = QueryTools.initQueryWrapper(GroupSchedule.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = groupScheduleService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ResponseBody
    @ApiOperation(value = "组与班次关系表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupSchedule.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addGroupSchedule(@RequestBody GroupSchedule groupSchedule) {
        groupScheduleService.save(groupSchedule);
        return Result.ok("add-ok", "添加成功！");
    }

    @ResponseBody
    @ApiOperation(value = "组与班次关系表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delGroupSchedule(@RequestBody GroupSchedule groupSchedule) {
        groupScheduleService.removeById(groupSchedule);
        return Result.ok("del-ok", "删除成功！");
    }

    @ResponseBody
    @ApiOperation(value = "组与班次关系表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupSchedule.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editGroupSchedule(@RequestBody GroupSchedule groupSchedule) {
        groupScheduleService.updateById(groupSchedule);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ResponseBody
    @ApiOperation(value = "组与班次关系表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = GroupSchedule.class, props = {}, remark = "组与班次关系表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = GroupSchedule.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        groupScheduleService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ResponseBody
    @ApiOperation(value = "组与班次关系表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelGroupSchedule(@RequestBody List<GroupSchedule> groupSchedules) {
        User user = LoginUtils.getCurrentUserInfo();
        if (groupSchedules.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<GroupSchedule> datasDb = groupScheduleService.listByIds(groupSchedules.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<GroupSchedule> can = new ArrayList<>();
        List<GroupSchedule> no = new ArrayList<>();
        for (GroupSchedule data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            groupScheduleService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ResponseBody
    @ApiOperation(value = "组与班次关系表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupSchedule.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(GroupSchedule groupSchedule) {
        GroupSchedule data = (GroupSchedule) groupScheduleService.getById(groupSchedule);
        return Result.ok().setData(data);
    }

}
