package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_counterparty_linkman")
@ApiModel(description="ht_counterparty_linkman")
public class CounterpartyLinkman  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="联系人id,主键",allowEmptyValue=true,example="",allowableValues="")
	String linkId;

	
	@ApiModelProperty(notes="向对方名称",allowEmptyValue=true,example="",allowableValues="")
	String cpId;

	
	@ApiModelProperty(notes="联系人名称",allowEmptyValue=true,example="",allowableValues="")
	String linkName;

	
	@ApiModelProperty(notes="联系人称呼",allowEmptyValue=true,example="",allowableValues="")
	String linkCall;

	
	@ApiModelProperty(notes="联系人岗位",allowEmptyValue=true,example="",allowableValues="")
	String linkJob;

	
	@ApiModelProperty(notes="联系人部门",allowEmptyValue=true,example="",allowableValues="")
	String linkDept;

	
	@ApiModelProperty(notes="联系人办公电话",allowEmptyValue=true,example="",allowableValues="")
	String linkPhone;

	
	@ApiModelProperty(notes="联系人邮箱",allowEmptyValue=true,example="",allowableValues="")
	String linkEmail;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *联系人id
	 **/
	public CounterpartyLinkman(String linkId) {
		this.linkId = linkId;
	}
    
    /**
     * ht_counterparty_linkman
     **/
	public CounterpartyLinkman() {
	}

}