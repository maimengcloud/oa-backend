package com.oa.hr.user.listenter;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.mdp.core.err.BizException;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.hr.user.entity.HrUserTrain;
import com.oa.hr.user.service.HrUserBaseInfoService;
import com.oa.hr.user.service.HrUserTrainService;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HrUserTrainExcelListener extends AnalysisEventListener<HrUserTrain> {

    //因为SubjectExcelListener不能交给spring进行管理，需要自己new，不能注入其他对象
    public HrUserTrainService hrUserTrainService;
    public HrUserBaseInfoService hrUserBaseInfoService;
    List<HrUserTrain> hrUserTrainArrayList = new ArrayList<HrUserTrain>();
    Map<String,Object> uidMap = new HashMap<String,Object>();

    public HrUserTrainExcelListener() {
    }

    public HrUserTrainExcelListener(HrUserBaseInfoService hrUserBaseInfoService, HrUserTrainService hrUserTrainService) {
        this.hrUserTrainService = hrUserTrainService;
        this.hrUserBaseInfoService = hrUserBaseInfoService;
    }

    //读取excel内容，一行一行进行读取
    @Override
    public void invoke(HrUserTrain hrUserTrain, AnalysisContext analysisContext) {

        if(!StringUtils.isEmpty(hrUserTrain)) {
            hrUserTrainArrayList.add(hrUserTrain);
        }

    }




    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        //验证员工是否存在
        String userErrorMsg = this.validateUserExist();
        //验证用户信息是否正确
        String userInfoErrorMsg = this.validateUserInfoExist();

        if(!StringUtils.isEmpty(userErrorMsg) ||
                !StringUtils.isEmpty(userInfoErrorMsg)) {
            throw new BizException(userErrorMsg  + userInfoErrorMsg );
        }
        //保存到数据库
        for (int i= 0; i < hrUserTrainArrayList.size(); i++ ){
            HrUserTrain hrUserTrain = hrUserTrainArrayList.get(i);
            hrUserTrain.setId(hrUserTrainService.createKey("id"));
            hrUserTrainService.insert(hrUserTrain);
        }
    }
    //验证用户是否存在
    public String validateUserExist() {
        String errMsg = "";
        for(int i= 0; i < hrUserTrainArrayList.size() ; i++){
            String userid = hrUserTrainArrayList.get(i).getUserid();
            Map user = (Map)uidMap.get(userid);
            if(StringUtils.isEmpty(user)){
                errMsg +=  "<span class='c-red pdl10'>"+ "第" + (i+1) + "行用户不存在" + "</span></br>";
                hrUserTrainArrayList.remove(i);
            }
        }
        return errMsg;
    }

    //验证用户信息是否正确
    public String validateUserInfoExist() {
        String errMsg = "";
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        for (int i = 0; i < hrUserTrainArrayList.size(); i++) {
            String userid = hrUserTrainArrayList.get(i).getUserid();
            String branchId1 = hrUserTrainArrayList.get(i).getBranchId();
            Map user = (Map) uidMap.get(userid);
            String displayUserid = (String) user.get("displayUserid");
            if (!branchId.equals(branchId1)) {
                errMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行【组织机构号不正确】" + "</span></br>";
            }
        }
        return errMsg;
    }
}
