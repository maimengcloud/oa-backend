package  com.oa.om.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import com.mdp.core.dao.annotation.TableIds;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author 唛盟开源 code-gen
 * @since 2024-8-22
 */
@Data
@TableName("om_category")
@Schema(description="办公用品分类")
public class OmCategory  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@Schema(description="主键,主键")
	String id;

	
	@Schema(description="名称")
	String name;

	
	@Schema(description="状态1启用0禁用")
	String status;

	
	@Schema(description="机构号")
	String branchId;

	
	@Schema(description="备注说明")
	String remark;

	/**
	 *主键
	 **/
	public OmCategory(String id) {
		this.id = id;
	}
    
    /**
     * 办公用品分类
     **/
	public OmCategory() {
	}

}