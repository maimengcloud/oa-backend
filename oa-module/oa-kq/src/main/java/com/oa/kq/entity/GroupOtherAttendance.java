package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_group_other_attendance")
@ApiModel(description="其它考勤人员表")
public class GroupOtherAttendance  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="组id",allowEmptyValue=true,example="",allowableValues="")
	String groupId;

	
	@ApiModelProperty(notes="其它考勤人员id",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *编号id
	 **/
	public GroupOtherAttendance(String id) {
		this.id = id;
	}
    
    /**
     * 其它考勤人员表
     **/
	public GroupOtherAttendance() {
	}

}