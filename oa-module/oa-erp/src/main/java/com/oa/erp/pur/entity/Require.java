package com.oa.erp.pur.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("pur_require")
@ApiModel(description="资产申购单")
public class Require  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键（采购申请单）,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="申购人编号",allowEmptyValue=true,example="",allowableValues="")
	String reqUserid;

	
	@ApiModelProperty(notes="申购人姓名",allowEmptyValue=true,example="",allowableValues="")
	String reqUsername;

	
	@ApiModelProperty(notes="申购单位编号",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptid;

	
	@ApiModelProperty(notes="申购单位名称",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptName;

	
	@ApiModelProperty(notes="申购标题",allowEmptyValue=true,example="",allowableValues="")
	String reqTitle;

	
	@ApiModelProperty(notes="申购机构编号",allowEmptyValue=true,example="",allowableValues="")
	String reqBranchId;

	
	@ApiModelProperty(notes="申购机构名称",allowEmptyValue=true,example="",allowableValues="")
	String reqBranchName;

	
	@ApiModelProperty(notes="申购日期",allowEmptyValue=true,example="",allowableValues="")
	Date reqDate;

	
	@ApiModelProperty(notes="是否计划内0否1是",allowEmptyValue=true,example="",allowableValues="")
	String isPlan;

	
	@ApiModelProperty(notes="申购事由",allowEmptyValue=true,example="",allowableValues="")
	String reqReason;

	
	@ApiModelProperty(notes="申购方式0采购1内部调拨",allowEmptyValue=true,example="",allowableValues="")
	String reqType;

	
	@ApiModelProperty(notes="采购询价意见",allowEmptyValue=true,example="",allowableValues="")
	String purchaseInquiryRemark;

	
	@ApiModelProperty(notes="单据状态hold",allowEmptyValue=true,example="",allowableValues="")
	String reqStatus;

	
	@ApiModelProperty(notes="对应流程实例ID",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="流程业务key",allowEmptyValue=true,example="",allowableValues="")
	String bizKey;

	
	@ApiModelProperty(notes="最后审批意见",allowEmptyValue=true,example="",allowableValues="")
	String commentMsg;

	
	@ApiModelProperty(notes="最后审批人",allowEmptyValue=true,example="",allowableValues="")
	String assignee;

	
	@ApiModelProperty(notes="最后审批人名称",allowEmptyValue=true,example="",allowableValues="")
	String assigneeName;

	
	@ApiModelProperty(notes="最后审批意见0不同意1同意",allowEmptyValue=true,example="",allowableValues="")
	String agree;

	
	@ApiModelProperty(notes="流程最后更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowLastTime;

	
	@ApiModelProperty(notes="流程结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowEndTime;

	
	@ApiModelProperty(notes="流程当前节点编号",allowEmptyValue=true,example="",allowableValues="")
	String actId;

	
	@ApiModelProperty(notes="当前任务名称",allowEmptyValue=true,example="",allowableValues="")
	String taskName;

	
	@ApiModelProperty(notes="0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	/**
	 *主键（采购申请单）
	 **/
	public Require(String id) {
		this.id = id;
	}
    
    /**
     * 资产申购单
     **/
	public Require() {
	}

}