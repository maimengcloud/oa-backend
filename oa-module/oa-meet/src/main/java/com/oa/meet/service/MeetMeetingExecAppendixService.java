package com.oa.meet.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.meet.entity.MeetMeetingExec;
import com.oa.meet.entity.MeetMeetingExecAppendix;
import com.oa.meet.mapper.MeetMeetingExecAppendixMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class MeetMeetingExecAppendixService extends BaseService<MeetMeetingExecAppendixMapper,MeetMeetingExecAppendix> {
	static Logger logger =LoggerFactory.getLogger(MeetMeetingExecAppendixService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}

	public void meetMeetingbatchDeleteUnexec(List<MeetMeetingExec> selectUnexecList) {
		baseMapper.meetMeetingbatchDeleteUnexec(selectUnexecList);
	}
}

