package com.oa.erp.pur.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("pur_order")
@ApiModel(description="pur_order")
public class Order  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="采购标题",allowEmptyValue=true,example="",allowableValues="")
	String purTitle;

	
	@ApiModelProperty(notes="采购申请人id",allowEmptyValue=true,example="",allowableValues="")
	String purUserid;

	
	@ApiModelProperty(notes="采购申请人姓名",allowEmptyValue=true,example="",allowableValues="")
	String purUsername;

	
	@ApiModelProperty(notes="采购申请人部门id",allowEmptyValue=true,example="",allowableValues="")
	String purDeptid;

	
	@ApiModelProperty(notes="采购申请人部门名称",allowEmptyValue=true,example="",allowableValues="")
	String purDeptName;

	
	@ApiModelProperty(notes="采购申请人机构编号",allowEmptyValue=true,example="",allowableValues="")
	String purBranchId;

	
	@ApiModelProperty(notes="采购申请人机构名称",allowEmptyValue=true,example="",allowableValues="")
	String purBranchName;

	
	@ApiModelProperty(notes="采购申请时间",allowEmptyValue=true,example="",allowableValues="")
	Date purDate;

	
	@ApiModelProperty(notes="采购事项",allowEmptyValue=true,example="",allowableValues="")
	String purItems;

	
	@ApiModelProperty(notes="采购单状态：0未发审，1审核中，2待入库，3已入库，4流程取消",allowEmptyValue=true,example="",allowableValues="")
	String orderStatus;

	
	@ApiModelProperty(notes="采购数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalStockNum;

	
	@ApiModelProperty(notes="到达数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalReachNum;

	
	@ApiModelProperty(notes="到达状态0-未到达1已全部到达",allowEmptyValue=true,example="",allowableValues="")
	String reachStatus;

	
	@ApiModelProperty(notes="对应流程实例ID",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="流程业务key",allowEmptyValue=true,example="",allowableValues="")
	String bizKey;

	
	@ApiModelProperty(notes="最后审批意见",allowEmptyValue=true,example="",allowableValues="")
	String commentMsg;

	
	@ApiModelProperty(notes="最后审批人",allowEmptyValue=true,example="",allowableValues="")
	String assignee;

	
	@ApiModelProperty(notes="最后审批人名称",allowEmptyValue=true,example="",allowableValues="")
	String assigneeName;

	
	@ApiModelProperty(notes="最后审批意见0不同意1同意",allowEmptyValue=true,example="",allowableValues="")
	String agree;

	
	@ApiModelProperty(notes="流程最后更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowLastTime;

	
	@ApiModelProperty(notes="流程结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowEndTime;

	
	@ApiModelProperty(notes="流程当前节点编号",allowEmptyValue=true,example="",allowableValues="")
	String actId;

	
	@ApiModelProperty(notes="当前任务名称",allowEmptyValue=true,example="",allowableValues="")
	String taskName;

	
	@ApiModelProperty(notes="0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	
	@ApiModelProperty(notes="采购总金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalStockAmount;

	
	@ApiModelProperty(notes="已入库总金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal totalReachAmount;

	/**
	 *主键
	 **/
	public Order(String id) {
		this.id = id;
	}
    
    /**
     * pur_order
     **/
	public Order() {
	}

}