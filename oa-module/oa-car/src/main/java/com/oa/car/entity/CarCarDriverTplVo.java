package com.oa.car.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 组织 com.qqkj  顶级模块 oa 大模块 car  小模块 <br> 
 * 实体 CarCarDriverTpl所有属性名: <br>
 *	carId,driverId,id,startTime,endTime,status,createTime,branchId;<br>
 * 表 OA.car_car_driver_tpl car_car_driver_tpl的所有字段名: <br>
 *	car_id,driver_id,id,start_time,end_time,status,create_time,branch_id;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
@ApiModel(description="car_car_driver_tpl")
public class CarCarDriverTplVo extends CarCarDriverTpl {


	@ApiModelProperty(notes="汽车名字",allowEmptyValue=true,example="",allowableValues="")
	String carName;

	@ApiModelProperty(notes="司机名字",allowEmptyValue=true,example="",allowableValues="")
	String driverName;


	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
}