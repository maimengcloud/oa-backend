package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_user_dynamically")
@ApiModel(description="员工动态表")
public class HrUserDynamically  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="用户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="操作时间",allowEmptyValue=true,example="",allowableValues="")
	Date time;

	
	@ApiModelProperty(notes="操作者",allowEmptyValue=true,example="",allowableValues="")
	String way;

	
	@ApiModelProperty(notes="操作方法",allowEmptyValue=true,example="",allowableValues="")
	String cause;

	
	@ApiModelProperty(notes="操作记录",allowEmptyValue=true,example="",allowableValues="")
	String record;

	
	@ApiModelProperty(notes="IP地址",allowEmptyValue=true,example="",allowableValues="")
	String ip;

	
	@ApiModelProperty(notes="浏览器",allowEmptyValue=true,example="",allowableValues="")
	String browser;

	
	@ApiModelProperty(notes="设备",allowEmptyValue=true,example="",allowableValues="")
	String equipment;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public HrUserDynamically(String id) {
		this.id = id;
	}
    
    /**
     * 员工动态表
     **/
	public HrUserDynamically() {
	}

}