package com.oa.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月7日
 */
@Data
@TableName("car_driver_sign_in")
@ApiModel(description="car_driver_sign_in")
public class CarDriverSignIn  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="打卡用户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="打卡司机编号",allowEmptyValue=true,example="",allowableValues="")
	String driverId;

	
	@ApiModelProperty(notes="车辆编号",allowEmptyValue=true,example="",allowableValues="")
	String carId;

	
	@ApiModelProperty(notes="申请用车编号（如果有，则填写）",allowEmptyValue=true,example="",allowableValues="")
	String requireId;

	
	@ApiModelProperty(notes="打卡位置",allowEmptyValue=true,example="",allowableValues="")
	String address;

	
	@ApiModelProperty(notes="打卡位置gps",allowEmptyValue=true,example="",allowableValues="")
	String addressGps;

	
	@ApiModelProperty(notes="打卡时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="备注说明",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="状态0-装货中-1出发-2途中-3到达-4卸货中-5卸货完成-6回程途中-7还车完成-8维修中-9故障待援-10故障修复完成",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="打卡用户编号",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="打卡人归属机构",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *主键
	 **/
	public CarDriverSignIn(String id) {
		this.id = id;
	}
    
    /**
     * car_driver_sign_in
     **/
	public CarDriverSignIn() {
	}

}