package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_user_dimission")
@ApiModel(description="员工离职表")
public class HrUserDimission  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="申请离职日期",allowEmptyValue=true,example="",allowableValues="")
	Date applyTime;

	
	@ApiModelProperty(notes="实际离职日期",allowEmptyValue=true,example="",allowableValues="")
	Date practicalTime;

	
	@ApiModelProperty(notes="薪资结算日期",allowEmptyValue=true,example="",allowableValues="")
	Date balanceDate;

	
	@ApiModelProperty(notes="离职原因",allowEmptyValue=true,example="",allowableValues="")
	String cause;

	
	@ApiModelProperty(notes="离职备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="员工编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public HrUserDimission(String id) {
		this.id = id;
	}
    
    /**
     * 员工离职表
     **/
	public HrUserDimission() {
	}

}