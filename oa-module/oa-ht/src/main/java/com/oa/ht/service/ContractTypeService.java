package com.oa.ht.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.mdp.core.service.BaseService;
import com.oa.ht.dto.ContractTypeTreeDto;
import com.oa.ht.entity.ContractType;
import com.oa.ht.mapper.ContractTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class ContractTypeService extends BaseService<ContractTypeMapper,ContractType> {
	static Logger logger =LoggerFactory.getLogger(ContractTypeService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}

	//根节点
	public final static String ROOT = "0";

	/**
	 * 获取合同类型树
	 * @param contractType
	 * @return
	 */
	public List<ContractTypeTreeDto> getTree(Map<String, Object> contractType) {
		List<ContractType> contractTypes = this.selectListByWhere(null);
		//转换Tree
		List<ContractTypeTreeDto> contractTypeTreeDtos = Lists.newArrayList();
		contractTypes.forEach(item -> {
			ContractTypeTreeDto contractTypeTreeDto = ContractTypeTreeDto.adapt(item);
			contractTypeTreeDtos.add(contractTypeTreeDto);
		});
		return ContractListToTree(contractTypeTreeDtos);
	}

	/**
	 * 抓换tree
	 * @param contractTypeTreeDtos
	 * @return
	 */
	private List<ContractTypeTreeDto> ContractListToTree(List<ContractTypeTreeDto> contractTypeTreeDtos) {
		if (CollectionUtils.isEmpty(contractTypeTreeDtos)) {
			return Lists.newArrayList();
		}

		Multimap<String, ContractTypeTreeDto> contractTypeTreeDtoArrayListMultimap = ArrayListMultimap.create();

		List<ContractTypeTreeDto> rootList = Lists.newArrayList();

		for(ContractTypeTreeDto dto : contractTypeTreeDtos) {
			contractTypeTreeDtoArrayListMultimap.put(dto.getTypeLevel(), dto);
			if(ROOT.equals(dto.getTypeLevel())) {
				rootList.add(dto);
			}
		}


		transformContractTypeTree(rootList, ROOT, contractTypeTreeDtoArrayListMultimap);
		return rootList;
	}

	/**
	 * 递归
	 * @param rootList
	 * @param level
	 * @param contractTypeTreeDtoArrayListMultimap
	 */
	private void transformContractTypeTree(List<ContractTypeTreeDto> rootList, String level, Multimap<String, ContractTypeTreeDto> contractTypeTreeDtoArrayListMultimap) {

		for (int i = 0; i < rootList.size(); i++) {
			//1.遍历每个元素
			ContractTypeTreeDto contractTypeTreeDto = rootList.get(i);
			//2.处理数据，计算下一层数据
			String nextLevel = calculateLevel(level, contractTypeTreeDto.getTypeId());
			//3. 处理下一层
			List<ContractTypeTreeDto> tempList = (List<ContractTypeTreeDto>) contractTypeTreeDtoArrayListMultimap.get(nextLevel);
			if (!CollectionUtils.isEmpty(tempList)) {
				//在此进行排序
				Collections.sort(tempList, new Comparator<ContractTypeTreeDto>() {
					@Override
					public int compare(ContractTypeTreeDto s1, ContractTypeTreeDto s2) {
						return Integer.valueOf(s1.getTypeId()).compareTo(Integer.valueOf(s2.getTypeId()));
					}
				});
				// 设置下一层部门
				contractTypeTreeDto.setChildren(tempList);
				// 递归，进入到下一层处理
				transformContractTypeTree(tempList, nextLevel, contractTypeTreeDtoArrayListMultimap);
			}
		}
	}

	public static String calculateLevel(String parentLevel, String parentId) {
		if (StringUtils.isEmpty(parentLevel)) {
			return ROOT;
		} else {
			String[] s = new String[]{parentLevel,parentId};
			return String.join(",", s);
		}
	}
	/** 请在此类添加自定义函数 */

}

