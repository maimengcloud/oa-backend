package com.oa.rc.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.rc.entity.RcSchedule;
import com.oa.rc.entity.RcScheduleExec;
import com.oa.rc.service.RcScheduleExecService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value="/*/oa/rc/rcScheduleExec")
@Api(tags = {"日程执行明细表-操作接口"})
public class RcScheduleExecController {

    static Logger logger = LoggerFactory.getLogger(RcScheduleExecController.class);

    @Autowired
    private RcScheduleExecService rcScheduleExecService;

    @ApiOperation(value = "日程执行明细表-查询列表", notes = " ")
    @ApiEntityParams(RcScheduleExec.class)
    @ApiResponses({@ApiResponse(code = 200, response = RcScheduleExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listRcScheduleExec(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        Tips tips = new Tips("查询成功");

        QueryWrapper<RcScheduleExec> qw = QueryTools.initQueryWrapper(RcScheduleExec.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = rcScheduleExecService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ApiOperation(value = "根据主键修改一条rc_schedule信息", notes = "editRcSchedule")
    @ApiResponses({@ApiResponse(code = 200, response = RcSchedule.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/complete", method = RequestMethod.POST)
    public Result completeRcSchedule(@RequestBody RcScheduleExec rcScheduleExec) {
        Tips tips = new Tips("成功更新一条数据");

            RcScheduleExec rcScheduleExecDB = new RcScheduleExec();
            rcScheduleExecDB.setExecStatus("1");
            rcScheduleExecDB.setId(rcScheduleExec.getId());
            rcScheduleExecService.updateSomeFieldByPk(rcScheduleExecDB);

        return Result.ok().setData(rcScheduleExec);
    }

    @ApiOperation(value = "日程执行明细表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = RcScheduleExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addRcScheduleExec(@RequestBody RcScheduleExec rcScheduleExec) {
        rcScheduleExecService.save(rcScheduleExec);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "日程执行明细表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delRcScheduleExec(@RequestBody RcScheduleExec rcScheduleExec) {
        rcScheduleExecService.removeById(rcScheduleExec);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "日程执行明细表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = RcScheduleExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editRcScheduleExec(@RequestBody RcScheduleExec rcScheduleExec) {
        rcScheduleExecService.updateById(rcScheduleExec);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "日程执行明细表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = RcScheduleExec.class, props = {}, remark = "日程执行明细表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = RcScheduleExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        rcScheduleExecService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "日程执行明细表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelRcScheduleExec(@RequestBody List<RcScheduleExec> rcScheduleExecs) {
        User user = LoginUtils.getCurrentUserInfo();
        if (rcScheduleExecs.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<RcScheduleExec> datasDb = rcScheduleExecService.listByIds(rcScheduleExecs.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<RcScheduleExec> can = new ArrayList<>();
        List<RcScheduleExec> no = new ArrayList<>();
        for (RcScheduleExec data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            rcScheduleExecService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "日程执行明细表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = RcScheduleExec.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(RcScheduleExec rcScheduleExec) {
        RcScheduleExec data = (RcScheduleExec) rcScheduleExecService.getById(rcScheduleExec);
        return Result.ok().setData(data);
    }

}
