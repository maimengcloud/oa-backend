package com.oa.kq.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.err.BizException;
import com.mdp.core.service.BaseService;
import com.oa.kq.entity.WorkOvertimeInfo;
import com.oa.kq.mapper.WorkOvertimeInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class WorkOvertimeInfoService extends BaseService<WorkOvertimeInfoMapper, WorkOvertimeInfo> {
    static Logger logger = LoggerFactory.getLogger(WorkOvertimeInfoService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }


    /** 请在此类添加自定义函数 */


    /**
     * 流程审批过程中回调该接口，更新业务数据
     * 如果发起流程时上送了restUrl，则无论流程中是否配置了监听器都会在流程发生以下事件时推送数据过来
     * eventName: PROCESS_STARTED 流程启动完成 全局
     * PROCESS_COMPLETED 流程正常结束 全局
     * PROCESS_CANCELLED 流程删除 全局
     * create 人工任务启动
     * complete 人工任务完成
     * assignment 人工任务分配给了具体的人
     * delete 人工任务被删除
     * TASK_COMPLETED_FORM_DATA_UPDATE 人工任务提交完成后，智能表单数据更新
     * <p>
     * 其中 create/complete/assignment/delete事件是需要在模型中人工节点上配置了委托代理表达式 ${taskBizCallListener}才会推送过来。
     * 在人工任务节点上配置 任务监听器  建议事件为 complete,其它assignment/create/complete/delete也可以，一般建议在complete,委托代理表达式 ${taskBizCallListener}
     *
     * @param flowVars {flowBranchId,agree,procInstId,startUserid,assignee,actId,taskName,mainTitle,branchId,bizKey,commentMsg,eventName,modelKey} 等
     * @return 如果tips.isOk==false，将影响流程提交
     **/
    public void processApprova(IPage page, QueryWrapper qw, Map<String, Object> flowVars) {
        String eventName = (String) flowVars.get("eventName");
        String agree = (String) flowVars.get("agree");
        String branchId = (String) flowVars.get("branchId");
        String workOvertimeId = (String) flowVars.get("workOvertimeId");
        String bizKey = (String) flowVars.get("bizKey");
        if ("work_overtime_info_approva".equals(bizKey)) {
        } else {
            throw new BizException("不支持的业务,请上送业务编码【bizKey】参数");
        }
        if ("complete".equals(eventName)) {
            if ("1".equals(agree)) {
                this.updateFlowStateByProcInst(null, flowVars);
            } else {
                this.updateFlowStateByProcInst(null, flowVars);
            }
        } else {
            if ("PROCESS_STARTED".equals(eventName)) {
                Map<String, Object> bizQuery = new HashMap<>();
                bizQuery.put("id", workOvertimeId);
                if (StringUtils.isEmpty(workOvertimeId)) {
                    throw new BizException("请上送workOvertimeId");
                }
                if (StringUtils.isEmpty(branchId)) {
                    throw new BizException("请上送branchId");
                }
                List<Map<String, Object>> bizList = this.selectListMapByWhere(page, qw, bizQuery);
                if (bizList == null || bizList.size() == 0) {
                    throw new BizException("没有找到对应加班申请单,申请单为【" + workOvertimeId + "】");
                } else {
                    Map<String, Object> bizObject = bizList.get(0);
                    if ("1".equals(bizObject.get("bizFlowState"))) {
                        throw new BizException("该加班申请单正在审批中，不能再发起审批");
                    }
                }
                flowVars.put("id", this.createKey("id"));
                baseMapper.insertProcessApprova(flowVars);
                this.updateFlowStateByProcInst("1", flowVars);
            } else if ("PROCESS_COMPLETED".equals(eventName)) {
                flowVars.put("flowEndTime", new Date());//传入任意数值，需要更新结束时间这一字段
                if ("1".equals(agree)) {
                    this.updateFlowStateByProcInst("2", flowVars);

                } else {
                    this.updateFlowStateByProcInst("3", flowVars);
                }
            } else if ("PROCESS_CANCELLED".equals(eventName)) {
                flowVars.put("flowEndTime", new Date());//传入任意数值，需要更新结束时间这一字段
                this.updateFlowStateByProcInst("4", flowVars);
            }
        }
    }

    private void updateFlowStateByProcInstForDeleteSuccess(Map<String, Object> flowVars) {
        baseMapper.updateFlowStateByProcInstForDeleteSuccess(flowVars);

    }

    public void updateFlowStateByProcInst(String flowState, Map<String, Object> flowVars) {
        flowVars.put("flowState", flowState);
        flowVars.put("bizFlowState", flowState);
        if ("1".equals(flowState)) {
            flowVars.put("bizProcInstId", flowVars.get("procInstId"));
        }
        baseMapper.updateProcessApprova(flowVars);
    }
}

