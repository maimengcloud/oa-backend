package com.oa.hr.user.listenter;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.mdp.core.err.BizException;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.hr.user.entity.HrUserWorked;
import com.oa.hr.user.service.HrUserBaseInfoService;
import com.oa.hr.user.service.HrUserWorkedService;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HrUserWorkedExcelListener extends AnalysisEventListener<HrUserWorked> {

    //因为SubjectExcelListener不能交给spring进行管理，需要自己new，不能注入其他对象
    public HrUserWorkedService hrUserWorkedService;
    public HrUserBaseInfoService hrUserBaseInfoService;
    List<HrUserWorked> hrUserWorkedList = new ArrayList<HrUserWorked>();
    List<Map<String,Object>> userList;
    Map<String,Object> uidMap = new HashMap<String,Object>();


    public HrUserWorkedExcelListener() {
    }

    public HrUserWorkedExcelListener(HrUserBaseInfoService hrUserBaseInfoService, HrUserWorkedService hrUserWorkedService) {
        this.hrUserWorkedService = hrUserWorkedService;
        this.hrUserBaseInfoService = hrUserBaseInfoService;
    }

    //读取excel内容，一行一行进行读取
    @Override
    public void invoke(HrUserWorked hrUserWorked, AnalysisContext analysisContext) {

        if(!StringUtils.isEmpty(hrUserWorked)) {
            hrUserWorkedList.add(hrUserWorked);
        }

    }




    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        //验证员工是否存在
        String userErrorMsg = this.validateUserExist();
        //验证用户信息是否正确
        String userInfoErrorMsg = this.validateUserInfoExist();

        if(!StringUtils.isEmpty(userErrorMsg) ||
                !StringUtils.isEmpty(userInfoErrorMsg)) {
            throw new BizException(userErrorMsg  + userInfoErrorMsg );
        }
        //保存到数据库
        for (int i= 0; i < hrUserWorkedList.size(); i++ ){
            HrUserWorked hrUserWorked = hrUserWorkedList.get(i);
            hrUserWorked.setId(hrUserWorkedService.createKey("id"));
            hrUserWorkedService.insert(hrUserWorked);
        }
    }
    //验证用户是否存在
    public String validateUserExist() {
        String errMsg = "";
        for(int i= 0; i < hrUserWorkedList.size() ; i++){
            String userid = hrUserWorkedList.get(i).getUserid();
            Map user = (Map)uidMap.get(userid);
            if(StringUtils.isEmpty(user)){
                errMsg +=  "<span class='c-red pdl10'>"+ "第" + (i+1) + "行用户不存在" + "</span></br>";
                hrUserWorkedList.remove(i);
            }
        }
        return errMsg;
    }

    //验证用户信息是否正确
    public String validateUserInfoExist() {
        String errMsg = "";
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        for (int i = 0; i < hrUserWorkedList.size(); i++) {
            String userid = hrUserWorkedList.get(i).getUserid();
            String branchId1 = hrUserWorkedList.get(i).getBranchId();
            Map user = (Map) uidMap.get(userid);
            String displayUserid = (String) user.get("displayUserid");
            if (!branchId.equals(branchId1)) {
                errMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行【组织机构号不正确】" + "</span></br>";
            }
        }
        return errMsg;
    }
}
