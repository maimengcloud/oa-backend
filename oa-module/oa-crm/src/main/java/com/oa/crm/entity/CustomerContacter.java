package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_contacter")
@ApiModel(description="客户联系人表")
public class CustomerContacter  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="联系人id,主键",allowEmptyValue=true,example="",allowableValues="")
	String contacterId;

	
	@ApiModelProperty(notes="联系人名称",allowEmptyValue=true,example="",allowableValues="")
	String contacterName;

	
	@ApiModelProperty(notes="客户id",allowEmptyValue=true,example="",allowableValues="")
	String customerId;

	
	@ApiModelProperty(notes="客户名称",allowEmptyValue=true,example="",allowableValues="")
	String customerName;

	
	@ApiModelProperty(notes="称呼",allowEmptyValue=true,example="",allowableValues="")
	String title;

	
	@ApiModelProperty(notes="称呼名称",allowEmptyValue=true,example="",allowableValues="")
	String titleName;

	
	@ApiModelProperty(notes="别名",allowEmptyValue=true,example="",allowableValues="")
	String anotherName;

	
	@ApiModelProperty(notes="姓",allowEmptyValue=true,example="",allowableValues="")
	String firstName;

	
	@ApiModelProperty(notes="岗位名称",allowEmptyValue=true,example="",allowableValues="")
	String jobTitle;

	
	@ApiModelProperty(notes="项目角色",allowEmptyValue=true,example="",allowableValues="")
	String projectRole;

	
	@ApiModelProperty(notes="意向判断",allowEmptyValue=true,example="",allowableValues="")
	String attitude;

	
	@ApiModelProperty(notes="关注点",allowEmptyValue=true,example="",allowableValues="")
	String attention;

	
	@ApiModelProperty(notes="电子邮件",allowEmptyValue=true,example="",allowableValues="")
	String contacterEmail;

	
	@ApiModelProperty(notes="办公室电话",allowEmptyValue=true,example="",allowableValues="")
	String phoneOffice;

	
	@ApiModelProperty(notes="住宅电话",allowEmptyValue=true,example="",allowableValues="")
	String phoneHome;

	
	@ApiModelProperty(notes="移动电话",allowEmptyValue=true,example="",allowableValues="")
	String mobilePhone;

	
	@ApiModelProperty(notes="传真",allowEmptyValue=true,example="",allowableValues="")
	String contacterFax;

	
	@ApiModelProperty(notes="IM号码",allowEmptyValue=true,example="",allowableValues="")
	String imcode;

	
	@ApiModelProperty(notes="出生年月",allowEmptyValue=true,example="",allowableValues="")
	Date birthday;

	
	@ApiModelProperty(notes="生日提醒前几天",allowEmptyValue=true,example="",allowableValues="")
	Integer birthdayNotifydays;

	
	@ApiModelProperty(notes="语言",allowEmptyValue=true,example="",allowableValues="")
	String contacterLanguage;

	
	@ApiModelProperty(notes="是否主联系人",allowEmptyValue=true,example="",allowableValues="")
	String isMain;

	
	@ApiModelProperty(notes="是否人脉",allowEmptyValue=true,example="",allowableValues="")
	String isPerson;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="备注文档",allowEmptyValue=true,example="",allowableValues="")
	String remarkDoc;

	
	@ApiModelProperty(notes="备注文档名称",allowEmptyValue=true,example="",allowableValues="")
	String remarkDocName;

	
	@ApiModelProperty(notes="上级id",allowEmptyValue=true,example="",allowableValues="")
	String parentId;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *联系人id
	 **/
	public CustomerContacter(String contacterId) {
		this.contacterId = contacterId;
	}
    
    /**
     * 客户联系人表
     **/
	public CustomerContacter() {
	}

}