package com.oa.crm.vo;

import com.oa.crm.entity.CustomerShare;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CustomerShareVo extends CustomerShare {

    private List<Map<String, Object>> shareObjs = new ArrayList<>();

    public List<Map<String, Object>> getShareObjs() {
        return shareObjs;
    }

    public void setShareObjs(List<Map<String, Object>> shareObjs) {
        this.shareObjs = shareObjs;
    }
}
