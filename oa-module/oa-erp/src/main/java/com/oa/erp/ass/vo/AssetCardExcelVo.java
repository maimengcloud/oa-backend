package com.oa.erp.ass.vo;

import com.alibaba.excel.annotation.ExcelProperty;

import java.util.Date;

/**
 * 资产卡片Excels导入实体类
 */
public class AssetCardExcelVo {

    //资产名称
    @ExcelProperty(index = 0)
    private String assetName;

    //资产编号
    @ExcelProperty(index = 1)
    private String assetSn;

    //分类Id
    private String categoryId;

    //分类名称
    @ExcelProperty(index = 2)
    private String categoryName;

    //资产序号
    @ExcelProperty(index = 3)
    private String assetNo;

    //规格
    @ExcelProperty(index = 4)
    private String spec;

    //计量单位
    @ExcelProperty(index = 5)
    private String measUnit;

    //资产原值
    @ExcelProperty(index = 6)
    private String orignUnitAmount;

    //币种
    @ExcelProperty(index = 7)
    private String currency;

    //供应商id
    private String supplierId;

    //供应商名称
    @ExcelProperty(index = 8)
    private String supplierName;

    //使用年限
    @ExcelProperty(index = 9)
    private String servLife;

    //住址机构代码
    @ExcelProperty(index = 10)
    private String branchId;

    //使用部门id
    private String deptid;

    //使用部门名称
    @ExcelProperty(index = 11)
    private String deptName;

    //仓库编号
    private String warehouseId;

    //仓库名称
    @ExcelProperty(index = 12)
    private String warehouseName;

    //存放地址
    private String storeAddress;

    //责任人id
    private String personLiableUserid;

    //责任人姓名
    @ExcelProperty(index = 13)
    private String personLiableUsername;

    //采购人id
    private String purchaseUserid;

    //采购人姓名
    @ExcelProperty(index = 14)
    private String purchaseUsername;

    //采购日期
    @ExcelProperty(index = 15)
    private String purchaseDate;

    //入库单号
    @ExcelProperty(index = 16)
    private String receiptNo;

    //入库日期
    @ExcelProperty(index = 17)
    private Date warehouseDate;

    //保修期月
    @ExcelProperty(index = 18)
    private String defectsLiabilityPeriod;

    //资产状态
    @ExcelProperty(index = 19)
    private String cardStatus;

    //开始使用日期
    @ExcelProperty(index = 20)
    private String startUseDate;

    //配置说明
    @ExcelProperty(index = 21)
    private String confDesc;

    //备注
    @ExcelProperty(index = 22)
    private String remark;


    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetSn() {
        return assetSn;
    }

    public void setAssetSn(String assetSn) {
        this.assetSn = assetSn;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getMeasUnit() {
        return measUnit;
    }

    public void setMeasUnit(String measUnit) {
        this.measUnit = measUnit;
    }

    public String getOrignUnitAmount() {
        return orignUnitAmount;
    }

    public void setOrignUnitAmount(String orignUnitAmount) {
        this.orignUnitAmount = orignUnitAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getServLife() {
        return servLife;
    }

    public void setServLife(String servLife) {
        this.servLife = servLife;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getDeptid() {
        return deptid;
    }

    public void setDeptid(String deptid) {
        this.deptid = deptid;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getPersonLiableUserid() {
        return personLiableUserid;
    }

    public void setPersonLiableUserid(String personLiableUserid) {
        this.personLiableUserid = personLiableUserid;
    }

    public String getPersonLiableUsername() {
        return personLiableUsername;
    }

    public void setPersonLiableUsername(String personLiableUsername) {
        this.personLiableUsername = personLiableUsername;
    }

    public String getPurchaseUserid() {
        return purchaseUserid;
    }

    public void setPurchaseUserid(String purchaseUserid) {
        this.purchaseUserid = purchaseUserid;
    }

    public String getPurchaseUsername() {
        return purchaseUsername;
    }

    public void setPurchaseUsername(String purchaseUsername) {
        this.purchaseUsername = purchaseUsername;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public Date getWarehouseDate() {
        return warehouseDate;
    }

    public void setWarehouseDate(Date warehouseDate) {
        this.warehouseDate = warehouseDate;
    }

    public String getDefectsLiabilityPeriod() {
        return defectsLiabilityPeriod;
    }

    public void setDefectsLiabilityPeriod(String defectsLiabilityPeriod) {
        this.defectsLiabilityPeriod = defectsLiabilityPeriod;
    }

    public String getStartUseDate() {
        return startUseDate;
    }

    public void setStartUseDate(String startUseDate) {
        this.startUseDate = startUseDate;
    }

    public String getConfDesc() {
        return confDesc;
    }

    public void setConfDesc(String confDesc) {
        this.confDesc = confDesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }
}
