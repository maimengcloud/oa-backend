package com.oa.hr.skill.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.err.BizException;
import com.mdp.core.service.BaseService;
import com.oa.hr.skill.entity.HrSkill;
import com.oa.hr.skill.entity.HrSkillCategory;
import com.oa.hr.skill.mapper.SkillMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */

@Service("oa.hr.skill.hrSkillService")
public class SkillService extends BaseService<SkillMapper, HrSkill> {
    static Logger logger = LoggerFactory.getLogger(SkillService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    @Autowired
    SkillCategoryService skillCategoryService;

    public List<Map<String, Object>> getAllHrSkill(Map<String, Object> params) {
        Map<String, Object> result = new HashMap();
        String shopId = (String) params.get("shopId");
        String branchId = (String) params.get("branchId");
        if (StringUtils.isEmpty(branchId)) {
            throw new BizException("请传递branchId");
        }
        /*List<Map<String, Object>> userAlreadyTag = this.selectListMapByWhere(params);*/
        List<Map<String, Object>> allTag = baseMapper.selectAllHrSkill(params);
        return allTag;
    }

    @Transactional
    public int deleteHrSkillCategory(HrSkillCategory tagCategory) {
        String categoryId = tagCategory.getId();
        String branchId = tagCategory.getBranchId();
        if (StringUtils.isEmpty(categoryId)) {
            throw new BizException("请传递标签分类id");
        } else if (StringUtils.isEmpty(branchId)) {
            throw new BizException("请传递branchId");
        }

        int i = skillCategoryService.deleteByPk(tagCategory);
        HrSkill customerDeleteTagObject = new HrSkill();
        customerDeleteTagObject.setCategoryId(categoryId);
        customerDeleteTagObject.setBranchId(branchId);
        baseMapper.deleteByCategoryId(customerDeleteTagObject);
        return i;
    }

    public List<Map<String, Object>> selectSkillByUserid(Map<String, Object> empMap) {

        return baseMapper.selectSkillByUserid(empMap);
    }
}

