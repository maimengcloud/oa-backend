package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_group_position")
@ApiModel(description="考勤与地点关联表")
public class GroupPosition  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="经度",allowEmptyValue=true,example="",allowableValues="")
	String longitude;

	
	@ApiModelProperty(notes="纬度",allowEmptyValue=true,example="",allowableValues="")
	String latitude;

	
	@ApiModelProperty(notes="位置描述",allowEmptyValue=true,example="",allowableValues="")
	String position;

	
	@ApiModelProperty(notes="位置名称",allowEmptyValue=true,example="",allowableValues="")
	String positionName;

	
	@ApiModelProperty(notes="所属组别id",allowEmptyValue=true,example="",allowableValues="")
	String groupId;

	
	@ApiModelProperty(notes="高德地图地址id",allowEmptyValue=true,example="",allowableValues="")
	String poiId;

	
	@ApiModelProperty(notes="云用户机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *编号
	 **/
	public GroupPosition(String id) {
		this.id = id;
	}
    
    /**
     * 考勤与地点关联表
     **/
	public GroupPosition() {
	}

}