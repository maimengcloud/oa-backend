package com.oa.erp.ass.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ass_asset_inventory_task_user")
@ApiModel(description="ass_asset_inventory_task_user")
public class AssetInventoryTaskUser  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="用户Id",allowEmptyValue=true,example="",allowableValues="")
	String inventoryUserid;

	
	@ApiModelProperty(notes="用户名称",allowEmptyValue=true,example="",allowableValues="")
	String inventoryUsername;

	
	@ApiModelProperty(notes="盘点任务Id",allowEmptyValue=true,example="",allowableValues="")
	String taskId;

	/**
	 *id
	 **/
	public AssetInventoryTaskUser(String id) {
		this.id = id;
	}
    
    /**
     * ass_asset_inventory_task_user
     **/
	public AssetInventoryTaskUser() {
	}

}