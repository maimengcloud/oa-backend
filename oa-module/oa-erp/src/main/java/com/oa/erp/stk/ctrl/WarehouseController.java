package com.oa.erp.stk.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.erp.stk.entity.Warehouse;
import com.oa.erp.stk.service.WarehouseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/erp/stk/warehouse")
@Api(tags = {"仓库-操作接口"})
public class WarehouseController {

    static Logger logger = LoggerFactory.getLogger(WarehouseController.class);

    @Autowired
    private WarehouseService warehouseService;

    @ApiOperation(value = "仓库-查询列表", notes = " ")
    @ApiEntityParams(Warehouse.class)
    @ApiResponses({@ApiResponse(code = 200, response = Warehouse.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listWarehouse(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("branchId", user.getBranchId());
        QueryWrapper<Warehouse> qw = QueryTools.initQueryWrapper(Warehouse.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = warehouseService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "仓库-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Warehouse.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addWarehouse(@RequestBody Warehouse warehouse) {
        if (StringUtils.isEmpty(warehouse.getId())) {
            warehouse.setId(warehouseService.createKey("id"));
        } else {
            Warehouse warehouseQuery = new Warehouse(warehouse.getId());
            if (warehouseService.countByWhere(warehouseQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        User user = LoginUtils.getCurrentUserInfo();
        warehouse.setBranchId(user.getBranchId());
        warehouseService.insert(warehouse);

        return Result.ok("add-ok", "添加成功！").setData(warehouse);
    }

    @ApiOperation(value = "仓库-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delWarehouse(@RequestBody Warehouse warehouse) {
        warehouseService.deleteByPk(warehouse);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "仓库-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Warehouse.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editWarehouse(@RequestBody Warehouse warehouse) {
        warehouseService.updateByPk(warehouse);
        return Result.ok("edit-ok", "修改成功！").setData(warehouse);
    }

    @ApiOperation(value = "仓库-批量修改某些字段", notes = "")
    @ApiEntityParams(value = Warehouse.class, props = {}, remark = "仓库", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = Warehouse.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        warehouseService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "仓库-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelWarehouse(@RequestBody List<Warehouse> warehouses) {
        User user = LoginUtils.getCurrentUserInfo();
        if (warehouses.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<Warehouse> datasDb = warehouseService.listByIds(warehouses.stream().map(i -> i.getId()).collect(Collectors.toList()));
        List<Warehouse> can = new ArrayList<>();
        List<Warehouse> no = new ArrayList<>();
        for (Warehouse data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            warehouseService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }
        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "仓库-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Warehouse.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(Warehouse warehouse) {
        Warehouse data = (Warehouse) warehouseService.getById(warehouse);
        return Result.ok().setData(data);
    }
}
