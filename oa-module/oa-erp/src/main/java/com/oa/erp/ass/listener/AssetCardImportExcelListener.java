package com.oa.erp.ass.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.mdp.core.err.BizException;
import com.oa.common.service.SysDeptService;
import com.oa.common.service.SysUserService;
import com.oa.erp.ass.service.AssetCardService;
import com.oa.erp.ass.service.AssetCategoryService;
import com.oa.erp.ass.vo.AssetCardExcelVo;
import com.oa.erp.stk.service.WarehouseService;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AssetCardImportExcelListener extends AnalysisEventListener<AssetCardExcelVo> {

    //资产卡片服务
    private AssetCardService assetCardService;

    //仓库服务
    private WarehouseService warehouseService;

    //资产分类服务
    private AssetCategoryService assetCategoryService;

    //部门服务
    private SysDeptService deptService;

    //系统用户服务
    private SysUserService sysUserService;


    List<AssetCardExcelVo> list = new ArrayList<AssetCardExcelVo>();


    public AssetCardImportExcelListener() {

    }

    public AssetCardImportExcelListener(AssetCardService assetCardService, WarehouseService warehouseService, AssetCategoryService assetCategoryService, SysDeptService deptService, SysUserService sysUserService) {
        this.assetCardService = assetCardService;
        this.warehouseService = warehouseService;
        this.assetCategoryService = assetCategoryService;
        this.deptService = deptService;
        this.sysUserService = sysUserService;
    }


    @Override
    public void invoke(AssetCardExcelVo assetCardExcelVo, AnalysisContext analysisContext) {
        if (!StringUtils.isEmpty(assetCardExcelVo)) {
            list.add(assetCardExcelVo);
        }
        System.out.println(list + "list---------------------------------------");
    }


    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        System.out.println(list.toString());
        //验证数据

        //1.资产分类数据验证
        String cateGoryErrorMsg = this.validateCateGoryData();

        //2.部门验证
        String deptErrorMsg = this.validateDeptData();

        //3.仓库数据验证
        String warehouseErroMsg = this.validateWarehouseData();

        //4.人员验证
        String userErrorMsg = this.validateUserDataErrorMsg();

        //5.验证资产编码，资产编码不能重复
        String assetSnErrorMsg = this.validateAssetSn();

        if (!StringUtils.isEmpty(cateGoryErrorMsg) || !StringUtils.isEmpty(deptErrorMsg) || !StringUtils.isEmpty(warehouseErroMsg) || !StringUtils.isEmpty(userErrorMsg) || !StringUtils.isEmpty(assetSnErrorMsg)) {
            throw new BizException(cateGoryErrorMsg + deptErrorMsg + warehouseErroMsg + userErrorMsg + assetSnErrorMsg);
        }

        //保存数据到数据库
        assetCardService.batchSaveDataByExcel(list);
        list.clear();
    }

    //验证资产编码
    private String validateAssetSn() {
        String errorMsg = "";
        List<Map<String, Object>> assetCards = assetCardService.selectListMapByWhere(null, null, null);
        Map<String, Object> param = new HashMap<>();

        for (int i = 0; i < list.size(); i++) {
            Boolean flag = false;
            for (Map<String, Object> stringObjectMap : assetCards) {
                //责任人
                if (!StringUtils.isEmpty(list.get(i).getAssetSn())) {
                    if (list.get(i).getAssetSn().equals(stringObjectMap.get("assetSn"))) {
                        flag = true;
                    }
                } else {
                    errorMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行资产编码不能为空" + "</span></br>";
                }
            }

            if (flag) {
                errorMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行 " + list.get(i).getAssetSn() + " 该资产编码已存在" + "</span></br>";
            }
        }
        return errorMsg;
    }

    //用户相关验证
    private String validateUserDataErrorMsg() {
        String errorMsg = "";
//        List<Map<String, Object>>  userList = sysUserService.selectList("selectListMapByWhere2", "");
        List<Map<String, Object>> userList = sysUserService.selectListMapByWhere(null, null, null);
        for (int i = 0; i < list.size(); i++) {
            for (Map<String, Object> stringObjectMap : userList) {
                //责任人
                if (!StringUtils.isEmpty(list.get(i).getPersonLiableUsername())) {
                    if (list.get(i).getPersonLiableUsername().equals(stringObjectMap.get("username"))) {
                        list.get(i).setPersonLiableUserid(stringObjectMap.get("userid").toString());
                    }
                }

                //采购人
                if (!StringUtils.isEmpty(list.get(i).getPurchaseUsername())) {
                    if (list.get(i).getPurchaseUsername().equals(stringObjectMap.get("username"))) {
                        list.get(i).setPurchaseUserid(stringObjectMap.get("userid").toString());
                    }
                }
            }

            if (!StringUtils.isEmpty(list.get(i).getPersonLiableUsername()) && StringUtils.isEmpty(list.get(i).getPersonLiableUserid())) {
                errorMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行请填写正确的责任人" + "</span></br>";
            }

            if (!StringUtils.isEmpty(list.get(i).getPurchaseUsername()) && StringUtils.isEmpty(list.get(i).getPurchaseUserid())) {
                errorMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行请填写正确的采购人" + "</span></br>";
            }
        }
        return errorMsg;
    }

    //部门验证
    private String validateDeptData() {
        String errorMsg = "";
//        List<Map<String, Object>>  deptList = deptService.selectListMapByWhere(null);
        List<Map<String, Object>> deptList = deptService.selectListMapByWhere(null, null, null);
        Map<String, Object> param = new HashMap<>();

        deptList.forEach(item -> {
            param.put(item.get("deptName").toString(), item);
        });

        for (int i = 0; i < list.size(); i++) {
            if (!StringUtils.isEmpty(list.get(i).getDeptName())) {
                if (StringUtils.isEmpty(param.get(list.get(i).getDeptName()))) {
                    errorMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行请填写正确的所属部门 " + "</span></br>";
                } else {
                    Map<String, Object> map = (Map<String, Object>) param.get(list.get(i).getDeptName());
                    list.get(i).setDeptid(map.get("deptid").toString());
                }
            }
        }
        return errorMsg;
    }

    //验证资产分类数据
    private String validateCateGoryData() {
        String errorMsg = "";
//        List<Map<String, Object>> categoryList = assetCategoryService.selectListMapByWhere(null);
        List<Map<String, Object>> categoryList = assetCategoryService.selectListMapByWhere(null, null, null);
        Map<String, Object> param = new HashMap<>();

        categoryList.forEach(item -> {
            param.put(item.get("categoryName").toString(), item);
        });

        for (int i = 0; i < list.size(); i++) {
            if (!StringUtils.isEmpty(list.get(i).getCategoryName())) {
                if (StringUtils.isEmpty(param.get(list.get(i).getCategoryName()))) {
                    errorMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行请填写正确的资产分类" + "</span></br>";
                } else {
                    Map<String, Object> map = (Map<String, Object>) param.get(list.get(i).getCategoryName());
                    list.get(i).setCategoryId(map.get("categoryId").toString());
                }
            } else {
                errorMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行资产分类不能为空" + "</span></br>";
                ;
            }

        }
        return errorMsg;
    }

    //验证仓库数据
    private String validateWarehouseData() {
        String errorMsg = "";
//        List<Map<String, Object>> warehouseList = warehouseService.selectListMapByWhere(null);
        List<Map<String, Object>> warehouseList = warehouseService.selectListMapByWhere(null, null, null);
        Map<String, Object> param = new HashMap<>();

        warehouseList.forEach(item -> {
            param.put(item.get("name").toString(), item);
        });

        for (int i = 0; i < list.size(); i++) {
            if (!StringUtils.isEmpty(list.get(i).getWarehouseName())) {
                if (StringUtils.isEmpty(param.get(list.get(i).getWarehouseName()))) {
                    errorMsg += "<span class='c-red pdl10'>" + "第" + (i + 1) + "行请填写正确的仓库名称" + "</span></br>";
                    ;
                } else {
                    Map<String, Object> map = (Map<String, Object>) param.get(list.get(i).getWarehouseName());
                    list.get(i).setWarehouseId(map.get("id").toString());
                    list.get(i).setStoreAddress(map.get("address").toString());
                }
            }
        }
        return errorMsg;
    }

}
