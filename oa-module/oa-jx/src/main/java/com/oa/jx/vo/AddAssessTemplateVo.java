package com.oa.jx.vo;

import com.oa.jx.entity.JxAssessContent;
import com.oa.jx.entity.JxAssessTemplate;
import com.oa.jx.entity.JxAssessTemplateLink;

import java.util.List;

public class AddAssessTemplateVo extends JxAssessTemplate {
   private List<JxAssessContent> accessContents ;
   private List<JxAssessTemplateLink> links ;

    public List<JxAssessContent> getAccessContents() {
        return accessContents;
    }

    public void setAccessContents(List<JxAssessContent> accessContents) {
        this.accessContents = accessContents;
    }

    public List<JxAssessTemplateLink> getLinks() {
        return links;
    }

    public void setLinks(List<JxAssessTemplateLink> links) {
        this.links = links;
    }
}
