package com.oa.erp.ass.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ass_asset_card")
@ApiModel(description="`ass_asset_card`")
public class AssetCard  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键id,主键",allowEmptyValue=true,example="",allowableValues="")
	String cardId;

	
	@ApiModelProperty(notes="关联的资产id",allowEmptyValue=true,example="",allowableValues="")
	String detailId;

	
	@ApiModelProperty(notes="卡片状态0闲置1在用2借用4报废5待领取",allowEmptyValue=true,example="",allowableValues="")
	String cardStatus;

	
	@ApiModelProperty(notes="资产编码",allowEmptyValue=true,example="",allowableValues="")
	String assetSn;

	
	@ApiModelProperty(notes="资产序列号",allowEmptyValue=true,example="",allowableValues="")
	String assetNo;

	
	@ApiModelProperty(notes="规格型号",allowEmptyValue=true,example="",allowableValues="")
	String spec;

	
	@ApiModelProperty(notes="计量单位",allowEmptyValue=true,example="",allowableValues="")
	String measUnit;

	
	@ApiModelProperty(notes="资产原值",allowEmptyValue=true,example="",allowableValues="")
	String orignUnitAmount;

	
	@ApiModelProperty(notes="使用期限",allowEmptyValue=true,example="",allowableValues="")
	String servLife;

	
	@ApiModelProperty(notes="组织机构代码",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="使用部门",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="使用部门名称",allowEmptyValue=true,example="",allowableValues="")
	String deptName;

	
	@ApiModelProperty(notes="责任人",allowEmptyValue=true,example="",allowableValues="")
	String personLiableUserid;

	
	@ApiModelProperty(notes="责任人名称",allowEmptyValue=true,example="",allowableValues="")
	String personLiableUsername;

	
	@ApiModelProperty(notes="采购人",allowEmptyValue=true,example="",allowableValues="")
	String purchaseUserid;

	
	@ApiModelProperty(notes="采购人姓名",allowEmptyValue=true,example="",allowableValues="")
	String purchaseUsername;

	
	@ApiModelProperty(notes="采购日期",allowEmptyValue=true,example="",allowableValues="")
	Date purchaseDate;

	
	@ApiModelProperty(notes="供应商id",allowEmptyValue=true,example="",allowableValues="")
	String supplierId;

	
	@ApiModelProperty(notes="供应商名称",allowEmptyValue=true,example="",allowableValues="")
	String supplierName;

	
	@ApiModelProperty(notes="所属父资产",allowEmptyValue=true,example="",allowableValues="")
	String passetSn;

	
	@ApiModelProperty(notes="所属父资产名称",allowEmptyValue=true,example="",allowableValues="")
	String passetName;

	
	@ApiModelProperty(notes="保修期（月）",allowEmptyValue=true,example="",allowableValues="")
	String defectsLiabilityPeriod;

	
	@ApiModelProperty(notes="入库日期",allowEmptyValue=true,example="",allowableValues="")
	Date warehouseDate;

	
	@ApiModelProperty(notes="开始使用日期",allowEmptyValue=true,example="",allowableValues="")
	Date startUseDate;

	
	@ApiModelProperty(notes="盘点任务",allowEmptyValue=true,example="",allowableValues="")
	String inventoryTask;

	
	@ApiModelProperty(notes="配置说明",allowEmptyValue=true,example="",allowableValues="")
	String confDesc;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="资产图片",allowEmptyValue=true,example="",allowableValues="")
	String assetImageUrl;

	
	@ApiModelProperty(notes="创建人",allowEmptyValue=true,example="",allowableValues="")
	String createUserid;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="创建人名称",allowEmptyValue=true,example="",allowableValues="")
	String createUsername;

	
	@ApiModelProperty(notes="最后修改人",allowEmptyValue=true,example="",allowableValues="")
	String lastEditUserid;

	
	@ApiModelProperty(notes="最后修改时间",allowEmptyValue=true,example="",allowableValues="")
	Date lastEditTime;

	
	@ApiModelProperty(notes="最后修改人名称",allowEmptyValue=true,example="",allowableValues="")
	String lastEditUsername;

	
	@ApiModelProperty(notes="入库单号",allowEmptyValue=true,example="",allowableValues="")
	String receiptNo;

	
	@ApiModelProperty(notes="资产名称",allowEmptyValue=true,example="",allowableValues="")
	String assetName;

	
	@ApiModelProperty(notes="仓库编号",allowEmptyValue=true,example="",allowableValues="")
	String warehouseId;

	
	@ApiModelProperty(notes="仓库名称",allowEmptyValue=true,example="",allowableValues="")
	String warehouseName;

	
	@ApiModelProperty(notes="存放地点",allowEmptyValue=true,example="",allowableValues="")
	String storeAddress;

	
	@ApiModelProperty(notes="产品编号",allowEmptyValue=true,example="",allowableValues="")
	String productId;

	
	@ApiModelProperty(notes="产品编码",allowEmptyValue=true,example="",allowableValues="")
	String productSn;

	
	@ApiModelProperty(notes="品牌编号",allowEmptyValue=true,example="",allowableValues="")
	String brandId;

	
	@ApiModelProperty(notes="品牌名称",allowEmptyValue=true,example="",allowableValues="")
	String brandName;

	
	@ApiModelProperty(notes="hold",allowEmptyValue=true,example="",allowableValues="")
	String cardType;

	
	@ApiModelProperty(notes="资产分类Id",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="资产分类名称",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	
	@ApiModelProperty(notes="是否正在盘点",allowEmptyValue=true,example="",allowableValues="")
	String isInventory;

	
	@ApiModelProperty(notes="资产币种人名币CNY,港元HKD,美元USD,欧元EUR,英镑GBP,日元JPY",allowEmptyValue=true,example="",allowableValues="")
	String currency;

	
	@ApiModelProperty(notes="库存数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal stockNum;

	
	@ApiModelProperty(notes="按数量拆分",allowEmptyValue=true,example="",allowableValues="")
	String splitNum;

	
	@ApiModelProperty(notes="资产处置单-残值",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal residualValue;

	
	@ApiModelProperty(notes="资产处置单-净值",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal netWorth;

	
	@ApiModelProperty(notes="资产处置单-报废变卖值",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal premiumSale;

	
	@ApiModelProperty(notes="成本中心编号",allowEmptyValue=true,example="",allowableValues="")
	String cbCenterId;

	
	@ApiModelProperty(notes="成本中心名称",allowEmptyValue=true,example="",allowableValues="")
	String cbCenterName;

	
	@ApiModelProperty(notes="当前流程状态0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	
	@ApiModelProperty(notes="当前审批流程实例",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="当前审批流标题",allowEmptyValue=true,example="",allowableValues="")
	String flowTitle;

	
	@ApiModelProperty(notes="是否在维修0否1是",allowEmptyValue=true,example="",allowableValues="")
	String isMaint;

	
	@ApiModelProperty(notes="开始维修日期",allowEmptyValue=true,example="",allowableValues="")
	Date maintStartTime;

	
	@ApiModelProperty(notes="维修完成日期",allowEmptyValue=true,example="",allowableValues="")
	Date maintEndTime;

	/**
	 *主键id
	 **/
	public AssetCard(String cardId) {
		this.cardId = cardId;
	}
    
    /**
     * ass_asset_card
     **/
	public AssetCard() {
	}

}