package com.oa.jx.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.jx.entity.JxAssessTemplateLink;
import com.oa.jx.service.JxAssessTemplateLinkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mdp.core.utils.BaseUtils.map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/jx/jxAssessTemplateLink")
@Api(tags = {"考核模板和用户的关联表-操作接口"})
public class JxAssessTemplateLinkController {

    static Logger logger = LoggerFactory.getLogger(JxAssessTemplateLinkController.class);

    @Autowired
    private JxAssessTemplateLinkService jxAssessTemplateLinkService;

    @ApiOperation(value = "考核模板和用户的关联表-查询列表", notes = " ")
    @ApiEntityParams(JxAssessTemplateLink.class)
    @ApiResponses({
            @ApiResponse(code = 200, response = JxAssessTemplateLink.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
    })
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listJxAssessTemplateLink(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "templateIdslinkTypeslinkPkIds");
        QueryWrapper<JxAssessTemplateLink> qw = QueryTools.initQueryWrapper(JxAssessTemplateLink.class, params);

        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = jxAssessTemplateLinkService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "考核模板和用户的关联表-新增", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = JxAssessTemplateLink.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addJxAssessTemplateLink(@RequestBody JxAssessTemplateLink jxAssessTemplateLink) {
        jxAssessTemplateLinkService.save(jxAssessTemplateLink);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "考核模板和用户的关联表-删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
    })
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delJxAssessTemplateLink(@RequestBody JxAssessTemplateLink jxAssessTemplateLink) {
        jxAssessTemplateLinkService.removeById(jxAssessTemplateLink);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "考核模板和用户的关联表-修改", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = JxAssessTemplateLink.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editJxAssessTemplateLink(@RequestBody JxAssessTemplateLink jxAssessTemplateLink) {
        jxAssessTemplateLinkService.updateById(jxAssessTemplateLink);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "考核模板和用户的关联表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = JxAssessTemplateLink.class, props = {}, remark = "考核模板和用户的关联表", paramType = "body")
    @ApiResponses({
            @ApiResponse(code = 200, response = JxAssessTemplateLink.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        jxAssessTemplateLinkService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "考核模板和用户的关联表-批量删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
    })
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelJxAssessTemplateLink(@RequestBody List<JxAssessTemplateLink> jxAssessTemplateLinks) {
        User user = LoginUtils.getCurrentUserInfo();
        if (jxAssessTemplateLinks.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<JxAssessTemplateLink> datasDb = jxAssessTemplateLinkService.listByIds(jxAssessTemplateLinks.stream().map(i -> map("templateId", i.getTemplateId(), "linkType", i.getLinkType(), "linkPkId", i.getLinkPkId())).collect(Collectors.toList()));

        List<JxAssessTemplateLink> can = new ArrayList<>();
        List<JxAssessTemplateLink> no = new ArrayList<>();
        for (JxAssessTemplateLink data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            jxAssessTemplateLinkService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getTemplateId() + " " + i.getLinkType() + " " + i.getLinkPkId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "考核模板和用户的关联表-根据主键查询一条数据", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = JxAssessTemplateLink.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(JxAssessTemplateLink jxAssessTemplateLink) {
        JxAssessTemplateLink data = (JxAssessTemplateLink) jxAssessTemplateLinkService.getById(jxAssessTemplateLink);
        return Result.ok().setData(data);
    }

}
