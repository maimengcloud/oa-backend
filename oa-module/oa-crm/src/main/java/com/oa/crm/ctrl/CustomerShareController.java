package com.oa.crm.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.crm.entity.CustomerShare;
import com.oa.crm.service.CustomerShareService;
import com.oa.crm.vo.CustomerShareVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/crm/customerShare")
@Api(tags = {"crm_customer_share-操作接口"})
public class CustomerShareController {

    static Logger logger = LoggerFactory.getLogger(CustomerShareController.class);

    @Autowired
    private CustomerShareService customerShareService;

    @ApiOperation(value = "crm_customer_share-查询列表", notes = " ")
    @ApiEntityParams(CustomerShare.class)
    @ApiResponses({@ApiResponse(code = 200, response = CustomerShare.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCustomerShare(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "shareIds");
        QueryWrapper<CustomerShare> qw = QueryTools.initQueryWrapper(CustomerShare.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = customerShareService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "crm_customer_share-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerShare.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCustomerShare(@RequestBody CustomerShareVo customerShareVo) {


        if (StringUtils.isEmpty(customerShareVo.getShareId())) {
            customerShareVo.setShareId(customerShareService.createKey("shareId"));
        } else {
            CustomerShare customerShareQuery = new CustomerShare(customerShareVo.getShareId());
            if (customerShareService.countByWhere(customerShareQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        customerShareService.insertCustomerShare(customerShareVo);

        return Result.ok("add-ok", "添加成功！").setData(customerShareVo);
    }

    @ApiOperation(value = "crm_customer_share-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCustomerShare(@RequestBody CustomerShare customerShare) {
        customerShareService.removeById(customerShare);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "crm_customer_share-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerShare.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCustomerShare(@RequestBody CustomerShare customerShare) {
        customerShareService.updateById(customerShare);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "crm_customer_share-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CustomerShare.class, props = {}, remark = "crm_customer_share", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerShare.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        customerShareService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "crm_customer_share-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCustomerShare(@RequestBody List<CustomerShare> customerShares) {
        User user = LoginUtils.getCurrentUserInfo();
        if (customerShares.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CustomerShare> datasDb = customerShareService.listByIds(customerShares.stream().map(i -> i.getShareId()).collect(Collectors.toList()));

        List<CustomerShare> can = new ArrayList<>();
        List<CustomerShare> no = new ArrayList<>();
        for (CustomerShare data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            customerShareService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getShareId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "crm_customer_share-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CustomerShare.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CustomerShare customerShare) {
        CustomerShare data = (CustomerShare) customerShareService.getById(customerShare);
        return Result.ok().setData(data);
    }

}
