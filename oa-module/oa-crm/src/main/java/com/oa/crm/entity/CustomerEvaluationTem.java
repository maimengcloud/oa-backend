package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_evaluation_tem")
@ApiModel(description="crm_customer_evaluation_tem")
public class CustomerEvaluationTem  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="tem_id,主键",allowEmptyValue=true,example="",allowableValues="")
	String temId;

	
	@ApiModelProperty(notes="名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="权重",allowEmptyValue=true,example="",allowableValues="")
	Integer proportion;

	/**
	 *tem_id
	 **/
	public CustomerEvaluationTem(String temId) {
		this.temId = temId;
	}
    
    /**
     * crm_customer_evaluation_tem
     **/
	public CustomerEvaluationTem() {
	}

}