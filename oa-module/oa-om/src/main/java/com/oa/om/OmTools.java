package com.oa.om;

import com.mdp.core.entity.Result;
import com.mdp.core.err.BizException;
import com.mdp.core.utils.NumberUtil;
import com.mdp.core.utils.ObjectTools;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.om.entity.OmApplianceStorage;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OmTools {

    static  Map<String,String> forbidFields=new HashMap<>();
    static {
        forbidFields.put("applianceId","产品编号");
        forbidFields.put("num","出入库数量");
        forbidFields.put("reqNum","申请数量");
        forbidFields.put("purchaseNum","采购单数量");
        forbidFields.put("standard","物品规格");
        forbidFields.put("unit","单位");
        forbidFields.put("direct","方向");
        forbidFields.put("billType","单据类型");
    }

    /**
     * 是否为物品管理员
     * @return
     */
    public static boolean isOmAdmin(){
        return LoginUtils.hasAnyRoles("omAdmin");
    }
    public static void validOmAdminQx(){
        if(isOmAdmin()){
            return;
        }
        throw new BizException("not-om-admin","您不是办公用品管理员，无权限操作");
    }

    /**
     * 审核通过后，禁止修改某些字段
     * @param params
     * @param bizFlowState
     */
    public static void validBizFlowStateIs(Map<String,Object> params,String bizFlowState){
        String pbfs= (String) params.get("bizFlowState");
        if("2".equals(bizFlowState) && ObjectTools.isNotEmpty(pbfs) && !pbfs.equals(bizFlowState)){
            throw new BizException("bizFlowState-not-2","已审核通过的数据不允许更改审核状态");
        }
        if("2".equals(bizFlowState)){
            for (Map.Entry<String, String> ky : forbidFields.entrySet()) {
                if(params.containsKey(ky.getKey())){
                    throw new BizException("bizFlowState-2-not-allow-fields","已审核通过的数据不允许更改【%s】",ky.getValue());
                }
            }

        }
    }

    /**
     *
     * @param storages 库存流水
     * @param direct 1-入库，2-出库
     * @return
     */
    public static BigDecimal sumStock(List<OmApplianceStorage> storages,String direct){
       BigDecimal sum=BigDecimal.ZERO;
       if(storages==null || storages.size()==0){
           return sum;
       }

        for (OmApplianceStorage s : storages) {
            if(direct.equals(s.getDirect())){
                sum=sum.add(NumberUtil.getBigDecimal(s.getNum(),BigDecimal.ZERO));
            }
        }
        return sum;
    }

    public static BigDecimal sumStock(List<OmApplianceStorage> storages){
        BigDecimal sum=BigDecimal.ZERO;
        if(storages==null || storages.size()==0){
            return sum;
        }
        for (OmApplianceStorage s : storages) {
                if("1".equals(s.getDirect())){
                    sum=sum.add(NumberUtil.getBigDecimal(s.getNum(),BigDecimal.ZERO));
                }else{
                    sum=sum.subtract(NumberUtil.getBigDecimal(s.getNum(),BigDecimal.ZERO));
                }
            }
            return sum;
    }
}
