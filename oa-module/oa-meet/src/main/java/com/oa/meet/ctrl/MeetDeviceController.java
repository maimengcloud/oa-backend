package com.oa.meet.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.meet.entity.MeetDevice;
import com.oa.meet.service.MeetDeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/meet/meetDevice")
@Api(tags = {"会议设备管理-操作接口"})
public class MeetDeviceController {

    static Logger logger = LoggerFactory.getLogger(MeetDeviceController.class);

    @Autowired
    private MeetDeviceService meetDeviceService;

    @ApiOperation(value = "会议设备管理-查询列表", notes = " ")
    @ApiEntityParams(MeetDevice.class)
    @ApiResponses({@ApiResponse(code = 200, response = MeetDevice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMeetDevice(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<MeetDevice> qw = QueryTools.initQueryWrapper(MeetDevice.class, params);
        IPage page = QueryTools.initPage(params);

        List<Map<String, Object>> datas = meetDeviceService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "会议设备管理-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetDevice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMeetDevice(@RequestBody MeetDevice meetDevice) {


        if (StringUtils.isEmpty(meetDevice.getBranchId())) {
            meetDevice.setBranchId(LoginUtils.getCurrentUserInfo().getBranchId());
        }


        if (StringUtils.isEmpty(meetDevice.getId())) {
            meetDevice.setId(meetDeviceService.createKey("id"));
        } else {
            MeetDevice meetDeviceQuery = new MeetDevice(meetDevice.getId());
            if (meetDeviceService.countByWhere(meetDeviceQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        meetDeviceService.insert(meetDevice);

        return Result.ok("add-ok", "添加成功！").setData(meetDevice);
    }

    @ApiOperation(value = "会议设备管理-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMeetDevice(@RequestBody MeetDevice meetDevice) {


        meetDeviceService.deleteByPk(meetDevice);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "会议设备管理-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetDevice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMeetDevice(@RequestBody MeetDevice meetDevice) {
        meetDeviceService.updateByPk(meetDevice);
        return Result.ok("edit-ok", "修改成功！").setData(meetDevice);
    }

    @ApiOperation(value = "会议设备管理-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MeetDevice.class, props = {}, remark = "会议设备管理", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MeetDevice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        meetDeviceService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "会议设备管理-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMeetDevice(@RequestBody List<MeetDevice> meetDevices) {
        User user = LoginUtils.getCurrentUserInfo();
        if (meetDevices.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MeetDevice> datasDb = meetDeviceService.listByIds(meetDevices.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MeetDevice> can = new ArrayList<>();
        List<MeetDevice> no = new ArrayList<>();
        for (MeetDevice data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            meetDeviceService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "会议设备管理-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetDevice.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MeetDevice meetDevice) {
        MeetDevice data = (MeetDevice) meetDeviceService.getById(meetDevice);
        return Result.ok().setData(data);
    }

}
