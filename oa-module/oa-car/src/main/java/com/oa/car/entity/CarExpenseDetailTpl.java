package com.oa.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author code-gen
 * @since 2023年10月7日
 */
@Data
@TableName("car_expense_detail_tpl")
@ApiModel(description="car_expense_detail_tpl")
public class CarExpenseDetailTpl  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="费用项目编号",allowEmptyValue=true,example="",allowableValues="")
	String itemId;

	
	@ApiModelProperty(notes="费用项目",allowEmptyValue=true,example="",allowableValues="")
	String itemName;

	
	@ApiModelProperty(notes="费用",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal feeAmount;

	
	@ApiModelProperty(notes="费用说明",allowEmptyValue=true,example="",allowableValues="")
	String feeRemark;

	
	@ApiModelProperty(notes="归属机构",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *主键
	 **/
	public CarExpenseDetailTpl(String id) {
		this.id = id;
	}
    
    /**
     * car_expense_detail_tpl
     **/
	public CarExpenseDetailTpl() {
	}

}