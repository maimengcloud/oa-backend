package com.oa.jx.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("jx_assess_class")
@ApiModel(description="考核等级设置")
public class JxAssessClass  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
    @TableIds
	
    @ApiModelProperty(notes="等级类型,1-优秀，2-良好，3-中等，4-合格，5-不合格,主键",allowEmptyValue=true,example="",allowableValues="")
    String classType;
    @TableIds
	
    @ApiModelProperty(notes="机构号,主键",allowEmptyValue=true,example="",allowableValues="")
    String branchId;

	
	@ApiModelProperty(notes="等级名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="评定范围0-100之间",allowEmptyValue=true,example="",allowableValues="")
	Integer startPoints;

	
	@ApiModelProperty(notes="评定范围0-100之间",allowEmptyValue=true,example="",allowableValues="")
	Integer endPoints;

	
	@ApiModelProperty(notes="该等级在同部门中占比0-100中",allowEmptyValue=true,example="",allowableValues="")
	Integer deptRatio;

	/**
	 *等级类型,1-优秀，2-良好，3-中等，4-合格，5-不合格,机构号
	 **/
	public JxAssessClass(String classType,String branchId) {
		this.classType = classType;
		this.branchId = branchId;
	}
    
    /**
     * 考核等级设置
     **/
	public JxAssessClass() {
	}

}