package com.oa.meet.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("meet_boardroom")
@ApiModel(description="会议室")
public class MeetBoardroom  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="会议室名称",allowEmptyValue=true,example="",allowableValues="")
	String name;

	
	@ApiModelProperty(notes="会议室类型id",allowEmptyValue=true,example="",allowableValues="")
	String typeId;

	
	@ApiModelProperty(notes="会议室类型",allowEmptyValue=true,example="",allowableValues="")
	String typeName;

	
	@ApiModelProperty(notes="会议室电话",allowEmptyValue=true,example="",allowableValues="")
	String phone;

	
	@ApiModelProperty(notes="会议人数",allowEmptyValue=true,example="",allowableValues="")
	Integer number;

	
	@ApiModelProperty(notes="会议设备，多项用逗号分隔",allowEmptyValue=true,example="",allowableValues="")
	String device;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="状态1启用0禁用",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="归属机构",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *主键
	 **/
	public MeetBoardroom(String id) {
		this.id = id;
	}
    
    /**
     * 会议室
     **/
	public MeetBoardroom() {
	}

}