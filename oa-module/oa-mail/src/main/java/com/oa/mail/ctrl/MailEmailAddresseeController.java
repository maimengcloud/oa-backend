package com.oa.mail.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.mail.entity.MailEmail;
import com.oa.mail.entity.MailEmailAddressee;
import com.oa.mail.service.MailEmailAddresseeService;
import com.oa.mail.service.MailEmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/mail/mailEmailAddressee")
@Api(tags = {"邮件收件人表-操作接口"})
public class MailEmailAddresseeController {

    static Logger logger = LoggerFactory.getLogger(MailEmailAddresseeController.class);

    @Autowired
    private MailEmailAddresseeService mailEmailAddresseeService;

    @Autowired
    private MailEmailService mailEmailService;

    @ApiOperation(value = "邮件收件人表-查询列表", notes = " ")
    @ApiEntityParams(MailEmailAddressee.class)
    @ApiResponses({@ApiResponse(code = 200, response = MailEmailAddressee.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMailEmailAddressee(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<MailEmailAddressee> qw = QueryTools.initQueryWrapper(MailEmailAddressee.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = mailEmailAddresseeService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "邮件收件人表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MailEmailAddressee.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMailEmailAddressee(@RequestBody MailEmailAddressee mailEmailAddressee) {
        mailEmailAddresseeService.save(mailEmailAddressee);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "邮件收件人表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMailEmailAddressee(@RequestBody MailEmailAddressee mailEmailAddressee) {


        MailEmailAddressee result = mailEmailAddresseeService.selectOneObject(mailEmailAddressee);
        mailEmailAddresseeService.deleteByPk(mailEmailAddressee);
        if (!StringUtils.isEmpty(result)) {
            MailEmailAddressee params = new MailEmailAddressee();
            params.setEmailId(result.getEmailId());
            List<MailEmailAddressee> mailEmailAddressees = mailEmailAddresseeService.selectListByWhere(params);
            if (mailEmailAddressees.isEmpty()) {
                MailEmail mailEmail = new MailEmail();
                mailEmail.setId(result.getEmailId());
                mailEmailService.deleteByPk(mailEmail);
            }
        }


        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "根据主键修改一条mail_email_addressee信息", notes = "editMailEmailAddressee")
    @ApiResponses({@ApiResponse(code = 200, response = MailEmailAddressee.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/setStar", method = RequestMethod.POST)
    public Result setStar(@RequestBody MailEmailAddressee mailEmailAddressee) {


        if (!StringUtils.isEmpty(mailEmailAddressee.getId())) {
            mailEmailAddresseeService.updateByPk(mailEmailAddressee);
        } else {
            MailEmailAddressee queryObject = new MailEmailAddressee();
            queryObject.setEmailId(mailEmailAddressee.getEmailId());
            queryObject.setUserId(mailEmailAddressee.getUserId());
            List<MailEmailAddressee> mailEmailAddressees = mailEmailAddresseeService.selectListByWhere(queryObject);
            if (mailEmailAddressees != null && !mailEmailAddressees.isEmpty()) {
                for (MailEmailAddressee emailAddressee : mailEmailAddressees) {
                    emailAddressee.setIsStar(mailEmailAddressee.getIsStar());
                    mailEmailAddresseeService.updateByPk(emailAddressee);
                }
            }
        }

        return Result.ok().setData(mailEmailAddressee);
    }

    @ApiOperation(value = "邮件收件人表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MailEmailAddressee.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMailEmailAddressee(@RequestBody MailEmailAddressee mailEmailAddressee) {


        mailEmailAddresseeService.updateByPk(mailEmailAddressee);

        return Result.ok("edit-ok", "修改成功！").setData(mailEmailAddressee);
    }

    @ApiOperation(value = "将邮件设置为已读", notes = "setIsReadMailEmail")
    @ApiResponses({@ApiResponse(code = 200, response = MailEmailAddressee.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/setRead", method = RequestMethod.POST)
    public Result editMailEmailAddresseeIsRead(@RequestBody MailEmailAddressee mailEmailAddressee) {


        mailEmailAddresseeService.updateByPk(mailEmailAddressee);

        return Result.ok().setData(mailEmailAddressee);
    }

    @ApiOperation(value = "邮件收件人表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MailEmailAddressee.class, props = {}, remark = "邮件收件人表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MailEmailAddressee.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        mailEmailAddresseeService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "邮件收件人表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMailEmailAddressee(@RequestBody List<MailEmailAddressee> mailEmailAddressees) {
        User user = LoginUtils.getCurrentUserInfo();
        if (mailEmailAddressees.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MailEmailAddressee> datasDb = mailEmailAddresseeService.listByIds(mailEmailAddressees.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MailEmailAddressee> can = new ArrayList<>();
        List<MailEmailAddressee> no = new ArrayList<>();
        for (MailEmailAddressee data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            mailEmailAddresseeService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "邮件收件人表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MailEmailAddressee.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MailEmailAddressee mailEmailAddressee) {
        MailEmailAddressee data = (MailEmailAddressee) mailEmailAddresseeService.getById(mailEmailAddressee);
        return Result.ok().setData(data);
    }

}
