package com.oa.ht.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.ht.entity.ContractRisk;
import com.oa.ht.service.ContractRiskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/ht/contractRisk")
@Api(tags = {"ht_contract_risk-操作接口"})
public class ContractRiskController {

    static Logger logger = LoggerFactory.getLogger(ContractRiskController.class);

    @Autowired
    private ContractRiskService contractRiskService;

    @ApiOperation(value = "ht_contract_risk-查询列表", notes = " ")
    @ApiEntityParams(ContractRisk.class)
    @ApiResponses({@ApiResponse(code = 200, response = ContractRisk.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listContractRisk(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "riskIds");

        QueryWrapper<ContractRisk> qw = QueryTools.initQueryWrapper(ContractRisk.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = contractRiskService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "ht_contract_risk-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractRisk.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addContractRisk(@RequestBody ContractRisk contractRisk) {
        if (StringUtils.isEmpty(contractRisk.getRiskId())) {
            contractRisk.setRiskId(contractRiskService.createKey("riskId"));
        } else {
            ContractRisk contractRiskQuery = new ContractRisk(contractRisk.getRiskId());
            if (contractRiskService.countByWhere(contractRiskQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        User user = LoginUtils.getCurrentUserInfo();
        contractRisk.setCreateUserId(user.getUserid());
        contractRisk.setCreateUserName(user.getUsername());
        contractRisk.setCreateTime(new Date());
        contractRisk.setBranchId(user.getBranchId());
        contractRiskService.insert(contractRisk);
        return Result.ok("add-ok", "添加成功！").setData(contractRisk);
    }

    @ApiOperation(value = "ht_contract_risk-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delContractRisk(@RequestBody ContractRisk contractRisk) {
        contractRiskService.deleteByPk(contractRisk);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "ht_contract_risk-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractRisk.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editContractRisk(@RequestBody ContractRisk contractRisk) {
        // todo 数据库的表 字段 create__time
        contractRiskService.updateByPk(contractRisk);
        return Result.ok("edit-ok", "修改成功！").setData(contractRisk);
    }

    @ApiOperation(value = "ht_contract_risk-批量修改某些字段", notes = "")
    @ApiEntityParams(value = ContractRisk.class, props = {}, remark = "ht_contract_risk", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = ContractRisk.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        contractRiskService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "ht_contract_risk-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelContractRisk(@RequestBody List<ContractRisk> contractRisks) {
        User user = LoginUtils.getCurrentUserInfo();
        if (contractRisks.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<ContractRisk> datasDb = contractRiskService.listByIds(contractRisks.stream().map(i -> i.getRiskId()).collect(Collectors.toList()));

        List<ContractRisk> can = new ArrayList<>();
        List<ContractRisk> no = new ArrayList<>();
        for (ContractRisk data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            contractRiskService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getRiskId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ht_contract_risk-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractRisk.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(ContractRisk contractRisk) {
        ContractRisk data = (ContractRisk) contractRiskService.getById(contractRisk);
        return Result.ok().setData(data);
    }

}
