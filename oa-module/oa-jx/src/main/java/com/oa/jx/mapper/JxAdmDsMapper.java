package com.oa.jx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oa.jx.entity.JxAdmDs;

import java.util.List;
import java.util.Map;

public interface JxAdmDsMapper extends BaseMapper<JxAdmDs> {
    List<Map<String, Object>> selectUserByUsername(Map<String, Object> params);

    List<Map<String, Object>> selectUserByWhere(Map<String, Object> jxAssessGrade);


    List<Map<String, Object>> selectDeptByUserid(Map<String, Object> userMap);

    List<String> selectUsersByDeptid(Map dept);
}