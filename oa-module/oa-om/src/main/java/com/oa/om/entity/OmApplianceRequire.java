package  com.oa.om.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import com.mdp.core.dao.annotation.TableIds;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 唛盟开源 code-gen
 * @since 2024-8-22
 */
@Data
@TableName("om_appliance_require")
@Schema(description="办公用品申请")
public class OmApplianceRequire  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@Schema(description="申请单号,主键")
	String id;

	
	@Schema(description="用品id")
	String applianceId;

	
	@Schema(description="用品名称")
	String applianceName;

	
	@Schema(description="规格")
	String standard;

	
	@Schema(description="申请数量")
	BigDecimal reqNum;

	
	@Schema(description="出库总数量-根据出入库汇总")
	BigDecimal totalOutNum;

	
	@Schema(description="说明")
	String remark;

	
	@Schema(description="申请用户id")
	String userid;

	
	@Schema(description="申请用户")
	String username;

	
	@Schema(description="使用类型:1个人使用,2部门使用")
	String useType;

	
	@Schema(description="领取方式:1自取,2快递,3他人代领")
	String receiveType;

	
	@Schema(description="领取人")
	String receiveName;

	
	@Schema(description="领用日期")
	Date useTime;

	
	@Schema(description="申请时间")
	Date createTime;

	
	@Schema(description="状态0初始,1审批中,2已通过-待领取,3已领取待归还,4未通过,5已归还")
	String status;

	
	@Schema(description="机构号")
	String branchId;

	
	@Schema(description="部门名称")
	String deptName;

	
	@Schema(description="申领部门编号")
	String deptid;

	
	@Schema(description="当前流程状态0初始1审批中2审批通过3审批不通过4流程取消或者删除")
	String bizFlowState;

	
	@Schema(description="审批流实例编号")
	String bizProcInstId;

	
	@Schema(description="ltime")
	Date ltime;

	
	@Schema(description="归还总数量-根据入库流水计算")
	BigDecimal totalBackNum;

	
	@Schema(description="归还日期")
	Date backTime;

	
	@Schema(description="经办人")
	String backToUserid;

	
	@Schema(description="经办人姓名")
	String backToUsername;

	
	@Schema(description="领取人编号")
	String receiveUserid;

	
	@Schema(description="单位")
	String unit;

	
	@Schema(description="单据类型固定值apply_for_use")
	String billType;

	/**
	 *申请单号
	 **/
	public OmApplianceRequire(String id) {
		this.id = id;
	}
    
    /**
     * 办公用品申请
     **/
	public OmApplianceRequire() {
	}

}