package com.oa.duban.entity;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class DubanItemVo extends DubanItem {

    @ApiModelProperty(notes="协办部门",allowEmptyValue=true,example="",allowableValues="")
    List<DubanItemCoopDept> coopDept;

    @ApiModelProperty(notes = "协办",allowEmptyValue = true,example = "",allowableValues = "")
    List<DubanItemCoopUser> coopUser;

    @ApiModelProperty(notes = "督办任务列表",allowEmptyValue = true,example = "",allowableValues = "")
    List<DubanItemTaskVo> tasks;

    public List<DubanItemCoopDept> getCoopDept() {
        return coopDept;
    }

    public void setCoopDept(List<DubanItemCoopDept> coopDept) {
        this.coopDept = coopDept;
    }

    public List<DubanItemCoopUser> getCoopUser() {
        return coopUser;
    }

    public void setCoopUser(List<DubanItemCoopUser> coopUser) {
        this.coopUser = coopUser;
    }

    public List<DubanItemTaskVo> getTasks() {
        return tasks;
    }

    public void setTasks(List<DubanItemTaskVo> tasks) {
        this.tasks = tasks;
    }
}
