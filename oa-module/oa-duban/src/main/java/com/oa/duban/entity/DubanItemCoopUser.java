package com.oa.duban.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("duban_item_coop_user")
@ApiModel(description="督办协助表")
public class DubanItemCoopUser  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="督办id",allowEmptyValue=true,example="",allowableValues="")
	String dubanItemId;

	
	@ApiModelProperty(notes="协助用户id",allowEmptyValue=true,example="",allowableValues="")
	String userId;

	
	@ApiModelProperty(notes="协助用户",allowEmptyValue=true,example="",allowableValues="")
	String username;

	/**
	 *主键
	 **/
	public DubanItemCoopUser(String id) {
		this.id = id;
	}
    
    /**
     * 督办协助表
     **/
	public DubanItemCoopUser() {
	}

}