package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_receive_invoice")
@ApiModel(description="合同收款与开票关系")
public class ContractReceiveInvoice  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
    @TableIds
	
    @ApiModelProperty(notes="收款表主键,主键",allowEmptyValue=true,example="",allowableValues="")
    String receiveId;
    @TableIds
	
    @ApiModelProperty(notes="开票表主键,主键",allowEmptyValue=true,example="",allowableValues="")
    String invoiceId;

	
	@ApiModelProperty(notes="合同编号",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	/**
	 *收款表主键,开票表主键
	 **/
	public ContractReceiveInvoice(String receiveId,String invoiceId) {
		this.receiveId = receiveId;
		this.invoiceId = invoiceId;
	}
    
    /**
     * 合同收款与开票关系
     **/
	public ContractReceiveInvoice() {
	}

}