package com.oa.erp.ass.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ass_asset_detail")
@ApiModel(description="ass_asset_detail")
public class AssetDetail  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="资产编码",allowEmptyValue=true,example="",allowableValues="")
	String assetSn;

	
	@ApiModelProperty(notes="资产类别",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	
	@ApiModelProperty(notes="资产类别名称",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;

	
	@ApiModelProperty(notes="资产序列号",allowEmptyValue=true,example="",allowableValues="")
	String assetNo;

	
	@ApiModelProperty(notes="规格型号",allowEmptyValue=true,example="",allowableValues="")
	String spec;

	
	@ApiModelProperty(notes="计量单位",allowEmptyValue=true,example="",allowableValues="")
	String measUnit;

	
	@ApiModelProperty(notes="资产原值",allowEmptyValue=true,example="",allowableValues="")
	String orignUnitAmount;

	
	@ApiModelProperty(notes="使用期限",allowEmptyValue=true,example="",allowableValues="")
	String servLife;

	
	@ApiModelProperty(notes="组织机构代码",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="使用部门",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="使用部门名称",allowEmptyValue=true,example="",allowableValues="")
	String deptName;

	
	@ApiModelProperty(notes="存放地点",allowEmptyValue=true,example="",allowableValues="")
	String storeAddress;

	
	@ApiModelProperty(notes="责任人",allowEmptyValue=true,example="",allowableValues="")
	String personLiableUserid;

	
	@ApiModelProperty(notes="采购人",allowEmptyValue=true,example="",allowableValues="")
	String purchaseUserid;

	
	@ApiModelProperty(notes="采购人名称",allowEmptyValue=true,example="",allowableValues="")
	String purchaseUsername;

	
	@ApiModelProperty(notes="采购时间",allowEmptyValue=true,example="",allowableValues="")
	Date purchaseDate;

	
	@ApiModelProperty(notes="供应商编号",allowEmptyValue=true,example="",allowableValues="")
	String supplierId;

	
	@ApiModelProperty(notes="所属父资产资产编码",allowEmptyValue=true,example="",allowableValues="")
	String passetSn;

	
	@ApiModelProperty(notes="所属父资产",allowEmptyValue=true,example="",allowableValues="")
	String passetName;

	
	@ApiModelProperty(notes="保修期（月）",allowEmptyValue=true,example="",allowableValues="")
	String defectsLiabilityPeriod;

	
	@ApiModelProperty(notes="入库日期",allowEmptyValue=true,example="",allowableValues="")
	Date warehouseDate;

	
	@ApiModelProperty(notes="资产状态0闲置1在用2借用3维修4报废",allowEmptyValue=true,example="",allowableValues="")
	String assetStatus;

	
	@ApiModelProperty(notes="开始使用日期",allowEmptyValue=true,example="",allowableValues="")
	Date startUseDate;

	
	@ApiModelProperty(notes="盘点任务",allowEmptyValue=true,example="",allowableValues="")
	String inventoryTask;

	
	@ApiModelProperty(notes="配置说明",allowEmptyValue=true,example="",allowableValues="")
	String confDesc;

	
	@ApiModelProperty(notes="备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="资产图片",allowEmptyValue=true,example="",allowableValues="")
	String assetImageUrl;

	
	@ApiModelProperty(notes="创建人",allowEmptyValue=true,example="",allowableValues="")
	String createUserid;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="创建人名称",allowEmptyValue=true,example="",allowableValues="")
	String createUsername;

	
	@ApiModelProperty(notes="最后修改人",allowEmptyValue=true,example="",allowableValues="")
	String lastEditUserid;

	
	@ApiModelProperty(notes="最后修改时间",allowEmptyValue=true,example="",allowableValues="")
	Date lastEditTime;

	
	@ApiModelProperty(notes="最后修改人名称",allowEmptyValue=true,example="",allowableValues="")
	String lastEditUsername;

	
	@ApiModelProperty(notes="入库单号",allowEmptyValue=true,example="",allowableValues="")
	String receiptNo;

	
	@ApiModelProperty(notes="供应商",allowEmptyValue=true,example="",allowableValues="")
	String supplierName;

	
	@ApiModelProperty(notes="资产名称",allowEmptyValue=true,example="",allowableValues="")
	String assetName;

	
	@ApiModelProperty(notes="总数量(如果有卡片，则=card_stock_num)",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal stockNum;

	
	@ApiModelProperty(notes="仓库编号",allowEmptyValue=true,example="",allowableValues="")
	String warehouseId;

	
	@ApiModelProperty(notes="仓库名称",allowEmptyValue=true,example="",allowableValues="")
	String warehouseName;

	
	@ApiModelProperty(notes="产品编号",allowEmptyValue=true,example="",allowableValues="")
	String productId;

	
	@ApiModelProperty(notes="产品编码",allowEmptyValue=true,example="",allowableValues="")
	String productSn;

	
	@ApiModelProperty(notes="品牌编号",allowEmptyValue=true,example="",allowableValues="")
	String brandId;

	
	@ApiModelProperty(notes="品牌名称",allowEmptyValue=true,example="",allowableValues="")
	String brandName;

	
	@ApiModelProperty(notes="资产卡片的卡片数量，注意不是卡片的库存数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal cardNum;

	
	@ApiModelProperty(notes="是否创建卡片0否1是，同种资产要么都建卡要么都不建卡",allowEmptyValue=true,example="",allowableValues="")
	String openCard;

	
	@ApiModelProperty(notes="按数量拆分成多张卡片",allowEmptyValue=true,example="",allowableValues="")
	String splitNum;

	
	@ApiModelProperty(notes="卡片中库存数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal cardStockNum;

	
	@ApiModelProperty(notes="资产处置单-残值",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal residualValue;

	
	@ApiModelProperty(notes="资产处置单-净值",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal netWorth;

	
	@ApiModelProperty(notes="资产处置单-报废变卖值",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal premiumSale;

	
	@ApiModelProperty(notes="成本中心编号",allowEmptyValue=true,example="",allowableValues="")
	String cbCenterId;

	
	@ApiModelProperty(notes="成本中心名称",allowEmptyValue=true,example="",allowableValues="")
	String cbCenterName;

	/**
	 *id
	 **/
	public AssetDetail(String id) {
		this.id = id;
	}
    
    /**
     * ass_asset_detail
     **/
	public AssetDetail() {
	}

}