package com.oa.meet.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.meet.entity.MeetCategory;
import com.oa.meet.service.MeetCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value="/*/oa/meet/meetCategory")
@Api(tags = {"会议分类表-操作接口"})
public class MeetCategoryController {

    static Logger logger = LoggerFactory.getLogger(MeetCategoryController.class);

    @Autowired
    private MeetCategoryService meetCategoryService;

    @ApiOperation(value = "会议分类表-查询列表", notes = " ")
    @ApiEntityParams(MeetCategory.class)
    @ApiResponses({@ApiResponse(code = 200, response = MeetCategory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMeetCategory(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<MeetCategory> qw = QueryTools.initQueryWrapper(MeetCategory.class, params);
        IPage page = QueryTools.initPage(params);

        List<Map<String, Object>> datas = meetCategoryService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "会议分类表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetCategory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMeetCategory(@RequestBody MeetCategory meetCategory) {


        if (StringUtils.isEmpty(meetCategory.getCreateTime())) {
            meetCategory.setCreateTime(new Date());
        }
        if (StringUtils.isEmpty(meetCategory.getBranchId())) {
            meetCategory.setBranchId(LoginUtils.getCurrentUserInfo().getBranchId());
        }

        
            if (StringUtils.isEmpty(meetCategory.getId())) {
                meetCategory.setId(meetCategoryService.createKey("id"));
            } else {
                MeetCategory meetCategoryQuery = new MeetCategory(meetCategory.getId());
                if (meetCategoryService.countByWhere(meetCategoryQuery) > 0) {
                    return Result.error("编号重复，请修改编号再提交");
                }
            }
            meetCategoryService.insert(meetCategory);
        
        return Result.ok("add-ok", "添加成功！").setData(meetCategory);
    }

    @ApiOperation(value = "会议分类表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMeetCategory(@RequestBody MeetCategory meetCategory) {

        
            meetCategoryService.deleteByPk(meetCategory);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "会议分类表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetCategory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMeetCategory(@RequestBody MeetCategory meetCategory) {

        
            meetCategoryService.updateByPk(meetCategory);
        
        return Result.ok("edit-ok", "修改成功！").setData(meetCategory);
    }

    @ApiOperation(value = "会议分类表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MeetCategory.class, props = {}, remark = "会议分类表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MeetCategory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        meetCategoryService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "会议分类表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMeetCategory(@RequestBody List<MeetCategory> meetCategorys) {
        User user = LoginUtils.getCurrentUserInfo();
        if (meetCategorys.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MeetCategory> datasDb = meetCategoryService.listByIds(meetCategorys.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MeetCategory> can = new ArrayList<>();
        List<MeetCategory> no = new ArrayList<>();
        for (MeetCategory data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            meetCategoryService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "会议分类表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetCategory.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MeetCategory meetCategory) {
        MeetCategory data = (MeetCategory) meetCategoryService.getById(meetCategory);
        return Result.ok().setData(data);
    }

}
