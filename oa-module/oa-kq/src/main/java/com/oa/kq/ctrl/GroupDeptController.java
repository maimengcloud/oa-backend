package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.query.QueryTools;
import com.mdp.core.service.SequenceService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.GroupDept;
import com.oa.kq.service.GroupDeptService;
import com.oa.utils.ExcelView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Controller
@RequestMapping(value = "/*/oa/kq/groupDept")
@Api(tags = {"考勤组与部门关系表-操作接口"})
public class GroupDeptController {

    static Logger logger = LoggerFactory.getLogger(GroupDeptController.class);

    @Autowired
    private GroupDeptService groupDeptService;

    @Autowired
    private SequenceService seqService;

    /**
     * 跳转到查询页面listGroupDepart.jsp
     */
    @RequestMapping(value="/listGroupDepart")
    public ModelAndView listGroupDepart(@ModelAttribute("groupDepart") GroupDept groupDepart, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        mv.addObject("data",groupDepart);
        return mv;
    }


    /**
     * 跳转到新增页面addGroupDepart.jsp
     */
    @RequestMapping(value="/addGroupDepart")
    public ModelAndView addGroupDepart(){

        ModelAndView mv = new ModelAndView();
        GroupDept groupDepart=new GroupDept();
        mv.addObject("data",groupDepart);
        return mv;
    }

    /**
     * 跳转到修改页面editGroupDepart.jsp
     */
    @RequestMapping(value="/editGroupDepart")
    public ModelAndView editGroupDepart( @ModelAttribute("groupDepart") GroupDept groupDepart, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        groupDepart = groupDeptService.selectOneObject(groupDepart);	//根据ID读取
        mv.addObject("data",groupDepart);
        return mv;
    }


    /**
     * 请求需带json后缀,如selectListGroupDepartByPage.json
     * 根据条件查询数据对象列表
     */
    @RequestMapping(value="/selectListGroupDepartByPage")
    public ModelAndView selectListGroupDepartByPage( @ModelAttribute("groupDepart") GroupDept groupDepart, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();

        List<GroupDept>	groupDepartList = groupDeptService.selectListByWhere(groupDepart);	//列出GroupDepart列表
        mv.addObject("data",groupDepartList);
        Tips tips=new Tips("查询成功");
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 新增一条数据
     */
    @RequestMapping(value="/insertGroupDepart")
    public ModelAndView insertGroupDepart( @ModelAttribute("groupDepart") GroupDept groupDepart, BindingResult result, Model model) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功新增一条数据");
        if(StringUtils.isEmpty(groupDepart.getId())){
            groupDepart.setId(seqService.getTablePK("groupDepart", "id"));
        }
        groupDeptService.insert(groupDepart);
        mv.addObject("data",groupDepart);
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 根据主键删除1条数据
     */
    @RequestMapping(value="/deleteGroupDepartByPk")
    public ModelAndView deleteGroupDepartByPk(@ModelAttribute("groupDepart") GroupDept groupDept, BindingResult result){
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功删除一条数据");
        groupDeptService.deleteByPk(groupDept);
        mv.addObject("tips", tips);
        return mv;
    }

    /**
     * 根据主键修改一条数据
     */
    @RequestMapping(value="/updateGroupDepartByPk")
    public ModelAndView updateGroupDepartByPk( @ModelAttribute("groupDepart") GroupDept groupDepart, BindingResult result, Model model) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功更新一条数据");
        groupDeptService.updateByPk(groupDepart);
        mv.addObject("data",groupDepart);
        mv.addObject("tips", tips);
        return mv;
    }




    /**
     * 批量删除
     */
    @RequestMapping(value="/batchDeleteGroupDepart")
    public ModelAndView batchDeleteGroupDepart(@RequestParam("id[]") String[] id) {
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功删除"+id.length+"条数据");
        List<GroupDept> list=new ArrayList<GroupDept>();
        for(int i=0;i<id.length;i++){
            GroupDept groupDepart=new GroupDept();
            groupDepart.setId(id[i]);
            list.add(groupDepart);
        }
        groupDeptService.batchDelete(list);
        mv.addObject("tips", tips);
        return mv;
    }

    /*
     * 导出到excel
     * @return
     */
    @RequestMapping(value="/exportGroupDepartToExcel")
    public ModelAndView exportGroupDepartToExcel( @ModelAttribute("groupDepart") GroupDept groupDept, BindingResult result, Model model){
        ModelAndView mv = new ModelAndView();
        Tips tips=new Tips("成功导出数据");
        try{
            Map<String,Object> dataMap = new HashMap<String,Object>();
            List<String> titles = new ArrayList<String>();
            titles.add("考勤组id");
            titles.add("部门id");
            titles.add("编号id");
            dataMap.put("titles", titles);
            List<GroupDept> GroupDepartList = groupDeptService.selectListByWhere(groupDept);
            List<Map<String,Object>> allList = new ArrayList<Map<String,Object>>();
            for(int i=0;i<GroupDepartList.size();i++){
                Map<String,Object> row = new HashMap<String,Object>();
                row.put("var1", GroupDepartList.get(i).getGroupId());
                row.put("var2", GroupDepartList.get(i).getDeptid());
                row.put("var3", GroupDepartList.get(i).getId());
                allList.add(row);
            }
            dataMap.put("varList", allList);
            ExcelView erv = new ExcelView();
            mv = new ModelAndView(erv,dataMap);
        } catch(Exception e){
            logger.error(e.toString(), e);
            tips.setErrMsg("导出数据异常");
        }
        mv.addObject("tips", tips);
        return mv;
    }

    @ResponseBody
    @ApiOperation(value = "考勤组与部门关系表-查询列表", notes = " ")
    @ApiEntityParams(GroupDept.class)
    @ApiResponses({@ApiResponse(code = 200, response = GroupDept.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listGroupDept(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        QueryWrapper<GroupDept> qw = QueryTools.initQueryWrapper(GroupDept.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = groupDeptService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @ResponseBody
    @ApiOperation(value = "考勤组与部门关系表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupDept.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addGroupDept(@RequestBody GroupDept groupDept) {
        groupDeptService.save(groupDept);
        return Result.ok("add-ok", "添加成功！");
    }

    @ResponseBody
    @ApiOperation(value = "考勤组与部门关系表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delGroupDept(@RequestBody GroupDept groupDept) {
        groupDeptService.removeById(groupDept);
        return Result.ok("del-ok", "删除成功！");
    }

    @ResponseBody
    @ApiOperation(value = "考勤组与部门关系表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupDept.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editGroupDept(@RequestBody GroupDept groupDept) {
        groupDeptService.updateById(groupDept);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ResponseBody
    @ApiOperation(value = "考勤组与部门关系表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = GroupDept.class, props = {}, remark = "考勤组与部门关系表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = GroupDept.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        groupDeptService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ResponseBody
    @ApiOperation(value = "考勤组与部门关系表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelGroupDept(@RequestBody List<GroupDept> groupDepts) {
        User user = LoginUtils.getCurrentUserInfo();
        if (groupDepts.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<GroupDept> datasDb = groupDeptService.listByIds(groupDepts.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<GroupDept> can = new ArrayList<>();
        List<GroupDept> no = new ArrayList<>();
        for (GroupDept data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            groupDeptService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ResponseBody
    @ApiOperation(value = "考勤组与部门关系表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = GroupDept.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(GroupDept groupDept) {
        GroupDept data = (GroupDept) groupDeptService.getById(groupDept);
        return Result.ok().setData(data);
    }

}
