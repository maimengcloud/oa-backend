package com;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

//@SpringBootApplication
@SpringCloudApplication
@EnableRedisHttpSession
public class OaApplication  {
	
 
	public static void main(String[] args) {
		SpringApplication.run(OaApplication.class,args);

	}
}
