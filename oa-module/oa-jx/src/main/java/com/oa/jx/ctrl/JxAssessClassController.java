package com.oa.jx.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.jx.entity.JxAssessClass;
import com.oa.jx.service.JxAssessClassService;
import com.oa.jx.vo.JxAssessClassList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mdp.core.utils.BaseUtils.map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/jx/jxAssessClass")
@Api(tags = {"考核等级设置-操作接口"})
public class JxAssessClassController {

    static Logger logger = LoggerFactory.getLogger(JxAssessClassController.class);

    @Autowired
    private JxAssessClassService jxAssessClassService;

    @ApiOperation(value = "考核等级设置-查询列表", notes = " ")
    @ApiEntityParams(JxAssessClass.class)
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessClass.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listJxAssessClass(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<JxAssessClass> qw = QueryTools.initQueryWrapper(JxAssessClass.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = jxAssessClassService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }

    @RequestMapping(value = "/saveAssessClassList", method = RequestMethod.POST)
    public Result saveAssessClassList(@RequestBody JxAssessClassList jxAssessClassList) {
        jxAssessClassService.saveAssessClassList(null, null, jxAssessClassList);
        return Result.ok("", "成功保存数据").setData(jxAssessClassList);
    }

    @ApiOperation(value = "考核等级设置-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessClass.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addJxAssessClass(@RequestBody JxAssessClass jxAssessClass) {
        jxAssessClassService.save(jxAssessClass);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "考核等级设置-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delJxAssessClass(@RequestBody JxAssessClass jxAssessClass) {
        jxAssessClassService.removeById(jxAssessClass);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "考核等级设置-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessClass.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editJxAssessClass(@RequestBody JxAssessClass jxAssessClass) {
        jxAssessClassService.updateById(jxAssessClass);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "考核等级设置-批量修改某些字段", notes = "")
    @ApiEntityParams(value = JxAssessClass.class, props = {}, remark = "考核等级设置", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessClass.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        jxAssessClassService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "考核等级设置-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelJxAssessClass(@RequestBody List<JxAssessClass> jxAssessClasss) {
        User user = LoginUtils.getCurrentUserInfo();
        if (jxAssessClasss.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<JxAssessClass> datasDb = jxAssessClassService.listByIds(jxAssessClasss.stream().map(i -> map("classType", i.getClassType(), "branchId", i.getBranchId())).collect(Collectors.toList()));

        List<JxAssessClass> can = new ArrayList<>();
        List<JxAssessClass> no = new ArrayList<>();
        for (JxAssessClass data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            jxAssessClassService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getClassType() + " " + i.getBranchId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "考核等级设置-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessClass.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(JxAssessClass jxAssessClass) {
        JxAssessClass data = (JxAssessClass) jxAssessClassService.getById(jxAssessClass);
        return Result.ok().setData(data);
    }

}
