package com.oa.crm.vo;

import com.oa.crm.entity.Customer;
import com.oa.crm.entity.CustomerContacter;

import java.util.ArrayList;
import java.util.List;

public class CustomerAddVo {

    private Customer customer;

    private List<CustomerContacter> contacterList = new ArrayList<>();

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<CustomerContacter> getContacterList() {
        return contacterList;
    }

    public void setContacterList(List<CustomerContacter> contacterList) {
        this.contacterList = contacterList;
    }
}
