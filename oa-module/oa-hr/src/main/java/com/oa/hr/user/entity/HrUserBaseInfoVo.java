package com.oa.hr.user.entity;

import java.util.List;

public class HrUserBaseInfoVo extends HrUserBaseInfo{

    List<String> skillIds;

    public List<String> getSkillIds() {
        return skillIds;
    }

    public void setSkillIds(List<String> skillIds) {
        this.skillIds = skillIds;
    }
}
