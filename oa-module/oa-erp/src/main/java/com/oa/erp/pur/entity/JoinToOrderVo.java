package com.oa.erp.pur.entity;

import java.util.List;

public class JoinToOrderVo {

    String purOrderId;

    List<RequireDetail> requireDetails;

    public String getPurOrderId() {
        return purOrderId;
    }

    public void setPurOrderId(String purOrderId) {
        this.purOrderId = purOrderId;
    }

    public List<RequireDetail> getRequireDetails() {
        return requireDetails;
    }

    public void setRequireDetails(List<RequireDetail> requireDetails) {
        this.requireDetails = requireDetails;
    }


}
