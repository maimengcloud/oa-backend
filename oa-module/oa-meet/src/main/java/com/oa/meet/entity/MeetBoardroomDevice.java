package com.oa.meet.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("meet_boardroom_device")
@ApiModel(description="会议室设备")
public class MeetBoardroomDevice  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="会议室id",allowEmptyValue=true,example="",allowableValues="")
	String meetBoardroomId;

	
	@ApiModelProperty(notes="设备id",allowEmptyValue=true,example="",allowableValues="")
	String deviceId;

	
	@ApiModelProperty(notes="设备名称",allowEmptyValue=true,example="",allowableValues="")
	String deviceName;

	
	@ApiModelProperty(notes="备注说明",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	/**
	 *主键
	 **/
	public MeetBoardroomDevice(String id) {
		this.id = id;
	}
    
    /**
     * 会议室设备
     **/
	public MeetBoardroomDevice() {
	}

}