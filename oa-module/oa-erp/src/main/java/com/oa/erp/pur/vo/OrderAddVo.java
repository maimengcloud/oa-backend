package com.oa.erp.pur.vo;

import com.oa.erp.pur.entity.Order;
import com.oa.erp.pur.entity.OrderDetail;

import java.util.ArrayList;
import java.util.List;

public class OrderAddVo  {

    private Order order;

    private List<DetailLinkVo> detailLinks;

    private List<OrderDetail> orderDetailList = new ArrayList<>();

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<OrderDetail> getOrderDetailList() {
        return orderDetailList;
    }

    public void setOrderDetailList(List<OrderDetail> orderDetailList) {
        this.orderDetailList = orderDetailList;
    }

    public List<DetailLinkVo> getDetailLinks() {
        return detailLinks;
    }

    public void setDetailLinks(List<DetailLinkVo> detailLinks) {
        this.detailLinks = detailLinks;
    }
}
