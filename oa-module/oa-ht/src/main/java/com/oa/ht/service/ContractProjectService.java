package com.oa.ht.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.ht.entity.ContractProject;
import com.oa.ht.entity.ContractProjectBatchVo;
import com.oa.ht.mapper.ContractProjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class ContractProjectService extends BaseService<ContractProjectMapper,ContractProject> {
	static Logger logger =LoggerFactory.getLogger(ContractProjectService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}


	@Autowired
	ContractLogService contractLogService;

	public List<ContractProject> selectProjectByIds(Map<String, Object> query) {
		return baseMapper.selectProjectByIds(query);
	}


	/**
	 * 批量新增、修改、删除一体化
	 */
	public void batchSave(ContractProjectBatchVo contractProjectsVo) {
		User user= LoginUtils.getCurrentUserInfo();
		String htId=contractProjectsVo.getHtId();
		ContractProject contractProject=new ContractProject();
		contractProject.setHtId(htId);
		if(contractProjectsVo.getContractProjects()==null || contractProjectsVo.getContractProjects().size()==0){
			this.deleteByWhere(contractProject);
			contractLogService.addLog(htId,user.getUserid(),user.getUsername(),"清空所有项目");
			return;
		}
		List<ContractProject> contractProjectsDb=this.selectListByWhere(contractProject);

		if(contractProjectsDb==null || contractProjectsDb.size()==0){
			for (ContractProject condition : contractProjectsVo.getContractProjects()) {
				condition.setId(this.createKey("id"));
				condition.setHtId(htId);
			}
			this.batchInsert(contractProjectsVo.getContractProjects());
			contractLogService.addLog(htId,user.getUserid(),user.getUsername(),"新增"+contractProjectsVo.getContractProjects().size()+"条项目");

			return;
		}


		Map<String,ContractProject> dbMap=new HashMap<>();
		Map<String,ContractProject> voMap=new HashMap<>();
		List<ContractProject> addList=new ArrayList<>();
		List<ContractProject> updateList=new ArrayList<>();
		List<ContractProject> delList=new ArrayList<>();

		for (ContractProject condition : contractProjectsDb) {
			dbMap.put(condition.getId(),condition);
		}

		for (ContractProject condition : contractProjectsVo.getContractProjects()) {
			voMap.put(condition.getId(),condition);
		}


		for (ContractProject condition : contractProjectsDb) {
			if(voMap.containsKey(condition.getId())){
				updateList.add(voMap.get(condition.getId()));
			}else{
				delList.add(condition);
			}
		}

		for (ContractProject condition : contractProjectsVo.getContractProjects()) {
			if(StringUtils.hasText(condition.getId())){
				if(!dbMap.containsKey(condition.getId())){

					condition.setHtId(htId);
					addList.add(condition);
				}
			}else{
				condition.setId(this.createKey("id"));
				condition.setHtId(htId);
				addList.add(condition);
			}

		}

		if(delList.size()>0){
			this.batchDelete(delList);
			contractLogService.addLog(htId,user.getUserid(),user.getUsername(),"删除"+delList.size()+"条项目");

		}

		if(addList.size()>0){
			this.batchInsert(addList);
			contractLogService.addLog(htId,user.getUserid(),user.getUsername(),"新增"+addList.size()+"条项目");

		}

		if(updateList.size()>0){
			this.batchUpdate(updateList);
			contractLogService.addLog(htId,user.getUserid(),user.getUsername(),"修改"+updateList.size()+"条项目");

		}

	}

}

