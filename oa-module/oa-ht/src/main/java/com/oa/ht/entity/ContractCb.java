package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_cb")
@ApiModel(description="ht_contract_cb")
public class ContractCb  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="合同销售成本id,主键",allowEmptyValue=true,example="",allowableValues="")
	String cbId;

	
	@ApiModelProperty(notes="合同id",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	
	@ApiModelProperty(notes="成本类型",allowEmptyValue=true,example="",allowableValues="")
	String cbType;

	
	@ApiModelProperty(notes="成本人天估算",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal cbMandayAmount;

	
	@ApiModelProperty(notes="实际人天",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal cbManday;

	
	@ApiModelProperty(notes="成本总计",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal cbGrossAmount;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *合同销售成本id
	 **/
	public ContractCb(String cbId) {
		this.cbId = cbId;
	}
    
    /**
     * ht_contract_cb
     **/
	public ContractCb() {
	}

}