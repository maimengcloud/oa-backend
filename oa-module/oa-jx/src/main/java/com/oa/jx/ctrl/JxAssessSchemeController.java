package com.oa.jx.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.jx.entity.JxAssessScheme;
import com.oa.jx.entity.JxAssessSchemeExec;
import com.oa.jx.service.JxAssessSchemeExecService;
import com.oa.jx.service.JxAssessSchemeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/jx/jxAssessScheme")
@Api(tags = {"考核方案-操作接口"})
public class JxAssessSchemeController {

    static Logger logger = LoggerFactory.getLogger(JxAssessSchemeController.class);

    @Autowired
    private JxAssessSchemeService jxAssessSchemeService;
    @Autowired
    private JxAssessSchemeExecService jxAssessSchemeExecService;

    @ApiOperation(value = "考核方案-查询列表", notes = " ")
    @ApiEntityParams(JxAssessScheme.class)
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessScheme.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listJxAssessScheme(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        params.put("branchId", user.getBranchId());
        QueryWrapper<JxAssessScheme> qw = QueryTools.initQueryWrapper(JxAssessScheme.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = jxAssessSchemeService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "考核方案-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessScheme.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addJxAssessScheme(@RequestBody JxAssessScheme jxAssessScheme) {
        User cuser = LoginUtils.getCurrentUserInfo();
        if (StringUtils.isEmpty(jxAssessScheme.getId())) {
            jxAssessScheme.setId(jxAssessSchemeService.createKey("id"));
        } else {
            JxAssessScheme jxAssessSchemeQuery = new JxAssessScheme(jxAssessScheme.getId());
            if (jxAssessSchemeService.countByWhere(jxAssessSchemeQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        if (StringUtils.isEmpty(jxAssessScheme.getTemplateId())) {
            return Result.error("templateId-is-null", "模板编号不能为空");
        }
        if (StringUtils.isEmpty(jxAssessScheme.getTemplateName())) {
            return Result.error("templateName-is-null", "模板名称不能为空");
        }

        if (StringUtils.isEmpty(jxAssessScheme.getName())) {
            return Result.error("name-is-null", "方案名称不能为空");
        }
        jxAssessScheme.setBranchId(cuser.getBranchId());
        jxAssessSchemeService.insert(jxAssessScheme);
        return Result.ok("add-ok", "添加成功！").setData(jxAssessScheme);
    }

    @ApiOperation(value = "考核方案-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delJxAssessScheme(@RequestBody JxAssessScheme jxAssessScheme) {
        if (!StringUtils.hasText(jxAssessScheme.getId())) {
            return Result.error("id-is-null", "id不能为空");
        }
        JxAssessSchemeExec exec = new JxAssessSchemeExec();
        exec.setSchemeId(jxAssessScheme.getId());
        if (this.jxAssessSchemeExecService.countByWhere(exec) > 0) {
            return Result.error("exec-exists", "该方案已被使用，不能删除");
        }
        ;
        JxAssessScheme jxAssessSchemeDb = this.jxAssessSchemeService.selectOneObject(jxAssessScheme);
        if (jxAssessSchemeDb == null) {
            return Result.error("data-is-null", "数据不存在");
        }
        jxAssessSchemeService.deleteByPk(jxAssessScheme);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "考核方案-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessScheme.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editJxAssessScheme(@RequestBody JxAssessScheme jxAssessScheme) {
        if (!StringUtils.hasText(jxAssessScheme.getId())) {
            return Result.error("id-is-null", "id不能为空");
        }
        if (StringUtils.isEmpty(jxAssessScheme.getTemplateId())) {
            return Result.error("templateId-is-null", "模板编号不能为空");
        }
        if (StringUtils.isEmpty(jxAssessScheme.getTemplateName())) {
            return Result.error("templateName-is-null", "模板名称不能为空");
        }

        if (StringUtils.isEmpty(jxAssessScheme.getName())) {
            return Result.error("name-is-null", "方案名称不能为空");
        }
        JxAssessScheme jxAssessSchemeDb = this.jxAssessSchemeService.selectOneObject(jxAssessScheme);
        if (jxAssessSchemeDb == null) {
            return Result.error("data-is-null", "数据不存在");
        }
        jxAssessSchemeService.updateByPk(jxAssessScheme);

        return Result.ok("edit-ok", "修改成功！").setData(jxAssessScheme);
    }

    @ApiOperation(value = "根据主键复制一条jx_assess_scheme信息", notes = "editJxAssessScheme")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessScheme.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/copy", method = RequestMethod.POST)
    public Result copyJxAssessScheme(@RequestBody JxAssessScheme jxAssessScheme) {
        if (!StringUtils.hasText(jxAssessScheme.getId())) {
            return Result.error("id-is-null", "id不能为空");
        }
        JxAssessScheme jxAssessSchemeDb = this.jxAssessSchemeService.selectOneObject(jxAssessScheme);
        if (jxAssessSchemeDb == null) {
            return Result.error("data-is-null", "数据不存在");
        }
        jxAssessSchemeService.copyJxAssessScheme(jxAssessSchemeDb);

        return Result.ok().setData(jxAssessScheme);
    }

    @ApiOperation(value = "考核方案-批量修改某些字段", notes = "")
    @ApiEntityParams(value = JxAssessScheme.class, props = {}, remark = "考核方案", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessScheme.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        jxAssessSchemeService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "考核方案-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelJxAssessScheme(@RequestBody List<JxAssessScheme> jxAssessSchemes) {
        User user = LoginUtils.getCurrentUserInfo();
        if (jxAssessSchemes.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<JxAssessScheme> datasDb = jxAssessSchemeService.listByIds(jxAssessSchemes.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<JxAssessScheme> can = new ArrayList<>();
        List<JxAssessScheme> no = new ArrayList<>();
        for (JxAssessScheme data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            jxAssessSchemeService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "考核方案-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = JxAssessScheme.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(JxAssessScheme jxAssessScheme) {
        JxAssessScheme data = (JxAssessScheme) jxAssessSchemeService.getById(jxAssessScheme);
        return Result.ok().setData(data);
    }

}
