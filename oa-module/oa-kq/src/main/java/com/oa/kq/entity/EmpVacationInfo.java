package com.oa.kq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("kq_emp_vacation_info")
@ApiModel(description="员工请假信息表")
public class EmpVacationInfo  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="ID,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="员工id",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="员工名称",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="部门id",allowEmptyValue=true,example="",allowableValues="")
	String deptid;

	
	@ApiModelProperty(notes="部门名称",allowEmptyValue=true,example="",allowableValues="")
	String deptName;

	
	@ApiModelProperty(notes="请假类别",allowEmptyValue=true,example="",allowableValues="")
	String vacationId;

	
	@ApiModelProperty(notes="开始日期",allowEmptyValue=true,example="",allowableValues="")
	Date beginDate;

	
	@ApiModelProperty(notes="结束日期",allowEmptyValue=true,example="",allowableValues="")
	Date endDate;

	
	@ApiModelProperty(notes="请假事由",allowEmptyValue=true,example="",allowableValues="")
	String reason;

	
	@ApiModelProperty(notes="组织id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="开始时间作废",allowEmptyValue=true,example="",allowableValues="")
	String beginTime;

	
	@ApiModelProperty(notes="结束时间作废",allowEmptyValue=true,example="",allowableValues="")
	String endTime;

	
	@ApiModelProperty(notes="紧急程度",allowEmptyValue=true,example="",allowableValues="")
	String emergencyDegree;

	
	@ApiModelProperty(notes="职责描述",allowEmptyValue=true,example="",allowableValues="")
	String dutyDescription;

	
	@ApiModelProperty(notes="请假小时数",allowEmptyValue=true,example="",allowableValues="")
	String vacationHours;

	
	@ApiModelProperty(notes="标题",allowEmptyValue=true,example="",allowableValues="")
	String title;

	
	@ApiModelProperty(notes="附件",allowEmptyValue=true,example="",allowableValues="")
	String accessory;

	
	@ApiModelProperty(notes="当前流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="当前流程状态0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	
	@ApiModelProperty(notes="假期类型",allowEmptyValue=true,example="",allowableValues="")
	String vacationType;

	
	@ApiModelProperty(notes="假期名称",allowEmptyValue=true,example="",allowableValues="")
	String vacationName;

	/**
	 *ID
	 **/
	public EmpVacationInfo(String id) {
		this.id = id;
	}
    
    /**
     * 员工请假信息表
     **/
	public EmpVacationInfo() {
	}

}