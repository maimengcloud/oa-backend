package com.oa.mail.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.mail.entity.MailAccount;
import com.oa.mail.service.MailAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/mail/mailAccount")
@Api(tags = {"邮件附件表-操作接口"})
public class MailAccountController {

    static Logger logger = LoggerFactory.getLogger(MailAccountController.class);

    @Autowired
    private MailAccountService mailAccountService;


    @ApiOperation(value = "查询mail_account下新的邮件信息", notes = "checkNewEmail,条件之间是 and关系,模糊查询写法如 {studentName:'%才哥%'}")
    @RequestMapping(value = "/checkNewEmail", method = RequestMethod.GET)
    public Result checkMailAccountNewEmail(MailAccount mailAccount) {
        List<Map<String, Object>> mailAccountList = mailAccountService.checkMailAccountNewEmail(mailAccount);

        return Result.ok().setData(mailAccountList);
    }

    @ApiOperation(value = "邮件附件表-查询列表", notes = " ")
    @ApiEntityParams(MailAccount.class)
    @ApiResponses({@ApiResponse(code = 200, response = MailAccount.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMailAccount(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<MailAccount> qw = QueryTools.initQueryWrapper(MailAccount.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = mailAccountService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "邮件附件表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MailAccount.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMailAccount(@RequestBody MailAccount mailAccount) {


        if (StringUtils.isEmpty(mailAccount.getId())) {
            mailAccount.setId(mailAccountService.createKey("id"));
        } else {
            MailAccount mailAccountQuery = new MailAccount(mailAccount.getId());
            if (mailAccountService.countByWhere(mailAccountQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        mailAccountService.insert(mailAccount);

        return Result.ok("add-ok", "添加成功！").setData(mailAccount);
    }

    @ApiOperation(value = "测试mail_account信息", notes = "testMailAccount")
    @ApiResponses({@ApiResponse(code = 200, response = MailAccount.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public Result testMailAccount(@RequestBody MailAccount mailAccount) {


        mailAccountService.testAccount(mailAccount);

        return Result.ok().setData(mailAccount);
    }

    @ApiOperation(value = "邮件附件表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMailAccount(@RequestBody MailAccount mailAccount) {


        mailAccountService.deleteByPk(mailAccount);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "邮件附件表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MailAccount.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMailAccount(@RequestBody MailAccount mailAccount) {


        mailAccountService.updateByPk(mailAccount);

        return Result.ok("edit-ok", "修改成功！").setData(mailAccount);
    }

    @ApiOperation(value = "邮件附件表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MailAccount.class, props = {}, remark = "邮件附件表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MailAccount.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        mailAccountService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "邮件附件表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMailAccount(@RequestBody List<MailAccount> mailAccounts) {
        User user = LoginUtils.getCurrentUserInfo();
        if (mailAccounts.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MailAccount> datasDb = mailAccountService.listByIds(mailAccounts.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MailAccount> can = new ArrayList<>();
        List<MailAccount> no = new ArrayList<>();
        for (MailAccount data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            mailAccountService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "邮件附件表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MailAccount.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MailAccount mailAccount) {
        MailAccount data = (MailAccount) mailAccountService.getById(mailAccount);
        return Result.ok().setData(data);
    }

}
