package com.oa.car.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.car.entity.CarRouteDetailTpl;
import com.oa.car.entity.CarRouteTpl;
import com.oa.car.entity.CarRouteTplVo;
import com.oa.car.mapper.CarRouteTplMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@Service
public class CarRouteTplService extends BaseService<CarRouteTplMapper, CarRouteTpl> {
    static Logger logger = LoggerFactory.getLogger(CarRouteTplService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }


    @Autowired
    CarRouteDetailTplService carRouteDetailTplService;

    @Transactional
    public void save(CarRouteTplVo carRouteTplVo) {

        Tips tips = new Tips("成功新增一条数据");

        CarRouteTpl carRouteTpl = new CarRouteTpl();
        BeanUtils.copyProperties(carRouteTplVo, carRouteTpl);


        if (!carRouteTpl.getIsSignIn().equals("0") && !carRouteTpl.getIsSignIn().equals("1")) {
            log.info("是否打卡输入有误，请重新输入");
            throw new BizException(tips);
        }

        if (!carRouteTpl.getHasDetail().equals("0") && !carRouteTpl.getHasDetail().equals("1")) {
            log.info("是否有子路线输入有误，请重新输入");
            throw new BizException(tips);
        }

        if (StringUtils.isEmpty(carRouteTpl.getBranchId())) {
            //获取登录的机构号
            String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
            carRouteTpl.setBranchId(branchId);
        }

        if (StringUtils.isEmpty(carRouteTpl.getId())) {
            carRouteTpl.setId(this.createKey("id"));
        } else {
            CarRouteTpl carRouteTplQuery = new CarRouteTpl(carRouteTplVo.getId());
            if (this.countByWhere(carRouteTplQuery) > 0) {
                log.info("编号重复，请修改编号再提交");
                throw new BizException(tips);
            }
        }
        this.insert(carRouteTpl);
        saveOtherMessage(carRouteTplVo, carRouteTpl);

    }

    /**
     * 保存子路线信息
     *
     * @param carRouteTplVo
     * @param carRouteTpl
     */
    private void saveOtherMessage(CarRouteTplVo carRouteTplVo, CarRouteTpl carRouteTpl) {
        //保存子路线的信息
        List<CarRouteDetailTpl> carRouteDetailTplsNew = carRouteTplVo.getCarRouteDetailTplsNew();
        if (carRouteDetailTplsNew != null && carRouteDetailTplsNew.size() > 0) {
            carRouteDetailTplsNew.forEach((carRequireRouteDetail) -> {
                carRequireRouteDetail.setRouteId(carRouteTpl.getId());
                String isSignIn = carRequireRouteDetail.getIsSignIn();
                if (isSignIn.equals("true")) {
                    carRequireRouteDetail.setIsSignIn("1");
                } else {
                    carRequireRouteDetail.setIsSignIn("0");
                }
                carRouteDetailTplService.save(carRequireRouteDetail);
            });
        }
    }


    @Transactional
    public Map<String, Object> updateMessage(CarRouteTplVo carRouteTplVo) {

        Map<String, Object> m = new HashMap<>();
        Tips tips = new Tips("成功更新一条数据");

        CarRouteTpl carRouteTpl = new CarRouteTpl();
        BeanUtils.copyProperties(carRouteTplVo, carRouteTpl);

        try {

            if (StringUtils.isEmpty(carRouteTpl.getId())) {
                log.info("主键不能为空");
                throw new BizException(tips);
            }
            if (!carRouteTpl.getIsSignIn().equals("0") && !carRouteTpl.getIsSignIn().equals("1")) {
                log.info("是否打卡输入有误，请重新输入");
                throw new BizException(tips);
            }

            if (!carRouteTpl.getHasDetail().equals("0") && !carRouteTpl.getHasDetail().equals("1")) {
                log.info("是否有子路线输入有误，请重新输入");
                throw new BizException(tips);
            }
            //更新父路线信息
            this.updateByPk(carRouteTpl);

            //删除子路线信息
            CarRouteDetailTpl carRouteDetailTpl = new CarRouteDetailTpl();
            carRouteDetailTpl.setRouteId(carRouteTpl.getId());
            List<CarRouteDetailTpl> carRouteDetailTpls = carRouteDetailTplService.selectListByWhere(carRouteDetailTpl);
            if (carRouteDetailTpls != null && carRouteDetailTpls.size() > 0) {
                carRouteDetailTpls.forEach((carRouteDetail) -> {
                    carRouteDetailTplService.deleteByPk(carRouteDetail);
                });
            }

            if (carRouteTpl.getHasDetail().equals("1"))
                //保存子路线信息
                saveOtherMessage(carRouteTplVo, carRouteTpl);

            m.put("data", carRouteTplVo);
        } catch (BizException e) {
            tips = e.getTips();
            throw new BizException(tips);
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new BizException(tips);
        }
        m.put("tips", tips);
        return m;

    }

    @Transactional
    public void deleteMessage(CarRouteTpl carRouteTpl) {
        Map<String, Object> m = new HashMap<>();
        Tips tips = new Tips("成功删除一条数据");

        try {
            this.deleteByPk(carRouteTpl);

            //删除子路线信息
            CarRouteDetailTpl carRouteDetailTpl = new CarRouteDetailTpl();
            carRouteDetailTpl.setRouteId(carRouteTpl.getId());
            List<CarRouteDetailTpl> carRouteDetailTpls = carRouteDetailTplService.selectListByWhere(carRouteDetailTpl);
            if (carRouteDetailTpls != null && carRouteDetailTpls.size() > 0) {
                carRouteDetailTpls.forEach((carRouteDetail) -> {
                    carRouteDetailTplService.deleteByPk(carRouteDetail);
                });
            }
        } catch (BizException e) {
            tips = e.getTips();
            throw new BizException(tips);
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new BizException(tips);
        }
    }

    @Transactional
    public void batchDeleteMessgae(List<CarRouteTpl> carRouteTpls) {
        Tips tips = new Tips("成功删除" + carRouteTpls.size() + "条数据");

        try {
            if (carRouteTpls != null && carRouteTpls.size() > 0) {
                carRouteTpls.forEach((carRouteTpl) -> {
                    deleteMessage(carRouteTpl);
                });
            }
        } catch (BizException e) {
            tips = e.getTips();
            throw new BizException(tips);
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new BizException(tips);
        }
    }
}

