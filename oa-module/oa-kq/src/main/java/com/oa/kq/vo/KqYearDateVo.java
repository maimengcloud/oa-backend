package com.oa.kq.vo;

import java.util.List;
import java.util.Map;

public class KqYearDateVo {
    private String year;
    private List<Map<String,Object>> holidays;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<Map<String, Object>> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<Map<String, Object>> holidays) {
        this.holidays = holidays;
    }
}
