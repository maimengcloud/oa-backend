package com.oa.car.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.car.entity.CarOfficeCar;
import com.oa.car.service.CarOfficeCarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@RestController
@RequestMapping(value = "/*/oa/car/carOfficeCar")
@Api(tags = {"公务车登记-操作接口"})
public class CarOfficeCarController {

    static Logger logger = LoggerFactory.getLogger(CarOfficeCarController.class);

    @Autowired
    private CarOfficeCarService carOfficeCarService;

    @ApiOperation(value = "公务车登记-查询列表", notes = " ")
    @ApiEntityParams(CarOfficeCar.class)
    @ApiResponses({@ApiResponse(code = 200, response = CarOfficeCar.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCarOfficeCar(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        //获取登录的机构号
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        params.put("branchId", branchId);

        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CarOfficeCar> qw = QueryTools.initQueryWrapper(CarOfficeCar.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = carOfficeCarService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "公务车登记-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarOfficeCar.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCarOfficeCar(@RequestBody CarOfficeCar carOfficeCar) {
        
        if (StringUtils.isEmpty(carOfficeCar.getCarBranchId())) {
            User user = LoginUtils.getCurrentUserInfo();
            carOfficeCar.setCarBranchId(user.getBranchId());
            carOfficeCar.setCarBranchName(user.getBranchName());
        }

        if (StringUtils.isEmpty(carOfficeCar.getCreateTime())) {
            carOfficeCar.setCreateTime(new Date());
        }


        if (StringUtils.isEmpty(carOfficeCar.getId())) {
            carOfficeCar.setId(carOfficeCarService.createKey("id"));
        } else {
            CarOfficeCar carOfficeCarQuery = new CarOfficeCar(carOfficeCar.getId());
            if (carOfficeCarService.countByWhere(carOfficeCarQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        carOfficeCarService.save(carOfficeCar);


        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "公务车登记-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCarOfficeCar(@RequestBody CarOfficeCar carOfficeCar) {
        
        if (StringUtils.isEmpty(carOfficeCar.getId())) {
            return Result.error("主键不能为空");
        }


        carOfficeCarService.removeById(carOfficeCar);


        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "公务车登记-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarOfficeCar.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCarOfficeCar(@RequestBody CarOfficeCar carOfficeCar) {
        
        if (StringUtils.isEmpty(carOfficeCar.getId())) {
            return Result.error("主键不能为空");
        }


        if (StringUtils.isEmpty(carOfficeCar.getCarBranchId())) {
            User user = LoginUtils.getCurrentUserInfo();
            carOfficeCar.setCarBranchId(user.getBranchId());
            carOfficeCar.setCarBranchName(user.getBranchName());
        }


        carOfficeCarService.updateById(carOfficeCar);

        return Result.ok("edit-ok", "修改成功！").setData(carOfficeCar);
    }

    @ApiOperation(value = "公务车登记-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CarOfficeCar.class, props = {}, remark = "公务车登记", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CarOfficeCar.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        carOfficeCarService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "公务车登记-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCarOfficeCar(@RequestBody List<CarOfficeCar> carOfficeCars) {
        User user = LoginUtils.getCurrentUserInfo();
        if (carOfficeCars.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CarOfficeCar> datasDb = carOfficeCarService.listByIds(carOfficeCars.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CarOfficeCar> can = new ArrayList<>();
        List<CarOfficeCar> no = new ArrayList<>();
        for (CarOfficeCar data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            carOfficeCarService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "公务车登记-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarOfficeCar.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CarOfficeCar carOfficeCar) {
        CarOfficeCar data = (CarOfficeCar) carOfficeCarService.getById(carOfficeCar);
        return Result.ok().setData(data);
    }

}
