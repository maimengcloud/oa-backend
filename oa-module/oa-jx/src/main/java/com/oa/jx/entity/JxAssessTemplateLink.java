package com.oa.jx.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("jx_assess_template_link")
@ApiModel(description="考核模板和用户的关联表")
public class JxAssessTemplateLink  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
    @TableIds
	
    @ApiModelProperty(notes="考核模板id,主键",allowEmptyValue=true,example="",allowableValues="")
    String templateId;
    @TableIds
	
    @ApiModelProperty(notes="用户类型0-用户,1-部门，2-机构，3-项目，4-岗位,主键",allowEmptyValue=true,example="",allowableValues="")
    String linkType;
    @TableIds
	
    @ApiModelProperty(notes="当用户类型为0时存放用户编号，当用户类型为部门时，存部门编号，依次类推,主键",allowEmptyValue=true,example="",allowableValues="")
    String linkPkId;

	
	@ApiModelProperty(notes="组织id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="关联名称",allowEmptyValue=true,example="",allowableValues="")
	String linkPkName;

	
	@ApiModelProperty(notes="创建时间",allowEmptyValue=true,example="",allowableValues="")
	Date ctime;

	/**
	 *考核模板id,用户类型0-用户,1-部门，2-机构，3-项目，4-岗位,当用户类型为0时存放用户编号，当用户类型为部门时，存部门编号，依次类推
	 **/
	public JxAssessTemplateLink(String templateId,String linkType,String linkPkId) {
		this.templateId = templateId;
		this.linkType = linkType;
		this.linkPkId = linkPkId;
	}
    
    /**
     * 考核模板和用户的关联表
     **/
	public JxAssessTemplateLink() {
	}

}