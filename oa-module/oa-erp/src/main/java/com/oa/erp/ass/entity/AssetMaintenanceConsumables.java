package com.oa.erp.ass.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ass_asset_maintenance_consumables")
@ApiModel(description="资产维修/保养单配件耗材表")
public class AssetMaintenanceConsumables  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键（采购申请单）,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="维修单编号",allowEmptyValue=true,example="",allowableValues="")
	String requireId;

	
	@ApiModelProperty(notes="配件/耗材名字",allowEmptyValue=true,example="",allowableValues="")
	String consuName;

	
	@ApiModelProperty(notes="规格型号",allowEmptyValue=true,example="",allowableValues="")
	String consuSpec;

	
	@ApiModelProperty(notes="数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal consuNum;

	
	@ApiModelProperty(notes="单价",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal unitPrice;

	
	@ApiModelProperty(notes="说明",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	/**
	 *主键（采购申请单）
	 **/
	public AssetMaintenanceConsumables(String id) {
		this.id = id;
	}
    
    /**
     * 资产维修/保养单配件耗材表
     **/
	public AssetMaintenanceConsumables() {
	}

}