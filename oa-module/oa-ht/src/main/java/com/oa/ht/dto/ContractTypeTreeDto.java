package com.oa.ht.dto;

import com.google.common.collect.Lists;
import com.oa.ht.entity.ContractType;
import org.springframework.beans.BeanUtils;

import java.util.List;

public class ContractTypeTreeDto extends ContractType {

    private List<ContractTypeTreeDto> children =  Lists.newArrayList();

    public static ContractTypeTreeDto adapt(ContractType contractType) {
        ContractTypeTreeDto contractTypeTreeDto = new ContractTypeTreeDto();
        BeanUtils.copyProperties(contractType, contractTypeTreeDto);
        return contractTypeTreeDto;
    }

    public List<ContractTypeTreeDto> getChildren() {
        return children;
    }

    public void setChildren(List<ContractTypeTreeDto> children) {
        this.children = children;
    }
}

