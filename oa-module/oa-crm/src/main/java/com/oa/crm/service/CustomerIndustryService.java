package com.oa.crm.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.mdp.core.service.BaseService;
import com.oa.crm.dto.CustomerIndustryTreeDto;
import com.oa.crm.entity.CustomerIndustry;
import com.oa.crm.mapper.CustomerIndustryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class CustomerIndustryService extends BaseService<CustomerIndustryMapper,CustomerIndustry> {
	static Logger logger =LoggerFactory.getLogger(CustomerIndustryService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}

	public final static String ROOT = "0";

	/**
	 * 查询客户行业树
	 * @return
	 */
	public List<CustomerIndustryTreeDto> selectCustomerIndustryTree() {
		List<CustomerIndustry> categoryTreeDtos = this.selectListByWhere(null);
		//转换Tree
		List<CustomerIndustryTreeDto> customerIndustryTreeDtos = new ArrayList<>();

		for(CustomerIndustry customerIndustry : categoryTreeDtos) {
			CustomerIndustryTreeDto customerIndustryTreeDto = CustomerIndustryTreeDto.adapt(customerIndustry);
			customerIndustryTreeDtos.add(customerIndustryTreeDto);
		}

		return CustomerIndustryListToTree(customerIndustryTreeDtos);
	}

	private List<CustomerIndustryTreeDto> CustomerIndustryListToTree(List<CustomerIndustryTreeDto> customerIndustryTreeDtos) {
		if (CollectionUtils.isEmpty(customerIndustryTreeDtos)) {
			return Lists.newArrayList();
		}
		Multimap<String, CustomerIndustryTreeDto> customerIndustryTreeDtoMultimap = ArrayListMultimap.create();
		List<CustomerIndustryTreeDto> rootList = Lists.newArrayList();
		for(CustomerIndustryTreeDto dto : customerIndustryTreeDtos) {
			customerIndustryTreeDtoMultimap.put(dto.getLevel(), dto);
			if(ROOT.equals(dto.getLevel())) {
				rootList.add(dto);
			}
		}
		//递归生成tree
		transformCategoryTree(rootList, ROOT, customerIndustryTreeDtoMultimap);
		return rootList;
	}

	private void transformCategoryTree(List<CustomerIndustryTreeDto> rootList, String level, Multimap<String, CustomerIndustryTreeDto> customerIndustryTreeDtoMultimap) {
		for (int i = 0; i < rootList.size(); i++) {
			//1.遍历每个元素
			CustomerIndustryTreeDto customerIndustryTreeDto = rootList.get(i);
			//2.处理数据，计算下一层数据
			String nextLevel = calculateLevel(level, customerIndustryTreeDto.getId());
			//3. 处理下一层
			List<CustomerIndustryTreeDto> tempList = (List<CustomerIndustryTreeDto>) customerIndustryTreeDtoMultimap.get(nextLevel);
			if (!CollectionUtils.isEmpty(tempList)) {
				// 设置下一层部门
				customerIndustryTreeDto.setChildren(tempList);
				// 递归，进入到下一层处理
				transformCategoryTree(tempList, nextLevel, customerIndustryTreeDtoMultimap);
			}
		}
	}

	public static String calculateLevel(String parentLevel, String parentId) {
		if (StringUtils.isEmpty(parentLevel)) {
			return ROOT;
		} else {
			String[] s = new String[]{parentLevel,parentId};
			return String.join(",", s);
		}
	}

	/** 请在此类添加自定义函数 */
}

