package com.oa.car.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.car.entity.CarExpenseDetailTpl;
import com.oa.car.service.CarExpenseDetailTplService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@RestController
@RequestMapping(value = "/*/oa/car/carExpenseDetailTpl")
@Api(tags = {"car_expense_detail_tpl-操作接口"})
public class CarExpenseDetailTplController {

    static Logger logger = LoggerFactory.getLogger(CarExpenseDetailTplController.class);

    @Autowired
    private CarExpenseDetailTplService carExpenseDetailTplService;

    @ApiOperation(value = "car_expense_detail_tpl-查询列表", notes = " ")
    @ApiEntityParams(CarExpenseDetailTpl.class)
    @ApiResponses({@ApiResponse(code = 200, response = CarExpenseDetailTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCarExpenseDetailTpl(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        //获取登录的机构号
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        params.put("branchId", branchId);

        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CarExpenseDetailTpl> qw = QueryTools.initQueryWrapper(CarExpenseDetailTpl.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = carExpenseDetailTplService.selectListMapByWhere(page, qw, params);
        
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "car_expense_detail_tpl-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarExpenseDetailTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCarExpenseDetailTpl(@RequestBody CarExpenseDetailTpl carExpenseDetailTpl) {
        
        if (StringUtils.isEmpty(carExpenseDetailTpl.getBranchId())) {
            //获取登录的机构号
            String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
            carExpenseDetailTpl.setBranchId(branchId);
        }


        if (StringUtils.isEmpty(carExpenseDetailTpl.getId())) {
            carExpenseDetailTpl.setId(carExpenseDetailTplService.createKey("id"));
        } else {
            CarExpenseDetailTpl carExpenseDetailTplQuery = new CarExpenseDetailTpl(carExpenseDetailTpl.getId());
            if (carExpenseDetailTplService.countByWhere(carExpenseDetailTplQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        carExpenseDetailTplService.save(carExpenseDetailTpl);


        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "car_expense_detail_tpl-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCarExpenseDetailTpl(@RequestBody CarExpenseDetailTpl carExpenseDetailTpl) {
        

        carExpenseDetailTplService.deleteByPk(carExpenseDetailTpl);

        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "car_expense_detail_tpl-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarExpenseDetailTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCarExpenseDetailTpl(@RequestBody CarExpenseDetailTpl carExpenseDetailTpl) {
        

        if (StringUtils.isEmpty(carExpenseDetailTpl.getId())) {
            return Result.error("主键不能为空");
        }
        if (StringUtils.isEmpty(carExpenseDetailTpl.getBranchId())) {
            //获取登录的机构号
            String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
            carExpenseDetailTpl.setBranchId(branchId);
        }


        carExpenseDetailTplService.updateByPk(carExpenseDetailTpl);

        return Result.ok("edit-ok", "修改成功！").setData(carExpenseDetailTpl);
    }

    @ApiOperation(value = "car_expense_detail_tpl-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CarExpenseDetailTpl.class, props = {}, remark = "car_expense_detail_tpl", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CarExpenseDetailTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        carExpenseDetailTplService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "car_expense_detail_tpl-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCarExpenseDetailTpl(@RequestBody List<CarExpenseDetailTpl> carExpenseDetailTpls) {
        User user = LoginUtils.getCurrentUserInfo();
        if (carExpenseDetailTpls.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CarExpenseDetailTpl> datasDb = carExpenseDetailTplService.listByIds(carExpenseDetailTpls.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CarExpenseDetailTpl> can = new ArrayList<>();
        List<CarExpenseDetailTpl> no = new ArrayList<>();
        for (CarExpenseDetailTpl data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            carExpenseDetailTplService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "car_expense_detail_tpl-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarExpenseDetailTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CarExpenseDetailTpl carExpenseDetailTpl) {
        CarExpenseDetailTpl data = (CarExpenseDetailTpl) carExpenseDetailTplService.getById(carExpenseDetailTpl);
        return Result.ok().setData(data);
    }

}
