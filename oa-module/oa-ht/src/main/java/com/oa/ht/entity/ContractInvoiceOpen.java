package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_invoice_open")
@ApiModel(description="ht_contract_invoice_open")
public class ContractInvoiceOpen  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="合同id",allowEmptyValue=true,example="",allowableValues="")
	String htId;

	
	@ApiModelProperty(notes="申请人id",allowEmptyValue=true,example="",allowableValues="")
	String requireId;

	
	@ApiModelProperty(notes="相对方id",allowEmptyValue=true,example="",allowableValues="")
	String cpId;

	
	@ApiModelProperty(notes="申请人名称",allowEmptyValue=true,example="",allowableValues="")
	String requireName;

	
	@ApiModelProperty(notes="发票抬头",allowEmptyValue=true,example="",allowableValues="")
	String title;

	
	@ApiModelProperty(notes="纳税人识别号",allowEmptyValue=true,example="",allowableValues="")
	String gtaxid;

	
	@ApiModelProperty(notes="发票编号",allowEmptyValue=true,example="",allowableValues="")
	String number;

	
	@ApiModelProperty(notes="金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal amount;

	
	@ApiModelProperty(notes="票点",allowEmptyValue=true,example="",allowableValues="")
	Integer point;

	
	@ApiModelProperty(notes="内容",allowEmptyValue=true,example="",allowableValues="")
	String content;

	
	@ApiModelProperty(notes="类型1.增值税专业发票",allowEmptyValue=true,example="",allowableValues="")
	String type;

	
	@ApiModelProperty(notes="状态1.未收款2.已收款",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="发票状态：1.已开票",allowEmptyValue=true,example="",allowableValues="")
	String invoiceStatus;

	
	@ApiModelProperty(notes="回款时间",allowEmptyValue=true,example="",allowableValues="")
	Date returnDate;

	
	@ApiModelProperty(notes="开票时间",allowEmptyValue=true,example="",allowableValues="")
	Date openDate;

	
	@ApiModelProperty(notes="联系方式",allowEmptyValue=true,example="",allowableValues="")
	String relationWay;

	
	@ApiModelProperty(notes="快递单号",allowEmptyValue=true,example="",allowableValues="")
	String expressNumber;

	
	@ApiModelProperty(notes="收件地址",allowEmptyValue=true,example="",allowableValues="")
	String address;

	
	@ApiModelProperty(notes="签收时间",allowEmptyValue=true,example="",allowableValues="")
	Date signDate;

	
	@ApiModelProperty(notes="签收人",allowEmptyValue=true,example="",allowableValues="")
	String signPeople;

	
	@ApiModelProperty(notes="合同编号",allowEmptyValue=true,example="",allowableValues="")
	String htNumber;

	
	@ApiModelProperty(notes="create_user_id",allowEmptyValue=true,example="",allowableValues="")
	String createUserId;

	
	@ApiModelProperty(notes="create_user_name",allowEmptyValue=true,example="",allowableValues="")
	String createUserName;

	
	@ApiModelProperty(notes="create_time",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="branch_id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="收款条件编号",allowEmptyValue=true,example="",allowableValues="")
	String conditionId;

	
	@ApiModelProperty(notes="相对方名称",allowEmptyValue=true,example="",allowableValues="")
	String cpName;

	
	@ApiModelProperty(notes="发票链接地址",allowEmptyValue=true,example="",allowableValues="")
	String fileUrl;

	/**
	 *id
	 **/
	public ContractInvoiceOpen(String id) {
		this.id = id;
	}
    
    /**
     * ht_contract_invoice_open
     **/
	public ContractInvoiceOpen() {
	}

}