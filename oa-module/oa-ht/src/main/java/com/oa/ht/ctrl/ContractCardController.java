package com.oa.ht.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.audit.log.client.annotation.AuditLog;
import com.mdp.audit.log.client.annotation.OperType;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.ht.entity.ContractCard;
import com.oa.ht.service.ContractCardService;
import com.oa.ht.vo.ContractCardAddVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/ht/contractCard")
@Api(tags = {"ht_contract_card-操作接口"})
public class ContractCardController {

    static Logger logger = LoggerFactory.getLogger(ContractCardController.class);

    @Autowired
    private ContractCardService contractCardService;

    @ApiOperation(value = "ht_contract_card-查询列表", notes = " ")
    @ApiEntityParams(ContractCard.class)
    @ApiResponses({@ApiResponse(code = 200, response = ContractCard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listContractCard(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "htIds");
        RequestUtils.transformArray(params, "cbCenterIds");
        QueryWrapper<ContractCard> qw = QueryTools.initQueryWrapper(ContractCard.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = contractCardService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "ht_contract_card-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractCard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addContractCard(@RequestBody ContractCard contractCard) {
        contractCardService.save(contractCard);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "ht_contract_card-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delContractCard(@RequestBody ContractCard contractCard) {
        contractCardService.removeById(contractCard);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "ht_contract_card-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractCard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editContractCard(@RequestBody ContractCardAddVo contractCardAddVo) {

        contractCardService.updateContractCard(contractCardAddVo);

        return Result.ok("edit-ok", "修改成功！").setData(contractCardAddVo);
    }

    @ApiOperation(value = "计算合同的各项数据金额", notes = "computeAmount")
    @ApiResponses({@ApiResponse(code = 200, response = ContractCard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/computeAmount", method = RequestMethod.GET)
    public Result computeAmount() {
        Map<String, Object> result = contractCardService.computeAmount();
        return Result.ok("query-ok", "查询成功").setData(result);
    }

    @RequestMapping(value = "/loadChartData", method = RequestMethod.GET)
    public Result getChartData(@RequestParam Map<String, Object> contractCard) {
        String branchId = (String) contractCard.get("branchId");
        contractCard.remove("branchId");
        contractCard.put("res.branchId", branchId);
        QueryWrapper<ContractCard> qw = QueryTools.initQueryWrapper(ContractCard.class, contractCard);
        IPage page = QueryTools.initPage(contractCard);
        Map<String, Object> result = contractCardService.getChartData(page, qw, contractCard);
        return Result.ok("query-ok", "查询成功").setData(result);
    }

    @RequestMapping(value = "/getStatisticalStatement", method = RequestMethod.GET)
    public Result getStatisticalStatement(@RequestParam Map<String, Object> contractCard) {
        Map<String, Object> result = contractCardService.getStatisticalStatement(contractCard);
        return Result.ok("query-ok", "查询成功").setData(result);
    }

    @AuditLog(firstMenu = "合同管理", secondMenu = "合同审批", func = "processApprova", funcDesc = "合同审批", operType = OperType.UPDATE)
    @RequestMapping(value = "/processApprova", method = RequestMethod.POST)
    public Result processApprova(@RequestBody Map<String, Object> flowVars) {

        this.contractCardService.processApprova(flowVars);
        logger.debug("procInstId=====" + flowVars.get("procInstId"));

        return Result.ok("submit-ok", "成功提交审批");
    }


    @ApiOperation(value = "ht_contract_card-批量修改某些字段", notes = "")
    @ApiEntityParams(value = ContractCard.class, props = {}, remark = "ht_contract_card", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = ContractCard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        contractCardService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "ht_contract_card-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelContractCard(@RequestBody List<ContractCard> contractCards) {
        User user = LoginUtils.getCurrentUserInfo();
        if (contractCards.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<ContractCard> datasDb = contractCardService.listByIds(contractCards.stream().map(ContractCard::getHtId).collect(Collectors.toList()));

        List<ContractCard> can = new ArrayList<>();
        List<ContractCard> no = new ArrayList<>();
        for (ContractCard data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            contractCardService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getHtId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ht_contract_card-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractCard.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(ContractCard contractCard) {
        ContractCard data = (ContractCard) contractCardService.getById(contractCard);
        return Result.ok().setData(data);
    }

}
