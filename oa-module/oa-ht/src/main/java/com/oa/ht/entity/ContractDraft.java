package com.oa.ht.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ht_contract_draft")
@ApiModel(description="ht_contract_draft")
public class ContractDraft  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="标题",allowEmptyValue=true,example="",allowableValues="")
	String title;

	
	@ApiModelProperty(notes="申请人",allowEmptyValue=true,example="",allowableValues="")
	String reqUserId;

	
	@ApiModelProperty(notes="申请人名称",allowEmptyValue=true,example="",allowableValues="")
	String reqUserName;

	
	@ApiModelProperty(notes="部门",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptId;

	
	@ApiModelProperty(notes="部门名称",allowEmptyValue=true,example="",allowableValues="")
	String reqDeptName;

	
	@ApiModelProperty(notes="申请日期",allowEmptyValue=true,example="",allowableValues="")
	Date reqDate;

	
	@ApiModelProperty(notes="合同名称",allowEmptyValue=true,example="",allowableValues="")
	String htName;

	
	@ApiModelProperty(notes="签约主体",allowEmptyValue=true,example="",allowableValues="")
	String htParty;

	
	@ApiModelProperty(notes="签约主体名称",allowEmptyValue=true,example="",allowableValues="")
	String htPartyName;

	
	@ApiModelProperty(notes="合同编号",allowEmptyValue=true,example="",allowableValues="")
	String htSn;

	
	@ApiModelProperty(notes="合同生效日期",allowEmptyValue=true,example="",allowableValues="")
	Date htBeginDate;

	
	@ApiModelProperty(notes="合同总金额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal htAmount;

	
	@ApiModelProperty(notes="合同重要条款",allowEmptyValue=true,example="",allowableValues="")
	String htImportantClause;

	
	@ApiModelProperty(notes="合同说明",allowEmptyValue=true,example="",allowableValues="")
	String htDemand;

	
	@ApiModelProperty(notes="客户id",allowEmptyValue=true,example="",allowableValues="")
	String cpId;

	
	@ApiModelProperty(notes="客户名称",allowEmptyValue=true,example="",allowableValues="")
	String cpName;

	
	@ApiModelProperty(notes="客户法定代表人",allowEmptyValue=true,example="",allowableValues="")
	String cpFddb;

	
	@ApiModelProperty(notes="客户级别",allowEmptyValue=true,example="",allowableValues="")
	String cpLevel;

	
	@ApiModelProperty(notes="客户类型",allowEmptyValue=true,example="",allowableValues="")
	String cpType;

	
	@ApiModelProperty(notes="客户地址",allowEmptyValue=true,example="",allowableValues="")
	String cpAddress;

	
	@ApiModelProperty(notes="客户开户行",allowEmptyValue=true,example="",allowableValues="")
	String cpOpenBank;

	
	@ApiModelProperty(notes="客户开户行账号",allowEmptyValue=true,example="",allowableValues="")
	String cpBankAccount;

	
	@ApiModelProperty(notes="联系人名称",allowEmptyValue=true,example="",allowableValues="")
	String linkName;

	
	@ApiModelProperty(notes="联系人电话",allowEmptyValue=true,example="",allowableValues="")
	String linkPhone;

	
	@ApiModelProperty(notes="合同父级分类id",allowEmptyValue=true,example="",allowableValues="")
	String htParentType;

	
	@ApiModelProperty(notes="合同分类",allowEmptyValue=true,example="",allowableValues="")
	String htType;

	
	@ApiModelProperty(notes="0否",allowEmptyValue=true,example="",allowableValues="")
	String isUseMoban;

	
	@ApiModelProperty(notes="范本id",allowEmptyValue=true,example="",allowableValues="")
	String fanbenId;

	
	@ApiModelProperty(notes="0保存1提交",allowEmptyValue=true,example="",allowableValues="")
	String status;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="相对方工商注册号-纳税人识别号",allowEmptyValue=true,example="",allowableValues="")
	String cpGszch;

	
	@ApiModelProperty(notes="开票类型1-增值税专用发票，2-增值税普通发票，3-其它",allowEmptyValue=true,example="",allowableValues="")
	String invoiceType;

	
	@ApiModelProperty(notes="税率",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal invoicePoint;

	
	@ApiModelProperty(notes="合同分类名称",allowEmptyValue=true,example="",allowableValues="")
	String htTypeName;

	
	@ApiModelProperty(notes="合同大类名称",allowEmptyValue=true,example="",allowableValues="")
	String htParentTypeName;

	
	@ApiModelProperty(notes="合同附件列表，逗号分割多个",allowEmptyValue=true,example="",allowableValues="")
	String htFiles;

	
	@ApiModelProperty(notes="合同成本总额",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal htCbAmount;

	
	@ApiModelProperty(notes="成本中心编号",allowEmptyValue=true,example="",allowableValues="")
	String cbCenterId;

	
	@ApiModelProperty(notes="成本中心名称",allowEmptyValue=true,example="",allowableValues="")
	String cbCenterName;

	/**
	 *id
	 **/
	public ContractDraft(String id) {
		this.id = id;
	}
    
    /**
     * ht_contract_draft
     **/
	public ContractDraft() {
	}

}