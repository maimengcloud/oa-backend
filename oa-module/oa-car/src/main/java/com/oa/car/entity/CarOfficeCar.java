package com.oa.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月7日
 */
@Data
@TableName("car_office_car")
@ApiModel(description="公务车登记")
public class CarOfficeCar  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="车牌号",allowEmptyValue=true,example="",allowableValues="")
	String carNum;

	
	@ApiModelProperty(notes="车名",allowEmptyValue=true,example="",allowableValues="")
	String carName;

	
	@ApiModelProperty(notes="汽车品牌编号",allowEmptyValue=true,example="",allowableValues="")
	String carBrandId;

	
	@ApiModelProperty(notes="汽车品牌名称",allowEmptyValue=true,example="",allowableValues="")
	String carBrandName;

	
	@ApiModelProperty(notes="汽车前照片",allowEmptyValue=true,example="",allowableValues="")
	String carFrontImageUrl;

	
	@ApiModelProperty(notes="汽车后照片",allowEmptyValue=true,example="",allowableValues="")
	String carBackImageUrl;

	
	@ApiModelProperty(notes="汽车内饰照片",allowEmptyValue=true,example="",allowableValues="")
	String carInnerImageUrl;

	
	@ApiModelProperty(notes="汽车类型(0小轿车1商务车2面包车3小货车4大货车)",allowEmptyValue=true,example="",allowableValues="")
	String carType;

	
	@ApiModelProperty(notes="汽车状态1启用0禁用",allowEmptyValue=true,example="",allowableValues="")
	String carStatus;

	
	@ApiModelProperty(notes="汽车归属部门",allowEmptyValue=true,example="",allowableValues="")
	String carDeptid;

	
	@ApiModelProperty(notes="汽车归属机构",allowEmptyValue=true,example="",allowableValues="")
	String carBranchId;

	
	@ApiModelProperty(notes="汽车归属部门名称",allowEmptyValue=true,example="",allowableValues="")
	String carDeptName;

	
	@ApiModelProperty(notes="汽车归属机构名称",allowEmptyValue=true,example="",allowableValues="")
	String carBranchName;

	
	@ApiModelProperty(notes="新增时间",allowEmptyValue=true,example="",allowableValues="")
	Date createTime;

	
	@ApiModelProperty(notes="汽车座位数",allowEmptyValue=true,example="",allowableValues="")
	Integer carSeating;

	
	@ApiModelProperty(notes="汽车载重量(kg)",allowEmptyValue=true,example="",allowableValues="")
	Integer carLoadWeight;

	/**
	 *主键
	 **/
	public CarOfficeCar(String id) {
		this.id = id;
	}
    
    /**
     * 公务车登记
     **/
	public CarOfficeCar() {
	}

}