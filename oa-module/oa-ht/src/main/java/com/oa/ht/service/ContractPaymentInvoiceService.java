package com.oa.ht.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.ht.entity.ContractPaymentInvoice;
import com.oa.ht.entity.PaymentLinkInvoicesVo;
import com.oa.ht.mapper.ContractPaymentInvoiceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class ContractPaymentInvoiceService extends BaseService<ContractPaymentInvoiceMapper,ContractPaymentInvoice> {
	static Logger logger =LoggerFactory.getLogger(ContractPaymentInvoiceService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}


	@Autowired
	ContractLogService contractLogService;

	@Transactional
	public void paymentLinkInvoices(PaymentLinkInvoicesVo linkInvoices) {
		User user= LoginUtils.getCurrentUserInfo();
		ContractPaymentInvoice paymentInvoice=new ContractPaymentInvoice();
		paymentInvoice.setPaymentId(linkInvoices.getPaymentId());
		this.deleteByWhere(paymentInvoice);
		List<ContractPaymentInvoice> links=new ArrayList<>();
		if(linkInvoices.getInvoiceIds()!=null && linkInvoices.getInvoiceIds().size()>0){
			for (String invoiceId : linkInvoices.getInvoiceIds()) {
				ContractPaymentInvoice link=new ContractPaymentInvoice();
				link.setInvoiceId(invoiceId);
				link.setPaymentId(linkInvoices.getPaymentId());
				link.setHtId(linkInvoices.getHtId());
				links.add(link);
			}
			this.batchInsert(links);
			contractLogService.addLog(linkInvoices.getHtId(),user.getUserid(),user.getUsername(),"关联发票"+links.size()+"张");
		}else{
			contractLogService.addLog(linkInvoices.getHtId(),user.getUserid(),user.getUsername(),"清除付款单关联的发票");
		}
	}
}

