package com.oa.erp.pur.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("pur_brand")
@ApiModel(description="pur_brand")
public class Brand  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="品牌id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="品牌名称",allowEmptyValue=true,example="",allowableValues="")
	String brandName;

	
	@ApiModelProperty(notes="首字母",allowEmptyValue=true,example="",allowableValues="")
	String firstLetter;

	
	@ApiModelProperty(notes="是否为品牌制作商",allowEmptyValue=true,example="",allowableValues="")
	Integer factoryStatus;

	
	@ApiModelProperty(notes="品牌logo",allowEmptyValue=true,example="",allowableValues="")
	String logo;

	
	@ApiModelProperty(notes="专区大图",allowEmptyValue=true,example="",allowableValues="")
	String bigPic;

	
	@ApiModelProperty(notes="品牌故事",allowEmptyValue=true,example="",allowableValues="")
	String brandStory;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	
	@ApiModelProperty(notes="负责人",allowEmptyValue=true,example="",allowableValues="")
	String personUser;

	
	@ApiModelProperty(notes="联系电话",allowEmptyValue=true,example="",allowableValues="")
	String personPhone;

	
	@ApiModelProperty(notes="入驻时间",allowEmptyValue=true,example="",allowableValues="")
	Date brandDate;

	
	@ApiModelProperty(notes="品牌状态",allowEmptyValue=true,example="",allowableValues="")
	String brandStatus;

	
	@ApiModelProperty(notes="品牌供应商地址",allowEmptyValue=true,example="",allowableValues="")
	String brandAddress;

	
	@ApiModelProperty(notes="是否盈余",allowEmptyValue=true,example="",allowableValues="")
	Integer isProfit;

	/**
	 *品牌id
	 **/
	public Brand(String id) {
		this.id = id;
	}
    
    /**
     * pur_brand
     **/
	public Brand() {
	}

}