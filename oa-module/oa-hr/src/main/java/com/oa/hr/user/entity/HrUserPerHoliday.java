package com.oa.hr.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("hr_user_per_holiday")
@ApiModel(description="员工个人假期表")
public class HrUserPerHoliday  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="用户编号",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="年份",allowEmptyValue=true,example="",allowableValues="")
	String year;

	
	@ApiModelProperty(notes="剩余带薪年假",allowEmptyValue=true,example="",allowableValues="")
	String annualLeave;

	
	@ApiModelProperty(notes="剩余调休假期",allowEmptyValue=true,example="",allowableValues="")
	String paidLeave;

	
	@ApiModelProperty(notes="剩余带薪病假",allowEmptyValue=true,example="",allowableValues="")
	String paidSickLeave;

	
	@ApiModelProperty(notes="剩余调休假",allowEmptyValue=true,example="",allowableValues="")
	String leaveInLieu;

	
	@ApiModelProperty(notes="剩余婚假",allowEmptyValue=true,example="",allowableValues="")
	String marriageLeave;

	
	@ApiModelProperty(notes="剩余年假",allowEmptyValue=true,example="",allowableValues="")
	String annualVacation;

	
	@ApiModelProperty(notes="剩余病假",allowEmptyValue=true,example="",allowableValues="")
	String sickLeave;

	
	@ApiModelProperty(notes="剩余总天数",allowEmptyValue=true,example="",allowableValues="")
	String totalDays;

	
	@ApiModelProperty(notes="剩余哺乳假",allowEmptyValue=true,example="",allowableValues="")
	String lactation;

	
	@ApiModelProperty(notes="剩余丧假",allowEmptyValue=true,example="",allowableValues="")
	String funeralLeave;

	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *id
	 **/
	public HrUserPerHoliday(String id) {
		this.id = id;
	}
    
    /**
     * 员工个人假期表
     **/
	public HrUserPerHoliday() {
	}

}