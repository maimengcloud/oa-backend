package com.oa.kq.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.entity.Tips;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.kq.entity.Holidays;
import com.oa.kq.service.HolidaysService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value = "/*/oa/kq/holidays")
@Api(tags = {"节假日表-操作接口"})
public class HolidaysController {

    static Logger logger = LoggerFactory.getLogger(HolidaysController.class);

    @Autowired
    private HolidaysService holidaysService;

    @ApiOperation(value = "节假日表-查询列表", notes = " ")
    @ApiEntityParams(Holidays.class)
    @ApiResponses({@ApiResponse(code = 200, response = Holidays.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listHolidays(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<Holidays> qw = QueryTools.initQueryWrapper(Holidays.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = holidaysService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "节假日表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Holidays.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addHolidays(@RequestBody Holidays holidays) {
        Map<String, Object> m = new HashMap<>();
        Tips tips = new Tips("成功新增一条数据");
        if (StringUtils.isEmpty(holidays.getId())) {
            holidays.setId(holidaysService.createKey("id"));
        } else {
            Holidays kqHolidaysQuery = new Holidays(holidays.getId());
            if (holidaysService.countByWhere(kqHolidaysQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        holidaysService.insert(holidays);
        return Result.ok("add-ok", "添加成功！").setData(holidays);
    }

    @ApiOperation(value = "节假日表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delHolidays(@RequestBody Holidays holidays) {
        holidaysService.deleteByPk(holidays);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "节假日表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Holidays.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editHolidays(@RequestBody Holidays kqHolidays) {
        holidaysService.updateByPk(kqHolidays);
        return Result.ok("edit-ok", "修改成功！").setData(kqHolidays);
    }

    @ApiOperation(value = "节假日表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = Holidays.class, props = {}, remark = "节假日表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = Holidays.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        holidaysService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "节假日表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelHolidays(@RequestBody List<Holidays> holidayss) {
        User user = LoginUtils.getCurrentUserInfo();
        if (holidayss.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<Holidays> datasDb = holidaysService.listByIds(holidayss.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<Holidays> can = new ArrayList<>();
        List<Holidays> no = new ArrayList<>();
        for (Holidays data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            holidaysService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "节假日表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = Holidays.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(Holidays holidays) {
        Holidays data = (Holidays) holidaysService.getById(holidays);
        return Result.ok().setData(data);
    }

}
