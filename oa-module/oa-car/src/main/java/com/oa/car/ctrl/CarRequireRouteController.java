package com.oa.car.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.car.entity.CarRequireRoute;
import com.oa.car.entity.CarRouteTpl;
import com.oa.car.service.CarRequireRouteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@RestController
@RequestMapping(value = "/*/oa/car/carRequireRoute")
@Api(tags = {"car_require_route-操作接口"})
public class CarRequireRouteController {

    static Logger logger = LoggerFactory.getLogger(CarRequireRouteController.class);

    @Autowired
    private CarRequireRouteService carRequireRouteService;

    @ApiOperation(value = "car_require_route-查询列表", notes = " ")
    @ApiEntityParams(CarRequireRoute.class)
    @ApiResponses({@ApiResponse(code = 200, response = CarRequireRoute.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCarRequireRoute(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<CarRequireRoute> qw = QueryTools.initQueryWrapper(CarRequireRoute.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = carRequireRouteService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "car_require_route-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRequireRoute.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCarRequireRoute(@RequestBody CarRequireRoute carRequireRoute) {
        carRequireRouteService.save(carRequireRoute);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "car_require_route-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCarRequireRoute(@RequestBody CarRequireRoute carRequireRoute) {
        carRequireRouteService.removeById(carRequireRoute);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "car_require_route-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRequireRoute.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCarRequireRoute(@RequestBody CarRequireRoute carRequireRoute) {
        carRequireRouteService.updateById(carRequireRoute);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "car_require_route-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CarRequireRoute.class, props = {}, remark = "car_require_route", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CarRequireRoute.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        carRequireRouteService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "car_require_route-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCarRequireRoute(@RequestBody List<CarRequireRoute> carRequireRoutes) {
        User user = LoginUtils.getCurrentUserInfo();
        if (carRequireRoutes.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CarRequireRoute> datasDb = carRequireRouteService.listByIds(carRequireRoutes.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CarRequireRoute> can = new ArrayList<>();
        List<CarRequireRoute> no = new ArrayList<>();
        for (CarRequireRoute data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            carRequireRouteService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "car_require_route-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarRequireRoute.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CarRequireRoute carRequireRoute) {
        CarRequireRoute data = (CarRequireRoute) carRequireRouteService.getById(carRequireRoute);
        return Result.ok().setData(data);
    }

    @ApiOperation(value = "获取总路线的地址信息和子路线的地址信息", notes = "addCarRouteTpl,主键如果为空，后台自动生成")
    @ApiResponses({@ApiResponse(code = 200, response = CarRouteTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/listAddressGPS", method = RequestMethod.GET)
    public Result selectAddressGPS(@RequestParam() Map<String, Object> map) {
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (!StringUtils.isEmpty(map.get("startTime"))) {
            Date startTime = new Date(Long.parseLong(map.get("startTime").toString()));
            //String startTime = simpleDateFormat.format(new Date(Long.parseLong(map.get("startTime").toString())));
            map.put("startTime", startTime);
        }
        if (!StringUtils.isEmpty(map.get("endTime"))) {
            Date endTime = new Date(Long.parseLong(map.get("endTime").toString()));
            //String endTime = simpleDateFormat.format(Long.parseLong(map.get("endTime").toString()));
            map.put("endTime", endTime);
        }
        if (!StringUtils.isEmpty(map.get("ids"))) {
            String ids = (String) map.get("ids");
            String[] split = ids.split(",");
            map.put("ids", split);
        }

        QueryWrapper<CarRequireRoute> qw = QueryTools.initQueryWrapper(CarRequireRoute.class, map);
        IPage page = QueryTools.initPage(map);

        List<Map<String, Object>> carRouteTplList = carRequireRouteService.selectAddressGPS(page, qw, map);    //列出CarRouteDetailTpl列表

        return Result.ok().setData(carRouteTplList);
    }
}
