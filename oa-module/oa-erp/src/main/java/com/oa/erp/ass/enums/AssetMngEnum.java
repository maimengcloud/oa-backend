package com.oa.erp.ass.enums;

public enum  AssetMngEnum {

    LEASE("lease", "租用"),
    REALLOCATION("reallocation", "调拨"),
    MAINT("maint", "维修"),
    USE("use", "领用"),
    RETURN("return", "归还"),
    CHANGE("change", "变更"),
    DISPOSE("dispose", "处置"),
    INVENTORY("inventory", "盘点");
    ;

    AssetMngEnum(String status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    private String status;

    private String desc;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public static AssetMngEnum getType(String dataTypeCode){
        for(AssetMngEnum enums : AssetMngEnum.values()){
            if(enums.status.equals(dataTypeCode)){
                return enums;
            }
        }
        return null;
    }



}
