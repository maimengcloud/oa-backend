package com.oa.crm.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.err.BizException;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.crm.entity.CustomerShare;
import com.oa.crm.enums.CustomerShareEnum;
import com.oa.crm.mapper.CustomerShareMapper;
import com.oa.crm.vo.CustomerShareVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class CustomerShareService extends BaseService<CustomerShareMapper,CustomerShare> {
	static Logger logger =LoggerFactory.getLogger(CustomerShareService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}


	/**
	 * 添加客户共享权限
	 * @param customerShareVo
	 */
	public void insertCustomerShare(CustomerShareVo customerShareVo) {
		User user = LoginUtils.getCurrentUserInfo();
		List<CustomerShare> customerShares = new ArrayList<>();
		List<Map<String, Object>> mapList = customerShareVo.getShareObjs();
		if(CollectionUtils.isEmpty(mapList)) {
			throw new BizException("共享对象不存在");
		}
		for (Map<String, Object> map : mapList) {
			CustomerShare customerShare = new CustomerShare();
			BeanUtils.copyProperties(customerShareVo, customerShare);
			customerShare.setShareId(this.createKey("shareId"));
			customerShare.setBranchId(user.getBranchId());
			customerShare.setShareObj((String) map.get("id"));
			customerShare.setShareObjName((String) map.get("name"));
			customerShares.add(customerShare);
		}
		this.batchInsert(customerShares);
	}

	/**
	 * 插入添加时的客户权限， 默认修改和更新
	 * @param customerId
	 */
	@Transactional
	public void insertAddCustomerShare(String customerId, String userid, String username) {
		User user = LoginUtils.getCurrentUserInfo();
		List<CustomerShare> customerShares = new ArrayList<>();
		CustomerShare seeCustomerShare = new CustomerShareVo();
		seeCustomerShare.setShareLevel(CustomerShareEnum.SHARE_LEVEL_SEE.getCode());
		seeCustomerShare.setShareType(CustomerShareEnum.SHARE_TYPE_USER.getCode());
		seeCustomerShare.setCustomerId(customerId);
		seeCustomerShare.setBranchId(user.getBranchId());
		seeCustomerShare.setShareId(this.createKey("shareId"));
		seeCustomerShare.setShareObj(userid);
		seeCustomerShare.setShareObjName(username);
		customerShares.add(seeCustomerShare);
		CustomerShare editCustomerShare = new CustomerShareVo();
		BeanUtils.copyProperties(seeCustomerShare, editCustomerShare);
		editCustomerShare.setShareLevel(CustomerShareEnum.SHARE_LEVEL_EDIT.getCode());
		editCustomerShare.setShareId(this.createKey("shareId"));
		customerShares.add(editCustomerShare);

		this.batchInsert(customerShares);
	}
}

