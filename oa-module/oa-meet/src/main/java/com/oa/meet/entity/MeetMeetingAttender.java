package com.oa.meet.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月9日
 */
@Data
@TableName("meet_meeting_attender")
@ApiModel(description="会议参会人员表")
public class MeetMeetingAttender  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="会议id",allowEmptyValue=true,example="",allowableValues="")
	String meetingId;

	
	@ApiModelProperty(notes="参会用户id",allowEmptyValue=true,example="",allowableValues="")
	String userid;

	
	@ApiModelProperty(notes="参会用户",allowEmptyValue=true,example="",allowableValues="")
	String username;

	
	@ApiModelProperty(notes="参加备注",allowEmptyValue=true,example="",allowableValues="")
	String remark;

	
	@ApiModelProperty(notes="用户类型1企业员工2非企业员工",allowEmptyValue=true,example="",allowableValues="")
	String userType;

	
	@ApiModelProperty(notes="0临时参会人员1正式参会人员",allowEmptyValue=true,example="",allowableValues="")
	String attType;

	
	@ApiModelProperty(notes="手机号码",allowEmptyValue=true,example="",allowableValues="")
	String phoneno;

	
	@ApiModelProperty(notes="邮箱号码",allowEmptyValue=true,example="",allowableValues="")
	String email;

	/**
	 *主键
	 **/
	public MeetMeetingAttender(String id) {
		this.id = id;
	}
    
    /**
     * 会议参会人员表
     **/
	public MeetMeetingAttender() {
	}

}