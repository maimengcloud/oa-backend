package com.oa.common.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.common.entity.SysUser;
import com.oa.common.mapper.SysUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class SysUserService extends BaseService<SysUserMapper, SysUser> {
    static Logger logger = LoggerFactory.getLogger(SysUserService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }

    /**
     * 查询员工信息
     *
     * @param sysUser
     * @return
     */
    @DS("adm-ds")
    public Map<String, Object> selectUserInfomation(Map<String, Object> sysUser) {
        return baseMapper.selectUserInfomation(sysUser);
    }

    /**
     * 查询adm 用户的部门、部门领导、岗位，岗位名称，上级部门编号，上级领导，上上级领导
     *
     * @param sysUser
     * @return
     */
    @DS("adm-ds")
    public List<Map<String, Object>> selectUserDeptPost(Map<String, Object> sysUser) {
        return baseMapper.selectUserDeptPost(sysUser);
    }

//    @DS("adm-ds")
//    public <T> T selectOneObject(T parameter) {
//        return super.selectOneObject(parameter);
//    }

//    @DS("adm-ds")
//    @Override
//    public <T> int updateSomeFieldByPk(T parameter) {
//        return super.updateSomeFieldByPk(parameter);
//    }

//    @DS("adm-ds")
//    @Override
//    public <T> int updateByPk(T parameter) {
//        return super.updateByPk(parameter);
//    }

    @DS("adm-ds")
    public Map<String, Object> getDept(String deptid) {
        return baseMapper.getDept(deptid);
    }

//    @DS("adm-ds")
//    @Override
//    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> parameter) {
//        return baseMapper.selectListMapByWhere(page, ew, parameter);
//    }

    @DS("adm-ds")
    public Map<String, Object> getUserDeptPost(Map<String, Object> sysUser) {
        List<Map<String, Object>> dpus = this.selectUserDeptPost(sysUser);
        if (dpus != null && dpus.size() > 0) {
            Map<String, Object> dpu = null;
            Optional<Map<String, Object>> o = dpus.stream().filter(i -> "1".equals(i.get("master"))).findFirst();
            if (o.isPresent()) {
                dpu = o.get();
            } else {
                dpu = dpus.get(0);
            }
            return dpu;
        }
        return null;
    }

    @DS("adm-ds")
    public List<String> getSubDeptidsByDeptid(String deptid) {
        return baseMapper.getSubDeptidsByDeptid( deptid);
    }

}

