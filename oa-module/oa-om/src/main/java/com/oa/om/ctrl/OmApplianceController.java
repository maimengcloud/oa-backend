package com.oa.om.ctrl;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import com.mdp.core.utils.ObjectTools;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import static com.mdp.core.utils.BaseUtils.*;
import com.mdp.core.entity.LangTips;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;

import com.oa.om.service.OmApplianceService;
import com.oa.om.entity.OmAppliance;
/**
 * @author 唛盟开源 code-gen
 * @since 2024-8-21
 */
@RestController
@RequestMapping(value="/oa/om/omAppliance")
@Api(tags={"办公用品-操作接口"})
public class OmApplianceController {
	
	static Logger logger =LoggerFactory.getLogger(OmApplianceController.class);
	
	@Autowired
	private OmApplianceService omApplianceService;

	@Operation( summary =  "办公用品-查询列表")
	@ApiEntityParams(OmAppliance.class)
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listOmAppliance(  @RequestParam Map<String,Object> params){
			User user=LoginUtils.getCurrentUserInfo();
			QueryWrapper<OmAppliance> qw = QueryTools.initQueryWrapper(OmAppliance.class , params);
			IPage page=QueryTools.initPage(params);
			List<Map<String,Object>> datas = omApplianceService.selectListMapByWhere(page,qw,params);
			return Result.ok("query-ok","查询成功").setData(datas).setTotal(page.getTotal());
	}
	

	@Operation( summary =  "办公用品-新增")
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addOmAppliance(@RequestBody OmAppliance omAppliance) {
        if(ObjectTools.isEmpty(omAppliance.getStock())){
            omAppliance.setStock(BigDecimal.ZERO);
        }
		 omApplianceService.save(omAppliance);
         return Result.ok("add-ok","添加成功！").setData(omAppliance);
	}

    @Operation( summary =  "办公用品-库存盘点")
    @RequestMapping(value="/stockTaking",method=RequestMethod.POST)
    public Result stockTaking(@RequestBody OmAppliance omAppliance) {
        omApplianceService.stockTaking(omAppliance.getId());
        return Result.ok("stockTaking-ok","盘点成功！");
    }

	@Operation( summary =  "办公用品-删除")
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delOmAppliance(@RequestBody OmAppliance omAppliance){
		omApplianceService.removeById(omAppliance);
        return Result.ok("del-ok","删除成功！");
	}

	@Operation( summary =  "办公用品-修改")
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editOmAppliance(@RequestBody OmAppliance omAppliance) {
		omApplianceService.updateById(omAppliance);
        return Result.ok("edit-ok","修改成功！");
	}

    @Operation( summary =  "办公用品-批量修改某些字段" )
    @ApiEntityParams( value = OmAppliance.class, props={ }, remark = "办公用品", paramType = "body" )
	@RequestMapping(value="/editSomeFields",method=RequestMethod.POST)
	public Result editSomeFields(   @RequestBody Map<String,Object> params) {
            User user= LoginUtils.getCurrentUserInfo();
            omApplianceService.editSomeFields(params);
            return Result.ok("edit-ok","更新成功");
	}

	@Operation( summary =  "办公用品-批量删除")
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelOmAppliance(@RequestBody List<OmAppliance> omAppliances) {
	    User user= LoginUtils.getCurrentUserInfo();
        if(omAppliances.size()<=0){
            return Result.error("batchDel-data-err-0","请上送待删除数据列表");
        }
         List<OmAppliance> datasDb=omApplianceService.listByIds(omAppliances.stream().map(i-> i.getId() ).collect(Collectors.toList()));

        List<OmAppliance> can=new ArrayList<>();
        List<OmAppliance> no=new ArrayList<>();
        for (OmAppliance data : datasDb) {
            if(true){
                can.add(data);
            }else{
                no.add(data);
            }
        }
        List<String> msgs=new ArrayList<>();
        if(can.size()>0){
            omApplianceService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num","成功删除%s条数据.",can.size()));
        }

        if(no.size()>0){
            msgs.add(LangTips.transMsg("not-allow-del-num","以下%s条数据不能删除:【%s】",no.size(),no.stream().map(i-> i.getId() ).collect(Collectors.joining(","))));
        }
        if(can.size()>0){
             return Result.ok(msgs.stream().collect(Collectors.joining()));
        }else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
	} 

	@Operation( summary =  "办公用品-根据主键查询一条数据")
    @RequestMapping(value="/queryById",method=RequestMethod.GET)
    public Result queryById(OmAppliance omAppliance) {
        OmAppliance data = (OmAppliance) omApplianceService.getById(omAppliance);
        return Result.ok().setData(data);
    }

}
