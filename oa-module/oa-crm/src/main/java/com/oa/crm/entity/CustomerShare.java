package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_share")
@ApiModel(description="crm_customer_share")
public class CustomerShare  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="共享id,主键",allowEmptyValue=true,example="",allowableValues="")
	String shareId;

	
	@ApiModelProperty(notes="客户id",allowEmptyValue=true,example="",allowableValues="")
	String customerId;

	
	@ApiModelProperty(notes="dept:",allowEmptyValue=true,example="",allowableValues="")
	String shareType;

	
	@ApiModelProperty(notes="共享对象",allowEmptyValue=true,example="",allowableValues="")
	String shareObj;

	
	@ApiModelProperty(notes="共享对象名称",allowEmptyValue=true,example="",allowableValues="")
	String shareObjName;

	
	@ApiModelProperty(notes="see:查看edit编辑",allowEmptyValue=true,example="",allowableValues="")
	String shareLevel;

	
	@ApiModelProperty(notes="机构id",allowEmptyValue=true,example="",allowableValues="")
	String branchId;

	/**
	 *共享id
	 **/
	public CustomerShare(String shareId) {
		this.shareId = shareId;
	}
    
    /**
     * crm_customer_share
     **/
	public CustomerShare() {
	}

}