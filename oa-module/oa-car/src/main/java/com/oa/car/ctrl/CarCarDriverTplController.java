package com.oa.car.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.car.entity.CarCarDriverTpl;
import com.oa.car.service.CarCarDriverTplService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月7日
 */
@RestController
@RequestMapping(value = "/*/oa/car/carCarDriverTpl")
@Api(tags = {"司机汽车关联表-操作接口"})
public class CarCarDriverTplController {

    static Logger logger = LoggerFactory.getLogger(CarCarDriverTplController.class);

    @Autowired
    private CarCarDriverTplService carCarDriverTplService;

    @ApiOperation(value = "司机汽车关联表-查询列表", notes = " ")
    @ApiEntityParams(CarCarDriverTpl.class)
    @ApiResponses({@ApiResponse(code = 200, response = CarCarDriverTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listCarCarDriverTpl(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        //获取登录的机构号
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        params.put("branchId", branchId);
        QueryWrapper<CarCarDriverTpl> qw = QueryTools.initQueryWrapper(CarCarDriverTpl.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = carCarDriverTplService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "司机汽车关联表-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarCarDriverTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addCarCarDriverTpl(@RequestBody CarCarDriverTpl carCarDriverTpl) {
        if (StringUtils.isEmpty(carCarDriverTpl.getBranchId())) {
            //获取登录的机构号
            String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
            carCarDriverTpl.setBranchId(branchId);
        }
        if (StringUtils.isEmpty(carCarDriverTpl.getCreateTime())) {
            carCarDriverTpl.setCreateTime(new Date());
        }
        if (StringUtils.isEmpty(carCarDriverTpl.getId())) {
            carCarDriverTpl.setId(carCarDriverTplService.createKey("id"));
        } else {
            CarCarDriverTpl carCarDriverTplQuery = new CarCarDriverTpl(carCarDriverTpl.getId());
            if (carCarDriverTplService.countByWhere(carCarDriverTplQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        carCarDriverTplService.insert(carCarDriverTpl);
        return Result.ok("add-ok", "添加成功！").setData(carCarDriverTpl);
    }

    @ApiOperation(value = "司机汽车关联表-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delCarCarDriverTpl(@RequestBody CarCarDriverTpl carCarDriverTpl) {
        carCarDriverTplService.removeById(carCarDriverTpl);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "司机汽车关联表-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarCarDriverTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editCarCarDriverTpl(@RequestBody CarCarDriverTpl carCarDriverTpl) {
        carCarDriverTplService.updateByPk(carCarDriverTpl);
        return Result.ok("edit-ok", "修改成功！").setData(carCarDriverTpl);
    }

    @ApiOperation(value = "司机汽车关联表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = CarCarDriverTpl.class, props = {}, remark = "司机汽车关联表", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = CarCarDriverTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        carCarDriverTplService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "司机汽车关联表-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelCarCarDriverTpl(@RequestBody List<CarCarDriverTpl> carCarDriverTpls) {
        User user = LoginUtils.getCurrentUserInfo();
        if (carCarDriverTpls.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<CarCarDriverTpl> datasDb = carCarDriverTplService.listByIds(carCarDriverTpls.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<CarCarDriverTpl> can = new ArrayList<>();
        List<CarCarDriverTpl> no = new ArrayList<>();
        for (CarCarDriverTpl data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            carCarDriverTplService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "司机汽车关联表-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = CarCarDriverTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(CarCarDriverTpl carCarDriverTpl) {
        CarCarDriverTpl data = (CarCarDriverTpl) carCarDriverTplService.getById(carCarDriverTpl);
        return Result.ok().setData(data);
    }

    @ApiOperation(value = "查询car_car_driver_tpl信息列表并带上汽车名和司机名称返回", notes = "listCarCarDriverTpl,条件之间是 and关系,模糊查询写法如 {studentName:'%才哥%'}")
    @ApiEntityParams(CarCarDriverTpl.class)
    @ApiResponses({@ApiResponse(code = 200, response = CarCarDriverTpl.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/infos", method = RequestMethod.GET)
    public Result selectCarCarDriverTplAndInfos(@RequestParam Map<String, Object> carCarDriverTpl) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(carCarDriverTpl, "ids");
        QueryWrapper<CarCarDriverTpl> qw = QueryTools.initQueryWrapper(CarCarDriverTpl.class, carCarDriverTpl);
        IPage page = QueryTools.initPage(carCarDriverTpl);
        //获取登录的机构号
        String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
        carCarDriverTpl.put("branchId", branchId);
        List<Map<String, Object>> carCarDriverTplList = carCarDriverTplService.selectListMapByWhereAndCarDriver(carCarDriverTpl);    //列出CarCarDriverTpl列表
        return Result.ok("query-ok", "查询成功").setData(carCarDriverTplList).setTotal(page.getTotal());
    }

}
