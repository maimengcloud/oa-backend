package com.oa.duban.entity;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class DubanItemTaskVo extends DubanItemTask {

    @ApiModelProperty(notes="任务协办部门",allowEmptyValue=true,example="",allowableValues="")
    List<DubanItemTaskCoopDept> coopDept;

    @ApiModelProperty(notes = "任务协办", allowableValues = "",allowEmptyValue = true,example = "")
    List<DubanItemTaskCoopUser> coopUser;

    public List<DubanItemTaskCoopDept> getCoopDept() {
        return coopDept;
    }

    public void setCoopDept(List<DubanItemTaskCoopDept> coopDept) {
        this.coopDept = coopDept;
    }

    public List<DubanItemTaskCoopUser> getCoopUser() {
        return coopUser;
    }

    public void setCoopUser(List<DubanItemTaskCoopUser> coopUser) {
        this.coopUser = coopUser;
    }
}
