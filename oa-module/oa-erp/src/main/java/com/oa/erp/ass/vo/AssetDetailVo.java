package com.oa.erp.ass.vo;

import com.oa.erp.ass.entity.AssetCard;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 资产明细增强类
 */
public class AssetDetailVo extends AssetCard {

    //卡片记录熟悉
    String assetStatusAfter;
    String assetStatusBefore;
    BigDecimal adjustStockNum;
    Date allocDate;
    String nowPsersonLiableUserid;
    String nowPsersonLiableUsername;
    String nowDeptid;
    String nowDeptName;
    String nowBranchId;
    String nowBranchName;
    String nowStoreAddress;
    String nowWarehouseId;
    String nowWarehouseName;
    String nowStoreAddressGps;
    BigDecimal residualValue;
    BigDecimal netWorth;
    BigDecimal premiumSale;
    String disposalType;
    String maintType;
    BigDecimal maintFee;
    String maintOutBranchId;
    String maintOutBranchName;
    BigDecimal maintOrignAmountPct;
    Date createDate;
    String requireId;
    String disposeRemark;


    public BigDecimal getAdjustStockNum() {
        return adjustStockNum;
    }

    public void setAdjustStockNum(BigDecimal adjustStockNum) {
        this.adjustStockNum = adjustStockNum;
    }

    public Date getAllocDate() {
        return allocDate;
    }

    public void setAllocDate(Date allocDate) {
        this.allocDate = allocDate;
    }

    public String getNowPsersonLiableUserid() {
        return nowPsersonLiableUserid;
    }

    public void setNowPsersonLiableUserid(String nowPsersonLiableUserid) {
        this.nowPsersonLiableUserid = nowPsersonLiableUserid;
    }

    public String getNowPsersonLiableUsername() {
        return nowPsersonLiableUsername;
    }

    public void setNowPsersonLiableUsername(String nowPsersonLiableUsername) {
        this.nowPsersonLiableUsername = nowPsersonLiableUsername;
    }

    public String getNowDeptid() {
        return nowDeptid;
    }

    public void setNowDeptid(String nowDeptid) {
        this.nowDeptid = nowDeptid;
    }

    public String getNowDeptName() {
        return nowDeptName;
    }

    public void setNowDeptName(String nowDeptName) {
        this.nowDeptName = nowDeptName;
    }

    public String getNowBranchId() {
        return nowBranchId;
    }

    public void setNowBranchId(String nowBranchId) {
        this.nowBranchId = nowBranchId;
    }

    public String getNowBranchName() {
        return nowBranchName;
    }

    public void setNowBranchName(String nowBranchName) {
        this.nowBranchName = nowBranchName;
    }

    public String getNowStoreAddress() {
        return nowStoreAddress;
    }

    public void setNowStoreAddress(String nowStoreAddress) {
        this.nowStoreAddress = nowStoreAddress;
    }

    public String getNowWarehouseId() {
        return nowWarehouseId;
    }

    public void setNowWarehouseId(String nowWarehouseId) {
        this.nowWarehouseId = nowWarehouseId;
    }

    public String getNowWarehouseName() {
        return nowWarehouseName;
    }

    public void setNowWarehouseName(String nowWarehouseName) {
        this.nowWarehouseName = nowWarehouseName;
    }

    public String getNowStoreAddressGps() {
        return nowStoreAddressGps;
    }

    public void setNowStoreAddressGps(String nowStoreAddressGps) {
        this.nowStoreAddressGps = nowStoreAddressGps;
    }

    public BigDecimal getResidualValue() {
        return residualValue;
    }

    public void setResidualValue(BigDecimal residualValue) {
        this.residualValue = residualValue;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public BigDecimal getPremiumSale() {
        return premiumSale;
    }

    public void setPremiumSale(BigDecimal premiumSale) {
        this.premiumSale = premiumSale;
    }

    public String getDisposalType() {
        return disposalType;
    }

    public void setDisposalType(String disposalType) {
        this.disposalType = disposalType;
    }

    public String getMaintType() {
        return maintType;
    }

    public void setMaintType(String maintType) {
        this.maintType = maintType;
    }

    public BigDecimal getMaintFee() {
        return maintFee;
    }

    public void setMaintFee(BigDecimal maintFee) {
        this.maintFee = maintFee;
    }

    public String getMaintOutBranchId() {
        return maintOutBranchId;
    }

    public void setMaintOutBranchId(String maintOutBranchId) {
        this.maintOutBranchId = maintOutBranchId;
    }

    public String getMaintOutBranchName() {
        return maintOutBranchName;
    }

    public void setMaintOutBranchName(String maintOutBranchName) {
        this.maintOutBranchName = maintOutBranchName;
    }

    public BigDecimal getMaintOrignAmountPct() {
        return maintOrignAmountPct;
    }

    public void setMaintOrignAmountPct(BigDecimal maintOrignAmountPct) {
        this.maintOrignAmountPct = maintOrignAmountPct;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getRequireId() {
        return requireId;
    }

    public void setRequireId(String requireId) {
        this.requireId = requireId;
    }

    public String getDisposeRemark() {
        return disposeRemark;
    }

    public void setDisposeRemark(String disposeRemark) {
        this.disposeRemark = disposeRemark;
    }


    public String getAssetStatusAfter() {
        return assetStatusAfter;
    }

    public void setAssetStatusAfter(String assetStatusAfter) {
        this.assetStatusAfter = assetStatusAfter;
    }

    public String getAssetStatusBefore() {
        return assetStatusBefore;
    }

    public void setAssetStatusBefore(String assetStatusBefore) {
        this.assetStatusBefore = assetStatusBefore;
    }
}


