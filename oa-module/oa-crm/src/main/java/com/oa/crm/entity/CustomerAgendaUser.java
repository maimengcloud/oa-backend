package com.oa.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("crm_customer_agenda_user")
@ApiModel(description="crm_customer_agenda_user")
public class CustomerAgendaUser  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="计划接收人id",allowEmptyValue=true,example="",allowableValues="")
	String receiveUserId;

	
	@ApiModelProperty(notes="计划接收人名称",allowEmptyValue=true,example="",allowableValues="")
	String receiveUserName;

	
	@ApiModelProperty(notes="日程id",allowEmptyValue=true,example="",allowableValues="")
	String agendaId;

	/**
	 *id
	 **/
	public CustomerAgendaUser(String id) {
		this.id = id;
	}
    
    /**
     * crm_customer_agenda_user
     **/
	public CustomerAgendaUser() {
	}

}