package com.oa.duban.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.duban.entity.DubanItemTask;
import com.oa.duban.service.DubanItemTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/duban/dubanItemTask")
@Api(tags = {"督办任务表-操作接口"})
public class DubanItemTaskController {

    static Logger logger = LoggerFactory.getLogger(DubanItemTaskController.class);

    @Autowired
    private DubanItemTaskService dubanItemTaskService;

    @ApiOperation(value = "督办任务表-查询列表", notes = " ")
    @ApiEntityParams(DubanItemTask.class)
    @ApiResponses({
            @ApiResponse(code = 200, response = DubanItemTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
    })
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listDubanItemTask(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        if (StringUtils.isEmpty(params.get("branchId"))) {
            String branchId = LoginUtils.getCurrentUserInfo().getBranchId();
            params.put("branchId", branchId);
        }
        if (!StringUtils.isEmpty(params.get("categoryId"))) {
            params.put("res.category_id", params.get("categoryId"));
            params.remove("categoryId");
        }
        if (!StringUtils.isEmpty(params.get("status"))) {
            params.put("res.status", params.get("status"));
            params.remove("status");
        }
        if (!StringUtils.isEmpty(params.get("name"))) {
            params.put("res.name", params.get("name"));
            params.remove("name");
        }
        if (!StringUtils.isEmpty(params.get("startTimeSelectStart"))) {
            Date startTime = new Date(Long.parseLong(params.get("startTimeSelectStart").toString()));
            params.put("startTimeSelectStart", startTime.toString());
        }
        if (!StringUtils.isEmpty(params.get("startTimeSelectEnd"))) {
            Date endTime = new Date(Long.parseLong(params.get("startTimeSelectEnd").toString()));
            params.put("startTimeSelectEnd", endTime.toString());
        }
        if (!StringUtils.isEmpty(params.get("endTimeSelectStart"))) {
            Date startTime = new Date(Long.parseLong(params.get("endTimeSelectStart").toString()));
            params.put("endTimeSelectStart", startTime.toString());
        }
        if (!StringUtils.isEmpty(params.get("endTimeSelectEnd"))) {
            Date endTime = new Date(Long.parseLong(params.get("endTimeSelectEnd").toString()));
            params.put("endTimeSelectEnd", endTime.toString());
        }

        if (!StringUtils.isEmpty(params.get("coopUsers"))) {
            String ids = (String) params.get("coopUsers");
            String[] split = ids.split(",");
            params.put("coopUsers", split);
        }

        RequestUtils.transformArray(params, "ids");

        QueryWrapper<DubanItemTask> qw = QueryTools.initQueryWrapper(DubanItemTask.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = dubanItemTaskService.selectListMapByWhereAndTopQuery(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "督办任务表-新增", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = DubanItemTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addDubanItemTask(@RequestBody DubanItemTask dubanItemTask) {
        dubanItemTaskService.save(dubanItemTask);
        return Result.ok("add-ok", "添加成功！");
    }

    @ApiOperation(value = "督办任务表-删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")
    })
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delDubanItemTask(@RequestBody DubanItemTask dubanItemTask) {
        dubanItemTaskService.removeById(dubanItemTask);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "督办任务表-修改", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = DubanItemTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editDubanItemTask(@RequestBody DubanItemTask dubanItemTask) {
        dubanItemTaskService.updateById(dubanItemTask);
        return Result.ok("edit-ok", "修改成功！");
    }

    @ApiOperation(value = "督办任务表-批量修改某些字段", notes = "")
    @ApiEntityParams(value = DubanItemTask.class, props = {}, remark = "督办任务表", paramType = "body")
    @ApiResponses({
            @ApiResponse(code = 200, response = DubanItemTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        dubanItemTaskService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "督办任务表-批量删除", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")
    })
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelDubanItemTask(@RequestBody List<DubanItemTask> dubanItemTasks) {
        User user = LoginUtils.getCurrentUserInfo();
        if (dubanItemTasks.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<DubanItemTask> datasDb = dubanItemTaskService.listByIds(dubanItemTasks.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<DubanItemTask> can = new ArrayList<>();
        List<DubanItemTask> no = new ArrayList<>();
        for (DubanItemTask data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            dubanItemTaskService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "督办任务表-根据主键查询一条数据", notes = " ")
    @ApiResponses({
            @ApiResponse(code = 200, response = DubanItemTask.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")
    })
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(DubanItemTask dubanItemTask) {
        DubanItemTask data = (DubanItemTask) dubanItemTaskService.getById(dubanItemTask);
        return Result.ok().setData(data);
    }

}
