package  com.oa.common.service;

import java.util.*;
import java.text.SimpleDateFormat;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.mdp.core.utils.BaseUtils;
import org.springframework.beans.factory.annotation.Autowired; 
import  com.oa.common.service.SysUserService;
import  com.oa.common.entity.SysUser;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * @author code-gen
 * @since 2024-10-18
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestSysUserService  {

	@Autowired
	SysUserService sysUserService;

	/**
	 * 新增一条数据
	 ***/
	@Test
	public void insert() {
		Map<String,Object> p=BaseUtils.map("unionid","p54A","displayUserid","ezT9","userid","8yYc","locked","1","startdate",new Date("2024-10-18 9:30:9"),"nickname","Eoz7","username","t0l2","phoneno","wxOI","password","b5jN","salt","1eR5","pwdtype","m","headimgurl","ssvG","country","vPN8","city","8l4F","province","xdR0","address","lhRa","sex","dX7e","enddate",new Date("2024-10-18 9:30:9"),"districtId","ZbP1","email","an44","fgOne","m7B3","fgTwo","a5Ci","fgThr","2e4m","idCardNo","A9Zz","officePhoneno","vQrQ","bizProcInstId","mSzB","bizFlowState","S","memType","s","orgId","AlGY","emailBak","5QBe","pwdStrong","4","lockType","9","lockRemark","D324","ltime",new Date("2024-10-18 9:30:9"),"atype","y","branchId","1LLs","continent","yQP4","cpaType","V8gi","cpaOrg","vGGp","roleids","8wXJ","birthday",new Date("2024-10-18 9:30:9"),"shopId","OdUL","profeId","88VL","profeName","rIwt","gradeId","xrFZ","gradeName","G79L","ilvlId","Z0QU","ilvlName","Jpqp","istatus","Z","istime",new Date("2024-10-18 9:30:9"),"ietime",new Date("2024-10-18 9:30:9"),"validLvls","61eJ","features","Ao6r","profeType","6OUD","ustatus","n","creditId","Tabm","creditScore",8400,"guardId","3","open","8","remark","H3ND","bizHours","BjTd","skillIds","AKx9","skillNames","rHHI","defLogin","N","cpaUserid","8hXn");
		SysUser sysUser=BaseUtils.fromMap(p,SysUser.class);
		sysUserService.save(sysUser);
		//Assert.assertEquals(1, result);
	}
	 
}
