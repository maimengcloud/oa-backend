package com.oa.ht.entity;

import java.util.List;

public class ContractCbBatchVo {

    String htId;
    List<ContractCb> contractCbs;


    public String getHtId() {
        return htId;
    }

    public void setHtId(String htId) {
        this.htId = htId;
    }

    public List<ContractCb> getContractCbs() {
        return contractCbs;
    }

    public void setContractCbs(List<ContractCb> contractCbs) {
        this.contractCbs = contractCbs;
    }
}
