package com.oa.erp.ass.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author code-gen
 * @since 2023年10月8日
 */
@Data
@TableName("ass_asset_card_oper")
@ApiModel(description="ass_asset_card_oper")
public class AssetCardOper  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@ApiModelProperty(notes="主键,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	
	@ApiModelProperty(notes="卡片主键Id",allowEmptyValue=true,example="",allowableValues="")
	String cardId;

	
	@ApiModelProperty(notes="资产编码",allowEmptyValue=true,example="",allowableValues="")
	String assetSn;

	
	@ApiModelProperty(notes="操作后资产状态卡片状态0闲置1在用2借用4报废5待领取",allowEmptyValue=true,example="",allowableValues="")
	String assetStatusAfter;

	
	@ApiModelProperty(notes="操作前资产状态卡片状态0闲置1在用2借用4报废5待领取",allowEmptyValue=true,example="",allowableValues="")
	String assetStatusBefore;

	
	@ApiModelProperty(notes="单据编号",allowEmptyValue=true,example="",allowableValues="")
	String assetMngId;

	
	@ApiModelProperty(notes="调整库存数量",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal adjustStockNum;

	
	@ApiModelProperty(notes="调拨日期",allowEmptyValue=true,example="",allowableValues="")
	Date allocDate;

	
	@ApiModelProperty(notes="新责任人",allowEmptyValue=true,example="",allowableValues="")
	String nowPsersonLiableUserid;

	
	@ApiModelProperty(notes="新责任人名称",allowEmptyValue=true,example="",allowableValues="")
	String nowPsersonLiableUsername;

	
	@ApiModelProperty(notes="新使用部门编号",allowEmptyValue=true,example="",allowableValues="")
	String nowDeptid;

	
	@ApiModelProperty(notes="新使用部门名称",allowEmptyValue=true,example="",allowableValues="")
	String nowDeptName;

	
	@ApiModelProperty(notes="新机构编号",allowEmptyValue=true,example="",allowableValues="")
	String nowBranchId;

	
	@ApiModelProperty(notes="新机构名称",allowEmptyValue=true,example="",allowableValues="")
	String nowBranchName;

	
	@ApiModelProperty(notes="新地点",allowEmptyValue=true,example="",allowableValues="")
	String nowStoreAddress;

	
	@ApiModelProperty(notes="仓库编号",allowEmptyValue=true,example="",allowableValues="")
	String nowWarehouseId;

	
	@ApiModelProperty(notes="仓库名称",allowEmptyValue=true,example="",allowableValues="")
	String nowWarehouseName;

	
	@ApiModelProperty(notes="新地点定位",allowEmptyValue=true,example="",allowableValues="")
	String nowStoreAddressGps;

	
	@ApiModelProperty(notes="资产处置单-残值",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal residualValue;

	
	@ApiModelProperty(notes="资产处置单-净值",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal netWorth;

	
	@ApiModelProperty(notes="资产出直单-报废变卖值",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal premiumSale;

	
	@ApiModelProperty(notes="资产处置单-处置方式0出让1转让2出售3报废4报损5置换6盘亏7拍卖8捐赠",allowEmptyValue=true,example="",allowableValues="")
	String disposalType;

	
	@ApiModelProperty(notes="维修方式",allowEmptyValue=true,example="",allowableValues="")
	String maintType;

	
	@ApiModelProperty(notes="维修费用",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal maintFee;

	
	@ApiModelProperty(notes="外部维修单位",allowEmptyValue=true,example="",allowableValues="")
	String maintOutBranchId;

	
	@ApiModelProperty(notes="外部维修单位名称",allowEmptyValue=true,example="",allowableValues="")
	String maintOutBranchName;

	
	@ApiModelProperty(notes="占原值百分比",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal maintOrignAmountPct;

	
	@ApiModelProperty(notes="创建日期",allowEmptyValue=true,example="",allowableValues="")
	Date createDate;

	
	@ApiModelProperty(notes="ass_asset_mng操作单编号",allowEmptyValue=true,example="",allowableValues="")
	String requireId;

	
	@ApiModelProperty(notes="资产处置备注文本",allowEmptyValue=true,example="",allowableValues="")
	String disposeRemark;

	
	@ApiModelProperty(notes="老的责任人",allowEmptyValue=true,example="",allowableValues="")
	String oldPsersonLiableUserid;

	
	@ApiModelProperty(notes="老的责任人名称",allowEmptyValue=true,example="",allowableValues="")
	String oldPsersonLiableUsername;

	
	@ApiModelProperty(notes="老的使用部门编号",allowEmptyValue=true,example="",allowableValues="")
	String oldDeptid;

	
	@ApiModelProperty(notes="老的使用部门名称",allowEmptyValue=true,example="",allowableValues="")
	String oldDeptName;

	
	@ApiModelProperty(notes="老的机构编号",allowEmptyValue=true,example="",allowableValues="")
	String oldBranchId;

	
	@ApiModelProperty(notes="老的机构名称",allowEmptyValue=true,example="",allowableValues="")
	String oldBranchName;

	
	@ApiModelProperty(notes="老的地点",allowEmptyValue=true,example="",allowableValues="")
	String oldStoreAddress;

	
	@ApiModelProperty(notes="老的仓库编号",allowEmptyValue=true,example="",allowableValues="")
	String oldWarehouseId;

	
	@ApiModelProperty(notes="老的仓库名称",allowEmptyValue=true,example="",allowableValues="")
	String oldWarehouseName;

	
	@ApiModelProperty(notes="老的新地点定位",allowEmptyValue=true,example="",allowableValues="")
	String oldStoreAddressGps;

	
	@ApiModelProperty(notes="资产编号",allowEmptyValue=true,example="",allowableValues="")
	String detailId;

	
	@ApiModelProperty(notes="操作方式lease租用reallocation调拨maint维修use领用return归还change变更dispose处置inventory盘点",allowEmptyValue=true,example="",allowableValues="")
	String opType;

	
	@ApiModelProperty(notes="成本中心编号",allowEmptyValue=true,example="",allowableValues="")
	String cbCenterId;

	
	@ApiModelProperty(notes="成本中心名称",allowEmptyValue=true,example="",allowableValues="")
	String cbCenterName;

	
	@ApiModelProperty(notes="对应流程实例ID",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;

	
	@ApiModelProperty(notes="流程业务key",allowEmptyValue=true,example="",allowableValues="")
	String bizKey;

	
	@ApiModelProperty(notes="最后审批意见",allowEmptyValue=true,example="",allowableValues="")
	String commentMsg;

	
	@ApiModelProperty(notes="最后审批人",allowEmptyValue=true,example="",allowableValues="")
	String assignee;

	
	@ApiModelProperty(notes="最后审批人名称",allowEmptyValue=true,example="",allowableValues="")
	String assigneeName;

	
	@ApiModelProperty(notes="最后审批意见0不同意1同意",allowEmptyValue=true,example="",allowableValues="")
	String agree;

	
	@ApiModelProperty(notes="流程最后更新时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowLastTime;

	
	@ApiModelProperty(notes="流程结束时间",allowEmptyValue=true,example="",allowableValues="")
	Date flowEndTime;

	
	@ApiModelProperty(notes="流程当前节点编号",allowEmptyValue=true,example="",allowableValues="")
	String actId;

	
	@ApiModelProperty(notes="当前任务名称",allowEmptyValue=true,example="",allowableValues="")
	String taskName;

	
	@ApiModelProperty(notes="0初始1审批中2审批通过3审批不通过4流程取消或者删除",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;

	/**
	 *主键
	 **/
	public AssetCardOper(String id) {
		this.id = id;
	}
    
    /**
     * ass_asset_card_oper
     **/
	public AssetCardOper() {
	}

}