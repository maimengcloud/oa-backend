package com.oa.ht.vo;

import com.google.common.collect.Lists;
import com.oa.ht.entity.Counterparty;
import com.oa.ht.entity.CounterpartyLinkman;

import java.util.List;

public class CounterpartyAddVo extends Counterparty {

    private List<CounterpartyLinkman> linkmanList = Lists.newArrayList();

    public List<CounterpartyLinkman> getLinkmanList() {
        return linkmanList;
    }

    public void setLinkmanList(List<CounterpartyLinkman> linkmanList) {
        this.linkmanList = linkmanList;
    }

}
