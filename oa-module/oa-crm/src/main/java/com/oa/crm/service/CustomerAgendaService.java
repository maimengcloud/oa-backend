package com.oa.crm.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.crm.entity.Customer;
import com.oa.crm.entity.CustomerAgenda;
import com.oa.crm.entity.CustomerAgendaDoc;
import com.oa.crm.entity.CustomerAgendaUser;
import com.oa.crm.mapper.CustomerAgendaMapper;
import com.oa.crm.vo.CustomerAgendaAddVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class CustomerAgendaService extends BaseService<CustomerAgendaMapper, CustomerAgenda> {
    static Logger logger = LoggerFactory.getLogger(CustomerAgendaService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }


    @Autowired
    private CustomerAgendaDocService customerAgendaDocService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerAgendaUserService customerAgendaUserService;

    /**
     * 添加客户日程
     *
     * @param customerAgendaAddVo
     */
    @Transactional
    public void insertCustomerAgenda(CustomerAgendaAddVo customerAgendaAddVo) {

        User user = LoginUtils.getCurrentUserInfo();

        //1.添加日程
        CustomerAgenda customerAgenda = customerAgendaAddVo.getCustomerAgenda();

        //设置创建人信息
        customerAgenda.setAgendaCreateId(user.getUserid());
        customerAgenda.setAgendaCreateImg(user.getHeadimgurl());
        customerAgenda.setAgendaCreateName(user.getUsername());
        customerAgenda.setAgendaCreateTime(new Date());
        customerAgenda.setBranchId(user.getBranchId());

        //如果等于客户联系的话,那么就需要自己设置联系的标题,
        if ("0".equals(customerAgenda.getIsPlan())) {
            customerAgenda.setAgendaText("客户联系 - " + customerAgenda.getCustomerName());
            customerAgenda.setUrgentLevel("正常");
            customerAgenda.setUrgentLevelName("正常");
        }

        this.insert(customerAgenda);

        //2.如果有文档，则添加文档
        List<CustomerAgendaDoc> customerAgendaDocs = customerAgendaAddVo.getCustomerAgendaDocList();
        if (!CollectionUtils.isEmpty(customerAgendaDocs)) {
            for (CustomerAgendaDoc customerAgendaDoc : customerAgendaDocs) {
                customerAgendaDoc.setId(customerAgendaDocService.createKey("id"));
                customerAgendaDoc.setAgendaId(customerAgenda.getAgendaId());
            }
            customerAgendaDocService.batchInsert(customerAgendaDocs);
        }


        //3.如果有接收用户则添加接收用户
        List<CustomerAgendaUser> customerAgendaUsers = customerAgendaAddVo.getCustomerAgendaUsers();
        if (!CollectionUtils.isEmpty(customerAgendaUsers)) {
            for (CustomerAgendaUser customerAgendaUser : customerAgendaUsers) {
                customerAgendaUser.setId(customerAgendaUserService.createKey("id"));
                customerAgendaUser.setAgendaId(customerAgenda.getAgendaId());
            }
            customerAgendaUserService.batchInsert(customerAgendaUsers);
        }

        //3.更新客户的最后联系时间为当前时间
//		Map<String, Object> updateParam = new HashMap<>();
//		updateParam.put("customerId", customerAgenda.getCustomerId());
//		updateParam.put("lastContactTime", new Date());
//		customerService.update("updateLastContactTime", updateParam);
        Customer customer = new Customer();
        customer.setLastContactTime(new Date());
        customer.setCustomerId(customer.getCustomerId());
        customerService.saveOrUpdate(customer);
    }

    /**
     * 查询日程相关的doc记录
     *
     * @param customerAgendaList
     */
    public List<Map<String, Object>> selectAgendaDoc(List<Map<String, Object>> customerAgendaList) {
        //1.获取agentId
        List<String> agendaIds = new ArrayList<>();
        for (Map<String, Object> stringObjectMap : customerAgendaList) {
            agendaIds.add((String) stringObjectMap.get("agendaId"));
        }

        if (agendaIds.isEmpty()) return customerAgendaList;

        //查询相关联的doc
//        List<CustomerAgendaDoc> customerAgendaDocs = customerAgendaDocService.selectList("selectDocByAgenda", agendaIds);
        List<CustomerAgendaDoc> customerAgendaDocs = customerAgendaDocService.selectDocByAgend(agendaIds);

        if (CollectionUtils.isEmpty(customerAgendaDocs)) return customerAgendaList;

        //修改原数据
        for (Map<String, Object> stringObjectMap : customerAgendaList) {
            List<Map<String, Object>> docList = new ArrayList<>();
            for (CustomerAgendaDoc customerAgendaDoc : customerAgendaDocs) {
                if (stringObjectMap.get("agendaId").equals(customerAgendaDoc.getAgendaId())) {
                    Map<String, Object> docMap = new HashMap<>();
                    docMap.put("id", customerAgendaDoc.getId());
                    docMap.put("docId", customerAgendaDoc.getDocId());
                    docMap.put("docName", customerAgendaDoc.getDocName());
                    docMap.put("docUrl", customerAgendaDoc.getDocUrl());
                    docMap.put("docType", customerAgendaDoc.getDocType());
                    docList.add(docMap);
                }
            }
            stringObjectMap.put("docList", docList);
        }

        return customerAgendaList;
    }
}

