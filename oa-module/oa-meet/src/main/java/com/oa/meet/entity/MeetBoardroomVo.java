package com.oa.meet.entity;

import io.swagger.annotations.ApiModel;

import java.util.List;

/**
 * 组织 com.qqkj  顶级模块 oa 大模块 meet  小模块 <br> 
 * 实体 MeetBoardroom所有属性名: <br>
 *	id,name,typeId,typeName,phone,number,device,remark,status,branchId;<br>
 * 表 OA.meet_boardroom meet_boardroom的所有字段名: <br>
 *	id,name,type_id,type_name,phone,number,device,remark,status,branch_id;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
@ApiModel(description="meet_boardroom")
public class MeetBoardroomVo extends MeetBoardroom {

	List<MeetBoardroomDevice> devices;

	List<MeetBoardroomAppendix> appendixList;

	public List<MeetBoardroomDevice> getDevices() {
		return devices;
	}

	public void setDevices(List<MeetBoardroomDevice> devices) {
		this.devices = devices;
	}

	public List<MeetBoardroomAppendix> getAppendixList() {
		return appendixList;
	}

	public void setAppendixList(List<MeetBoardroomAppendix> appendixList) {
		this.appendixList = appendixList;
	}
}