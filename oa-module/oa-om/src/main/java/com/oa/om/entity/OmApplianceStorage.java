package  com.oa.om.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import com.mdp.core.dao.annotation.TableIds;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 唛盟开源 code-gen
 * @since 2024-8-22
 */
@Data
@TableName("om_appliance_storage")
@Schema(description="办公用品出入库流水表")
public class OmApplianceStorage  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.ASSIGN_ID)
	
	@Schema(description="库存流水编号,主键")
	String id;

	
	@Schema(description="用品id")
	String applianceId;

	
	@Schema(description="用品名称")
	String applianceName;

	
	@Schema(description="规格")
	String standard;

	
	@Schema(description="单价")
	BigDecimal price;

	
	@Schema(description="入库数量")
	BigDecimal num;

	
	@Schema(description="金额")
	BigDecimal otalPrice;

	
	@Schema(description="操作人id")
	String userid;

	
	@Schema(description="操作人名字")
	String username;

	
	@Schema(description="出入库日期")
	Date createTime;

	
	@Schema(description="备注")
	String remark;

	
	@Schema(description="供应商id")
	String supplierId;

	
	@Schema(description="供应商")
	String supplierName;

	
	@Schema(description="机构号")
	String branchId;

	
	@Schema(description="当前流程状态0初始1审批中2审批通过3审批不通过4流程取消或者删除")
	String bizFlowState;

	
	@Schema(description="审批流实例编号")
	String bizProcInstId;

	
	@Schema(description="最后更新日期")
	Date ltime;

	
	@Schema(description="出入库方向2-出库，1-入库")
	String direct;

	
	@Schema(description="单位")
	String unit;

	
	@Schema(description="物品流向最终用户，出库的话领取人，入库仓管员")
	String targetUserid;

	
	@Schema(description="物品流向最终用户，出库的话领取人，入库的话仓管员")
	String targetUsername;

	
	@Schema(description="物品从谁那里获得，出库仓管员，入库归还人")
	String fromUserid;

	
	@Schema(description="物品从谁那里获得，出库仓管员，入库归还人")
	String fromUsername;

	
	@Schema(description="单据类型")
	String billType;

	
	@Schema(description="单据编号")
	String billId;

	/**
	 *库存流水编号
	 **/
	public OmApplianceStorage(String id) {
		this.id = id;
	}
    
    /**
     * 办公用品出入库流水表
     **/
	public OmApplianceStorage() {
	}

}