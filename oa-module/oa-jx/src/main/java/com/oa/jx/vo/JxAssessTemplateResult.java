package com.oa.jx.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JxAssessTemplateResult {
    private String name;
    private String type;
    private List<Map<String,Object>> jxAssessTemplates = new ArrayList<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Map<String, Object>> getJxAssessTemplates() {
        return jxAssessTemplates;
    }

    public void setJxAssessTemplates(List<Map<String, Object>> jxAssessTemplates) {
        this.jxAssessTemplates = jxAssessTemplates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
