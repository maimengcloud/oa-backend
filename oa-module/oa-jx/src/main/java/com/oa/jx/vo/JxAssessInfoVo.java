package com.oa.jx.vo;

import com.oa.jx.entity.JxAssessGrade;

import java.util.List;
import java.util.Map;

public class JxAssessInfoVo extends JxAssessGrade {

    private List<Map<String,Object>> contents ;

    /**
     * selfConfirm-员工确认->待主管确认
     * reConfirm-主管确认->待自评
     * rejectReConfirm-主管拒绝，待员工重新确认
     * selfEvalConfirm-自评提交->待复评
     * reEvalConfirm-复评提交->待核定
     * vouchEvalConfirm-核定提交->已核定
     */
    private String action="save";

    public List<Map<String, Object>> getContents() {
        return contents;
    }
    public void setContents(List<Map<String, Object>> contents) {
        this.contents = contents;
    }
    /**
     * selfConfirm-员工确认->待主管确认
     * reConfirm-主管确认->待自评
     * rejectReConfirm-主管拒绝，待员工重新确认
     * selfEvalConfirm-自评提交->待复评
     * reEvalConfirm-复评提交->待核定
     * vouchEvalConfirm-核定提交->已核定
     */
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
