package com.oa.kq.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.Tips;
import com.mdp.core.service.BaseService;
import com.mdp.core.utils.NumberUtil;
import com.oa.common.entity.UserBaseInfo;
import com.oa.kq.entity.VacationType;
import com.oa.kq.mapper.VacationTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class VacationTypeService extends BaseService<VacationTypeMapper,VacationType> {
	static Logger logger =LoggerFactory.getLogger(VacationTypeService.class);

	/**
	 * 自定义查询，支持多表关联
	 * @param page 分页条件
	 * @param ew 一定要，并且必须加@Param("ew")注解
	 * @param ext 如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
	 * @return
	 */
	public List<Map<String,Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String,Object> ext){
		return baseMapper.selectListMapByWhere(page,ew,ext);
	}


	/** 请在此类添加自定义函数 */

	public Tips checkEmpEffectKqVacationType(VacationType kqVacationType, UserBaseInfo user, int year){
		Tips tips = new Tips("成功");

		boolean sexRuleOk=true;
		if("0".equals(kqVacationType.getSexRule())){
			if(!"0".equals(user.getSex())){
				sexRuleOk=false;
			}
		}else if("1".equals(kqVacationType.getSexRule())){
			if(!"1".equals(user.getSex())){
				sexRuleOk=false;
			}
		}
		if(sexRuleOk==false){
			tips.setErrMsg("sex-not-right","不符合性别要求");
			return tips;
		}

		boolean empMonthsCheck=true;
		if(kqVacationType.getBeginEmpMonths()!=null&&kqVacationType.getEndEmpMonths()!=null && kqVacationType.getEndEmpMonths()>0){
			Integer beginEmpMonths= NumberUtil.getInteger(kqVacationType.getBeginEmpMonths(),0);
			Integer endEmpMonths=NumberUtil.getInteger(kqVacationType.getEndEmpMonths(),0);

			long empMonths=getEmpMonths(user.getStartTime(),year);
			Integer enterpriseYears= NumberUtil.getInteger(user.getEnterpriseYears(),0);
			if(enterpriseYears*12>empMonths){
				empMonths=enterpriseYears*12;
			}
			if(!(empMonths>=beginEmpMonths && empMonths<endEmpMonths)){
				empMonthsCheck=false;
			}
		}
		if(empMonthsCheck==false){
			tips.setErrMsg("empMonths-not-right","在企业服务年限不满足要求");
			return tips;
		}
		boolean workingMonthsCheck=true;
		if(kqVacationType.getBeginWorkingMonths()!=null&&kqVacationType.getEndWorkingMonths()!=null && kqVacationType.getEndWorkingMonths()>0){
			Integer beginWorkingMonths=NumberUtil.getInteger(kqVacationType.getBeginWorkingMonths(),0);
			Integer endWorkingMonths=NumberUtil.getInteger(kqVacationType.getEndWorkingMonths(),0);

			long workingMonths=0;

			long empMonths=getEmpMonths(user.getStartTime(),year);
			Integer enterpriseYears= NumberUtil.getInteger(user.getEnterpriseYears(),0);
			if(enterpriseYears*12>empMonths){
				empMonths=enterpriseYears*12;
			}
			workingMonths=empMonths;
			Integer workingYears= NumberUtil.getInteger(user.getWorkingYears(),0);
			if(empMonths<workingYears*12){
				workingMonths=workingYears*12;
			}
			if(!(workingMonths>=beginWorkingMonths && workingMonths<endWorkingMonths)){
				workingMonthsCheck=false;
			}
		}
		if(workingMonthsCheck==false){
			tips.setErrMsg("workingMonths-not-right","工龄年限不满足要求");
			return tips;
		}
		return tips;

	}

	public BigDecimal calcEmpEffectHours(VacationType kqVacationType, UserBaseInfo user, int year){
		Tips tips = this.checkEmpEffectKqVacationType(kqVacationType,user,year);
		if(tips.isOk()){
			return kqVacationType.getTotalHours();
		}else{
			return BigDecimal.ZERO;
		}
	}


	public Long getEmpMonths(Date beginWorkDate, int year){
		Calendar cStart=Calendar.getInstance();
		cStart.setTime(beginWorkDate);

		Calendar cEnd=Calendar.getInstance();
		cEnd.set(year,12,31);

		long millis=cEnd.getTimeInMillis()-cStart.getTimeInMillis();
		long months=millis/30/24/60/60/1000;
		return months;
	}
}

