package com.oa.om.ctrl;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import com.mdp.core.utils.NumberUtil;
import com.mdp.core.utils.ObjectTools;
import com.oa.om.OmTools;
import com.oa.om.entity.OmApplianceStorage;
import com.oa.om.service.OmApplianceService;
import com.oa.om.service.OmApplianceStorageService;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import static com.mdp.core.utils.BaseUtils.*;
import com.mdp.core.entity.LangTips;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;

import com.oa.om.service.OmAppliancePurchaseService;
import com.oa.om.entity.OmAppliancePurchase;
/**
 * @author 唛盟开源 code-gen
 * @since 2024-8-21
 */
@RestController
@RequestMapping(value="/oa/om/omAppliancePurchase")
@Api(tags={"办公用品采购表-操作接口"})
public class OmAppliancePurchaseController {
	
	static Logger logger =LoggerFactory.getLogger(OmAppliancePurchaseController.class);
	
	@Autowired
	private OmAppliancePurchaseService omAppliancePurchaseService;


    @Autowired
    private OmApplianceStorageService omApplianceStorageService;

	@Operation( summary =  "办公用品采购表-查询列表")
	@ApiEntityParams(OmAppliancePurchase.class)
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public Result listOmAppliancePurchase(  @RequestParam Map<String,Object> params){
			User user=LoginUtils.getCurrentUserInfo();
			QueryWrapper<OmAppliancePurchase> qw = QueryTools.initQueryWrapper(OmAppliancePurchase.class , params);
			IPage page=QueryTools.initPage(params);
			List<Map<String,Object>> datas = omAppliancePurchaseService.selectListMapByWhere(page,qw,params);
			return Result.ok("query-ok","查询成功").setData(datas).setTotal(page.getTotal());
	}
	

	@Operation( summary =  "办公用品采购表-新增")
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result addOmAppliancePurchase(@RequestBody OmAppliancePurchase omAppliancePurchase) {
        if(ObjectTools.isEmpty(omAppliancePurchase.getPurchaseNum())){
            omAppliancePurchase.setPurchaseNum(BigDecimal.ZERO);
        }
		 omAppliancePurchaseService.save(omAppliancePurchase);
         return Result.ok("add-ok","添加成功！").setData(omAppliancePurchase);
	}

	@Operation( summary =  "办公用品采购表-删除")
	@RequestMapping(value="/del",method=RequestMethod.POST)
	public Result delOmAppliancePurchase(@RequestBody OmAppliancePurchase omAppliancePurchase){
		omAppliancePurchaseService.removeById(omAppliancePurchase);
        return Result.ok("del-ok","删除成功！");
	}

	@Operation( summary =  "办公用品采购表-修改")
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public Result editOmAppliancePurchase(@RequestBody OmAppliancePurchase omAppliancePurchase) {
		omAppliancePurchaseService.updateById(omAppliancePurchase);
        return Result.ok("edit-ok","修改成功！");
	}

    @Operation( summary =  "办公用品采购表-批量修改某些字段" )
    @ApiEntityParams( value = OmAppliancePurchase.class, props={ }, remark = "办公用品采购表", paramType = "body" )
	@RequestMapping(value="/editSomeFields",method=RequestMethod.POST)
	public Result editSomeFields(   @RequestBody Map<String,Object> params) {
            User user= LoginUtils.getCurrentUserInfo();
            List<String> pks= (List<String>) params.get("$pks");
        List<OmAppliancePurchase> dbList=this.omAppliancePurchaseService.listByIds(pks);
        for (OmAppliancePurchase row : dbList) {
             OmTools.validBizFlowStateIs(params,row.getBizFlowState());
        }

                omAppliancePurchaseService.editSomeFields(params);
            return Result.ok("edit-ok","更新成功");
	}

	@Operation( summary =  "办公用品采购表-批量删除")
	@RequestMapping(value="/batchDel",method=RequestMethod.POST)
	public Result batchDelOmAppliancePurchase(@RequestBody List<OmAppliancePurchase> omAppliancePurchases) {
	    User user= LoginUtils.getCurrentUserInfo();
        if(omAppliancePurchases.size()<=0){
            return Result.error("batchDel-data-err-0","请上送待删除数据列表");
        }
         List<OmAppliancePurchase> datasDb=omAppliancePurchaseService.listByIds(omAppliancePurchases.stream().map(i-> i.getId() ).collect(Collectors.toList()));

        List<OmAppliancePurchase> can=new ArrayList<>();
        List<OmAppliancePurchase> no=new ArrayList<>();
        for (OmAppliancePurchase data : datasDb) {
            if(true){
                can.add(data);
            }else{
                no.add(data);
            }
        }
        List<String> msgs=new ArrayList<>();
        if(can.size()>0){
            omAppliancePurchaseService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num","成功删除%s条数据.",can.size()));
        }

        if(no.size()>0){
            msgs.add(LangTips.transMsg("not-allow-del-num","以下%s条数据不能删除:【%s】",no.size(),no.stream().map(i-> i.getId() ).collect(Collectors.joining(","))));
        }
        if(can.size()>0){
             return Result.ok(msgs.stream().collect(Collectors.joining()));
        }else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
	} 

	@Operation( summary =  "办公用品采购表-根据主键查询一条数据")
    @RequestMapping(value="/queryById",method=RequestMethod.GET)
    public Result queryById(OmAppliancePurchase omAppliancePurchase) {
        OmAppliancePurchase data = (OmAppliancePurchase) omAppliancePurchaseService.getById(omAppliancePurchase);
        return Result.ok().setData(data);
    }

}
