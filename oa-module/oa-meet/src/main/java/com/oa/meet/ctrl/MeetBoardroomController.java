package com.oa.meet.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.meet.entity.MeetBoardroom;
import com.oa.meet.entity.MeetBoardroomVo;
import com.oa.meet.service.MeetBoardroomService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@RestController
@RequestMapping(value="/*/oa/meet/meetBoardroom")
@Api(tags = {"会议室-操作接口"})
public class MeetBoardroomController {

    static Logger logger = LoggerFactory.getLogger(MeetBoardroomController.class);

    @Autowired
    private MeetBoardroomService meetBoardroomService;

    @ApiOperation(value = "会议室-查询列表", notes = " ")
    @ApiEntityParams(MeetBoardroom.class)
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroom.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listMeetBoardroom(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");
        QueryWrapper<MeetBoardroom> qw = QueryTools.initQueryWrapper(MeetBoardroom.class, params);
        IPage page = QueryTools.initPage(params);

        List<Map<String, Object>> datas = meetBoardroomService.selectListMapByWhere(page, qw, params);
        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "会议室-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroom.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addMeetBoardroom(@RequestBody MeetBoardroomVo meetBoardroomVo) {


        
            meetBoardroomService.save(meetBoardroomVo);
        
        return Result.ok("add-ok", "添加成功！").setData(meetBoardroomVo);
    }

    @ApiOperation(value = "查询meet_boardroom信息", notes = "条件之间是 and关系,模糊查询写法如 {studentName:'%才哥%'}")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键,主键", required = false), @ApiImplicitParam(name = "name", value = "会议室名称", required = false), @ApiImplicitParam(name = "typeId", value = "会议室类型id", required = false), @ApiImplicitParam(name = "typeName", value = "会议室类型", required = false), @ApiImplicitParam(name = "phone", value = "会议室电话", required = false), @ApiImplicitParam(name = "number", value = "会议人数", required = false), @ApiImplicitParam(name = "device", value = "会议设备，多项用逗号分隔", required = false), @ApiImplicitParam(name = "remark", value = "备注", required = false), @ApiImplicitParam(name = "status", value = "状态1启用0禁用", required = false), @ApiImplicitParam(name = "branchId", value = "归属机构", required = false), @ApiImplicitParam(name = "pageSize", value = "每页记录数", required = false), @ApiImplicitParam(name = "currentPage", value = "当前页码,从1开始", required = false), @ApiImplicitParam(name = "total", value = "总记录数,服务器端收到0时，会自动计算总记录数，如果上传>0的不自动计算", required = false), @ApiImplicitParam(name = "orderFields", value = "排序列 如性别、学生编号排序 ['sex','studentId']", required = false), @ApiImplicitParam(name = "orderDirs", value = "排序方式,与orderFields对应，升序 asc,降序desc 如 性别 升序、学生编号降序 ['asc','desc']", required = false)})
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroom.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},pageInfo:{total:总记录数},data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/getMeetBoardroom", method = RequestMethod.GET)
    public Result getMeetBoardroom(@RequestParam Map<String, Object> meetBoardroom) {
        MeetBoardroomVo meetBoardroomVo = meetBoardroomService.getMeetBoardroom(meetBoardroom);    //列出MeetBoardroom列表

        return Result.ok().setData(meetBoardroomVo);
    }

    @ApiOperation(value = "会议室-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delMeetBoardroom(@RequestBody MeetBoardroom meetBoardroom) {

        
            meetBoardroomService.removeMeetBoardroom(meetBoardroom);
        
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "会议室-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroom.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editMeetBoardroom(@RequestBody MeetBoardroomVo meetBoardroomVo) {

        
            //meetBoardroomService.updateByPk(meetBoardroom);
            Integer num = meetBoardroomService.editMeetBoardroom(meetBoardroomVo);
        
        return Result.ok("edit-ok", "修改成功！").setData(meetBoardroomVo);
    }

    @ApiOperation(value = "会议室-批量修改某些字段", notes = "")
    @ApiEntityParams(value = MeetBoardroom.class, props = {}, remark = "会议室", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroom.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        meetBoardroomService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "会议室-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelMeetBoardroom(@RequestBody List<MeetBoardroom> meetBoardrooms) {
        User user = LoginUtils.getCurrentUserInfo();
        if (meetBoardrooms.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<MeetBoardroom> datasDb = meetBoardroomService.listByIds(meetBoardrooms.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<MeetBoardroom> can = new ArrayList<>();
        List<MeetBoardroom> no = new ArrayList<>();
        for (MeetBoardroom data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            meetBoardroomService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "会议室-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = MeetBoardroom.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(MeetBoardroom meetBoardroom) {
        MeetBoardroom data = (MeetBoardroom) meetBoardroomService.getById(meetBoardroom);
        return Result.ok().setData(data);
    }

}
