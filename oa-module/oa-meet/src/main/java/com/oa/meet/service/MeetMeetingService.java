package com.oa.meet.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.Tips;
import com.mdp.core.err.BizException;
import com.mdp.core.service.BaseService;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.oa.meet.entity.*;
import com.oa.meet.mapper.MeetMeetingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月9日
 */
@Service
public class MeetMeetingService extends BaseService<MeetMeetingMapper, MeetMeeting> {
    static Logger logger = LoggerFactory.getLogger(MeetMeetingService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }


    @Autowired
    MeetMeetingExecService meetMeetingExecService;
    @Autowired
    MeetMeetingExecAttenderService meetMeetingExecAttenderService;
    @Autowired
    MeetMeetingExecAppendixService meetMeetingExecAppendixService;
    @Autowired
    MeetMeetingExecRecordService meetMeetingExecRecordService;
    @Autowired
    MeetMeetingAttenderService meetMeetingAttenderService;
    @Autowired
    MeetMeetingAppendixService meetMeetingAppendixService;


    /** 请在此类添加自定义函数 */
    /**
     * 定时器
     */
    @Scheduled(cron = "0 0/10 0 * * *")
    public void createMeetMeetingTime() {
        this.insertNewMeetMeetingExec();
    }

    void insertNewMeetMeetingExec() {
        Map<String, Object> params = new HashMap<>();
//		List<MeetMeetingVo> meetMeetingVoList = meetMeetingService.selectList("selectMeetMeeting", params);
        List<MeetMeetingVo> meetMeetingVoList = baseMapper.selectMeetMeeting(params);
        if (meetMeetingVoList == null || meetMeetingVoList.size() == 0) {
            //do nothing
        } else {
            for (MeetMeetingVo meetMeetingVo : meetMeetingVoList) {
                MeetMeeting meetMeeting = new MeetMeeting();
                BeanUtils.copyProperties(meetMeetingVo, meetMeeting);
                String[] circcleDates = new String[0];
                meetMeeting.setExecCalcDate(new Date());
                if ("date".equals(meetMeeting.getCircleType())) {
                    circcleDates = meetMeeting.getCirccleDates().split(",");
                    circcleDates = this.sore(circcleDates);
                } else {
                    try {
                        circcleDates = this.setCirccleDates(meetMeeting);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    circcleDates = this.sore(circcleDates);
                    Date nextUpdateTime = this.setNextUpdateTime(meetMeeting, circcleDates[0]);
                    meetMeeting.setNextUpdateTime(nextUpdateTime);
                }
                List<MeetMeetingExec> execs = new ArrayList<>();
                List<MeetMeetingExecAppendix> execscAppendix = new ArrayList<>();
                List<MeetMeetingExecAttender> execscAttender = new ArrayList<>();
                List<MeetMeetingAppendix> appendixList = meetMeetingVo.getMeetMeetingAppendix();
                List<MeetMeetingAttender> meetMeetingAttenderList = meetMeetingVo.getAttenders();

                for (String circcleDate : circcleDates) {
                    MeetMeetingExec meetMeetingExec = this.setMeetMeetingExec(meetMeeting, circcleDate);
                    if (meetMeetingAttenderList.size() > 0) {
                        meetMeetingAttenderList.forEach((meetMeetingAttender) -> {
                            MeetMeetingExecAttender meetMeetingExecAttender = this.setMeetMeetingExecAttender(meetMeetingAttender, meetMeeting, meetMeetingExec);
                            execscAttender.add(meetMeetingExecAttender);
                        });
                    }
                    if (!appendixList.isEmpty()) {
                        appendixList.forEach(appendix -> {
                            MeetMeetingExecAppendix meetMeetingExecAppendix = this.setMeetMeetingExecAppendix(meetMeeting, circcleDate, appendix, meetMeetingExec);
                            execscAppendix.add(meetMeetingExecAppendix);
                        });
                    }
                    execs.add(meetMeetingExec);
                }
//				int i = meetMeetingService.update("upDateMeetMeeting", meetMeeting);
                int i = baseMapper.upDateMeetMeeting(meetMeeting);
                if (i > 0) {
                    meetMeetingExecAttenderService.batchInsert(execscAttender);
//					meetMeetingAppendixService.batchInsert(execsc);

                    List<MeetMeetingAppendix> meetMeetingAppendixList = execscAppendix.stream().map(e -> {
                        MeetMeetingAppendix meetingAppendix = new MeetMeetingAppendix();
                        BeanUtils.copyProperties(e, meetingAppendix);
                        return meetingAppendix;
                    }).collect(Collectors.toList());
                    meetMeetingAppendixService.batchInsert(meetMeetingAppendixList);

//                    meetMeetingExecService.batchInsert(execscAppendix);
                    List<MeetMeetingExec> meetMeetingAppendices = execscAppendix.stream().map(e -> {
                        MeetMeetingExec meetingAppendix = new MeetMeetingExec();
                        BeanUtils.copyProperties(e, execscAppendix);
                        return meetingAppendix;
                    }).collect(Collectors.toList());
                    meetMeetingExecService.batchInsert(meetMeetingAppendices);

                    meetMeetingExecService.batchInsert(execs);
                }
            }
        }
    }

    //	删除所有未执行的会议及相关事务
    public void allDelete(MeetMeeting meetMeeting) {
        MeetMeetingExec meetMeetingExec = new MeetMeetingExec();
        meetMeetingExec.setMeetingId(meetMeeting.getId());
        List<MeetMeetingExec> selectUnexecList = meetMeetingExecService.meetMeetSelectList(meetMeetingExec);
        if (selectUnexecList.size() > 0) {
            meetMeetingExecService.meetMeetingbatchDeleteUnexec(selectUnexecList);

            meetMeetingExecAppendixService.meetMeetingbatchDeleteUnexec(selectUnexecList);

            meetMeetingExecAttenderService.meetMeetingbatchDeleteUnexec(selectUnexecList);

            meetMeetingExecRecordService.meetMeetingbatchDeleteUnexec(selectUnexecList);
            /*List<MeetMeetingAttender> meetMeetingAttenderList = new ArrayList<>();
            List<MeetMeetingAppendix> meetMeetingAppendixList = new ArrayList<>();
            for (MeetMeetingExec meetMeetingExecm: selectUnexecList){
                MeetMeetingAttender meetMeetingAttender = new MeetMeetingAttender();
                meetMeetingAttender.setMeetingId(meetMeetingExecm.getMeetingId());
                meetMeetingAttenderList.add(meetMeetingAttender);

                MeetMeetingAppendix meetMeetingAppendix = new MeetMeetingAppendix();
                meetMeetingAppendix.setMeetingId(meetMeetingExecm.getMeetingId());
                meetMeetingAppendixList.add(meetMeetingAppendix);
            }*/
            MeetMeetingAttender meetMeetingAttender = new MeetMeetingAttender();
            meetMeetingAttender.setMeetingId(meetMeeting.getId());

            MeetMeetingAppendix meetMeetingAppendix = new MeetMeetingAppendix();
            meetMeetingAppendix.setMeetingId(meetMeeting.getId());

            meetMeetingAttenderService.meetMeetingbatchDeleteUnexec(meetMeetingAttender);
            meetMeetingAppendixService.meetMeetingbatchDeleteUnexec(meetMeetingAppendix);


        }

    }

    public MeetMeetingVo updateMeeting(MeetMeetingVo meetMeetingVo) {
        MeetMeeting meetMeetingDB = new MeetMeeting();
        BeanUtils.copyProperties(meetMeetingVo, meetMeetingDB);
        this.allDelete(meetMeetingDB);
        Tips tips = new Tips();
        User user = LoginUtils.getCurrentUserInfo();

        //填充数据
        if (StringUtils.isEmpty(meetMeetingVo.getRequireUserid())) {
            meetMeetingVo.setRequireUserid(user.getUserid());
            meetMeetingVo.setRequireUsername(user.getUsername());
        }
        //出席员工数
        if (StringUtils.isEmpty(meetMeetingVo.getAttenderNum())) {
            meetMeetingVo.setAttenderNum(meetMeetingVo.getAttenders().size());
        }

        meetMeetingVo.setCreateTime(new Date());
        meetMeetingVo.setStatus("0");

        //保存会议基本信息
        MeetMeeting meetMeeting = new MeetMeeting();
        BeanUtils.copyProperties(meetMeetingVo, meetMeeting);
        meetMeeting.setExecCalcDate(new Date());
        meetMeeting.setCircleDaysOfMonth(meetMeetingVo.getCircleDaysOfMonth());
        meetMeeting.setCircleDaysOfWeek(meetMeetingVo.getCircleDaysOfWeek());
        meetMeeting.setCirccleDates(meetMeetingVo.getCirccleDates());
        String[] circcleDates = new String[0];
        try {
            if ("1".equals(meetMeeting.getCircleable())) {
                if ("date".equals(meetMeeting.getCircleType())) {
                    circcleDates = meetMeeting.getCirccleDates().split(",");
                    circcleDates = this.sore(circcleDates);
                } else {
                    try {
                        circcleDates = this.setCirccleDates(meetMeeting);
                        String s1 = "";
                        for (String s : circcleDates) {
                            System.out.println(s);
                            s1 = s1 + "," + s;
                        }
                        s1 = s1 + meetMeeting.getCircleDaysOfMonth();
                        meetMeetingVo.setCirccleDates(s1);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    String[] s = circcleDates;
                    s = this.sore(s);
                    Date nextUpdateTime = this.setNextUpdateTime(meetMeeting, s[0]);
                    meetMeeting.setNextUpdateTime(nextUpdateTime);
                }
            }

//		设置执行会议，执行成员和执行附件
            List<MeetMeetingExec> execs = new ArrayList<>();
            List<MeetMeetingExecAppendix> execscAppendix = new ArrayList<>();
            List<MeetMeetingExecAttender> execscAttender = new ArrayList<>();
            List<MeetMeetingAppendix> appendixList = meetMeetingVo.getMeetMeetingAppendix();
            List<MeetMeetingAttender> meetMeetingAttenderList = meetMeetingVo.getAttenders();


            if ("1".equals(meetMeeting.getCircleable())) {
                for (String circcleDate : circcleDates) {
                    try {
					/*SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Date date1 = new Date();
					String format1 = simpleDateFormat.format(date1.getTime());
					Date date2 =  simpleDateFormat.parse(format1);
					Date parse = simpleDateFormat.parse(circcleDate);
					if(parse.getTime()>=date2.getTime()){*/
                        MeetMeetingExec meetMeetingExec = this.setMeetMeetingExec(meetMeeting, circcleDate);
                        if (meetMeetingAttenderList.size() > 0) {
                            meetMeetingAttenderList.forEach((meetMeetingAttender) -> {
                                MeetMeetingExecAttender meetMeetingExecAttender = this.setMeetMeetingExecAttender(meetMeetingAttender, meetMeeting, meetMeetingExec);
                                execscAttender.add(meetMeetingExecAttender);
//						meetMeetingExecAttenderService.insert(meetMeetingExecAttender);
                            });
                        }
                        if (!appendixList.isEmpty()) {
                            appendixList.forEach(appendix -> {
                                MeetMeetingExecAppendix meetMeetingExecAppendix = this.setMeetMeetingExecAppendix(meetMeeting, circcleDate, appendix, meetMeetingExec);
                                execscAppendix.add(meetMeetingExecAppendix);
//						meetMeetingAppendixService.insert(meetMeetingExecAppendix);
                            });
                        }
//				meetMeetingExecService.insert(meetMeetingExec);
                        execs.add(meetMeetingExec);
//					}
                    } catch (Exception e) {
                        System.out.println(e);
                    }

                }
//			meetMeetingExecAttenderService.batchInsert(execscAttender);
//			meetMeetingAppendixService.batchInsert(execscAppendix);
//			meetMeetingExecService.batchInsert(execs);
            } else {
                MeetMeetingExec meetMeetingExec = new MeetMeetingExec();
                meetMeetingExec.setMeetingId(meetMeeting.getId());
                meetMeetingExec.setExecStatus("0");
                meetMeetingExec.setCtime(new Date());
                meetMeetingExec.setId(meetMeetingExecService.createKey("id"));
                meetMeetingExec.setExecUserid(meetMeeting.getRequireUserid());
                meetMeetingExec.setExecUsername(meetMeeting.getRequireUsername());
                meetMeetingExec.setExecTitle(meetMeeting.getName());
                meetMeetingExec.setFileCnt(meetMeeting.getFileCnt());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                meetMeetingExec.setExecDate(simpleDateFormat.format(calendar.getTime()));
                if (meetMeetingAttenderList.size() > 0) {
                    meetMeetingAttenderList.forEach((meetMeetingAttender) -> {
                        MeetMeetingExecAttender meetMeetingExecAttender = this.setMeetMeetingExecAttender(meetMeetingAttender, meetMeeting, meetMeetingExec);
                        execscAttender.add(meetMeetingExecAttender);
//					meetMeetingExecAttenderService.insert(meetMeetingExecAttender);
                    });
                }
                if (!appendixList.isEmpty()) {
                    appendixList.forEach(appendix -> {
                        MeetMeetingExecAppendix meetMeetingExecAppendix = this.setMeetMeetingExecAppendix(meetMeeting, meetMeetingExec.getExecDate(), appendix, meetMeetingExec);
                        execscAppendix.add(meetMeetingExecAppendix);
//					meetMeetingAppendixService.insert(meetMeetingExecAppendix);
                    });
                }
                execs.add(meetMeetingExec);
//			meetMeetingExecService.insert(meetMeetingExec);
            }
            meetMeetingExecAttenderService.batchInsert(execscAttender);

//            meetMeetingAppendixService.batchInsert(execscAppendix);
            List<MeetMeetingAppendix> meetMeetingAppendices = execscAppendix.stream().map(
                    (e) -> {
                        MeetMeetingAppendix meetingAppendix = new MeetMeetingAppendix();
                        BeanUtils.copyProperties(e, meetingAppendix);
                        return meetingAppendix;
                    }
            ).collect(Collectors.toList());
            meetMeetingAppendixService.batchInsert(meetMeetingAppendices);

            meetMeetingExecService.batchInsert(execs);
        } catch (Exception e) {
            System.out.println(e);
        }

        this.updateSomeFieldByPk(meetMeeting);

        //修改其它信息
        saveOtherMessage(meetMeetingVo, tips);

        return meetMeetingVo;
    }

    public MeetMeetingVo saveMeeting(MeetMeetingVo meetMeetingVo) {

        Tips tips = new Tips();
        User user = LoginUtils.getCurrentUserInfo();

        //填充数据
        if (StringUtils.isEmpty(meetMeetingVo.getRequireUserid())) {
            meetMeetingVo.setRequireUserid(user.getUserid());
            meetMeetingVo.setRequireUsername(user.getUsername());
        }
        //出席员工数
        if (StringUtils.isEmpty(meetMeetingVo.getAttenderNum())) {
            meetMeetingVo.setAttenderNum(meetMeetingVo.getAttenders().size());
        }

        meetMeetingVo.setCreateTime(new Date());
        meetMeetingVo.setStatus("0");

        if (StringUtils.isEmpty(meetMeetingVo.getId())) {
            meetMeetingVo.setId(this.createKey("id"));
        } else {
            MeetMeeting meetMeetingQuery = new MeetMeeting(meetMeetingVo.getId());
            if (this.countByWhere(meetMeetingQuery) > 0) {
                tips.setErrMsg("编号重复，请修改编号再提交");
                throw new BizException(tips);
            }
        }
        //保存会议基本信息
        MeetMeeting meetMeeting = new MeetMeeting();
        BeanUtils.copyProperties(meetMeetingVo, meetMeeting);
        meetMeeting.setExecCalcDate(new Date());
        meetMeeting.setCircleDaysOfMonth(meetMeetingVo.getCircleDaysOfMonth());
        meetMeeting.setCircleDaysOfWeek(meetMeetingVo.getCircleDaysOfWeek());
        meetMeeting.setCirccleDates(meetMeetingVo.getCirccleDates());
        String[] circcleDates = new String[0];
        if ("1".equals(meetMeeting.getCircleable())) {
            if ("date".equals(meetMeeting.getCircleType())) {
                circcleDates = meetMeeting.getCirccleDates().split(",");
                circcleDates = this.sore(circcleDates);
            } else {
                try {
                    circcleDates = this.setCirccleDates(meetMeeting);
                    String s1 = "";
                    for (String s : circcleDates) {
                        System.out.println(s);
                        s1 = s1 + "," + s;
                    }
                    s1 = s1 + meetMeeting.getCircleDaysOfMonth();
                    meetMeetingVo.setCirccleDates(s1);
                } catch (Exception e) {
                    System.out.println(e);
                }
                String[] s = circcleDates;
                s = this.sore(s);
                Date nextUpdateTime = this.setNextUpdateTime(meetMeeting, s[0]);
                meetMeeting.setNextUpdateTime(nextUpdateTime);
            }
        }

//		设置执行会议，执行成员和执行附件
        List<MeetMeetingExec> execs = new ArrayList<>();
        List<MeetMeetingExecAppendix> execscAppendix = new ArrayList<>();
        List<MeetMeetingExecAttender> execscAttender = new ArrayList<>();
        List<MeetMeetingAppendix> appendixList = meetMeetingVo.getMeetMeetingAppendix();
        List<MeetMeetingAttender> meetMeetingAttenderList = meetMeetingVo.getAttenders();


        if ("1".equals(meetMeeting.getCircleable())) {
            for (String circcleDate : circcleDates) {
                try {
					/*SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Date date1 = new Date();
					String format1 = simpleDateFormat.format(date1.getTime());
					Date date2 =  simpleDateFormat.parse(format1);
					Date parse = simpleDateFormat.parse(circcleDate);
					if(parse.getTime()>=date2.getTime()){*/
                    MeetMeetingExec meetMeetingExec = this.setMeetMeetingExec(meetMeeting, circcleDate);
                    if (meetMeetingAttenderList.size() > 0) {
                        meetMeetingAttenderList.forEach((meetMeetingAttender) -> {
                            MeetMeetingExecAttender meetMeetingExecAttender = this.setMeetMeetingExecAttender(meetMeetingAttender, meetMeeting, meetMeetingExec);
                            execscAttender.add(meetMeetingExecAttender);
//						meetMeetingExecAttenderService.insert(meetMeetingExecAttender);
                        });
                    }
                    if (!appendixList.isEmpty()) {
                        appendixList.forEach(appendix -> {
                            MeetMeetingExecAppendix meetMeetingExecAppendix = this.setMeetMeetingExecAppendix(meetMeeting, circcleDate, appendix, meetMeetingExec);
                            execscAppendix.add(meetMeetingExecAppendix);
//						meetMeetingAppendixService.insert(meetMeetingExecAppendix);
                        });
                    }
//				meetMeetingExecService.insert(meetMeetingExec);
                    execs.add(meetMeetingExec);
//					}
                } catch (Exception e) {
                    System.out.println(e);
                }

            }
//			meetMeetingExecAttenderService.batchInsert(execscAttender);
//			meetMeetingAppendixService.batchInsert(execscAppendix);
//			meetMeetingExecService.batchInsert(execs);
        } else {
            MeetMeetingExec meetMeetingExec = new MeetMeetingExec();
            meetMeetingExec.setMeetingId(meetMeeting.getId());
            meetMeetingExec.setExecStatus("0");
            meetMeetingExec.setCtime(new Date());
            meetMeetingExec.setId(meetMeetingExecService.createKey("id"));
            meetMeetingExec.setExecUserid(meetMeeting.getRequireUserid());
            meetMeetingExec.setExecUsername(meetMeeting.getRequireUsername());
            meetMeetingExec.setExecTitle(meetMeeting.getName());
            meetMeetingExec.setFileCnt(meetMeeting.getFileCnt());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            meetMeetingExec.setExecDate(simpleDateFormat.format(calendar.getTime()));
            if (meetMeetingAttenderList.size() > 0) {
                meetMeetingAttenderList.forEach((meetMeetingAttender) -> {
                    MeetMeetingExecAttender meetMeetingExecAttender = this.setMeetMeetingExecAttender(meetMeetingAttender, meetMeeting, meetMeetingExec);
                    execscAttender.add(meetMeetingExecAttender);
//					meetMeetingExecAttenderService.insert(meetMeetingExecAttender);
                });
            }
            if (!appendixList.isEmpty()) {
                appendixList.forEach(appendix -> {
                    MeetMeetingExecAppendix meetMeetingExecAppendix = this.setMeetMeetingExecAppendix(meetMeeting, meetMeetingExec.getExecDate(), appendix, meetMeetingExec);
                    execscAppendix.add(meetMeetingExecAppendix);
//					meetMeetingAppendixService.insert(meetMeetingExecAppendix);
                });
            }
            execs.add(meetMeetingExec);
//			meetMeetingExecService.insert(meetMeetingExec);
        }
        meetMeetingExecAttenderService.batchInsert(execscAttender);

//        meetMeetingAppendixService.batchInsert(execscAppendix);
        List<MeetMeetingAppendix> meetMeetingAppendices = execscAppendix.stream().map(
                (e) -> {
                    MeetMeetingAppendix meetingAppendix = new MeetMeetingAppendix();
                    BeanUtils.copyProperties(e, meetingAppendix);
                    return meetingAppendix;
                }
        ).collect(Collectors.toList());
        meetMeetingAppendixService.batchInsert(meetMeetingAppendices);

        meetMeetingExecService.batchInsert(execs);

        this.insert(meetMeeting);

        //保存其它信息
        saveOtherMessage(meetMeetingVo, tips);

        //生成循环数据

        return meetMeetingVo;
    }

    public MeetMeetingExecAttender setMeetMeetingExecAttender(MeetMeetingAttender meetMeetingAttender, MeetMeeting meetMeeting, MeetMeetingExec meetMeetingExec) {
        MeetMeetingExecAttender meetMeetingExecAttender = new MeetMeetingExecAttender();
        meetMeetingExecAttender.setId(meetMeetingExecAttenderService.createKey("id"));
        meetMeetingExecAttender.setMeetingId(meetMeeting.getId());
        meetMeetingExecAttender.setUserid(meetMeetingAttender.getUserid());
        meetMeetingExecAttender.setUsername(meetMeetingAttender.getUsername());
        meetMeetingExecAttender.setStatus("0");
        meetMeetingExecAttender.setRemark(meetMeetingAttender.getRemark());
        meetMeetingExecAttender.setSignStatus("0");
        meetMeetingExecAttender.setUserType(meetMeetingAttender.getUserType());
        meetMeetingExecAttender.setAttType("1");
        meetMeetingExecAttender.setExecId(meetMeetingExec.getId());
        meetMeetingExecAttender.setPhoneno(meetMeetingAttender.getPhoneno());
        meetMeetingExecAttender.setEmail(meetMeetingAttender.getEmail());
        meetMeetingExecAttender.setSentTime(meetMeeting.getRemindersBeforeStart());
        meetMeetingExecAttender.setSentContent(meetMeeting.getContent());
        return meetMeetingExecAttender;
    }

    public MeetMeetingExecAppendix setMeetMeetingExecAppendix(MeetMeeting meetMeeting, String circcleDate, MeetMeetingAppendix meetMeetingAppendix, MeetMeetingExec meetMeetingExec) {
        MeetMeetingExecAppendix meetMeetingExecAppendix = new MeetMeetingExecAppendix();
        meetMeetingExecAppendix.setId(meetMeetingAppendixService.createKey("id"));
        meetMeetingExecAppendix.setMeetingId(meetMeeting.getId());
        meetMeetingExecAppendix.setAddr(meetMeetingAppendix.getAddr());
        meetMeetingExecAppendix.setName(meetMeetingAppendix.getName());
        meetMeetingExecAppendix.setRemark(meetMeetingAppendix.getRemark());
        meetMeetingExecAppendix.setType(meetMeetingAppendix.getType());
        meetMeetingExecAppendix.setExecId(meetMeetingExec.getId());
        return meetMeetingExecAppendix;
    }

    public MeetMeetingExec setMeetMeetingExec(MeetMeeting meetMeeting, String circcleDate) {
        MeetMeetingExec meetMeetingExec = new MeetMeetingExec();
        meetMeetingExec.setMeetingId(meetMeeting.getId());
        meetMeetingExec.setExecDate(circcleDate);
        meetMeetingExec.setExecStatus("0");
        meetMeetingExec.setCtime(new Date());
        try {
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
            Date dd = sdf2.parse(circcleDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dd);
            if ("week".equals(meetMeeting.getCircleType())) {
                int weekday = calendar.get(Calendar.DAY_OF_WEEK);
                meetMeetingExec.setDayOfWeek(weekday - 1);
            }
            if ("month".equals(meetMeeting.getCircleType())) {
                meetMeetingExec.setDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        meetMeetingExec.setId(meetMeetingExecService.createKey("id"));
        meetMeetingExec.setExecUserid(meetMeeting.getRequireUserid());
        meetMeetingExec.setExecUsername(meetMeeting.getRequireUsername());
        meetMeetingExec.setExecTitle(meetMeeting.getName());
        meetMeetingExec.setFileCnt(meetMeeting.getFileCnt());
        return meetMeetingExec;
    }

    //设置下次更新时间
    public Date setNextUpdateTime(MeetMeeting meetMeeting, String circcleDates) {
        Date nextUpdateTime;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if ("week".equals(meetMeeting.getCircleType())) {
                Date date = simpleDateFormat.parse(circcleDates);
                long ts = date.getTime() + 1000 * 60 * 60 * 24 * 7;
                nextUpdateTime = new Date(ts);
            } else if ("month".equals(meetMeeting.getCircleType())) {
                Date parse = simpleDateFormat.parse(circcleDates);
                Calendar c = Calendar.getInstance();
                c.setTime(parse);
                int monday = c.get(Calendar.MONDAY) + 2;
				/*int circcleDate = Integer.parseInt(circcleDates[0]);
				c.set(Calendar.DAY_OF_WEEK,circcleDate);
				nextUpdateTime = c.getTime();*/
                if (c.get(Calendar.DATE) <= 28) {
                    if (monday < 12) {
                        nextUpdateTime = simpleDateFormat.parse((c.get(Calendar.YEAR) + "-" + monday + "-" + c.get(Calendar.DATE)));
                    } else {
                        nextUpdateTime = simpleDateFormat.parse(((c.get(Calendar.YEAR) + 1) + "-" + "1" + "-" + c.get(Calendar.DATE)));
                    }
                } else {
                    Date date = simpleDateFormat.parse(circcleDates);
                    long ts = date.getTime() + 1000 * 60 * 60 * 24 * 28;
                    nextUpdateTime = new Date(ts);
                }
            } else {
                nextUpdateTime = null;
            }

            return nextUpdateTime;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    //将月份周期转换为日期
    public String[] setCirccleDates(MeetMeeting meetMeeting) {
        String[] circcleDates = new String[0];
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            if ("week".equals(meetMeeting.getCircleType())) {
                circcleDates = meetMeeting.getCircleDaysOfWeek().split(",");
                Calendar calendar = Calendar.getInstance();
                for (int i = 0; i < circcleDates.length; i++) {
                    int circcleDate = Integer.parseInt(circcleDates[i]);
                    calendar.set(Calendar.DAY_OF_WEEK, circcleDate + 1);
                    circcleDates[i] = simpleDateFormat.format(calendar.getTime());
                }
            }
            if ("month".equals(meetMeeting.getCircleType())) {
                circcleDates = meetMeeting.getCircleDaysOfMonth().split(",");
                Calendar calendar = Calendar.getInstance();
                for (int i = 0; i < circcleDates.length; i++) {
                    int circcleDate = Integer.parseInt(circcleDates[i]);
                    calendar.set(Calendar.DAY_OF_MONTH, circcleDate);
                    circcleDates[i] = simpleDateFormat.format(calendar.getTime());
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return circcleDates;

    }

    //排序
    public String[] sore(String[] circcleDates) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            for (int x = 0; x < circcleDates.length - 1; x++) {
                for (int y = x + 1; y < circcleDates.length; y++) {
                    simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date1 = simpleDateFormat.parse(circcleDates[x]);
                    long ts1 = date1.getTime();
                    Date date2 = simpleDateFormat.parse(circcleDates[y]);
                    long ts2 = date2.getTime();
                    if (ts1 > ts2) {// 对时间进行排序
                        String temp = circcleDates[x];
                        circcleDates[x] = circcleDates[y];
                        circcleDates[y] = temp;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return circcleDates;
    }

    public void updateOtherMessage(MeetMeetingVo meetMeetingVo, Tips tips) {
        //保存参加员工
        List<MeetMeetingAttender> meetMeetingAttenderList = meetMeetingVo.getAttenders();
        if (!meetMeetingAttenderList.isEmpty()) {
            meetMeetingAttenderList.forEach((meetMeetingAttender) -> {
				/*if(StringUtils.isEmpty(meetMeetingAttender.getId())) {
					meetMeetingAttender.setId(meetMeetingAttenderService.createKey("id"));
				}else{
					MeetMeetingAttender meetMeetingAttenderQuery = new  MeetMeetingAttender(meetMeetingAttender.getId());
					if(meetMeetingAttenderService.countByWhere(meetMeetingAttenderQuery)>0){
						tips.setFailureMsg("编号重复，请修改编号再提交");
						throw new BizException(tips);
					}
				}*/
                meetMeetingAttenderService.updateSomeFieldByPk(meetMeetingAttender);
            });
        }
        //保存附件
        List<MeetMeetingAppendix> appendixList = meetMeetingVo.getMeetMeetingAppendix();
        if (!appendixList.isEmpty()) {
            appendixList.forEach(appendix -> {
                meetMeetingAppendixService.updateSomeFieldByPk(appendix);
            });
        }
    }

    public void saveOtherMessage(MeetMeetingVo meetMeetingVo, Tips tips) {
        //保存参加员工
        List<MeetMeetingAttender> meetMeetingAttenderList = meetMeetingVo.getAttenders();
        MeetMeeting meetMeeting = new MeetMeeting();
        BeanUtils.copyProperties(meetMeetingVo, meetMeeting);

        if (!meetMeetingAttenderList.isEmpty()) {
            meetMeetingAttenderList.forEach((meetMeetingAttender) -> {
                if (StringUtils.isEmpty(meetMeetingAttender.getId())) {
                    meetMeetingAttender.setId(meetMeetingAttenderService.createKey("id"));
                } else {
                    MeetMeetingAttender meetMeetingAttenderQuery = new MeetMeetingAttender(meetMeetingAttender.getId());
                    if (meetMeetingAttenderService.countByWhere(meetMeetingAttenderQuery) > 0) {
                        tips.setErrMsg("编号重复，请修改编号再提交");
                        throw new BizException(tips);
                    }
                }
                meetMeetingAttender.setMeetingId(meetMeeting.getId());
                meetMeetingAttenderService.insert(meetMeetingAttender);
            });
        }
        //保存附件
        List<MeetMeetingAppendix> appendixList = meetMeetingVo.getMeetMeetingAppendix();

        if (!appendixList.isEmpty()) {
            appendixList.forEach(appendix -> {
                if (StringUtils.isEmpty(appendix.getId())) {
                    appendix.setId(meetMeetingAttenderService.createKey("id"));
                } else {
                    MeetMeetingAttender meetMeetingAttenderQuery = new MeetMeetingAttender(appendix.getId());
                    if (meetMeetingAttenderService.countByWhere(meetMeetingAttenderQuery) > 0) {
                        tips.setErrMsg("编号重复，请修改编号再提交");
                        throw new BizException(tips);
                    }
                }
                appendix.setMeetingId(meetMeeting.getId());
                meetMeetingAppendixService.insert(appendix);
            });
        }
    }


    public Integer removeMeeting(MeetMeeting meetMeeting) {

        int delete = this.deleteByPk(meetMeeting);


        removeOtherMessage(meetMeeting);

        return delete;
    }

    public void removeOtherMessage(MeetMeeting meetMeeting) {
        MeetMeetingAttender meetMeetingAttender = new MeetMeetingAttender();
        meetMeetingAttender.setMeetingId(meetMeeting.getId());
        int i = meetMeetingAttenderService.deleteByWhere(meetMeetingAttender);
        MeetMeetingAppendix meetMeetingRelatedAppendix = new MeetMeetingAppendix();
        meetMeetingRelatedAppendix.setMeetingId(meetMeeting.getId());
        meetMeetingAppendixService.deleteByWhere(meetMeetingRelatedAppendix);

    }


    public List<Map<String, Object>> selectListMapByWhereByMine(Map<String, Object> meetMeeting) {

//        List<Map<String, Object>> mapList = this.selectList("selectListMapByWhereAndMine", Map.class);
        List<Map<String, Object>> mapList = this.selectListMapByWhere(null, null, meetMeeting);

       /* List<Map<String, Object>> mapList = this.selectListMapByWhere(meetMeeting);
        List<Map<String, Object>> results = new ArrayList<>();
        User currentUser = LoginUtils.getCurrentUserInfo();

        if (!mapList.isEmpty()){
            boolean finalMimeMeeting = (boolean) meetMeeting.get("mimeMeeting");
            mapList.forEach((meeting)->{
                String id = meeting.get("id").toString();
                MeetMeetingAttender meetMeetingAttender = new MeetMeetingAttender();
                meetMeetingAttender.setMeetingId(id);
                List<MeetMeetingAttender> attenderList = meetMeetingAttenderService.selectListByWhere(meetMeetingAttender);

                if (!attenderList.isEmpty()){
                    for (MeetMeetingAttender attender : attenderList) {
                        String userId = currentUser.getUserid();
                        if (userId.equals(attender.getUserId())){
                            if (finalMimeMeeting){
                                if (attender.getStatus().equals("1")){
                                    meeting.put("attender",attender);
                                    results.add(meeting);
                                }
                            }else {
                                meeting.put("attender",attender);
                                results.add(meeting);
                            }
                            break;
                        }
                    }
                }
            });
        }*/
        //mapList.removeAll(results);
        return mapList;
    }

    public MeetMeetingVo getMeetMeeting(Map<String, Object> meetMeetingParam) {
        MeetMeetingVo meetMeetingVo = new MeetMeetingVo();

        MeetMeeting meetMeeting = new MeetMeeting();
        meetMeeting.setId(meetMeetingParam.get("id").toString());
        MeetMeeting selectOneObject = selectOneObject(meetMeeting);
        BeanUtils.copyProperties(selectOneObject, meetMeetingVo);

        if (selectOneObject != null) {
            MeetMeetingAttender attender = new MeetMeetingAttender();
            attender.setMeetingId(selectOneObject.getId());
            List<MeetMeetingAttender> meetMeetingAttenders = meetMeetingAttenderService.selectListByWhere(attender);
            meetMeetingVo.setAttenders(meetMeetingAttenders);
/*

			MeetMeetingCustomer meetMeetingCustomer = new MeetMeetingCustomer();
			meetMeetingCustomer.setMeetingId(selectOneObject.getId());
			List<MeetMeetingCustomer> meetMeetingCustomers = meetMeetingCustomerService.selectListByWhere(meetMeetingCustomer);
			meetMeetingVo.setCustomers(meetMeetingCustomers);
*/

            MeetMeetingAppendix meetMeetingRelatedAppendix = new MeetMeetingAppendix();
            meetMeetingRelatedAppendix.setMeetingId(selectOneObject.getId());
            List<MeetMeetingAppendix> meetMeetingRelatedAppendices = meetMeetingAppendixService.selectListByWhere(meetMeetingRelatedAppendix);
            meetMeetingVo.setMeetMeetingAppendix(meetMeetingRelatedAppendices);
        }

        return meetMeetingVo;

    }

    public Integer editMeetMeeting(MeetMeetingVo meetMeetingVo) {
        MeetMeeting meetMeeting = new MeetMeeting();
        BeanUtils.copyProperties(meetMeetingVo, meetMeeting);
//        int update = update("updateByPk", meetMeeting);
        int update = super.updateByPk(meetMeeting);
        //先删除
        removeOtherMessage(meetMeeting);

        //再添加
        Tips tips = new Tips();
        saveOtherMessage(meetMeetingVo, tips);

        return update;

    }

    public List<Map<String, Object>> selectMyMeetMeetingListMapByWhere(Map<String, Object> meetMeetingDb) {
        return baseMapper.selectMyMeetMeetingListMapByWhere(meetMeetingDb);
    }

    public List<Map<String, Object>> selectAdminiMeetMeetingListMapByWhere(IPage page, QueryWrapper qw, Map<String, Object> meetMeeting) {
        return baseMapper.selectAdminiMeetMeetingListMapByWhere(page, qw, meetMeeting);
    }
}

