package com.oa.erp.ass.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.service.BaseService;
import com.oa.erp.ass.entity.AssetCardOper;
import com.oa.erp.ass.entity.AssetMng;
import com.oa.erp.ass.mapper.AssetCardOperMapper;
import com.oa.erp.ass.vo.AssetDetailVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@Service
public class AssetCardOperService extends BaseService<AssetCardOperMapper, AssetCardOper> {
    static Logger logger = LoggerFactory.getLogger(AssetCardOperService.class);

    /**
     * 自定义查询，支持多表关联
     *
     * @param page 分页条件
     * @param ew   一定要，并且必须加@Param("ew")注解
     * @param ext  如果xml中需要根据某些值进行特殊处理，可以通过这个进行传递，非必须，注解也可以不加
     * @return
     */
    public List<Map<String, Object>> selectListMapByWhere(IPage page, QueryWrapper ew, Map<String, Object> ext) {
        return baseMapper.selectListMapByWhere(page, ew, ext);
    }


    /**
     * 请在此类添加自定义函数
     */
    @Transactional
    public void insertDetailOper(List<AssetDetailVo> assetDetails, String afterStatus, String msgId) {
        List<AssetCardOper> assetDetailOpers = new ArrayList<>();
        for (AssetDetailVo assetDetail : assetDetails) {
            AssetCardOper target = new AssetCardOper();
            BeanUtils.copyProperties(assetDetail, target);
            //设置资产卡片关联Id
            target.setCardId(assetDetail.getCardId());
            target.setAssetSn(assetDetail.getAssetSn());
            //如果是盘点则不需要设置之前之后状态
            if (!"inventory".equals(afterStatus)) {
                //设置资产之前状态
                target.setAssetStatusBefore(assetDetail.getCardStatus());
                //设置之后状态，当资产进行变更时，不会改变自身状态
                target.setAssetStatusAfter(afterStatus == "change" ? assetDetail.getCardStatus() : afterStatus);
            }
            //创建日期
            target.setCreateDate(new Date());
            //关联单据id
            target.setRequireId(msgId);
            target.setId(this.createKey("id"));
            //设置之前的保存部门,责任人,仓库地址
            target.setOldDeptid(assetDetail.getDeptid());
            target.setOldDeptName(assetDetail.getDeptName());
            target.setOldPsersonLiableUserid(assetDetail.getPersonLiableUserid());
            target.setOldPsersonLiableUsername(assetDetail.getPersonLiableUsername());
            target.setOldStoreAddress(assetDetail.getStoreAddress());
            target.setOldWarehouseId(assetDetail.getWarehouseId());
            target.setOldWarehouseName(assetDetail.getWarehouseName());
            assetDetailOpers.add(target);
        }
        this.batchInsert(assetDetailOpers);
    }


    public List<Map<String, Object>> selectListWithMng(IPage page, QueryWrapper ew,Map<String, Object> assetDetailOper) {
        return baseMapper.selectListWithMng(assetDetailOper);
    }

    public List<String> getDetailAllProcInstIds(Map<String, Object> assetDetailOper) {
        return baseMapper.getDetailAllProcInstIds(assetDetailOper);
    }

    public void updateFlowStateByAssetMngId(String bizFlowState, Map<String, Object> flowVars) {
        baseMapper.updateFlowStateByAssetMngId(flowVars);
    }

    public void batchDeleteOperDetailByRequireId(List<AssetMng> assetMngs) {
        baseMapper.batchDeleteOperDetailByRequireId(assetMngs);
    }
}

