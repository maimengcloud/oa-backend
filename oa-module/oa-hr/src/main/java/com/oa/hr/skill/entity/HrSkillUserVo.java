package com.oa.hr.skill.entity;

import java.util.List;

public class HrSkillUserVo {

    List<String> skillIds;
    String userid;

    public List<String> getSkillIds() {
        return skillIds;
    }

    public void setSkillIds(List<String> skillIds) {
        this.skillIds = skillIds;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


}
