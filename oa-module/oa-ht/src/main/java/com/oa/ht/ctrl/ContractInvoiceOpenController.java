package com.oa.ht.ctrl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Result;
import com.mdp.core.query.QueryTools;
import com.mdp.core.utils.RequestUtils;
import com.mdp.safe.client.entity.User;
import com.mdp.safe.client.utils.LoginUtils;
import com.mdp.swagger.ApiEntityParams;
import com.oa.ht.entity.ContractCondition;
import com.oa.ht.entity.ContractInvoiceOpen;
import com.oa.ht.service.ContractConditionService;
import com.oa.ht.service.ContractInvoiceOpenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author maimeng-mdp code-gen
 * @since 2023年10月8日
 */
@RestController
@RequestMapping(value = "/*/oa/ht/contractInvoiceOpen")
@Api(tags = {"ht_contract_invoice_open-操作接口"})
public class ContractInvoiceOpenController {

    static Logger logger = LoggerFactory.getLogger(ContractInvoiceOpenController.class);

    @Autowired
    private ContractInvoiceOpenService contractInvoiceOpenService;

    @Autowired
    private ContractConditionService contractConditionService;

    @ApiOperation(value = "ht_contract_invoice_open-查询列表", notes = " ")
    @ApiEntityParams(ContractInvoiceOpen.class)
    @ApiResponses({@ApiResponse(code = 200, response = ContractInvoiceOpen.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Result listContractInvoiceOpen(@ApiIgnore @RequestParam Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        RequestUtils.transformArray(params, "ids");

        QueryWrapper<ContractInvoiceOpen> qw = QueryTools.initQueryWrapper(ContractInvoiceOpen.class, params);
        IPage page = QueryTools.initPage(params);
        List<Map<String, Object>> datas = contractInvoiceOpenService.selectListMapByWhere(page, qw, params);

        return Result.ok("query-ok", "查询成功").setData(datas).setTotal(page.getTotal());
    }


    @ApiOperation(value = "ht_contract_invoice_open-新增", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractInvoiceOpen.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addContractInvoiceOpen(@RequestBody ContractInvoiceOpen contractInvoiceOpen) {
        if (contractInvoiceOpen.getAmount() == null || contractInvoiceOpen.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            return Result.error("amount-is-zero", "开票金额不能为空，并且必须大于0");
        }
        if (StringUtils.isEmpty(contractInvoiceOpen.getId())) {
            contractInvoiceOpen.setId(contractInvoiceOpenService.createKey("id"));
        } else {
            ContractInvoiceOpen contractInvoiceOpenQuery = new ContractInvoiceOpen(contractInvoiceOpen.getId());
            if (contractInvoiceOpenService.countByWhere(contractInvoiceOpenQuery) > 0) {
                return Result.error("编号重复，请修改编号再提交");
            }
        }
        if (!StringUtils.hasText(contractInvoiceOpen.getConditionId())) {
            return Result.error("conditionId-is-null", "conditionId不能为空");
        }
        ContractCondition contractCondition = new ContractCondition(contractInvoiceOpen.getConditionId());
        contractCondition = this.contractConditionService.selectOneObject(contractCondition);
        if (contractCondition == null) {
            return Result.error("condition-not-exists", "款项不存在");
        } else {
            if (contractCondition.getInvoicedAmount() == null) {
                contractCondition.setInvoicedAmount(BigDecimal.ZERO);
            }
            if (contractCondition.getConditionAmount().compareTo(contractCondition.getInvoicedAmount()) <= 0) {
                return Result.error("all-is-open", "所有应付金额已开票，不能再开票");
            }
            if (contractCondition.getConditionAmount().subtract(contractCondition.getInvoicedAmount()).compareTo(contractInvoiceOpen.getAmount()) < 0) {
                return Result.error("amount-is-too-big", "本次开票金额大于未开票额，不能提交");
            }
        }
        User user = LoginUtils.getCurrentUserInfo();
        contractInvoiceOpen.setCreateUserId(user.getUserid());
        contractInvoiceOpen.setCreateUserName(user.getUsername());
        contractInvoiceOpen.setCreateTime(new Date());
        contractInvoiceOpen.setInvoiceStatus("1");
        contractInvoiceOpen.setStatus("1");
        contractInvoiceOpen.setBranchId(user.getBranchId());
        contractInvoiceOpenService.add(contractInvoiceOpen);
        return Result.ok("add-ok", "添加成功！").setData(contractInvoiceOpen);
    }

    @ApiOperation(value = "ht_contract_invoice_open-删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}}")})
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public Result delContractInvoiceOpen(@RequestBody ContractInvoiceOpen contractInvoiceOpen) {
        contractInvoiceOpenService.deleteByPk(contractInvoiceOpen);
        return Result.ok("del-ok", "删除成功！");
    }

    @ApiOperation(value = "ht_contract_invoice_open-修改", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractInvoiceOpen.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result editContractInvoiceOpen(@RequestBody ContractInvoiceOpen contractInvoiceOpen) {
        contractInvoiceOpenService.updateByPk(contractInvoiceOpen);
        return Result.ok("edit-ok", "修改成功！").setData(contractInvoiceOpen);
    }

    @ApiOperation(value = "修改状态", notes = "updateStatus")
    @ApiResponses({@ApiResponse(code = 200, response = ContractInvoiceOpen.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
    public Result updateStatus(@RequestBody Map<String, Object> params) {
        contractInvoiceOpenService.updateStatus(params);
        return Result.ok("update-ok", "成功更新一条数据");
    }

    @ApiOperation(value = "ht_contract_invoice_open-批量修改某些字段", notes = "")
    @ApiEntityParams(value = ContractInvoiceOpen.class, props = {}, remark = "ht_contract_invoice_open", paramType = "body")
    @ApiResponses({@ApiResponse(code = 200, response = ContractInvoiceOpen.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/editSomeFields", method = RequestMethod.POST)
    public Result editSomeFields(@ApiIgnore @RequestBody Map<String, Object> params) {
        User user = LoginUtils.getCurrentUserInfo();
        contractInvoiceOpenService.editSomeFields(params);
        return Result.ok("edit-ok", "更新成功");
    }

    @ApiOperation(value = "ht_contract_invoice_open-批量删除", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'}")})
    @RequestMapping(value = "/batchDel", method = RequestMethod.POST)
    public Result batchDelContractInvoiceOpen(@RequestBody List<ContractInvoiceOpen> contractInvoiceOpens) {
        User user = LoginUtils.getCurrentUserInfo();
        if (contractInvoiceOpens.size() <= 0) {
            return Result.error("batchDel-data-err-0", "请上送待删除数据列表");
        }
        List<ContractInvoiceOpen> datasDb = contractInvoiceOpenService.listByIds(contractInvoiceOpens.stream().map(i -> i.getId()).collect(Collectors.toList()));

        List<ContractInvoiceOpen> can = new ArrayList<>();
        List<ContractInvoiceOpen> no = new ArrayList<>();
        for (ContractInvoiceOpen data : datasDb) {
            if (true) {
                can.add(data);
            } else {
                no.add(data);
            }
        }
        List<String> msgs = new ArrayList<>();
        if (can.size() > 0) {
            contractInvoiceOpenService.removeByIds(can);
            msgs.add(LangTips.transMsg("del-ok-num", "成功删除%s条数据.", can.size()));
        }

        if (no.size() > 0) {
            msgs.add(LangTips.transMsg("not-allow-del-num", "以下%s条数据不能删除:【%s】", no.size(), no.stream().map(i -> i.getId()).collect(Collectors.joining(","))));
        }
        if (can.size() > 0) {
            return Result.ok(msgs.stream().collect(Collectors.joining()));
        } else {
            return Result.error(msgs.stream().collect(Collectors.joining()));
        }
    }

    @ApiOperation(value = "ht_contract_invoice_open-根据主键查询一条数据", notes = " ")
    @ApiResponses({@ApiResponse(code = 200, response = ContractInvoiceOpen.class, message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'失败时错误码'},data:数据对象}")})
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public Result queryById(ContractInvoiceOpen contractInvoiceOpen) {
        ContractInvoiceOpen data = (ContractInvoiceOpen) contractInvoiceOpenService.getById(contractInvoiceOpen);
        return Result.ok().setData(data);
    }

}
